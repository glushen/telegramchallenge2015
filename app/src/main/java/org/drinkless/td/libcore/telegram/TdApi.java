package org.drinkless.td.libcore.telegram;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

@SuppressWarnings({"unused", "UnnecessaryLocalVariable"}) public class TdApi {
    private static final char[] HEX_CHARACTERS = "0123456789ABCDEF".toCharArray();

    public abstract static class TLObject implements Parcelable {

        public abstract int getConstructor();

        public String toString() {
            StringBuilder s = new StringBuilder();
            toStringBuilder(0, s);
            return s.toString();
        }

        protected abstract void toStringBuilder(int shift, StringBuilder s);
    }

    public abstract static class TLFunction extends TLObject {
    }

    public static class Audio extends TLObject {
        public int duration;
        public String title;
        public String performer;
        public PhotoSize albumCoverThumb;
        public String fileName;
        public String mimeType;
        public File audio;

        public static final Creator<Audio> CREATOR = new Creator<Audio>() {

            @Override public Audio createFromParcel(Parcel in) {
                Audio data = new Audio();

                data.duration = in.readInt();
                data.title = in.readString();
                data.performer = in.readString();
                data.albumCoverThumb = in.readParcelable(PhotoSize.class.getClassLoader());
                data.fileName = in.readString();
                data.mimeType = in.readString();
                data.audio = in.readParcelable(File.class.getClassLoader());

                return data;
            }

            @Override public Audio[] newArray(int size) {
                return new Audio[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(duration);
            dest.writeString(title);
            dest.writeString(performer);
            dest.writeParcelable(this.albumCoverThumb, flags);
            dest.writeString(fileName);
            dest.writeString(mimeType);
            dest.writeParcelable(this.audio, flags);
        }
	
        public Audio() {
        }

        public Audio(int duration, String title, String performer, PhotoSize albumCoverThumb, String fileName, String mimeType, File audio) {
            this.duration = duration;
            this.title = title;
            this.performer = performer;
            this.albumCoverThumb = albumCoverThumb;
            this.fileName = fileName;
            this.mimeType = mimeType;
            this.audio = audio;
        }

        public static final int CONSTRUCTOR = -495790369;

        @Override
        public int getConstructor() {
            return -495790369;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Audio").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("duration").append(" = ").append(duration).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("performer").append(" = ").append(performer).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("albumCoverThumb").append(" = "); albumCoverThumb.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fileName").append(" = ").append(fileName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("mimeType").append(" = ").append(mimeType).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("audio").append(" = "); audio.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class AuthState extends TLObject {
    }

    public static class AuthStateWaitPhoneNumber extends AuthState {

        public static final Creator<AuthStateWaitPhoneNumber> CREATOR = new Creator<AuthStateWaitPhoneNumber>() {

            @Override public AuthStateWaitPhoneNumber createFromParcel(Parcel in) {
                AuthStateWaitPhoneNumber data = new AuthStateWaitPhoneNumber();

        
                return data;
            }

            @Override public AuthStateWaitPhoneNumber[] newArray(int size) {
                return new AuthStateWaitPhoneNumber[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public AuthStateWaitPhoneNumber() {
        }

        public static final int CONSTRUCTOR = 167878457;

        @Override
        public int getConstructor() {
            return 167878457;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("AuthStateWaitPhoneNumber").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class AuthStateWaitCode extends AuthState {

        public static final Creator<AuthStateWaitCode> CREATOR = new Creator<AuthStateWaitCode>() {

            @Override public AuthStateWaitCode createFromParcel(Parcel in) {
                AuthStateWaitCode data = new AuthStateWaitCode();

        
                return data;
            }

            @Override public AuthStateWaitCode[] newArray(int size) {
                return new AuthStateWaitCode[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public AuthStateWaitCode() {
        }

        public static final int CONSTRUCTOR = -1154394952;

        @Override
        public int getConstructor() {
            return -1154394952;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("AuthStateWaitCode").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class AuthStateWaitName extends AuthState {

        public static final Creator<AuthStateWaitName> CREATOR = new Creator<AuthStateWaitName>() {

            @Override public AuthStateWaitName createFromParcel(Parcel in) {
                AuthStateWaitName data = new AuthStateWaitName();

        
                return data;
            }

            @Override public AuthStateWaitName[] newArray(int size) {
                return new AuthStateWaitName[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public AuthStateWaitName() {
        }

        public static final int CONSTRUCTOR = -245435948;

        @Override
        public int getConstructor() {
            return -245435948;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("AuthStateWaitName").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class AuthStateWaitPassword extends AuthState {
        public String hint;
        public boolean hasRecovery;
        public String emailUnconfirmedPattern;

        public static final Creator<AuthStateWaitPassword> CREATOR = new Creator<AuthStateWaitPassword>() {

            @Override public AuthStateWaitPassword createFromParcel(Parcel in) {
                AuthStateWaitPassword data = new AuthStateWaitPassword();

                data.hint = in.readString();
                data.hasRecovery = in.readInt() != 0;
                data.emailUnconfirmedPattern = in.readString();

                return data;
            }

            @Override public AuthStateWaitPassword[] newArray(int size) {
                return new AuthStateWaitPassword[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(hint);
            dest.writeInt(this.hasRecovery ? 1 : 0);
            dest.writeString(emailUnconfirmedPattern);
        }
	
        public AuthStateWaitPassword() {
        }

        public AuthStateWaitPassword(String hint, boolean hasRecovery, String emailUnconfirmedPattern) {
            this.hint = hint;
            this.hasRecovery = hasRecovery;
            this.emailUnconfirmedPattern = emailUnconfirmedPattern;
        }

        public static final int CONSTRUCTOR = -338450931;

        @Override
        public int getConstructor() {
            return -338450931;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("AuthStateWaitPassword").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("hint").append(" = ").append(hint).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("hasRecovery").append(" = ").append(hasRecovery).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("emailUnconfirmedPattern").append(" = ").append(emailUnconfirmedPattern).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class AuthStateOk extends AuthState {

        public static final Creator<AuthStateOk> CREATOR = new Creator<AuthStateOk>() {

            @Override public AuthStateOk createFromParcel(Parcel in) {
                AuthStateOk data = new AuthStateOk();

        
                return data;
            }

            @Override public AuthStateOk[] newArray(int size) {
                return new AuthStateOk[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public AuthStateOk() {
        }

        public static final int CONSTRUCTOR = 1222968966;

        @Override
        public int getConstructor() {
            return 1222968966;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("AuthStateOk").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class AuthStateLoggingOut extends AuthState {

        public static final Creator<AuthStateLoggingOut> CREATOR = new Creator<AuthStateLoggingOut>() {

            @Override public AuthStateLoggingOut createFromParcel(Parcel in) {
                AuthStateLoggingOut data = new AuthStateLoggingOut();

        
                return data;
            }

            @Override public AuthStateLoggingOut[] newArray(int size) {
                return new AuthStateLoggingOut[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public AuthStateLoggingOut() {
        }

        public static final int CONSTRUCTOR = 965468001;

        @Override
        public int getConstructor() {
            return 965468001;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("AuthStateLoggingOut").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class BotCommand extends TLObject {
        public String command;
        public String description;

        public static final Creator<BotCommand> CREATOR = new Creator<BotCommand>() {

            @Override public BotCommand createFromParcel(Parcel in) {
                BotCommand data = new BotCommand();

                data.command = in.readString();
                data.description = in.readString();

                return data;
            }

            @Override public BotCommand[] newArray(int size) {
                return new BotCommand[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(command);
            dest.writeString(description);
        }
	
        public BotCommand() {
        }

        public BotCommand(String command, String description) {
            this.command = command;
            this.description = description;
        }

        public static final int CONSTRUCTOR = -1032140601;

        @Override
        public int getConstructor() {
            return -1032140601;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("BotCommand").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("command").append(" = ").append(command).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("description").append(" = ").append(description).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class BotInfo extends TLObject {
    }

    public static class BotInfoEmpty extends BotInfo {

        public static final Creator<BotInfoEmpty> CREATOR = new Creator<BotInfoEmpty>() {

            @Override public BotInfoEmpty createFromParcel(Parcel in) {
                BotInfoEmpty data = new BotInfoEmpty();

        
                return data;
            }

            @Override public BotInfoEmpty[] newArray(int size) {
                return new BotInfoEmpty[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public BotInfoEmpty() {
        }

        public static final int CONSTRUCTOR = -1154598962;

        @Override
        public int getConstructor() {
            return -1154598962;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("BotInfoEmpty").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class BotInfoGeneral extends BotInfo {
        public String shareText;
        public String description;
        public BotCommand[] commands;

        public static final Creator<BotInfoGeneral> CREATOR = new Creator<BotInfoGeneral>() {

            @Override public BotInfoGeneral createFromParcel(Parcel in) {
                BotInfoGeneral data = new BotInfoGeneral();

                data.shareText = in.readString();
                data.description = in.readString();
                data.commands = in.createTypedArray(BotCommand.CREATOR);

                return data;
            }

            @Override public BotInfoGeneral[] newArray(int size) {
                return new BotInfoGeneral[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(shareText);
            dest.writeString(description);
            dest.writeTypedArray(this.commands, flags);
        }
	
        public BotInfoGeneral() {
        }

        public BotInfoGeneral(String shareText, String description, BotCommand[] commands) {
            this.shareText = shareText;
            this.description = description;
            this.commands = commands;
        }

        public static final int CONSTRUCTOR = 1269773691;

        @Override
        public int getConstructor() {
            return 1269773691;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("BotInfoGeneral").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("shareText").append(" = ").append(shareText).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("description").append(" = ").append(description).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("commands").append(" = ").append("BotCommand[]").append(" {").append(Arrays.toString(commands)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Chat extends TLObject {
        public long id;
        public Message topMessage;
        public int unreadCount;
        public int lastReadInboxMessageId;
        public int lastReadOutboxMessageId;
        public NotificationSettings notificationSettings;
        public int replyMarkupMessageId;
        public ChatInfo type;

        public static final Creator<Chat> CREATOR = new Creator<Chat>() {

            @Override public Chat createFromParcel(Parcel in) {
                Chat data = new Chat();

                data.id = in.readLong();
                data.topMessage = in.readParcelable(Message.class.getClassLoader());
                data.unreadCount = in.readInt();
                data.lastReadInboxMessageId = in.readInt();
                data.lastReadOutboxMessageId = in.readInt();
                data.notificationSettings = in.readParcelable(NotificationSettings.class.getClassLoader());
                data.replyMarkupMessageId = in.readInt();
                data.type = in.readParcelable(ChatInfo.class.getClassLoader());

                return data;
            }

            @Override public Chat[] newArray(int size) {
                return new Chat[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(id);
            dest.writeParcelable(this.topMessage, flags);
            dest.writeInt(unreadCount);
            dest.writeInt(lastReadInboxMessageId);
            dest.writeInt(lastReadOutboxMessageId);
            dest.writeParcelable(this.notificationSettings, flags);
            dest.writeInt(replyMarkupMessageId);
            dest.writeParcelable(this.type, flags);
        }
	
        public Chat() {
        }

        public Chat(long id, Message topMessage, int unreadCount, int lastReadInboxMessageId, int lastReadOutboxMessageId, NotificationSettings notificationSettings, int replyMarkupMessageId, ChatInfo type) {
            this.id = id;
            this.topMessage = topMessage;
            this.unreadCount = unreadCount;
            this.lastReadInboxMessageId = lastReadInboxMessageId;
            this.lastReadOutboxMessageId = lastReadOutboxMessageId;
            this.notificationSettings = notificationSettings;
            this.replyMarkupMessageId = replyMarkupMessageId;
            this.type = type;
        }

        public static final int CONSTRUCTOR = -149531729;

        @Override
        public int getConstructor() {
            return -149531729;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Chat").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("topMessage").append(" = "); topMessage.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("unreadCount").append(" = ").append(unreadCount).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastReadInboxMessageId").append(" = ").append(lastReadInboxMessageId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastReadOutboxMessageId").append(" = ").append(lastReadOutboxMessageId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("notificationSettings").append(" = "); notificationSettings.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("replyMarkupMessageId").append(" = ").append(replyMarkupMessageId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("type").append(" = "); type.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class ChatInfo extends TLObject {
    }

    public static class PrivateChatInfo extends ChatInfo {
        public User user;

        public static final Creator<PrivateChatInfo> CREATOR = new Creator<PrivateChatInfo>() {

            @Override public PrivateChatInfo createFromParcel(Parcel in) {
                PrivateChatInfo data = new PrivateChatInfo();

                data.user = in.readParcelable(User.class.getClassLoader());

                return data;
            }

            @Override public PrivateChatInfo[] newArray(int size) {
                return new PrivateChatInfo[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.user, flags);
        }
	
        public PrivateChatInfo() {
        }

        public PrivateChatInfo(User user) {
            this.user = user;
        }

        public static final int CONSTRUCTOR = -241270753;

        @Override
        public int getConstructor() {
            return -241270753;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("PrivateChatInfo").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("user").append(" = "); user.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GroupChatInfo extends ChatInfo {
        public GroupChat groupChat;

        public static final Creator<GroupChatInfo> CREATOR = new Creator<GroupChatInfo>() {

            @Override public GroupChatInfo createFromParcel(Parcel in) {
                GroupChatInfo data = new GroupChatInfo();

                data.groupChat = in.readParcelable(GroupChat.class.getClassLoader());

                return data;
            }

            @Override public GroupChatInfo[] newArray(int size) {
                return new GroupChatInfo[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.groupChat, flags);
        }
	
        public GroupChatInfo() {
        }

        public GroupChatInfo(GroupChat groupChat) {
            this.groupChat = groupChat;
        }

        public static final int CONSTRUCTOR = 1106069512;

        @Override
        public int getConstructor() {
            return 1106069512;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GroupChatInfo").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("groupChat").append(" = "); groupChat.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ChatParticipant extends TLObject {
        public User user;
        public int inviterId;
        public int joinDate;
        public BotInfo botInfo;

        public static final Creator<ChatParticipant> CREATOR = new Creator<ChatParticipant>() {

            @Override public ChatParticipant createFromParcel(Parcel in) {
                ChatParticipant data = new ChatParticipant();

                data.user = in.readParcelable(User.class.getClassLoader());
                data.inviterId = in.readInt();
                data.joinDate = in.readInt();
                data.botInfo = in.readParcelable(BotInfo.class.getClassLoader());

                return data;
            }

            @Override public ChatParticipant[] newArray(int size) {
                return new ChatParticipant[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.user, flags);
            dest.writeInt(inviterId);
            dest.writeInt(joinDate);
            dest.writeParcelable(this.botInfo, flags);
        }
	
        public ChatParticipant() {
        }

        public ChatParticipant(User user, int inviterId, int joinDate, BotInfo botInfo) {
            this.user = user;
            this.inviterId = inviterId;
            this.joinDate = joinDate;
            this.botInfo = botInfo;
        }

        public static final int CONSTRUCTOR = -1270928779;

        @Override
        public int getConstructor() {
            return -1270928779;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ChatParticipant").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("user").append(" = "); user.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("inviterId").append(" = ").append(inviterId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("joinDate").append(" = ").append(joinDate).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("botInfo").append(" = "); botInfo.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Chats extends TLObject {
        public Chat[] chats;

        public static final Creator<Chats> CREATOR = new Creator<Chats>() {

            @Override public Chats createFromParcel(Parcel in) {
                Chats data = new Chats();

                data.chats = in.createTypedArray(Chat.CREATOR);

                return data;
            }

            @Override public Chats[] newArray(int size) {
                return new Chats[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedArray(this.chats, flags);
        }
	
        public Chats() {
        }

        public Chats(Chat[] chats) {
            this.chats = chats;
        }

        public static final int CONSTRUCTOR = -203185581;

        @Override
        public int getConstructor() {
            return -203185581;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Chats").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chats").append(" = ").append("Chat[]").append(" {").append(Arrays.toString(chats)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Contacts extends TLObject {
        public User[] users;

        public static final Creator<Contacts> CREATOR = new Creator<Contacts>() {

            @Override public Contacts createFromParcel(Parcel in) {
                Contacts data = new Contacts();

                data.users = in.createTypedArray(User.CREATOR);

                return data;
            }

            @Override public Contacts[] newArray(int size) {
                return new Contacts[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedArray(this.users, flags);
        }
	
        public Contacts() {
        }

        public Contacts(User[] users) {
            this.users = users;
        }

        public static final int CONSTRUCTOR = 1238821485;

        @Override
        public int getConstructor() {
            return 1238821485;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Contacts").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("users").append(" = ").append("User[]").append(" {").append(Arrays.toString(users)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Document extends TLObject {
        public String fileName;
        public String mimeType;
        public PhotoSize thumb;
        public File document;

        public static final Creator<Document> CREATOR = new Creator<Document>() {

            @Override public Document createFromParcel(Parcel in) {
                Document data = new Document();

                data.fileName = in.readString();
                data.mimeType = in.readString();
                data.thumb = in.readParcelable(PhotoSize.class.getClassLoader());
                data.document = in.readParcelable(File.class.getClassLoader());

                return data;
            }

            @Override public Document[] newArray(int size) {
                return new Document[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(fileName);
            dest.writeString(mimeType);
            dest.writeParcelable(this.thumb, flags);
            dest.writeParcelable(this.document, flags);
        }
	
        public Document() {
        }

        public Document(String fileName, String mimeType, PhotoSize thumb, File document) {
            this.fileName = fileName;
            this.mimeType = mimeType;
            this.thumb = thumb;
            this.document = document;
        }

        public static final int CONSTRUCTOR = 742605884;

        @Override
        public int getConstructor() {
            return 742605884;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Document").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fileName").append(" = ").append(fileName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("mimeType").append(" = ").append(mimeType).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("thumb").append(" = "); thumb.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("document").append(" = "); document.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Error extends TLObject {
        public int code;
        public String text;

        public static final Creator<Error> CREATOR = new Creator<Error>() {

            @Override public Error createFromParcel(Parcel in) {
                Error data = new Error();

                data.code = in.readInt();
                data.text = in.readString();

                return data;
            }

            @Override public Error[] newArray(int size) {
                return new Error[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(code);
            dest.writeString(text);
        }
	
        public Error() {
        }

        public Error(int code, String text) {
            this.code = code;
            this.text = text;
        }

        public static final int CONSTRUCTOR = -994444869;

        @Override
        public int getConstructor() {
            return -994444869;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Error").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("code").append(" = ").append(code).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("text").append(" = ").append(text).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class File extends TLObject {
        public int id;
        public String persistentId;
        public int size;
        public String path;

        public static final Creator<File> CREATOR = new Creator<File>() {

            @Override public File createFromParcel(Parcel in) {
                File data = new File();

                data.id = in.readInt();
                data.persistentId = in.readString();
                data.size = in.readInt();
                data.path = in.readString();

                return data;
            }

            @Override public File[] newArray(int size) {
                return new File[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(persistentId);
            dest.writeInt(size);
            dest.writeString(path);
        }
	
        public File() {
        }

        public File(int id, String persistentId, int size, String path) {
            this.id = id;
            this.persistentId = persistentId;
            this.size = size;
            this.path = path;
        }

        public static final int CONSTRUCTOR = -1956331593;

        @Override
        public int getConstructor() {
            return -1956331593;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("File").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("persistentId").append(" = ").append(persistentId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("size").append(" = ").append(size).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("path").append(" = ").append(path).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GroupChat extends TLObject {
        public int id;
        public String title;
        public int participantsCount;
        public ProfilePhoto photo;
        public boolean left;

        public static final Creator<GroupChat> CREATOR = new Creator<GroupChat>() {

            @Override public GroupChat createFromParcel(Parcel in) {
                GroupChat data = new GroupChat();

                data.id = in.readInt();
                data.title = in.readString();
                data.participantsCount = in.readInt();
                data.photo = in.readParcelable(ProfilePhoto.class.getClassLoader());
                data.left = in.readInt() != 0;

                return data;
            }

            @Override public GroupChat[] newArray(int size) {
                return new GroupChat[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(title);
            dest.writeInt(participantsCount);
            dest.writeParcelable(this.photo, flags);
            dest.writeInt(this.left ? 1 : 0);
        }
	
        public GroupChat() {
        }

        public GroupChat(int id, String title, int participantsCount, ProfilePhoto photo, boolean left) {
            this.id = id;
            this.title = title;
            this.participantsCount = participantsCount;
            this.photo = photo;
            this.left = left;
        }

        public static final int CONSTRUCTOR = 1946833410;

        @Override
        public int getConstructor() {
            return 1946833410;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GroupChat").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("participantsCount").append(" = ").append(participantsCount).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photo").append(" = "); photo.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("left").append(" = ").append(left).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GroupChatFull extends TLObject {
        public GroupChat groupChat;
        public int adminId;
        public ChatParticipant[] participants;
        public String inviteLink;

        public static final Creator<GroupChatFull> CREATOR = new Creator<GroupChatFull>() {

            @Override public GroupChatFull createFromParcel(Parcel in) {
                GroupChatFull data = new GroupChatFull();

                data.groupChat = in.readParcelable(GroupChat.class.getClassLoader());
                data.adminId = in.readInt();
                data.participants = in.createTypedArray(ChatParticipant.CREATOR);
                data.inviteLink = in.readString();

                return data;
            }

            @Override public GroupChatFull[] newArray(int size) {
                return new GroupChatFull[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.groupChat, flags);
            dest.writeInt(adminId);
            dest.writeTypedArray(this.participants, flags);
            dest.writeString(inviteLink);
        }
	
        public GroupChatFull() {
        }

        public GroupChatFull(GroupChat groupChat, int adminId, ChatParticipant[] participants, String inviteLink) {
            this.groupChat = groupChat;
            this.adminId = adminId;
            this.participants = participants;
            this.inviteLink = inviteLink;
        }

        public static final int CONSTRUCTOR = -2117185800;

        @Override
        public int getConstructor() {
            return -2117185800;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GroupChatFull").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("groupChat").append(" = "); groupChat.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("adminId").append(" = ").append(adminId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("participants").append(" = ").append("ChatParticipant[]").append(" {").append(Arrays.toString(participants)).append("}\n");
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("inviteLink").append(" = ").append(inviteLink).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputContact extends TLObject {
        public String phoneNumber;
        public String firstName;
        public String lastName;

        public static final Creator<InputContact> CREATOR = new Creator<InputContact>() {

            @Override public InputContact createFromParcel(Parcel in) {
                InputContact data = new InputContact();

                data.phoneNumber = in.readString();
                data.firstName = in.readString();
                data.lastName = in.readString();

                return data;
            }

            @Override public InputContact[] newArray(int size) {
                return new InputContact[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(phoneNumber);
            dest.writeString(firstName);
            dest.writeString(lastName);
        }
	
        public InputContact() {
        }

        public InputContact(String phoneNumber, String firstName, String lastName) {
            this.phoneNumber = phoneNumber;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public static final int CONSTRUCTOR = 1707235253;

        @Override
        public int getConstructor() {
            return 1707235253;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputContact").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("phoneNumber").append(" = ").append(phoneNumber).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("firstName").append(" = ").append(firstName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastName").append(" = ").append(lastName).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class InputFile extends TLObject {
    }

    public static class InputFileId extends InputFile {
        public int id;

        public static final Creator<InputFileId> CREATOR = new Creator<InputFileId>() {

            @Override public InputFileId createFromParcel(Parcel in) {
                InputFileId data = new InputFileId();

                data.id = in.readInt();

                return data;
            }

            @Override public InputFileId[] newArray(int size) {
                return new InputFileId[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
        }
	
        public InputFileId() {
        }

        public InputFileId(int id) {
            this.id = id;
        }

        public static final int CONSTRUCTOR = 1553438243;

        @Override
        public int getConstructor() {
            return 1553438243;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputFileId").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputFilePersistentId extends InputFile {
        public String persistentId;

        public static final Creator<InputFilePersistentId> CREATOR = new Creator<InputFilePersistentId>() {

            @Override public InputFilePersistentId createFromParcel(Parcel in) {
                InputFilePersistentId data = new InputFilePersistentId();

                data.persistentId = in.readString();

                return data;
            }

            @Override public InputFilePersistentId[] newArray(int size) {
                return new InputFilePersistentId[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(persistentId);
        }
	
        public InputFilePersistentId() {
        }

        public InputFilePersistentId(String persistentId) {
            this.persistentId = persistentId;
        }

        public static final int CONSTRUCTOR = 1856539551;

        @Override
        public int getConstructor() {
            return 1856539551;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputFilePersistentId").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("persistentId").append(" = ").append(persistentId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputFileLocal extends InputFile {
        public String path;

        public static final Creator<InputFileLocal> CREATOR = new Creator<InputFileLocal>() {

            @Override public InputFileLocal createFromParcel(Parcel in) {
                InputFileLocal data = new InputFileLocal();

                data.path = in.readString();

                return data;
            }

            @Override public InputFileLocal[] newArray(int size) {
                return new InputFileLocal[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(path);
        }
	
        public InputFileLocal() {
        }

        public InputFileLocal(String path) {
            this.path = path;
        }

        public static final int CONSTRUCTOR = 2056030919;

        @Override
        public int getConstructor() {
            return 2056030919;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputFileLocal").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("path").append(" = ").append(path).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class InputMessageContent extends TLObject {
    }

    public static class InputMessageText extends InputMessageContent {
        public String text;

        public static final Creator<InputMessageText> CREATOR = new Creator<InputMessageText>() {

            @Override public InputMessageText createFromParcel(Parcel in) {
                InputMessageText data = new InputMessageText();

                data.text = in.readString();

                return data;
            }

            @Override public InputMessageText[] newArray(int size) {
                return new InputMessageText[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(text);
        }
	
        public InputMessageText() {
        }

        public InputMessageText(String text) {
            this.text = text;
        }

        public static final int CONSTRUCTOR = -54904775;

        @Override
        public int getConstructor() {
            return -54904775;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageText").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("text").append(" = ").append(text).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessageAudio extends InputMessageContent {
        public InputFile audio;
        public int duration;
        public String title;
        public String performer;

        public static final Creator<InputMessageAudio> CREATOR = new Creator<InputMessageAudio>() {

            @Override public InputMessageAudio createFromParcel(Parcel in) {
                InputMessageAudio data = new InputMessageAudio();

                data.audio = in.readParcelable(InputFile.class.getClassLoader());
                data.duration = in.readInt();
                data.title = in.readString();
                data.performer = in.readString();

                return data;
            }

            @Override public InputMessageAudio[] newArray(int size) {
                return new InputMessageAudio[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.audio, flags);
            dest.writeInt(duration);
            dest.writeString(title);
            dest.writeString(performer);
        }
	
        public InputMessageAudio() {
        }

        public InputMessageAudio(InputFile audio, int duration, String title, String performer) {
            this.audio = audio;
            this.duration = duration;
            this.title = title;
            this.performer = performer;
        }

        public static final int CONSTRUCTOR = -674962739;

        @Override
        public int getConstructor() {
            return -674962739;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageAudio").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("audio").append(" = "); audio.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("duration").append(" = ").append(duration).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("performer").append(" = ").append(performer).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessageDocument extends InputMessageContent {
        public InputFile document;

        public static final Creator<InputMessageDocument> CREATOR = new Creator<InputMessageDocument>() {

            @Override public InputMessageDocument createFromParcel(Parcel in) {
                InputMessageDocument data = new InputMessageDocument();

                data.document = in.readParcelable(InputFile.class.getClassLoader());

                return data;
            }

            @Override public InputMessageDocument[] newArray(int size) {
                return new InputMessageDocument[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.document, flags);
        }
	
        public InputMessageDocument() {
        }

        public InputMessageDocument(InputFile document) {
            this.document = document;
        }

        public static final int CONSTRUCTOR = 75474869;

        @Override
        public int getConstructor() {
            return 75474869;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageDocument").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("document").append(" = "); document.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessagePhoto extends InputMessageContent {
        public InputFile photo;
        public String caption;

        public static final Creator<InputMessagePhoto> CREATOR = new Creator<InputMessagePhoto>() {

            @Override public InputMessagePhoto createFromParcel(Parcel in) {
                InputMessagePhoto data = new InputMessagePhoto();

                data.photo = in.readParcelable(InputFile.class.getClassLoader());
                data.caption = in.readString();

                return data;
            }

            @Override public InputMessagePhoto[] newArray(int size) {
                return new InputMessagePhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.photo, flags);
            dest.writeString(caption);
        }
	
        public InputMessagePhoto() {
        }

        public InputMessagePhoto(InputFile photo, String caption) {
            this.photo = photo;
            this.caption = caption;
        }

        public static final int CONSTRUCTOR = 762116923;

        @Override
        public int getConstructor() {
            return 762116923;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessagePhoto").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photo").append(" = "); photo.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("caption").append(" = ").append(caption).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessageSticker extends InputMessageContent {
        public InputFile sticker;

        public static final Creator<InputMessageSticker> CREATOR = new Creator<InputMessageSticker>() {

            @Override public InputMessageSticker createFromParcel(Parcel in) {
                InputMessageSticker data = new InputMessageSticker();

                data.sticker = in.readParcelable(InputFile.class.getClassLoader());

                return data;
            }

            @Override public InputMessageSticker[] newArray(int size) {
                return new InputMessageSticker[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.sticker, flags);
        }
	
        public InputMessageSticker() {
        }

        public InputMessageSticker(InputFile sticker) {
            this.sticker = sticker;
        }

        public static final int CONSTRUCTOR = 1579676898;

        @Override
        public int getConstructor() {
            return 1579676898;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageSticker").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("sticker").append(" = "); sticker.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessageVideo extends InputMessageContent {
        public InputFile video;
        public String caption;
        public int duration;

        public static final Creator<InputMessageVideo> CREATOR = new Creator<InputMessageVideo>() {

            @Override public InputMessageVideo createFromParcel(Parcel in) {
                InputMessageVideo data = new InputMessageVideo();

                data.video = in.readParcelable(InputFile.class.getClassLoader());
                data.caption = in.readString();
                data.duration = in.readInt();

                return data;
            }

            @Override public InputMessageVideo[] newArray(int size) {
                return new InputMessageVideo[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.video, flags);
            dest.writeString(caption);
            dest.writeInt(duration);
        }
	
        public InputMessageVideo() {
        }

        public InputMessageVideo(InputFile video, String caption, int duration) {
            this.video = video;
            this.caption = caption;
            this.duration = duration;
        }

        public static final int CONSTRUCTOR = -54736938;

        @Override
        public int getConstructor() {
            return -54736938;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageVideo").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("video").append(" = "); video.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("caption").append(" = ").append(caption).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("duration").append(" = ").append(duration).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessageVoice extends InputMessageContent {
        public InputFile voice;
        public int duration;

        public static final Creator<InputMessageVoice> CREATOR = new Creator<InputMessageVoice>() {

            @Override public InputMessageVoice createFromParcel(Parcel in) {
                InputMessageVoice data = new InputMessageVoice();

                data.voice = in.readParcelable(InputFile.class.getClassLoader());
                data.duration = in.readInt();

                return data;
            }

            @Override public InputMessageVoice[] newArray(int size) {
                return new InputMessageVoice[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.voice, flags);
            dest.writeInt(duration);
        }
	
        public InputMessageVoice() {
        }

        public InputMessageVoice(InputFile voice, int duration) {
            this.voice = voice;
            this.duration = duration;
        }

        public static final int CONSTRUCTOR = -2013998476;

        @Override
        public int getConstructor() {
            return -2013998476;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageVoice").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("voice").append(" = "); voice.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("duration").append(" = ").append(duration).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessageLocation extends InputMessageContent {
        public double longitude;
        public double latitude;

        public static final Creator<InputMessageLocation> CREATOR = new Creator<InputMessageLocation>() {

            @Override public InputMessageLocation createFromParcel(Parcel in) {
                InputMessageLocation data = new InputMessageLocation();

                data.longitude = in.readDouble();
                data.latitude = in.readDouble();

                return data;
            }

            @Override public InputMessageLocation[] newArray(int size) {
                return new InputMessageLocation[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeDouble(longitude);
            dest.writeDouble(latitude);
        }
	
        public InputMessageLocation() {
        }

        public InputMessageLocation(double longitude, double latitude) {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public static final int CONSTRUCTOR = 1494132433;

        @Override
        public int getConstructor() {
            return 1494132433;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageLocation").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("longitude").append(" = ").append(longitude).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("latitude").append(" = ").append(latitude).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessageVenue extends InputMessageContent {
        public double longitude;
        public double latitude;
        public String title;
        public String address;
        public String provider;
        public String venueId;

        public static final Creator<InputMessageVenue> CREATOR = new Creator<InputMessageVenue>() {

            @Override public InputMessageVenue createFromParcel(Parcel in) {
                InputMessageVenue data = new InputMessageVenue();

                data.longitude = in.readDouble();
                data.latitude = in.readDouble();
                data.title = in.readString();
                data.address = in.readString();
                data.provider = in.readString();
                data.venueId = in.readString();

                return data;
            }

            @Override public InputMessageVenue[] newArray(int size) {
                return new InputMessageVenue[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeDouble(longitude);
            dest.writeDouble(latitude);
            dest.writeString(title);
            dest.writeString(address);
            dest.writeString(provider);
            dest.writeString(venueId);
        }
	
        public InputMessageVenue() {
        }

        public InputMessageVenue(double longitude, double latitude, String title, String address, String provider, String venueId) {
            this.longitude = longitude;
            this.latitude = latitude;
            this.title = title;
            this.address = address;
            this.provider = provider;
            this.venueId = venueId;
        }

        public static final int CONSTRUCTOR = -971070542;

        @Override
        public int getConstructor() {
            return -971070542;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageVenue").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("longitude").append(" = ").append(longitude).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("latitude").append(" = ").append(latitude).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("address").append(" = ").append(address).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("provider").append(" = ").append(provider).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("venueId").append(" = ").append(venueId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessageContact extends InputMessageContent {
        public String phoneNumber;
        public String firstName;
        public String lastName;
        public int userId;

        public static final Creator<InputMessageContact> CREATOR = new Creator<InputMessageContact>() {

            @Override public InputMessageContact createFromParcel(Parcel in) {
                InputMessageContact data = new InputMessageContact();

                data.phoneNumber = in.readString();
                data.firstName = in.readString();
                data.lastName = in.readString();
                data.userId = in.readInt();

                return data;
            }

            @Override public InputMessageContact[] newArray(int size) {
                return new InputMessageContact[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(phoneNumber);
            dest.writeString(firstName);
            dest.writeString(lastName);
            dest.writeInt(userId);
        }
	
        public InputMessageContact() {
        }

        public InputMessageContact(String phoneNumber, String firstName, String lastName, int userId) {
            this.phoneNumber = phoneNumber;
            this.firstName = firstName;
            this.lastName = lastName;
            this.userId = userId;
        }

        public static final int CONSTRUCTOR = -1261805057;

        @Override
        public int getConstructor() {
            return -1261805057;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageContact").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("phoneNumber").append(" = ").append(phoneNumber).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("firstName").append(" = ").append(firstName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastName").append(" = ").append(lastName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class InputMessageForwarded extends InputMessageContent {
        public long fromChatId;
        public int messageId;

        public static final Creator<InputMessageForwarded> CREATOR = new Creator<InputMessageForwarded>() {

            @Override public InputMessageForwarded createFromParcel(Parcel in) {
                InputMessageForwarded data = new InputMessageForwarded();

                data.fromChatId = in.readLong();
                data.messageId = in.readInt();

                return data;
            }

            @Override public InputMessageForwarded[] newArray(int size) {
                return new InputMessageForwarded[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(fromChatId);
            dest.writeInt(messageId);
        }
	
        public InputMessageForwarded() {
        }

        public InputMessageForwarded(long fromChatId, int messageId) {
            this.fromChatId = fromChatId;
            this.messageId = messageId;
        }

        public static final int CONSTRUCTOR = 863879612;

        @Override
        public int getConstructor() {
            return 863879612;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("InputMessageForwarded").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fromChatId").append(" = ").append(fromChatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messageId").append(" = ").append(messageId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class LinkState extends TLObject {
    }

    public static class LinkStateNone extends LinkState {

        public static final Creator<LinkStateNone> CREATOR = new Creator<LinkStateNone>() {

            @Override public LinkStateNone createFromParcel(Parcel in) {
                LinkStateNone data = new LinkStateNone();

        
                return data;
            }

            @Override public LinkStateNone[] newArray(int size) {
                return new LinkStateNone[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public LinkStateNone() {
        }

        public static final int CONSTRUCTOR = 951430287;

        @Override
        public int getConstructor() {
            return 951430287;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("LinkStateNone").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class LinkStateKnowsPhoneNumber extends LinkState {

        public static final Creator<LinkStateKnowsPhoneNumber> CREATOR = new Creator<LinkStateKnowsPhoneNumber>() {

            @Override public LinkStateKnowsPhoneNumber createFromParcel(Parcel in) {
                LinkStateKnowsPhoneNumber data = new LinkStateKnowsPhoneNumber();

        
                return data;
            }

            @Override public LinkStateKnowsPhoneNumber[] newArray(int size) {
                return new LinkStateKnowsPhoneNumber[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public LinkStateKnowsPhoneNumber() {
        }

        public static final int CONSTRUCTOR = 380898199;

        @Override
        public int getConstructor() {
            return 380898199;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("LinkStateKnowsPhoneNumber").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class LinkStateContact extends LinkState {

        public static final Creator<LinkStateContact> CREATOR = new Creator<LinkStateContact>() {

            @Override public LinkStateContact createFromParcel(Parcel in) {
                LinkStateContact data = new LinkStateContact();

        
                return data;
            }

            @Override public LinkStateContact[] newArray(int size) {
                return new LinkStateContact[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public LinkStateContact() {
        }

        public static final int CONSTRUCTOR = -731324681;

        @Override
        public int getConstructor() {
            return -731324681;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("LinkStateContact").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Location extends TLObject {
        public double longitude;
        public double latitude;

        public static final Creator<Location> CREATOR = new Creator<Location>() {

            @Override public Location createFromParcel(Parcel in) {
                Location data = new Location();

                data.longitude = in.readDouble();
                data.latitude = in.readDouble();

                return data;
            }

            @Override public Location[] newArray(int size) {
                return new Location[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeDouble(longitude);
            dest.writeDouble(latitude);
        }
	
        public Location() {
        }

        public Location(double longitude, double latitude) {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public static final int CONSTRUCTOR = -1691941094;

        @Override
        public int getConstructor() {
            return -1691941094;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Location").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("longitude").append(" = ").append(longitude).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("latitude").append(" = ").append(latitude).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Message extends TLObject {
        public int id;
        public int fromId;
        public long chatId;
        public int date;
        public int forwardFromId;
        public int forwardDate;
        public int replyToMessageId;
        public MessageContent message;
        public ReplyMarkup replyMarkup;

        public static final Creator<Message> CREATOR = new Creator<Message>() {

            @Override public Message createFromParcel(Parcel in) {
                Message data = new Message();

                data.id = in.readInt();
                data.fromId = in.readInt();
                data.chatId = in.readLong();
                data.date = in.readInt();
                data.forwardFromId = in.readInt();
                data.forwardDate = in.readInt();
                data.replyToMessageId = in.readInt();
                data.message = in.readParcelable(MessageContent.class.getClassLoader());
                data.replyMarkup = in.readParcelable(ReplyMarkup.class.getClassLoader());

                return data;
            }

            @Override public Message[] newArray(int size) {
                return new Message[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(fromId);
            dest.writeLong(chatId);
            dest.writeInt(date);
            dest.writeInt(forwardFromId);
            dest.writeInt(forwardDate);
            dest.writeInt(replyToMessageId);
            dest.writeParcelable(this.message, flags);
            dest.writeParcelable(this.replyMarkup, flags);
        }
	
        public Message() {
        }

        public Message(int id, int fromId, long chatId, int date, int forwardFromId, int forwardDate, int replyToMessageId, MessageContent message, ReplyMarkup replyMarkup) {
            this.id = id;
            this.fromId = fromId;
            this.chatId = chatId;
            this.date = date;
            this.forwardFromId = forwardFromId;
            this.forwardDate = forwardDate;
            this.replyToMessageId = replyToMessageId;
            this.message = message;
            this.replyMarkup = replyMarkup;
        }

        public static final int CONSTRUCTOR = -974303888;

        @Override
        public int getConstructor() {
            return -974303888;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Message").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fromId").append(" = ").append(fromId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("date").append(" = ").append(date).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("forwardFromId").append(" = ").append(forwardFromId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("forwardDate").append(" = ").append(forwardDate).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("replyToMessageId").append(" = ").append(replyToMessageId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("message").append(" = "); message.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("replyMarkup").append(" = "); replyMarkup.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class MessageContent extends TLObject {
    }

    public static class MessageText extends MessageContent {
        public String text;

        public static final Creator<MessageText> CREATOR = new Creator<MessageText>() {

            @Override public MessageText createFromParcel(Parcel in) {
                MessageText data = new MessageText();

                data.text = in.readString();

                return data;
            }

            @Override public MessageText[] newArray(int size) {
                return new MessageText[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(text);
        }
	
        public MessageText() {
        }

        public MessageText(String text) {
            this.text = text;
        }

        public static final int CONSTRUCTOR = 934458430;

        @Override
        public int getConstructor() {
            return 934458430;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageText").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("text").append(" = ").append(text).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageAudio extends MessageContent {
        public Audio audio;

        public static final Creator<MessageAudio> CREATOR = new Creator<MessageAudio>() {

            @Override public MessageAudio createFromParcel(Parcel in) {
                MessageAudio data = new MessageAudio();

                data.audio = in.readParcelable(Audio.class.getClassLoader());

                return data;
            }

            @Override public MessageAudio[] newArray(int size) {
                return new MessageAudio[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.audio, flags);
        }
	
        public MessageAudio() {
        }

        public MessageAudio(Audio audio) {
            this.audio = audio;
        }

        public static final int CONSTRUCTOR = 239258829;

        @Override
        public int getConstructor() {
            return 239258829;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageAudio").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("audio").append(" = "); audio.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageDocument extends MessageContent {
        public Document document;

        public static final Creator<MessageDocument> CREATOR = new Creator<MessageDocument>() {

            @Override public MessageDocument createFromParcel(Parcel in) {
                MessageDocument data = new MessageDocument();

                data.document = in.readParcelable(Document.class.getClassLoader());

                return data;
            }

            @Override public MessageDocument[] newArray(int size) {
                return new MessageDocument[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.document, flags);
        }
	
        public MessageDocument() {
        }

        public MessageDocument(Document document) {
            this.document = document;
        }

        public static final int CONSTRUCTOR = -410248975;

        @Override
        public int getConstructor() {
            return -410248975;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageDocument").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("document").append(" = "); document.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessagePhoto extends MessageContent {
        public Photo photo;
        public String caption;

        public static final Creator<MessagePhoto> CREATOR = new Creator<MessagePhoto>() {

            @Override public MessagePhoto createFromParcel(Parcel in) {
                MessagePhoto data = new MessagePhoto();

                data.photo = in.readParcelable(Photo.class.getClassLoader());
                data.caption = in.readString();

                return data;
            }

            @Override public MessagePhoto[] newArray(int size) {
                return new MessagePhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.photo, flags);
            dest.writeString(caption);
        }
	
        public MessagePhoto() {
        }

        public MessagePhoto(Photo photo, String caption) {
            this.photo = photo;
            this.caption = caption;
        }

        public static final int CONSTRUCTOR = 1469704153;

        @Override
        public int getConstructor() {
            return 1469704153;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessagePhoto").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photo").append(" = "); photo.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("caption").append(" = ").append(caption).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageSticker extends MessageContent {
        public Sticker sticker;

        public static final Creator<MessageSticker> CREATOR = new Creator<MessageSticker>() {

            @Override public MessageSticker createFromParcel(Parcel in) {
                MessageSticker data = new MessageSticker();

                data.sticker = in.readParcelable(Sticker.class.getClassLoader());

                return data;
            }

            @Override public MessageSticker[] newArray(int size) {
                return new MessageSticker[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.sticker, flags);
        }
	
        public MessageSticker() {
        }

        public MessageSticker(Sticker sticker) {
            this.sticker = sticker;
        }

        public static final int CONSTRUCTOR = 1779022878;

        @Override
        public int getConstructor() {
            return 1779022878;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageSticker").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("sticker").append(" = "); sticker.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageVideo extends MessageContent {
        public Video video;
        public String caption;

        public static final Creator<MessageVideo> CREATOR = new Creator<MessageVideo>() {

            @Override public MessageVideo createFromParcel(Parcel in) {
                MessageVideo data = new MessageVideo();

                data.video = in.readParcelable(Video.class.getClassLoader());
                data.caption = in.readString();

                return data;
            }

            @Override public MessageVideo[] newArray(int size) {
                return new MessageVideo[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.video, flags);
            dest.writeString(caption);
        }
	
        public MessageVideo() {
        }

        public MessageVideo(Video video, String caption) {
            this.video = video;
            this.caption = caption;
        }

        public static final int CONSTRUCTOR = 1267590961;

        @Override
        public int getConstructor() {
            return 1267590961;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageVideo").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("video").append(" = "); video.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("caption").append(" = ").append(caption).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageVoice extends MessageContent {
        public Voice voice;

        public static final Creator<MessageVoice> CREATOR = new Creator<MessageVoice>() {

            @Override public MessageVoice createFromParcel(Parcel in) {
                MessageVoice data = new MessageVoice();

                data.voice = in.readParcelable(Voice.class.getClassLoader());

                return data;
            }

            @Override public MessageVoice[] newArray(int size) {
                return new MessageVoice[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.voice, flags);
        }
	
        public MessageVoice() {
        }

        public MessageVoice(Voice voice) {
            this.voice = voice;
        }

        public static final int CONSTRUCTOR = -1412619698;

        @Override
        public int getConstructor() {
            return -1412619698;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageVoice").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("voice").append(" = "); voice.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageWebPage extends MessageContent {
        public String text;
        public WebPage webPage;

        public static final Creator<MessageWebPage> CREATOR = new Creator<MessageWebPage>() {

            @Override public MessageWebPage createFromParcel(Parcel in) {
                MessageWebPage data = new MessageWebPage();

                data.text = in.readString();
                data.webPage = in.readParcelable(WebPage.class.getClassLoader());

                return data;
            }

            @Override public MessageWebPage[] newArray(int size) {
                return new MessageWebPage[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(text);
            dest.writeParcelable(this.webPage, flags);
        }
	
        public MessageWebPage() {
        }

        public MessageWebPage(String text, WebPage webPage) {
            this.text = text;
            this.webPage = webPage;
        }

        public static final int CONSTRUCTOR = 424964389;

        @Override
        public int getConstructor() {
            return 424964389;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageWebPage").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("text").append(" = ").append(text).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("webPage").append(" = "); webPage.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageLocation extends MessageContent {
        public Location location;

        public static final Creator<MessageLocation> CREATOR = new Creator<MessageLocation>() {

            @Override public MessageLocation createFromParcel(Parcel in) {
                MessageLocation data = new MessageLocation();

                data.location = in.readParcelable(Location.class.getClassLoader());

                return data;
            }

            @Override public MessageLocation[] newArray(int size) {
                return new MessageLocation[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.location, flags);
        }
	
        public MessageLocation() {
        }

        public MessageLocation(Location location) {
            this.location = location;
        }

        public static final int CONSTRUCTOR = 161545583;

        @Override
        public int getConstructor() {
            return 161545583;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageLocation").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("location").append(" = "); location.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageVenue extends MessageContent {
        public Location location;
        public String title;
        public String address;
        public String provider;
        public String venueId;

        public static final Creator<MessageVenue> CREATOR = new Creator<MessageVenue>() {

            @Override public MessageVenue createFromParcel(Parcel in) {
                MessageVenue data = new MessageVenue();

                data.location = in.readParcelable(Location.class.getClassLoader());
                data.title = in.readString();
                data.address = in.readString();
                data.provider = in.readString();
                data.venueId = in.readString();

                return data;
            }

            @Override public MessageVenue[] newArray(int size) {
                return new MessageVenue[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.location, flags);
            dest.writeString(title);
            dest.writeString(address);
            dest.writeString(provider);
            dest.writeString(venueId);
        }
	
        public MessageVenue() {
        }

        public MessageVenue(Location location, String title, String address, String provider, String venueId) {
            this.location = location;
            this.title = title;
            this.address = address;
            this.provider = provider;
            this.venueId = venueId;
        }

        public static final int CONSTRUCTOR = 586749589;

        @Override
        public int getConstructor() {
            return 586749589;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageVenue").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("location").append(" = "); location.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("address").append(" = ").append(address).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("provider").append(" = ").append(provider).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("venueId").append(" = ").append(venueId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageContact extends MessageContent {
        public String phoneNumber;
        public String firstName;
        public String lastName;
        public int userId;

        public static final Creator<MessageContact> CREATOR = new Creator<MessageContact>() {

            @Override public MessageContact createFromParcel(Parcel in) {
                MessageContact data = new MessageContact();

                data.phoneNumber = in.readString();
                data.firstName = in.readString();
                data.lastName = in.readString();
                data.userId = in.readInt();

                return data;
            }

            @Override public MessageContact[] newArray(int size) {
                return new MessageContact[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(phoneNumber);
            dest.writeString(firstName);
            dest.writeString(lastName);
            dest.writeInt(userId);
        }
	
        public MessageContact() {
        }

        public MessageContact(String phoneNumber, String firstName, String lastName, int userId) {
            this.phoneNumber = phoneNumber;
            this.firstName = firstName;
            this.lastName = lastName;
            this.userId = userId;
        }

        public static final int CONSTRUCTOR = 216059403;

        @Override
        public int getConstructor() {
            return 216059403;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageContact").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("phoneNumber").append(" = ").append(phoneNumber).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("firstName").append(" = ").append(firstName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastName").append(" = ").append(lastName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageGroupChatCreate extends MessageContent {
        public String title;
        public User[] participants;

        public static final Creator<MessageGroupChatCreate> CREATOR = new Creator<MessageGroupChatCreate>() {

            @Override public MessageGroupChatCreate createFromParcel(Parcel in) {
                MessageGroupChatCreate data = new MessageGroupChatCreate();

                data.title = in.readString();
                data.participants = in.createTypedArray(User.CREATOR);

                return data;
            }

            @Override public MessageGroupChatCreate[] newArray(int size) {
                return new MessageGroupChatCreate[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(title);
            dest.writeTypedArray(this.participants, flags);
        }
	
        public MessageGroupChatCreate() {
        }

        public MessageGroupChatCreate(String title, User[] participants) {
            this.title = title;
            this.participants = participants;
        }

        public static final int CONSTRUCTOR = -1856328772;

        @Override
        public int getConstructor() {
            return -1856328772;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageGroupChatCreate").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("participants").append(" = ").append("User[]").append(" {").append(Arrays.toString(participants)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageChatChangeTitle extends MessageContent {
        public String title;

        public static final Creator<MessageChatChangeTitle> CREATOR = new Creator<MessageChatChangeTitle>() {

            @Override public MessageChatChangeTitle createFromParcel(Parcel in) {
                MessageChatChangeTitle data = new MessageChatChangeTitle();

                data.title = in.readString();

                return data;
            }

            @Override public MessageChatChangeTitle[] newArray(int size) {
                return new MessageChatChangeTitle[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(title);
        }
	
        public MessageChatChangeTitle() {
        }

        public MessageChatChangeTitle(String title) {
            this.title = title;
        }

        public static final int CONSTRUCTOR = 748272449;

        @Override
        public int getConstructor() {
            return 748272449;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageChatChangeTitle").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageChatChangePhoto extends MessageContent {
        public Photo photo;

        public static final Creator<MessageChatChangePhoto> CREATOR = new Creator<MessageChatChangePhoto>() {

            @Override public MessageChatChangePhoto createFromParcel(Parcel in) {
                MessageChatChangePhoto data = new MessageChatChangePhoto();

                data.photo = in.readParcelable(Photo.class.getClassLoader());

                return data;
            }

            @Override public MessageChatChangePhoto[] newArray(int size) {
                return new MessageChatChangePhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.photo, flags);
        }
	
        public MessageChatChangePhoto() {
        }

        public MessageChatChangePhoto(Photo photo) {
            this.photo = photo;
        }

        public static final int CONSTRUCTOR = 319630249;

        @Override
        public int getConstructor() {
            return 319630249;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageChatChangePhoto").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photo").append(" = "); photo.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageChatDeletePhoto extends MessageContent {

        public static final Creator<MessageChatDeletePhoto> CREATOR = new Creator<MessageChatDeletePhoto>() {

            @Override public MessageChatDeletePhoto createFromParcel(Parcel in) {
                MessageChatDeletePhoto data = new MessageChatDeletePhoto();

        
                return data;
            }

            @Override public MessageChatDeletePhoto[] newArray(int size) {
                return new MessageChatDeletePhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public MessageChatDeletePhoto() {
        }

        public static final int CONSTRUCTOR = -184374809;

        @Override
        public int getConstructor() {
            return -184374809;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageChatDeletePhoto").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageChatAddParticipant extends MessageContent {
        public User user;

        public static final Creator<MessageChatAddParticipant> CREATOR = new Creator<MessageChatAddParticipant>() {

            @Override public MessageChatAddParticipant createFromParcel(Parcel in) {
                MessageChatAddParticipant data = new MessageChatAddParticipant();

                data.user = in.readParcelable(User.class.getClassLoader());

                return data;
            }

            @Override public MessageChatAddParticipant[] newArray(int size) {
                return new MessageChatAddParticipant[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.user, flags);
        }
	
        public MessageChatAddParticipant() {
        }

        public MessageChatAddParticipant(User user) {
            this.user = user;
        }

        public static final int CONSTRUCTOR = 1826239662;

        @Override
        public int getConstructor() {
            return 1826239662;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageChatAddParticipant").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("user").append(" = "); user.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageChatJoinByLink extends MessageContent {
        public int inviterId;

        public static final Creator<MessageChatJoinByLink> CREATOR = new Creator<MessageChatJoinByLink>() {

            @Override public MessageChatJoinByLink createFromParcel(Parcel in) {
                MessageChatJoinByLink data = new MessageChatJoinByLink();

                data.inviterId = in.readInt();

                return data;
            }

            @Override public MessageChatJoinByLink[] newArray(int size) {
                return new MessageChatJoinByLink[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(inviterId);
        }
	
        public MessageChatJoinByLink() {
        }

        public MessageChatJoinByLink(int inviterId) {
            this.inviterId = inviterId;
        }

        public static final int CONSTRUCTOR = 1832922905;

        @Override
        public int getConstructor() {
            return 1832922905;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageChatJoinByLink").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("inviterId").append(" = ").append(inviterId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageChatDeleteParticipant extends MessageContent {
        public User user;

        public static final Creator<MessageChatDeleteParticipant> CREATOR = new Creator<MessageChatDeleteParticipant>() {

            @Override public MessageChatDeleteParticipant createFromParcel(Parcel in) {
                MessageChatDeleteParticipant data = new MessageChatDeleteParticipant();

                data.user = in.readParcelable(User.class.getClassLoader());

                return data;
            }

            @Override public MessageChatDeleteParticipant[] newArray(int size) {
                return new MessageChatDeleteParticipant[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.user, flags);
        }
	
        public MessageChatDeleteParticipant() {
        }

        public MessageChatDeleteParticipant(User user) {
            this.user = user;
        }

        public static final int CONSTRUCTOR = 2091163657;

        @Override
        public int getConstructor() {
            return 2091163657;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageChatDeleteParticipant").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("user").append(" = "); user.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageDeleted extends MessageContent {

        public static final Creator<MessageDeleted> CREATOR = new Creator<MessageDeleted>() {

            @Override public MessageDeleted createFromParcel(Parcel in) {
                MessageDeleted data = new MessageDeleted();

        
                return data;
            }

            @Override public MessageDeleted[] newArray(int size) {
                return new MessageDeleted[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public MessageDeleted() {
        }

        public static final int CONSTRUCTOR = 2145503191;

        @Override
        public int getConstructor() {
            return 2145503191;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageDeleted").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class MessageUnsupported extends MessageContent {

        public static final Creator<MessageUnsupported> CREATOR = new Creator<MessageUnsupported>() {

            @Override public MessageUnsupported createFromParcel(Parcel in) {
                MessageUnsupported data = new MessageUnsupported();

        
                return data;
            }

            @Override public MessageUnsupported[] newArray(int size) {
                return new MessageUnsupported[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public MessageUnsupported() {
        }

        public static final int CONSTRUCTOR = -1816726139;

        @Override
        public int getConstructor() {
            return -1816726139;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("MessageUnsupported").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Messages extends TLObject {
        public int totalCount;
        public Message[] messages;

        public static final Creator<Messages> CREATOR = new Creator<Messages>() {

            @Override public Messages createFromParcel(Parcel in) {
                Messages data = new Messages();

                data.totalCount = in.readInt();
                data.messages = in.createTypedArray(Message.CREATOR);

                return data;
            }

            @Override public Messages[] newArray(int size) {
                return new Messages[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(totalCount);
            dest.writeTypedArray(this.messages, flags);
        }
	
        public Messages() {
        }

        public Messages(int totalCount, Message[] messages) {
            this.totalCount = totalCount;
            this.messages = messages;
        }

        public static final int CONSTRUCTOR = 1550441659;

        @Override
        public int getConstructor() {
            return 1550441659;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Messages").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("totalCount").append(" = ").append(totalCount).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messages").append(" = ").append("Message[]").append(" {").append(Arrays.toString(messages)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class NotificationSettings extends TLObject {
        public int muteFor;
        public String sound;
        public boolean showPreviews;
        public int eventsMask;

        public static final Creator<NotificationSettings> CREATOR = new Creator<NotificationSettings>() {

            @Override public NotificationSettings createFromParcel(Parcel in) {
                NotificationSettings data = new NotificationSettings();

                data.muteFor = in.readInt();
                data.sound = in.readString();
                data.showPreviews = in.readInt() != 0;
                data.eventsMask = in.readInt();

                return data;
            }

            @Override public NotificationSettings[] newArray(int size) {
                return new NotificationSettings[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(muteFor);
            dest.writeString(sound);
            dest.writeInt(this.showPreviews ? 1 : 0);
            dest.writeInt(eventsMask);
        }
	
        public NotificationSettings() {
        }

        public NotificationSettings(int muteFor, String sound, boolean showPreviews, int eventsMask) {
            this.muteFor = muteFor;
            this.sound = sound;
            this.showPreviews = showPreviews;
            this.eventsMask = eventsMask;
        }

        public static final int CONSTRUCTOR = 826646433;

        @Override
        public int getConstructor() {
            return 826646433;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("NotificationSettings").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("muteFor").append(" = ").append(muteFor).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("sound").append(" = ").append(sound).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("showPreviews").append(" = ").append(showPreviews).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("eventsMask").append(" = ").append(eventsMask).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class NotificationSettingsScope extends TLObject {
    }

    public static class NotificationSettingsForChat extends NotificationSettingsScope {
        public long chatId;

        public static final Creator<NotificationSettingsForChat> CREATOR = new Creator<NotificationSettingsForChat>() {

            @Override public NotificationSettingsForChat createFromParcel(Parcel in) {
                NotificationSettingsForChat data = new NotificationSettingsForChat();

                data.chatId = in.readLong();

                return data;
            }

            @Override public NotificationSettingsForChat[] newArray(int size) {
                return new NotificationSettingsForChat[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
        }
	
        public NotificationSettingsForChat() {
        }

        public NotificationSettingsForChat(long chatId) {
            this.chatId = chatId;
        }

        public static final int CONSTRUCTOR = 1920084409;

        @Override
        public int getConstructor() {
            return 1920084409;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("NotificationSettingsForChat").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class NotificationSettingsForPrivateChats extends NotificationSettingsScope {

        public static final Creator<NotificationSettingsForPrivateChats> CREATOR = new Creator<NotificationSettingsForPrivateChats>() {

            @Override public NotificationSettingsForPrivateChats createFromParcel(Parcel in) {
                NotificationSettingsForPrivateChats data = new NotificationSettingsForPrivateChats();

        
                return data;
            }

            @Override public NotificationSettingsForPrivateChats[] newArray(int size) {
                return new NotificationSettingsForPrivateChats[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public NotificationSettingsForPrivateChats() {
        }

        public static final int CONSTRUCTOR = 792026226;

        @Override
        public int getConstructor() {
            return 792026226;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("NotificationSettingsForPrivateChats").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class NotificationSettingsForGroupChats extends NotificationSettingsScope {

        public static final Creator<NotificationSettingsForGroupChats> CREATOR = new Creator<NotificationSettingsForGroupChats>() {

            @Override public NotificationSettingsForGroupChats createFromParcel(Parcel in) {
                NotificationSettingsForGroupChats data = new NotificationSettingsForGroupChats();

        
                return data;
            }

            @Override public NotificationSettingsForGroupChats[] newArray(int size) {
                return new NotificationSettingsForGroupChats[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public NotificationSettingsForGroupChats() {
        }

        public static final int CONSTRUCTOR = -1019160145;

        @Override
        public int getConstructor() {
            return -1019160145;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("NotificationSettingsForGroupChats").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class NotificationSettingsForAllChats extends NotificationSettingsScope {

        public static final Creator<NotificationSettingsForAllChats> CREATOR = new Creator<NotificationSettingsForAllChats>() {

            @Override public NotificationSettingsForAllChats createFromParcel(Parcel in) {
                NotificationSettingsForAllChats data = new NotificationSettingsForAllChats();

        
                return data;
            }

            @Override public NotificationSettingsForAllChats[] newArray(int size) {
                return new NotificationSettingsForAllChats[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public NotificationSettingsForAllChats() {
        }

        public static final int CONSTRUCTOR = 2121050176;

        @Override
        public int getConstructor() {
            return 2121050176;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("NotificationSettingsForAllChats").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Ok extends TLObject {

        public static final Creator<Ok> CREATOR = new Creator<Ok>() {

            @Override public Ok createFromParcel(Parcel in) {
                Ok data = new Ok();

        
                return data;
            }

            @Override public Ok[] newArray(int size) {
                return new Ok[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public Ok() {
        }

        public static final int CONSTRUCTOR = -722616727;

        @Override
        public int getConstructor() {
            return -722616727;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Ok").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class OptionValue extends TLObject {
    }

    public static class OptionBoolean extends OptionValue {
        public boolean value;

        public static final Creator<OptionBoolean> CREATOR = new Creator<OptionBoolean>() {

            @Override public OptionBoolean createFromParcel(Parcel in) {
                OptionBoolean data = new OptionBoolean();

                data.value = in.readInt() != 0;

                return data;
            }

            @Override public OptionBoolean[] newArray(int size) {
                return new OptionBoolean[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.value ? 1 : 0);
        }
	
        public OptionBoolean() {
        }

        public OptionBoolean(boolean value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = 280624660;

        @Override
        public int getConstructor() {
            return 280624660;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("OptionBoolean").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append(value).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class OptionEmpty extends OptionValue {

        public static final Creator<OptionEmpty> CREATOR = new Creator<OptionEmpty>() {

            @Override public OptionEmpty createFromParcel(Parcel in) {
                OptionEmpty data = new OptionEmpty();

        
                return data;
            }

            @Override public OptionEmpty[] newArray(int size) {
                return new OptionEmpty[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public OptionEmpty() {
        }

        public static final int CONSTRUCTOR = 1025799436;

        @Override
        public int getConstructor() {
            return 1025799436;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("OptionEmpty").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class OptionInteger extends OptionValue {
        public int value;

        public static final Creator<OptionInteger> CREATOR = new Creator<OptionInteger>() {

            @Override public OptionInteger createFromParcel(Parcel in) {
                OptionInteger data = new OptionInteger();

                data.value = in.readInt();

                return data;
            }

            @Override public OptionInteger[] newArray(int size) {
                return new OptionInteger[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(value);
        }
	
        public OptionInteger() {
        }

        public OptionInteger(int value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = 1383938450;

        @Override
        public int getConstructor() {
            return 1383938450;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("OptionInteger").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append(value).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class OptionString extends OptionValue {
        public String value;

        public static final Creator<OptionString> CREATOR = new Creator<OptionString>() {

            @Override public OptionString createFromParcel(Parcel in) {
                OptionString data = new OptionString();

                data.value = in.readString();

                return data;
            }

            @Override public OptionString[] newArray(int size) {
                return new OptionString[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(value);
        }
	
        public OptionString() {
        }

        public OptionString(String value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = -841614037;

        @Override
        public int getConstructor() {
            return -841614037;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("OptionString").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append(value).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Photo extends TLObject {
        public long id;
        public PhotoSize[] photos;

        public static final Creator<Photo> CREATOR = new Creator<Photo>() {

            @Override public Photo createFromParcel(Parcel in) {
                Photo data = new Photo();

                data.id = in.readLong();
                data.photos = in.createTypedArray(PhotoSize.CREATOR);

                return data;
            }

            @Override public Photo[] newArray(int size) {
                return new Photo[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(id);
            dest.writeTypedArray(this.photos, flags);
        }
	
        public Photo() {
        }

        public Photo(long id, PhotoSize[] photos) {
            this.id = id;
            this.photos = photos;
        }

        public static final int CONSTRUCTOR = 700401344;

        @Override
        public int getConstructor() {
            return 700401344;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Photo").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photos").append(" = ").append("PhotoSize[]").append(" {").append(Arrays.toString(photos)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class PhotoSize extends TLObject {
        public String type;
        public File photo;
        public int width;
        public int height;

        public static final Creator<PhotoSize> CREATOR = new Creator<PhotoSize>() {

            @Override public PhotoSize createFromParcel(Parcel in) {
                PhotoSize data = new PhotoSize();

                data.type = in.readString();
                data.photo = in.readParcelable(File.class.getClassLoader());
                data.width = in.readInt();
                data.height = in.readInt();

                return data;
            }

            @Override public PhotoSize[] newArray(int size) {
                return new PhotoSize[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(type);
            dest.writeParcelable(this.photo, flags);
            dest.writeInt(width);
            dest.writeInt(height);
        }
	
        public PhotoSize() {
        }

        public PhotoSize(String type, File photo, int width, int height) {
            this.type = type;
            this.photo = photo;
            this.width = width;
            this.height = height;
        }

        public static final int CONSTRUCTOR = -796190918;

        @Override
        public int getConstructor() {
            return -796190918;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("PhotoSize").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("type").append(" = ").append(type).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photo").append(" = "); photo.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("width").append(" = ").append(width).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("height").append(" = ").append(height).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ProfilePhoto extends TLObject {
        public long id;
        public File small;
        public File big;

        public static final Creator<ProfilePhoto> CREATOR = new Creator<ProfilePhoto>() {

            @Override public ProfilePhoto createFromParcel(Parcel in) {
                ProfilePhoto data = new ProfilePhoto();

                data.id = in.readLong();
                data.small = in.readParcelable(File.class.getClassLoader());
                data.big = in.readParcelable(File.class.getClassLoader());

                return data;
            }

            @Override public ProfilePhoto[] newArray(int size) {
                return new ProfilePhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(id);
            dest.writeParcelable(this.small, flags);
            dest.writeParcelable(this.big, flags);
        }
	
        public ProfilePhoto() {
        }

        public ProfilePhoto(long id, File small, File big) {
            this.id = id;
            this.small = small;
            this.big = big;
        }

        public static final int CONSTRUCTOR = -1954106867;

        @Override
        public int getConstructor() {
            return -1954106867;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ProfilePhoto").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("small").append(" = "); small.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("big").append(" = "); big.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ProfilePhotoCrop extends TLObject {
        public double left;
        public double top;
        public double width;

        public static final Creator<ProfilePhotoCrop> CREATOR = new Creator<ProfilePhotoCrop>() {

            @Override public ProfilePhotoCrop createFromParcel(Parcel in) {
                ProfilePhotoCrop data = new ProfilePhotoCrop();

                data.left = in.readDouble();
                data.top = in.readDouble();
                data.width = in.readDouble();

                return data;
            }

            @Override public ProfilePhotoCrop[] newArray(int size) {
                return new ProfilePhotoCrop[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeDouble(left);
            dest.writeDouble(top);
            dest.writeDouble(width);
        }
	
        public ProfilePhotoCrop() {
        }

        public ProfilePhotoCrop(double left, double top, double width) {
            this.left = left;
            this.top = top;
            this.width = width;
        }

        public static final int CONSTRUCTOR = 800473724;

        @Override
        public int getConstructor() {
            return 800473724;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ProfilePhotoCrop").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("left").append(" = ").append(left).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("top").append(" = ").append(top).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("width").append(" = ").append(width).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class ReplyMarkup extends TLObject {
    }

    public static class ReplyMarkupNone extends ReplyMarkup {

        public static final Creator<ReplyMarkupNone> CREATOR = new Creator<ReplyMarkupNone>() {

            @Override public ReplyMarkupNone createFromParcel(Parcel in) {
                ReplyMarkupNone data = new ReplyMarkupNone();

        
                return data;
            }

            @Override public ReplyMarkupNone[] newArray(int size) {
                return new ReplyMarkupNone[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public ReplyMarkupNone() {
        }

        public static final int CONSTRUCTOR = -1623666456;

        @Override
        public int getConstructor() {
            return -1623666456;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ReplyMarkupNone").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ReplyMarkupHideKeyboard extends ReplyMarkup {
        public boolean personal;

        public static final Creator<ReplyMarkupHideKeyboard> CREATOR = new Creator<ReplyMarkupHideKeyboard>() {

            @Override public ReplyMarkupHideKeyboard createFromParcel(Parcel in) {
                ReplyMarkupHideKeyboard data = new ReplyMarkupHideKeyboard();

                data.personal = in.readInt() != 0;

                return data;
            }

            @Override public ReplyMarkupHideKeyboard[] newArray(int size) {
                return new ReplyMarkupHideKeyboard[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.personal ? 1 : 0);
        }
	
        public ReplyMarkupHideKeyboard() {
        }

        public ReplyMarkupHideKeyboard(boolean personal) {
            this.personal = personal;
        }

        public static final int CONSTRUCTOR = 1614435429;

        @Override
        public int getConstructor() {
            return 1614435429;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ReplyMarkupHideKeyboard").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("personal").append(" = ").append(personal).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ReplyMarkupForceReply extends ReplyMarkup {
        public boolean personal;

        public static final Creator<ReplyMarkupForceReply> CREATOR = new Creator<ReplyMarkupForceReply>() {

            @Override public ReplyMarkupForceReply createFromParcel(Parcel in) {
                ReplyMarkupForceReply data = new ReplyMarkupForceReply();

                data.personal = in.readInt() != 0;

                return data;
            }

            @Override public ReplyMarkupForceReply[] newArray(int size) {
                return new ReplyMarkupForceReply[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.personal ? 1 : 0);
        }
	
        public ReplyMarkupForceReply() {
        }

        public ReplyMarkupForceReply(boolean personal) {
            this.personal = personal;
        }

        public static final int CONSTRUCTOR = -1880611604;

        @Override
        public int getConstructor() {
            return -1880611604;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ReplyMarkupForceReply").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("personal").append(" = ").append(personal).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ReplyMarkupShowKeyboard extends ReplyMarkup {
        public String[][] rows;
        public boolean resizeKeyboard;
        public boolean oneTime;
        public boolean personal;

        public static final Creator<ReplyMarkupShowKeyboard> CREATOR = new Creator<ReplyMarkupShowKeyboard>() {

            @Override public ReplyMarkupShowKeyboard createFromParcel(Parcel in) {
                ReplyMarkupShowKeyboard data = new ReplyMarkupShowKeyboard();

                data.rows = new String[in.readInt()][]; for (int i = 0; i < data.rows.length; i++) data.rows[i] = in.createStringArray();
                data.resizeKeyboard = in.readInt() != 0;
                data.oneTime = in.readInt() != 0;
                data.personal = in.readInt() != 0;

                return data;
            }

            @Override public ReplyMarkupShowKeyboard[] newArray(int size) {
                return new ReplyMarkupShowKeyboard[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.rows.length); for (String[] arr : this.rows) dest.writeStringArray(arr);
            dest.writeInt(this.resizeKeyboard ? 1 : 0);
            dest.writeInt(this.oneTime ? 1 : 0);
            dest.writeInt(this.personal ? 1 : 0);
        }
	
        public ReplyMarkupShowKeyboard() {
        }

        public ReplyMarkupShowKeyboard(String[][] rows, boolean resizeKeyboard, boolean oneTime, boolean personal) {
            this.rows = rows;
            this.resizeKeyboard = resizeKeyboard;
            this.oneTime = oneTime;
            this.personal = personal;
        }

        public static final int CONSTRUCTOR = 22090330;

        @Override
        public int getConstructor() {
            return 22090330;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ReplyMarkupShowKeyboard").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("rows").append(" = ").append("String[][]").append(" {").append(Arrays.deepToString(rows)).append("}\n");
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("resizeKeyboard").append(" = ").append(resizeKeyboard).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("oneTime").append(" = ").append(oneTime).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("personal").append(" = ").append(personal).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class SearchMessagesFilter extends TLObject {
    }

    public static class SearchMessagesFilterEmpty extends SearchMessagesFilter {

        public static final Creator<SearchMessagesFilterEmpty> CREATOR = new Creator<SearchMessagesFilterEmpty>() {

            @Override public SearchMessagesFilterEmpty createFromParcel(Parcel in) {
                SearchMessagesFilterEmpty data = new SearchMessagesFilterEmpty();

        
                return data;
            }

            @Override public SearchMessagesFilterEmpty[] newArray(int size) {
                return new SearchMessagesFilterEmpty[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SearchMessagesFilterEmpty() {
        }

        public static final int CONSTRUCTOR = -869395657;

        @Override
        public int getConstructor() {
            return -869395657;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchMessagesFilterEmpty").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchMessagesFilterAudio extends SearchMessagesFilter {

        public static final Creator<SearchMessagesFilterAudio> CREATOR = new Creator<SearchMessagesFilterAudio>() {

            @Override public SearchMessagesFilterAudio createFromParcel(Parcel in) {
                SearchMessagesFilterAudio data = new SearchMessagesFilterAudio();

        
                return data;
            }

            @Override public SearchMessagesFilterAudio[] newArray(int size) {
                return new SearchMessagesFilterAudio[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SearchMessagesFilterAudio() {
        }

        public static final int CONSTRUCTOR = 867505275;

        @Override
        public int getConstructor() {
            return 867505275;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchMessagesFilterAudio").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchMessagesFilterDocument extends SearchMessagesFilter {

        public static final Creator<SearchMessagesFilterDocument> CREATOR = new Creator<SearchMessagesFilterDocument>() {

            @Override public SearchMessagesFilterDocument createFromParcel(Parcel in) {
                SearchMessagesFilterDocument data = new SearchMessagesFilterDocument();

        
                return data;
            }

            @Override public SearchMessagesFilterDocument[] newArray(int size) {
                return new SearchMessagesFilterDocument[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SearchMessagesFilterDocument() {
        }

        public static final int CONSTRUCTOR = 1526331215;

        @Override
        public int getConstructor() {
            return 1526331215;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchMessagesFilterDocument").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchMessagesFilterPhoto extends SearchMessagesFilter {

        public static final Creator<SearchMessagesFilterPhoto> CREATOR = new Creator<SearchMessagesFilterPhoto>() {

            @Override public SearchMessagesFilterPhoto createFromParcel(Parcel in) {
                SearchMessagesFilterPhoto data = new SearchMessagesFilterPhoto();

        
                return data;
            }

            @Override public SearchMessagesFilterPhoto[] newArray(int size) {
                return new SearchMessagesFilterPhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SearchMessagesFilterPhoto() {
        }

        public static final int CONSTRUCTOR = 925932293;

        @Override
        public int getConstructor() {
            return 925932293;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchMessagesFilterPhoto").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchMessagesFilterVideo extends SearchMessagesFilter {

        public static final Creator<SearchMessagesFilterVideo> CREATOR = new Creator<SearchMessagesFilterVideo>() {

            @Override public SearchMessagesFilterVideo createFromParcel(Parcel in) {
                SearchMessagesFilterVideo data = new SearchMessagesFilterVideo();

        
                return data;
            }

            @Override public SearchMessagesFilterVideo[] newArray(int size) {
                return new SearchMessagesFilterVideo[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SearchMessagesFilterVideo() {
        }

        public static final int CONSTRUCTOR = 115538222;

        @Override
        public int getConstructor() {
            return 115538222;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchMessagesFilterVideo").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchMessagesFilterVoice extends SearchMessagesFilter {

        public static final Creator<SearchMessagesFilterVoice> CREATOR = new Creator<SearchMessagesFilterVoice>() {

            @Override public SearchMessagesFilterVoice createFromParcel(Parcel in) {
                SearchMessagesFilterVoice data = new SearchMessagesFilterVoice();

        
                return data;
            }

            @Override public SearchMessagesFilterVoice[] newArray(int size) {
                return new SearchMessagesFilterVoice[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SearchMessagesFilterVoice() {
        }

        public static final int CONSTRUCTOR = 1123427595;

        @Override
        public int getConstructor() {
            return 1123427595;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchMessagesFilterVoice").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchMessagesFilterPhotoAndVideo extends SearchMessagesFilter {

        public static final Creator<SearchMessagesFilterPhotoAndVideo> CREATOR = new Creator<SearchMessagesFilterPhotoAndVideo>() {

            @Override public SearchMessagesFilterPhotoAndVideo createFromParcel(Parcel in) {
                SearchMessagesFilterPhotoAndVideo data = new SearchMessagesFilterPhotoAndVideo();

        
                return data;
            }

            @Override public SearchMessagesFilterPhotoAndVideo[] newArray(int size) {
                return new SearchMessagesFilterPhotoAndVideo[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SearchMessagesFilterPhotoAndVideo() {
        }

        public static final int CONSTRUCTOR = 1352130963;

        @Override
        public int getConstructor() {
            return 1352130963;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchMessagesFilterPhotoAndVideo").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchMessagesFilterUrl extends SearchMessagesFilter {

        public static final Creator<SearchMessagesFilterUrl> CREATOR = new Creator<SearchMessagesFilterUrl>() {

            @Override public SearchMessagesFilterUrl createFromParcel(Parcel in) {
                SearchMessagesFilterUrl data = new SearchMessagesFilterUrl();

        
                return data;
            }

            @Override public SearchMessagesFilterUrl[] newArray(int size) {
                return new SearchMessagesFilterUrl[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SearchMessagesFilterUrl() {
        }

        public static final int CONSTRUCTOR = -1828724341;

        @Override
        public int getConstructor() {
            return -1828724341;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchMessagesFilterUrl").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class SendMessageAction extends TLObject {
    }

    public static class SendMessageTypingAction extends SendMessageAction {

        public static final Creator<SendMessageTypingAction> CREATOR = new Creator<SendMessageTypingAction>() {

            @Override public SendMessageTypingAction createFromParcel(Parcel in) {
                SendMessageTypingAction data = new SendMessageTypingAction();

        
                return data;
            }

            @Override public SendMessageTypingAction[] newArray(int size) {
                return new SendMessageTypingAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SendMessageTypingAction() {
        }

        public static final int CONSTRUCTOR = 381645902;

        @Override
        public int getConstructor() {
            return 381645902;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageTypingAction").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessageCancelAction extends SendMessageAction {

        public static final Creator<SendMessageCancelAction> CREATOR = new Creator<SendMessageCancelAction>() {

            @Override public SendMessageCancelAction createFromParcel(Parcel in) {
                SendMessageCancelAction data = new SendMessageCancelAction();

        
                return data;
            }

            @Override public SendMessageCancelAction[] newArray(int size) {
                return new SendMessageCancelAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SendMessageCancelAction() {
        }

        public static final int CONSTRUCTOR = -44119819;

        @Override
        public int getConstructor() {
            return -44119819;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageCancelAction").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessageRecordVideoAction extends SendMessageAction {

        public static final Creator<SendMessageRecordVideoAction> CREATOR = new Creator<SendMessageRecordVideoAction>() {

            @Override public SendMessageRecordVideoAction createFromParcel(Parcel in) {
                SendMessageRecordVideoAction data = new SendMessageRecordVideoAction();

        
                return data;
            }

            @Override public SendMessageRecordVideoAction[] newArray(int size) {
                return new SendMessageRecordVideoAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SendMessageRecordVideoAction() {
        }

        public static final int CONSTRUCTOR = -1584933265;

        @Override
        public int getConstructor() {
            return -1584933265;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageRecordVideoAction").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessageUploadVideoAction extends SendMessageAction {
        public int progress;

        public static final Creator<SendMessageUploadVideoAction> CREATOR = new Creator<SendMessageUploadVideoAction>() {

            @Override public SendMessageUploadVideoAction createFromParcel(Parcel in) {
                SendMessageUploadVideoAction data = new SendMessageUploadVideoAction();

                data.progress = in.readInt();

                return data;
            }

            @Override public SendMessageUploadVideoAction[] newArray(int size) {
                return new SendMessageUploadVideoAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(progress);
        }
	
        public SendMessageUploadVideoAction() {
        }

        public SendMessageUploadVideoAction(int progress) {
            this.progress = progress;
        }

        public static final int CONSTRUCTOR = -378127636;

        @Override
        public int getConstructor() {
            return -378127636;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageUploadVideoAction").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("progress").append(" = ").append(progress).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessageRecordVoiceAction extends SendMessageAction {

        public static final Creator<SendMessageRecordVoiceAction> CREATOR = new Creator<SendMessageRecordVoiceAction>() {

            @Override public SendMessageRecordVoiceAction createFromParcel(Parcel in) {
                SendMessageRecordVoiceAction data = new SendMessageRecordVoiceAction();

        
                return data;
            }

            @Override public SendMessageRecordVoiceAction[] newArray(int size) {
                return new SendMessageRecordVoiceAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SendMessageRecordVoiceAction() {
        }

        public static final int CONSTRUCTOR = -1470755762;

        @Override
        public int getConstructor() {
            return -1470755762;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageRecordVoiceAction").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessageUploadVoiceAction extends SendMessageAction {
        public int progress;

        public static final Creator<SendMessageUploadVoiceAction> CREATOR = new Creator<SendMessageUploadVoiceAction>() {

            @Override public SendMessageUploadVoiceAction createFromParcel(Parcel in) {
                SendMessageUploadVoiceAction data = new SendMessageUploadVoiceAction();

                data.progress = in.readInt();

                return data;
            }

            @Override public SendMessageUploadVoiceAction[] newArray(int size) {
                return new SendMessageUploadVoiceAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(progress);
        }
	
        public SendMessageUploadVoiceAction() {
        }

        public SendMessageUploadVoiceAction(int progress) {
            this.progress = progress;
        }

        public static final int CONSTRUCTOR = 64055712;

        @Override
        public int getConstructor() {
            return 64055712;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageUploadVoiceAction").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("progress").append(" = ").append(progress).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessageUploadPhotoAction extends SendMessageAction {
        public int progress;

        public static final Creator<SendMessageUploadPhotoAction> CREATOR = new Creator<SendMessageUploadPhotoAction>() {

            @Override public SendMessageUploadPhotoAction createFromParcel(Parcel in) {
                SendMessageUploadPhotoAction data = new SendMessageUploadPhotoAction();

                data.progress = in.readInt();

                return data;
            }

            @Override public SendMessageUploadPhotoAction[] newArray(int size) {
                return new SendMessageUploadPhotoAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(progress);
        }
	
        public SendMessageUploadPhotoAction() {
        }

        public SendMessageUploadPhotoAction(int progress) {
            this.progress = progress;
        }

        public static final int CONSTRUCTOR = -774682074;

        @Override
        public int getConstructor() {
            return -774682074;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageUploadPhotoAction").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("progress").append(" = ").append(progress).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessageUploadDocumentAction extends SendMessageAction {
        public int progress;

        public static final Creator<SendMessageUploadDocumentAction> CREATOR = new Creator<SendMessageUploadDocumentAction>() {

            @Override public SendMessageUploadDocumentAction createFromParcel(Parcel in) {
                SendMessageUploadDocumentAction data = new SendMessageUploadDocumentAction();

                data.progress = in.readInt();

                return data;
            }

            @Override public SendMessageUploadDocumentAction[] newArray(int size) {
                return new SendMessageUploadDocumentAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(progress);
        }
	
        public SendMessageUploadDocumentAction() {
        }

        public SendMessageUploadDocumentAction(int progress) {
            this.progress = progress;
        }

        public static final int CONSTRUCTOR = -1441998364;

        @Override
        public int getConstructor() {
            return -1441998364;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageUploadDocumentAction").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("progress").append(" = ").append(progress).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessageGeoLocationAction extends SendMessageAction {

        public static final Creator<SendMessageGeoLocationAction> CREATOR = new Creator<SendMessageGeoLocationAction>() {

            @Override public SendMessageGeoLocationAction createFromParcel(Parcel in) {
                SendMessageGeoLocationAction data = new SendMessageGeoLocationAction();

        
                return data;
            }

            @Override public SendMessageGeoLocationAction[] newArray(int size) {
                return new SendMessageGeoLocationAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SendMessageGeoLocationAction() {
        }

        public static final int CONSTRUCTOR = 393186209;

        @Override
        public int getConstructor() {
            return 393186209;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageGeoLocationAction").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessageChooseContactAction extends SendMessageAction {

        public static final Creator<SendMessageChooseContactAction> CREATOR = new Creator<SendMessageChooseContactAction>() {

            @Override public SendMessageChooseContactAction createFromParcel(Parcel in) {
                SendMessageChooseContactAction data = new SendMessageChooseContactAction();

        
                return data;
            }

            @Override public SendMessageChooseContactAction[] newArray(int size) {
                return new SendMessageChooseContactAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public SendMessageChooseContactAction() {
        }

        public static final int CONSTRUCTOR = 1653390447;

        @Override
        public int getConstructor() {
            return 1653390447;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessageChooseContactAction").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Sticker extends TLObject {
        public long setId;
        public int width;
        public int height;
        public String emoji;
        public double rating;
        public PhotoSize thumb;
        public File sticker;

        public static final Creator<Sticker> CREATOR = new Creator<Sticker>() {

            @Override public Sticker createFromParcel(Parcel in) {
                Sticker data = new Sticker();

                data.setId = in.readLong();
                data.width = in.readInt();
                data.height = in.readInt();
                data.emoji = in.readString();
                data.rating = in.readDouble();
                data.thumb = in.readParcelable(PhotoSize.class.getClassLoader());
                data.sticker = in.readParcelable(File.class.getClassLoader());

                return data;
            }

            @Override public Sticker[] newArray(int size) {
                return new Sticker[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(setId);
            dest.writeInt(width);
            dest.writeInt(height);
            dest.writeString(emoji);
            dest.writeDouble(rating);
            dest.writeParcelable(this.thumb, flags);
            dest.writeParcelable(this.sticker, flags);
        }
	
        public Sticker() {
        }

        public Sticker(long setId, int width, int height, String emoji, double rating, PhotoSize thumb, File sticker) {
            this.setId = setId;
            this.width = width;
            this.height = height;
            this.emoji = emoji;
            this.rating = rating;
            this.thumb = thumb;
            this.sticker = sticker;
        }

        public static final int CONSTRUCTOR = -319806232;

        @Override
        public int getConstructor() {
            return -319806232;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Sticker").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("setId").append(" = ").append(setId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("width").append(" = ").append(width).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("height").append(" = ").append(height).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("emoji").append(" = ").append(emoji).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("rating").append(" = ").append(rating).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("thumb").append(" = "); thumb.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("sticker").append(" = "); sticker.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class StickerSet extends TLObject {
        public long id;
        public String title;
        public String name;
        public double rating;
        public boolean isInstalled;
        public boolean isEnabled;
        public boolean isOfficial;
        public Sticker[] stickers;

        public static final Creator<StickerSet> CREATOR = new Creator<StickerSet>() {

            @Override public StickerSet createFromParcel(Parcel in) {
                StickerSet data = new StickerSet();

                data.id = in.readLong();
                data.title = in.readString();
                data.name = in.readString();
                data.rating = in.readDouble();
                data.isInstalled = in.readInt() != 0;
                data.isEnabled = in.readInt() != 0;
                data.isOfficial = in.readInt() != 0;
                data.stickers = in.createTypedArray(Sticker.CREATOR);

                return data;
            }

            @Override public StickerSet[] newArray(int size) {
                return new StickerSet[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(id);
            dest.writeString(title);
            dest.writeString(name);
            dest.writeDouble(rating);
            dest.writeInt(this.isInstalled ? 1 : 0);
            dest.writeInt(this.isEnabled ? 1 : 0);
            dest.writeInt(this.isOfficial ? 1 : 0);
            dest.writeTypedArray(this.stickers, flags);
        }
	
        public StickerSet() {
        }

        public StickerSet(long id, String title, String name, double rating, boolean isInstalled, boolean isEnabled, boolean isOfficial, Sticker[] stickers) {
            this.id = id;
            this.title = title;
            this.name = name;
            this.rating = rating;
            this.isInstalled = isInstalled;
            this.isEnabled = isEnabled;
            this.isOfficial = isOfficial;
            this.stickers = stickers;
        }

        public static final int CONSTRUCTOR = 1942998252;

        @Override
        public int getConstructor() {
            return 1942998252;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("StickerSet").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("name").append(" = ").append(name).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("rating").append(" = ").append(rating).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isInstalled").append(" = ").append(isInstalled).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isEnabled").append(" = ").append(isEnabled).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isOfficial").append(" = ").append(isOfficial).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("stickers").append(" = ").append("Sticker[]").append(" {").append(Arrays.toString(stickers)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class StickerSetInfo extends TLObject {
        public long id;
        public String title;
        public String name;
        public double rating;
        public boolean isInstalled;
        public boolean isEnabled;
        public boolean isOfficial;
        public int size;

        public static final Creator<StickerSetInfo> CREATOR = new Creator<StickerSetInfo>() {

            @Override public StickerSetInfo createFromParcel(Parcel in) {
                StickerSetInfo data = new StickerSetInfo();

                data.id = in.readLong();
                data.title = in.readString();
                data.name = in.readString();
                data.rating = in.readDouble();
                data.isInstalled = in.readInt() != 0;
                data.isEnabled = in.readInt() != 0;
                data.isOfficial = in.readInt() != 0;
                data.size = in.readInt();

                return data;
            }

            @Override public StickerSetInfo[] newArray(int size) {
                return new StickerSetInfo[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(id);
            dest.writeString(title);
            dest.writeString(name);
            dest.writeDouble(rating);
            dest.writeInt(this.isInstalled ? 1 : 0);
            dest.writeInt(this.isEnabled ? 1 : 0);
            dest.writeInt(this.isOfficial ? 1 : 0);
            dest.writeInt(size);
        }
	
        public StickerSetInfo() {
        }

        public StickerSetInfo(long id, String title, String name, double rating, boolean isInstalled, boolean isEnabled, boolean isOfficial, int size) {
            this.id = id;
            this.title = title;
            this.name = name;
            this.rating = rating;
            this.isInstalled = isInstalled;
            this.isEnabled = isEnabled;
            this.isOfficial = isOfficial;
            this.size = size;
        }

        public static final int CONSTRUCTOR = -1268445223;

        @Override
        public int getConstructor() {
            return -1268445223;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("StickerSetInfo").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("name").append(" = ").append(name).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("rating").append(" = ").append(rating).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isInstalled").append(" = ").append(isInstalled).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isEnabled").append(" = ").append(isEnabled).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isOfficial").append(" = ").append(isOfficial).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("size").append(" = ").append(size).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class StickerSets extends TLObject {
        public StickerSetInfo[] sets;

        public static final Creator<StickerSets> CREATOR = new Creator<StickerSets>() {

            @Override public StickerSets createFromParcel(Parcel in) {
                StickerSets data = new StickerSets();

                data.sets = in.createTypedArray(StickerSetInfo.CREATOR);

                return data;
            }

            @Override public StickerSets[] newArray(int size) {
                return new StickerSets[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedArray(this.sets, flags);
        }
	
        public StickerSets() {
        }

        public StickerSets(StickerSetInfo[] sets) {
            this.sets = sets;
        }

        public static final int CONSTRUCTOR = -1141691090;

        @Override
        public int getConstructor() {
            return -1141691090;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("StickerSets").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("sets").append(" = ").append("StickerSetInfo[]").append(" {").append(Arrays.toString(sets)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Stickers extends TLObject {
        public Sticker[] stickers;

        public static final Creator<Stickers> CREATOR = new Creator<Stickers>() {

            @Override public Stickers createFromParcel(Parcel in) {
                Stickers data = new Stickers();

                data.stickers = in.createTypedArray(Sticker.CREATOR);

                return data;
            }

            @Override public Stickers[] newArray(int size) {
                return new Stickers[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedArray(this.stickers, flags);
        }
	
        public Stickers() {
        }

        public Stickers(Sticker[] stickers) {
            this.stickers = stickers;
        }

        public static final int CONSTRUCTOR = 1974859260;

        @Override
        public int getConstructor() {
            return 1974859260;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Stickers").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("stickers").append(" = ").append("Sticker[]").append(" {").append(Arrays.toString(stickers)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class Update extends TLObject {
    }

    public static class UpdateNewMessage extends Update {
        public Message message;

        public static final Creator<UpdateNewMessage> CREATOR = new Creator<UpdateNewMessage>() {

            @Override public UpdateNewMessage createFromParcel(Parcel in) {
                UpdateNewMessage data = new UpdateNewMessage();

                data.message = in.readParcelable(Message.class.getClassLoader());

                return data;
            }

            @Override public UpdateNewMessage[] newArray(int size) {
                return new UpdateNewMessage[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.message, flags);
        }
	
        public UpdateNewMessage() {
        }

        public UpdateNewMessage(Message message) {
            this.message = message;
        }

        public static final int CONSTRUCTOR = -563105266;

        @Override
        public int getConstructor() {
            return -563105266;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateNewMessage").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("message").append(" = "); message.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateMessageId extends Update {
        public long chatId;
        public int oldId;
        public int newId;

        public static final Creator<UpdateMessageId> CREATOR = new Creator<UpdateMessageId>() {

            @Override public UpdateMessageId createFromParcel(Parcel in) {
                UpdateMessageId data = new UpdateMessageId();

                data.chatId = in.readLong();
                data.oldId = in.readInt();
                data.newId = in.readInt();

                return data;
            }

            @Override public UpdateMessageId[] newArray(int size) {
                return new UpdateMessageId[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(oldId);
            dest.writeInt(newId);
        }
	
        public UpdateMessageId() {
        }

        public UpdateMessageId(long chatId, int oldId, int newId) {
            this.chatId = chatId;
            this.oldId = oldId;
            this.newId = newId;
        }

        public static final int CONSTRUCTOR = 1840811241;

        @Override
        public int getConstructor() {
            return 1840811241;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateMessageId").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("oldId").append(" = ").append(oldId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("newId").append(" = ").append(newId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateMessageDate extends Update {
        public long chatId;
        public int messageId;
        public int newDate;

        public static final Creator<UpdateMessageDate> CREATOR = new Creator<UpdateMessageDate>() {

            @Override public UpdateMessageDate createFromParcel(Parcel in) {
                UpdateMessageDate data = new UpdateMessageDate();

                data.chatId = in.readLong();
                data.messageId = in.readInt();
                data.newDate = in.readInt();

                return data;
            }

            @Override public UpdateMessageDate[] newArray(int size) {
                return new UpdateMessageDate[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(messageId);
            dest.writeInt(newDate);
        }
	
        public UpdateMessageDate() {
        }

        public UpdateMessageDate(long chatId, int messageId, int newDate) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.newDate = newDate;
        }

        public static final int CONSTRUCTOR = 211076103;

        @Override
        public int getConstructor() {
            return 211076103;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateMessageDate").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messageId").append(" = ").append(messageId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("newDate").append(" = ").append(newDate).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateMessageContent extends Update {
        public long chatId;
        public int messageId;
        public MessageContent newContent;

        public static final Creator<UpdateMessageContent> CREATOR = new Creator<UpdateMessageContent>() {

            @Override public UpdateMessageContent createFromParcel(Parcel in) {
                UpdateMessageContent data = new UpdateMessageContent();

                data.chatId = in.readLong();
                data.messageId = in.readInt();
                data.newContent = in.readParcelable(MessageContent.class.getClassLoader());

                return data;
            }

            @Override public UpdateMessageContent[] newArray(int size) {
                return new UpdateMessageContent[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(messageId);
            dest.writeParcelable(this.newContent, flags);
        }
	
        public UpdateMessageContent() {
        }

        public UpdateMessageContent(long chatId, int messageId, MessageContent newContent) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.newContent = newContent;
        }

        public static final int CONSTRUCTOR = 561472729;

        @Override
        public int getConstructor() {
            return 561472729;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateMessageContent").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messageId").append(" = ").append(messageId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("newContent").append(" = "); newContent.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateMessageSendFailed extends Update {
        public long chatId;
        public int messageId;
        public int errorCode;
        public String errorDescription;

        public static final Creator<UpdateMessageSendFailed> CREATOR = new Creator<UpdateMessageSendFailed>() {

            @Override public UpdateMessageSendFailed createFromParcel(Parcel in) {
                UpdateMessageSendFailed data = new UpdateMessageSendFailed();

                data.chatId = in.readLong();
                data.messageId = in.readInt();
                data.errorCode = in.readInt();
                data.errorDescription = in.readString();

                return data;
            }

            @Override public UpdateMessageSendFailed[] newArray(int size) {
                return new UpdateMessageSendFailed[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(messageId);
            dest.writeInt(errorCode);
            dest.writeString(errorDescription);
        }
	
        public UpdateMessageSendFailed() {
        }

        public UpdateMessageSendFailed(long chatId, int messageId, int errorCode, String errorDescription) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.errorCode = errorCode;
            this.errorDescription = errorDescription;
        }

        public static final int CONSTRUCTOR = 2098937137;

        @Override
        public int getConstructor() {
            return 2098937137;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateMessageSendFailed").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messageId").append(" = ").append(messageId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("errorCode").append(" = ").append(errorCode).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("errorDescription").append(" = ").append(errorDescription).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateChatReadInbox extends Update {
        public long chatId;
        public int lastReadInboxMessageId;
        public int unreadCount;

        public static final Creator<UpdateChatReadInbox> CREATOR = new Creator<UpdateChatReadInbox>() {

            @Override public UpdateChatReadInbox createFromParcel(Parcel in) {
                UpdateChatReadInbox data = new UpdateChatReadInbox();

                data.chatId = in.readLong();
                data.lastReadInboxMessageId = in.readInt();
                data.unreadCount = in.readInt();

                return data;
            }

            @Override public UpdateChatReadInbox[] newArray(int size) {
                return new UpdateChatReadInbox[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(lastReadInboxMessageId);
            dest.writeInt(unreadCount);
        }
	
        public UpdateChatReadInbox() {
        }

        public UpdateChatReadInbox(long chatId, int lastReadInboxMessageId, int unreadCount) {
            this.chatId = chatId;
            this.lastReadInboxMessageId = lastReadInboxMessageId;
            this.unreadCount = unreadCount;
        }

        public static final int CONSTRUCTOR = -58810942;

        @Override
        public int getConstructor() {
            return -58810942;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateChatReadInbox").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastReadInboxMessageId").append(" = ").append(lastReadInboxMessageId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("unreadCount").append(" = ").append(unreadCount).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateChatReadOutbox extends Update {
        public long chatId;
        public int lastReadOutboxMessageId;

        public static final Creator<UpdateChatReadOutbox> CREATOR = new Creator<UpdateChatReadOutbox>() {

            @Override public UpdateChatReadOutbox createFromParcel(Parcel in) {
                UpdateChatReadOutbox data = new UpdateChatReadOutbox();

                data.chatId = in.readLong();
                data.lastReadOutboxMessageId = in.readInt();

                return data;
            }

            @Override public UpdateChatReadOutbox[] newArray(int size) {
                return new UpdateChatReadOutbox[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(lastReadOutboxMessageId);
        }
	
        public UpdateChatReadOutbox() {
        }

        public UpdateChatReadOutbox(long chatId, int lastReadOutboxMessageId) {
            this.chatId = chatId;
            this.lastReadOutboxMessageId = lastReadOutboxMessageId;
        }

        public static final int CONSTRUCTOR = 877103058;

        @Override
        public int getConstructor() {
            return 877103058;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateChatReadOutbox").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastReadOutboxMessageId").append(" = ").append(lastReadOutboxMessageId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateChatReplyMarkup extends Update {
        public long chatId;
        public int replyMarkupMessageId;

        public static final Creator<UpdateChatReplyMarkup> CREATOR = new Creator<UpdateChatReplyMarkup>() {

            @Override public UpdateChatReplyMarkup createFromParcel(Parcel in) {
                UpdateChatReplyMarkup data = new UpdateChatReplyMarkup();

                data.chatId = in.readLong();
                data.replyMarkupMessageId = in.readInt();

                return data;
            }

            @Override public UpdateChatReplyMarkup[] newArray(int size) {
                return new UpdateChatReplyMarkup[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(replyMarkupMessageId);
        }
	
        public UpdateChatReplyMarkup() {
        }

        public UpdateChatReplyMarkup(long chatId, int replyMarkupMessageId) {
            this.chatId = chatId;
            this.replyMarkupMessageId = replyMarkupMessageId;
        }

        public static final int CONSTRUCTOR = 301018472;

        @Override
        public int getConstructor() {
            return 301018472;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateChatReplyMarkup").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("replyMarkupMessageId").append(" = ").append(replyMarkupMessageId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateNotificationSettings extends Update {
        public NotificationSettingsScope scope;
        public NotificationSettings notificationSettings;

        public static final Creator<UpdateNotificationSettings> CREATOR = new Creator<UpdateNotificationSettings>() {

            @Override public UpdateNotificationSettings createFromParcel(Parcel in) {
                UpdateNotificationSettings data = new UpdateNotificationSettings();

                data.scope = in.readParcelable(NotificationSettingsScope.class.getClassLoader());
                data.notificationSettings = in.readParcelable(NotificationSettings.class.getClassLoader());

                return data;
            }

            @Override public UpdateNotificationSettings[] newArray(int size) {
                return new UpdateNotificationSettings[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.scope, flags);
            dest.writeParcelable(this.notificationSettings, flags);
        }
	
        public UpdateNotificationSettings() {
        }

        public UpdateNotificationSettings(NotificationSettingsScope scope, NotificationSettings notificationSettings) {
            this.scope = scope;
            this.notificationSettings = notificationSettings;
        }

        public static final int CONSTRUCTOR = -1767306883;

        @Override
        public int getConstructor() {
            return -1767306883;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateNotificationSettings").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("scope").append(" = "); scope.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("notificationSettings").append(" = "); notificationSettings.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateDeleteMessages extends Update {
        public long chatId;
        public int[] messages;

        public static final Creator<UpdateDeleteMessages> CREATOR = new Creator<UpdateDeleteMessages>() {

            @Override public UpdateDeleteMessages createFromParcel(Parcel in) {
                UpdateDeleteMessages data = new UpdateDeleteMessages();

                data.chatId = in.readLong();
                data.messages = in.createIntArray();

                return data;
            }

            @Override public UpdateDeleteMessages[] newArray(int size) {
                return new UpdateDeleteMessages[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeIntArray(messages);
        }
	
        public UpdateDeleteMessages() {
        }

        public UpdateDeleteMessages(long chatId, int[] messages) {
            this.chatId = chatId;
            this.messages = messages;
        }

        public static final int CONSTRUCTOR = -1920566645;

        @Override
        public int getConstructor() {
            return -1920566645;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateDeleteMessages").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messages").append(" = ").append("int[]").append(" {").append(Arrays.toString(messages)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateUserAction extends Update {
        public long chatId;
        public int userId;
        public SendMessageAction action;

        public static final Creator<UpdateUserAction> CREATOR = new Creator<UpdateUserAction>() {

            @Override public UpdateUserAction createFromParcel(Parcel in) {
                UpdateUserAction data = new UpdateUserAction();

                data.chatId = in.readLong();
                data.userId = in.readInt();
                data.action = in.readParcelable(SendMessageAction.class.getClassLoader());

                return data;
            }

            @Override public UpdateUserAction[] newArray(int size) {
                return new UpdateUserAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(userId);
            dest.writeParcelable(this.action, flags);
        }
	
        public UpdateUserAction() {
        }

        public UpdateUserAction(long chatId, int userId, SendMessageAction action) {
            this.chatId = chatId;
            this.userId = userId;
            this.action = action;
        }

        public static final int CONSTRUCTOR = 223420164;

        @Override
        public int getConstructor() {
            return 223420164;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateUserAction").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("action").append(" = "); action.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateUserStatus extends Update {
        public int userId;
        public UserStatus status;

        public static final Creator<UpdateUserStatus> CREATOR = new Creator<UpdateUserStatus>() {

            @Override public UpdateUserStatus createFromParcel(Parcel in) {
                UpdateUserStatus data = new UpdateUserStatus();

                data.userId = in.readInt();
                data.status = in.readParcelable(UserStatus.class.getClassLoader());

                return data;
            }

            @Override public UpdateUserStatus[] newArray(int size) {
                return new UpdateUserStatus[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userId);
            dest.writeParcelable(this.status, flags);
        }
	
        public UpdateUserStatus() {
        }

        public UpdateUserStatus(int userId, UserStatus status) {
            this.userId = userId;
            this.status = status;
        }

        public static final int CONSTRUCTOR = 469489699;

        @Override
        public int getConstructor() {
            return 469489699;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateUserStatus").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("status").append(" = "); status.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateUser extends Update {
        public User user;

        public static final Creator<UpdateUser> CREATOR = new Creator<UpdateUser>() {

            @Override public UpdateUser createFromParcel(Parcel in) {
                UpdateUser data = new UpdateUser();

                data.user = in.readParcelable(User.class.getClassLoader());

                return data;
            }

            @Override public UpdateUser[] newArray(int size) {
                return new UpdateUser[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.user, flags);
        }
	
        public UpdateUser() {
        }

        public UpdateUser(User user) {
            this.user = user;
        }

        public static final int CONSTRUCTOR = 1183394041;

        @Override
        public int getConstructor() {
            return 1183394041;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateUser").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("user").append(" = "); user.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateUserBlocked extends Update {
        public int userId;
        public boolean isBlocked;

        public static final Creator<UpdateUserBlocked> CREATOR = new Creator<UpdateUserBlocked>() {

            @Override public UpdateUserBlocked createFromParcel(Parcel in) {
                UpdateUserBlocked data = new UpdateUserBlocked();

                data.userId = in.readInt();
                data.isBlocked = in.readInt() != 0;

                return data;
            }

            @Override public UpdateUserBlocked[] newArray(int size) {
                return new UpdateUserBlocked[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userId);
            dest.writeInt(this.isBlocked ? 1 : 0);
        }
	
        public UpdateUserBlocked() {
        }

        public UpdateUserBlocked(int userId, boolean isBlocked) {
            this.userId = userId;
            this.isBlocked = isBlocked;
        }

        public static final int CONSTRUCTOR = 1341545325;

        @Override
        public int getConstructor() {
            return 1341545325;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateUserBlocked").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isBlocked").append(" = ").append(isBlocked).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateChatTitle extends Update {
        public long chatId;
        public String title;

        public static final Creator<UpdateChatTitle> CREATOR = new Creator<UpdateChatTitle>() {

            @Override public UpdateChatTitle createFromParcel(Parcel in) {
                UpdateChatTitle data = new UpdateChatTitle();

                data.chatId = in.readLong();
                data.title = in.readString();

                return data;
            }

            @Override public UpdateChatTitle[] newArray(int size) {
                return new UpdateChatTitle[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeString(title);
        }
	
        public UpdateChatTitle() {
        }

        public UpdateChatTitle(long chatId, String title) {
            this.chatId = chatId;
            this.title = title;
        }

        public static final int CONSTRUCTOR = 1931125386;

        @Override
        public int getConstructor() {
            return 1931125386;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateChatTitle").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateChatPhoto extends Update {
        public long chatId;
        public ProfilePhoto photo;

        public static final Creator<UpdateChatPhoto> CREATOR = new Creator<UpdateChatPhoto>() {

            @Override public UpdateChatPhoto createFromParcel(Parcel in) {
                UpdateChatPhoto data = new UpdateChatPhoto();

                data.chatId = in.readLong();
                data.photo = in.readParcelable(ProfilePhoto.class.getClassLoader());

                return data;
            }

            @Override public UpdateChatPhoto[] newArray(int size) {
                return new UpdateChatPhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeParcelable(this.photo, flags);
        }
	
        public UpdateChatPhoto() {
        }

        public UpdateChatPhoto(long chatId, ProfilePhoto photo) {
            this.chatId = chatId;
            this.photo = photo;
        }

        public static final int CONSTRUCTOR = 977270305;

        @Override
        public int getConstructor() {
            return 977270305;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateChatPhoto").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photo").append(" = "); photo.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateChatParticipantsCount extends Update {
        public long chatId;
        public int participantsCount;

        public static final Creator<UpdateChatParticipantsCount> CREATOR = new Creator<UpdateChatParticipantsCount>() {

            @Override public UpdateChatParticipantsCount createFromParcel(Parcel in) {
                UpdateChatParticipantsCount data = new UpdateChatParticipantsCount();

                data.chatId = in.readLong();
                data.participantsCount = in.readInt();

                return data;
            }

            @Override public UpdateChatParticipantsCount[] newArray(int size) {
                return new UpdateChatParticipantsCount[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(participantsCount);
        }
	
        public UpdateChatParticipantsCount() {
        }

        public UpdateChatParticipantsCount(long chatId, int participantsCount) {
            this.chatId = chatId;
            this.participantsCount = participantsCount;
        }

        public static final int CONSTRUCTOR = 1188647993;

        @Override
        public int getConstructor() {
            return 1188647993;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateChatParticipantsCount").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("participantsCount").append(" = ").append(participantsCount).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateNewAuthorization extends Update {
        public int date;
        public String device;
        public String location;

        public static final Creator<UpdateNewAuthorization> CREATOR = new Creator<UpdateNewAuthorization>() {

            @Override public UpdateNewAuthorization createFromParcel(Parcel in) {
                UpdateNewAuthorization data = new UpdateNewAuthorization();

                data.date = in.readInt();
                data.device = in.readString();
                data.location = in.readString();

                return data;
            }

            @Override public UpdateNewAuthorization[] newArray(int size) {
                return new UpdateNewAuthorization[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(date);
            dest.writeString(device);
            dest.writeString(location);
        }
	
        public UpdateNewAuthorization() {
        }

        public UpdateNewAuthorization(int date, String device, String location) {
            this.date = date;
            this.device = device;
            this.location = location;
        }

        public static final int CONSTRUCTOR = -176559980;

        @Override
        public int getConstructor() {
            return -176559980;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateNewAuthorization").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("date").append(" = ").append(date).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("device").append(" = ").append(device).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("location").append(" = ").append(location).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateFileProgress extends Update {
        public int fileId;
        public int size;
        public int ready;

        public static final Creator<UpdateFileProgress> CREATOR = new Creator<UpdateFileProgress>() {

            @Override public UpdateFileProgress createFromParcel(Parcel in) {
                UpdateFileProgress data = new UpdateFileProgress();

                data.fileId = in.readInt();
                data.size = in.readInt();
                data.ready = in.readInt();

                return data;
            }

            @Override public UpdateFileProgress[] newArray(int size) {
                return new UpdateFileProgress[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(fileId);
            dest.writeInt(size);
            dest.writeInt(ready);
        }
	
        public UpdateFileProgress() {
        }

        public UpdateFileProgress(int fileId, int size, int ready) {
            this.fileId = fileId;
            this.size = size;
            this.ready = ready;
        }

        public static final int CONSTRUCTOR = 1340921194;

        @Override
        public int getConstructor() {
            return 1340921194;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateFileProgress").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fileId").append(" = ").append(fileId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("size").append(" = ").append(size).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("ready").append(" = ").append(ready).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateFile extends Update {
        public File file;

        public static final Creator<UpdateFile> CREATOR = new Creator<UpdateFile>() {

            @Override public UpdateFile createFromParcel(Parcel in) {
                UpdateFile data = new UpdateFile();

                data.file = in.readParcelable(File.class.getClassLoader());

                return data;
            }

            @Override public UpdateFile[] newArray(int size) {
                return new UpdateFile[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.file, flags);
        }
	
        public UpdateFile() {
        }

        public UpdateFile(File file) {
            this.file = file;
        }

        public static final int CONSTRUCTOR = 114132831;

        @Override
        public int getConstructor() {
            return 114132831;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateFile").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("file").append(" = "); file.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateOption extends Update {
        public String name;
        public OptionValue value;

        public static final Creator<UpdateOption> CREATOR = new Creator<UpdateOption>() {

            @Override public UpdateOption createFromParcel(Parcel in) {
                UpdateOption data = new UpdateOption();

                data.name = in.readString();
                data.value = in.readParcelable(OptionValue.class.getClassLoader());

                return data;
            }

            @Override public UpdateOption[] newArray(int size) {
                return new UpdateOption[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeParcelable(this.value, flags);
        }
	
        public UpdateOption() {
        }

        public UpdateOption(String name, OptionValue value) {
            this.name = name;
            this.value = value;
        }

        public static final int CONSTRUCTOR = 900822020;

        @Override
        public int getConstructor() {
            return 900822020;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateOption").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("name").append(" = ").append(name).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = "); value.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateStickers extends Update {

        public static final Creator<UpdateStickers> CREATOR = new Creator<UpdateStickers>() {

            @Override public UpdateStickers createFromParcel(Parcel in) {
                UpdateStickers data = new UpdateStickers();

        
                return data;
            }

            @Override public UpdateStickers[] newArray(int size) {
                return new UpdateStickers[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public UpdateStickers() {
        }

        public static final int CONSTRUCTOR = -456211753;

        @Override
        public int getConstructor() {
            return -456211753;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateStickers").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class User extends TLObject {
        public int id;
        public String firstName;
        public String lastName;
        public String username;
        public String phoneNumber;
        public UserStatus status;
        public ProfilePhoto profilePhoto;
        public LinkState myLink;
        public LinkState foreignLink;
        public UserType type;

        public static final Creator<User> CREATOR = new Creator<User>() {

            @Override public User createFromParcel(Parcel in) {
                User data = new User();

                data.id = in.readInt();
                data.firstName = in.readString();
                data.lastName = in.readString();
                data.username = in.readString();
                data.phoneNumber = in.readString();
                data.status = in.readParcelable(UserStatus.class.getClassLoader());
                data.profilePhoto = in.readParcelable(ProfilePhoto.class.getClassLoader());
                data.myLink = in.readParcelable(LinkState.class.getClassLoader());
                data.foreignLink = in.readParcelable(LinkState.class.getClassLoader());
                data.type = in.readParcelable(UserType.class.getClassLoader());

                return data;
            }

            @Override public User[] newArray(int size) {
                return new User[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(firstName);
            dest.writeString(lastName);
            dest.writeString(username);
            dest.writeString(phoneNumber);
            dest.writeParcelable(this.status, flags);
            dest.writeParcelable(this.profilePhoto, flags);
            dest.writeParcelable(this.myLink, flags);
            dest.writeParcelable(this.foreignLink, flags);
            dest.writeParcelable(this.type, flags);
        }
	
        public User() {
        }

        public User(int id, String firstName, String lastName, String username, String phoneNumber, UserStatus status, ProfilePhoto profilePhoto, LinkState myLink, LinkState foreignLink, UserType type) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.username = username;
            this.phoneNumber = phoneNumber;
            this.status = status;
            this.profilePhoto = profilePhoto;
            this.myLink = myLink;
            this.foreignLink = foreignLink;
            this.type = type;
        }

        public static final int CONSTRUCTOR = -1747251981;

        @Override
        public int getConstructor() {
            return -1747251981;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("User").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("id").append(" = ").append(id).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("firstName").append(" = ").append(firstName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastName").append(" = ").append(lastName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("username").append(" = ").append(username).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("phoneNumber").append(" = ").append(phoneNumber).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("status").append(" = "); status.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("profilePhoto").append(" = "); profilePhoto.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("myLink").append(" = "); myLink.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("foreignLink").append(" = "); foreignLink.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("type").append(" = "); type.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserFull extends TLObject {
        public User user;
        public boolean isBlocked;
        public BotInfo botInfo;

        public static final Creator<UserFull> CREATOR = new Creator<UserFull>() {

            @Override public UserFull createFromParcel(Parcel in) {
                UserFull data = new UserFull();

                data.user = in.readParcelable(User.class.getClassLoader());
                data.isBlocked = in.readInt() != 0;
                data.botInfo = in.readParcelable(BotInfo.class.getClassLoader());

                return data;
            }

            @Override public UserFull[] newArray(int size) {
                return new UserFull[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.user, flags);
            dest.writeInt(this.isBlocked ? 1 : 0);
            dest.writeParcelable(this.botInfo, flags);
        }
	
        public UserFull() {
        }

        public UserFull(User user, boolean isBlocked, BotInfo botInfo) {
            this.user = user;
            this.isBlocked = isBlocked;
            this.botInfo = botInfo;
        }

        public static final int CONSTRUCTOR = -1158674134;

        @Override
        public int getConstructor() {
            return -1158674134;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserFull").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("user").append(" = "); user.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isBlocked").append(" = ").append(isBlocked).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("botInfo").append(" = "); botInfo.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserProfilePhotos extends TLObject {
        public int totalCount;
        public Photo[] photos;

        public static final Creator<UserProfilePhotos> CREATOR = new Creator<UserProfilePhotos>() {

            @Override public UserProfilePhotos createFromParcel(Parcel in) {
                UserProfilePhotos data = new UserProfilePhotos();

                data.totalCount = in.readInt();
                data.photos = in.createTypedArray(Photo.CREATOR);

                return data;
            }

            @Override public UserProfilePhotos[] newArray(int size) {
                return new UserProfilePhotos[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(totalCount);
            dest.writeTypedArray(this.photos, flags);
        }
	
        public UserProfilePhotos() {
        }

        public UserProfilePhotos(int totalCount, Photo[] photos) {
            this.totalCount = totalCount;
            this.photos = photos;
        }

        public static final int CONSTRUCTOR = -1425984405;

        @Override
        public int getConstructor() {
            return -1425984405;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserProfilePhotos").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("totalCount").append(" = ").append(totalCount).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photos").append(" = ").append("Photo[]").append(" {").append(Arrays.toString(photos)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class UserStatus extends TLObject {
    }

    public static class UserStatusEmpty extends UserStatus {

        public static final Creator<UserStatusEmpty> CREATOR = new Creator<UserStatusEmpty>() {

            @Override public UserStatusEmpty createFromParcel(Parcel in) {
                UserStatusEmpty data = new UserStatusEmpty();

        
                return data;
            }

            @Override public UserStatusEmpty[] newArray(int size) {
                return new UserStatusEmpty[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public UserStatusEmpty() {
        }

        public static final int CONSTRUCTOR = 164646985;

        @Override
        public int getConstructor() {
            return 164646985;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserStatusEmpty").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserStatusOnline extends UserStatus {
        public int expires;

        public static final Creator<UserStatusOnline> CREATOR = new Creator<UserStatusOnline>() {

            @Override public UserStatusOnline createFromParcel(Parcel in) {
                UserStatusOnline data = new UserStatusOnline();

                data.expires = in.readInt();

                return data;
            }

            @Override public UserStatusOnline[] newArray(int size) {
                return new UserStatusOnline[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(expires);
        }
	
        public UserStatusOnline() {
        }

        public UserStatusOnline(int expires) {
            this.expires = expires;
        }

        public static final int CONSTRUCTOR = -306628279;

        @Override
        public int getConstructor() {
            return -306628279;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserStatusOnline").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("expires").append(" = ").append(expires).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserStatusOffline extends UserStatus {
        public int wasOnline;

        public static final Creator<UserStatusOffline> CREATOR = new Creator<UserStatusOffline>() {

            @Override public UserStatusOffline createFromParcel(Parcel in) {
                UserStatusOffline data = new UserStatusOffline();

                data.wasOnline = in.readInt();

                return data;
            }

            @Override public UserStatusOffline[] newArray(int size) {
                return new UserStatusOffline[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(wasOnline);
        }
	
        public UserStatusOffline() {
        }

        public UserStatusOffline(int wasOnline) {
            this.wasOnline = wasOnline;
        }

        public static final int CONSTRUCTOR = 9203775;

        @Override
        public int getConstructor() {
            return 9203775;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserStatusOffline").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("wasOnline").append(" = ").append(wasOnline).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserStatusRecently extends UserStatus {

        public static final Creator<UserStatusRecently> CREATOR = new Creator<UserStatusRecently>() {

            @Override public UserStatusRecently createFromParcel(Parcel in) {
                UserStatusRecently data = new UserStatusRecently();

        
                return data;
            }

            @Override public UserStatusRecently[] newArray(int size) {
                return new UserStatusRecently[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public UserStatusRecently() {
        }

        public static final int CONSTRUCTOR = -496024847;

        @Override
        public int getConstructor() {
            return -496024847;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserStatusRecently").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserStatusLastWeek extends UserStatus {

        public static final Creator<UserStatusLastWeek> CREATOR = new Creator<UserStatusLastWeek>() {

            @Override public UserStatusLastWeek createFromParcel(Parcel in) {
                UserStatusLastWeek data = new UserStatusLastWeek();

        
                return data;
            }

            @Override public UserStatusLastWeek[] newArray(int size) {
                return new UserStatusLastWeek[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public UserStatusLastWeek() {
        }

        public static final int CONSTRUCTOR = 129960444;

        @Override
        public int getConstructor() {
            return 129960444;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserStatusLastWeek").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserStatusLastMonth extends UserStatus {

        public static final Creator<UserStatusLastMonth> CREATOR = new Creator<UserStatusLastMonth>() {

            @Override public UserStatusLastMonth createFromParcel(Parcel in) {
                UserStatusLastMonth data = new UserStatusLastMonth();

        
                return data;
            }

            @Override public UserStatusLastMonth[] newArray(int size) {
                return new UserStatusLastMonth[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public UserStatusLastMonth() {
        }

        public static final int CONSTRUCTOR = 2011940674;

        @Override
        public int getConstructor() {
            return 2011940674;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserStatusLastMonth").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public abstract static class UserType extends TLObject {
    }

    public static class UserTypeGeneral extends UserType {

        public static final Creator<UserTypeGeneral> CREATOR = new Creator<UserTypeGeneral>() {

            @Override public UserTypeGeneral createFromParcel(Parcel in) {
                UserTypeGeneral data = new UserTypeGeneral();

        
                return data;
            }

            @Override public UserTypeGeneral[] newArray(int size) {
                return new UserTypeGeneral[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public UserTypeGeneral() {
        }

        public static final int CONSTRUCTOR = -955149573;

        @Override
        public int getConstructor() {
            return -955149573;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserTypeGeneral").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserTypeDeleted extends UserType {

        public static final Creator<UserTypeDeleted> CREATOR = new Creator<UserTypeDeleted>() {

            @Override public UserTypeDeleted createFromParcel(Parcel in) {
                UserTypeDeleted data = new UserTypeDeleted();

        
                return data;
            }

            @Override public UserTypeDeleted[] newArray(int size) {
                return new UserTypeDeleted[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public UserTypeDeleted() {
        }

        public static final int CONSTRUCTOR = -1807729372;

        @Override
        public int getConstructor() {
            return -1807729372;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserTypeDeleted").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserTypeBot extends UserType {
        public boolean canJoinGroupChats;
        public boolean canReadAllGroupChatMessages;

        public static final Creator<UserTypeBot> CREATOR = new Creator<UserTypeBot>() {

            @Override public UserTypeBot createFromParcel(Parcel in) {
                UserTypeBot data = new UserTypeBot();

                data.canJoinGroupChats = in.readInt() != 0;
                data.canReadAllGroupChatMessages = in.readInt() != 0;

                return data;
            }

            @Override public UserTypeBot[] newArray(int size) {
                return new UserTypeBot[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.canJoinGroupChats ? 1 : 0);
            dest.writeInt(this.canReadAllGroupChatMessages ? 1 : 0);
        }
	
        public UserTypeBot() {
        }

        public UserTypeBot(boolean canJoinGroupChats, boolean canReadAllGroupChatMessages) {
            this.canJoinGroupChats = canJoinGroupChats;
            this.canReadAllGroupChatMessages = canReadAllGroupChatMessages;
        }

        public static final int CONSTRUCTOR = -1312994781;

        @Override
        public int getConstructor() {
            return -1312994781;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserTypeBot").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("canJoinGroupChats").append(" = ").append(canJoinGroupChats).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("canReadAllGroupChatMessages").append(" = ").append(canReadAllGroupChatMessages).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UserTypeUnknown extends UserType {

        public static final Creator<UserTypeUnknown> CREATOR = new Creator<UserTypeUnknown>() {

            @Override public UserTypeUnknown createFromParcel(Parcel in) {
                UserTypeUnknown data = new UserTypeUnknown();

        
                return data;
            }

            @Override public UserTypeUnknown[] newArray(int size) {
                return new UserTypeUnknown[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public UserTypeUnknown() {
        }

        public static final int CONSTRUCTOR = -724541123;

        @Override
        public int getConstructor() {
            return -724541123;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UserTypeUnknown").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Video extends TLObject {
        public int duration;
        public PhotoSize thumb;
        public int width;
        public int height;
        public File video;

        public static final Creator<Video> CREATOR = new Creator<Video>() {

            @Override public Video createFromParcel(Parcel in) {
                Video data = new Video();

                data.duration = in.readInt();
                data.thumb = in.readParcelable(PhotoSize.class.getClassLoader());
                data.width = in.readInt();
                data.height = in.readInt();
                data.video = in.readParcelable(File.class.getClassLoader());

                return data;
            }

            @Override public Video[] newArray(int size) {
                return new Video[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(duration);
            dest.writeParcelable(this.thumb, flags);
            dest.writeInt(width);
            dest.writeInt(height);
            dest.writeParcelable(this.video, flags);
        }
	
        public Video() {
        }

        public Video(int duration, PhotoSize thumb, int width, int height, File video) {
            this.duration = duration;
            this.thumb = thumb;
            this.width = width;
            this.height = height;
            this.video = video;
        }

        public static final int CONSTRUCTOR = 481641164;

        @Override
        public int getConstructor() {
            return 481641164;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Video").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("duration").append(" = ").append(duration).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("thumb").append(" = "); thumb.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("width").append(" = ").append(width).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("height").append(" = ").append(height).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("video").append(" = "); video.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class Voice extends TLObject {
        public int duration;
        public String mimeType;
        public File voice;

        public static final Creator<Voice> CREATOR = new Creator<Voice>() {

            @Override public Voice createFromParcel(Parcel in) {
                Voice data = new Voice();

                data.duration = in.readInt();
                data.mimeType = in.readString();
                data.voice = in.readParcelable(File.class.getClassLoader());

                return data;
            }

            @Override public Voice[] newArray(int size) {
                return new Voice[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(duration);
            dest.writeString(mimeType);
            dest.writeParcelable(this.voice, flags);
        }
	
        public Voice() {
        }

        public Voice(int duration, String mimeType, File voice) {
            this.duration = duration;
            this.mimeType = mimeType;
            this.voice = voice;
        }

        public static final int CONSTRUCTOR = -1833368702;

        @Override
        public int getConstructor() {
            return -1833368702;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("Voice").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("duration").append(" = ").append(duration).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("mimeType").append(" = ").append(mimeType).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("voice").append(" = "); voice.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class WebPage extends TLObject {
        public String url;
        public String displayUrl;
        public String type;
        public String siteName;
        public String title;
        public String description;
        public Photo photo;
        public String embedUrl;
        public String embedType;
        public int embedWidth;
        public int embedHeight;
        public int duration;
        public String author;

        public static final Creator<WebPage> CREATOR = new Creator<WebPage>() {

            @Override public WebPage createFromParcel(Parcel in) {
                WebPage data = new WebPage();

                data.url = in.readString();
                data.displayUrl = in.readString();
                data.type = in.readString();
                data.siteName = in.readString();
                data.title = in.readString();
                data.description = in.readString();
                data.photo = in.readParcelable(Photo.class.getClassLoader());
                data.embedUrl = in.readString();
                data.embedType = in.readString();
                data.embedWidth = in.readInt();
                data.embedHeight = in.readInt();
                data.duration = in.readInt();
                data.author = in.readString();

                return data;
            }

            @Override public WebPage[] newArray(int size) {
                return new WebPage[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(url);
            dest.writeString(displayUrl);
            dest.writeString(type);
            dest.writeString(siteName);
            dest.writeString(title);
            dest.writeString(description);
            dest.writeParcelable(this.photo, flags);
            dest.writeString(embedUrl);
            dest.writeString(embedType);
            dest.writeInt(embedWidth);
            dest.writeInt(embedHeight);
            dest.writeInt(duration);
            dest.writeString(author);
        }
	
        public WebPage() {
        }

        public WebPage(String url, String displayUrl, String type, String siteName, String title, String description, Photo photo, String embedUrl, String embedType, int embedWidth, int embedHeight, int duration, String author) {
            this.url = url;
            this.displayUrl = displayUrl;
            this.type = type;
            this.siteName = siteName;
            this.title = title;
            this.description = description;
            this.photo = photo;
            this.embedUrl = embedUrl;
            this.embedType = embedType;
            this.embedWidth = embedWidth;
            this.embedHeight = embedHeight;
            this.duration = duration;
            this.author = author;
        }

        public static final int CONSTRUCTOR = -38536859;

        @Override
        public int getConstructor() {
            return -38536859;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("WebPage").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("url").append(" = ").append(url).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("displayUrl").append(" = ").append(displayUrl).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("type").append(" = ").append(type).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("siteName").append(" = ").append(siteName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("description").append(" = ").append(description).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photo").append(" = "); photo.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("embedUrl").append(" = ").append(embedUrl).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("embedType").append(" = ").append(embedType).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("embedWidth").append(" = ").append(embedWidth).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("embedHeight").append(" = ").append(embedHeight).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("duration").append(" = ").append(duration).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("author").append(" = ").append(author).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestBytes extends TLObject {
        public byte[] value;

        public static final Creator<TestBytes> CREATOR = new Creator<TestBytes>() {

            @Override public TestBytes createFromParcel(Parcel in) {
                TestBytes data = new TestBytes();

                data.value = in.createByteArray();

                return data;
            }

            @Override public TestBytes[] newArray(int size) {
                return new TestBytes[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeByteArray(value);
        }
	
        public TestBytes() {
        }

        public TestBytes(byte[] value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = 667099484;

        @Override
        public int getConstructor() {
            return 667099484;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestBytes").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append("bytes { "); { for (byte k : value) { int b = (int)k & 255; s.append(HEX_CHARACTERS[b >> 4]).append(HEX_CHARACTERS[b & 15]).append(' '); } } s.append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestEmpty extends TLObject {

        public static final Creator<TestEmpty> CREATOR = new Creator<TestEmpty>() {

            @Override public TestEmpty createFromParcel(Parcel in) {
                TestEmpty data = new TestEmpty();

        
                return data;
            }

            @Override public TestEmpty[] newArray(int size) {
                return new TestEmpty[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public TestEmpty() {
        }

        public static final int CONSTRUCTOR = 1453429851;

        @Override
        public int getConstructor() {
            return 1453429851;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestEmpty").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestInt extends TLObject {
        public int value;

        public static final Creator<TestInt> CREATOR = new Creator<TestInt>() {

            @Override public TestInt createFromParcel(Parcel in) {
                TestInt data = new TestInt();

                data.value = in.readInt();

                return data;
            }

            @Override public TestInt[] newArray(int size) {
                return new TestInt[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(value);
        }
	
        public TestInt() {
        }

        public TestInt(int value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = 1472758404;

        @Override
        public int getConstructor() {
            return 1472758404;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestInt").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append(value).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestString extends TLObject {
        public String value;

        public static final Creator<TestString> CREATOR = new Creator<TestString>() {

            @Override public TestString createFromParcel(Parcel in) {
                TestString data = new TestString();

                data.value = in.readString();

                return data;
            }

            @Override public TestString[] newArray(int size) {
                return new TestString[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(value);
        }
	
        public TestString() {
        }

        public TestString(String value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = -705221530;

        @Override
        public int getConstructor() {
            return -705221530;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestString").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append(value).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestVectorInt extends TLObject {
        public int[] value;

        public static final Creator<TestVectorInt> CREATOR = new Creator<TestVectorInt>() {

            @Override public TestVectorInt createFromParcel(Parcel in) {
                TestVectorInt data = new TestVectorInt();

                data.value = in.createIntArray();

                return data;
            }

            @Override public TestVectorInt[] newArray(int size) {
                return new TestVectorInt[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeIntArray(value);
        }
	
        public TestVectorInt() {
        }

        public TestVectorInt(int[] value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = -278984267;

        @Override
        public int getConstructor() {
            return -278984267;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestVectorInt").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append("int[]").append(" {").append(Arrays.toString(value)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestVectorIntObject extends TLObject {
        public TestInt[] value;

        public static final Creator<TestVectorIntObject> CREATOR = new Creator<TestVectorIntObject>() {

            @Override public TestVectorIntObject createFromParcel(Parcel in) {
                TestVectorIntObject data = new TestVectorIntObject();

                data.value = in.createTypedArray(TestInt.CREATOR);

                return data;
            }

            @Override public TestVectorIntObject[] newArray(int size) {
                return new TestVectorIntObject[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedArray(this.value, flags);
        }
	
        public TestVectorIntObject() {
        }

        public TestVectorIntObject(TestInt[] value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = 1915770327;

        @Override
        public int getConstructor() {
            return 1915770327;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestVectorIntObject").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append("TestInt[]").append(" {").append(Arrays.toString(value)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestVectorString extends TLObject {
        public String[] value;

        public static final Creator<TestVectorString> CREATOR = new Creator<TestVectorString>() {

            @Override public TestVectorString createFromParcel(Parcel in) {
                TestVectorString data = new TestVectorString();

                data.value = in.createStringArray();

                return data;
            }

            @Override public TestVectorString[] newArray(int size) {
                return new TestVectorString[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeStringArray(value);
        }
	
        public TestVectorString() {
        }

        public TestVectorString(String[] value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = 1800468445;

        @Override
        public int getConstructor() {
            return 1800468445;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestVectorString").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append("String[]").append(" {").append(Arrays.toString(value)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestVectorStringObject extends TLObject {
        public TestString[] value;

        public static final Creator<TestVectorStringObject> CREATOR = new Creator<TestVectorStringObject>() {

            @Override public TestVectorStringObject createFromParcel(Parcel in) {
                TestVectorStringObject data = new TestVectorStringObject();

                data.value = in.createTypedArray(TestString.CREATOR);

                return data;
            }

            @Override public TestVectorStringObject[] newArray(int size) {
                return new TestVectorStringObject[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedArray(this.value, flags);
        }
	
        public TestVectorStringObject() {
        }

        public TestVectorStringObject(TestString[] value) {
            this.value = value;
        }

        public static final int CONSTRUCTOR = -1261798902;

        @Override
        public int getConstructor() {
            return -1261798902;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestVectorStringObject").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = ").append("TestString[]").append(" {").append(Arrays.toString(value)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class AddChatParticipant extends TLFunction {
        public long chatId;
        public int userId;
        public int forwardLimit;

        public static final Creator<AddChatParticipant> CREATOR = new Creator<AddChatParticipant>() {

            @Override public AddChatParticipant createFromParcel(Parcel in) {
                AddChatParticipant data = new AddChatParticipant();

                data.chatId = in.readLong();
                data.userId = in.readInt();
                data.forwardLimit = in.readInt();

                return data;
            }

            @Override public AddChatParticipant[] newArray(int size) {
                return new AddChatParticipant[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(userId);
            dest.writeInt(forwardLimit);
        }
	
        public AddChatParticipant() {
        }

        public AddChatParticipant(long chatId, int userId, int forwardLimit) {
            this.chatId = chatId;
            this.userId = userId;
            this.forwardLimit = forwardLimit;
        }

        public static final int CONSTRUCTOR = 572126454;

        @Override
        public int getConstructor() {
            return 572126454;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("AddChatParticipant").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("forwardLimit").append(" = ").append(forwardLimit).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class BlockUser extends TLFunction {
        public int userId;

        public static final Creator<BlockUser> CREATOR = new Creator<BlockUser>() {

            @Override public BlockUser createFromParcel(Parcel in) {
                BlockUser data = new BlockUser();

                data.userId = in.readInt();

                return data;
            }

            @Override public BlockUser[] newArray(int size) {
                return new BlockUser[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userId);
        }
	
        public BlockUser() {
        }

        public BlockUser(int userId) {
            this.userId = userId;
        }

        public static final int CONSTRUCTOR = -200788058;

        @Override
        public int getConstructor() {
            return -200788058;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("BlockUser").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class CancelDownloadFile extends TLFunction {
        public int fileId;

        public static final Creator<CancelDownloadFile> CREATOR = new Creator<CancelDownloadFile>() {

            @Override public CancelDownloadFile createFromParcel(Parcel in) {
                CancelDownloadFile data = new CancelDownloadFile();

                data.fileId = in.readInt();

                return data;
            }

            @Override public CancelDownloadFile[] newArray(int size) {
                return new CancelDownloadFile[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(fileId);
        }
	
        public CancelDownloadFile() {
        }

        public CancelDownloadFile(int fileId) {
            this.fileId = fileId;
        }

        public static final int CONSTRUCTOR = 18489866;

        @Override
        public int getConstructor() {
            return 18489866;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("CancelDownloadFile").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fileId").append(" = ").append(fileId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ChangeChatPhoto extends TLFunction {
        public long chatId;
        public InputFile photo;
        public ProfilePhotoCrop crop;

        public static final Creator<ChangeChatPhoto> CREATOR = new Creator<ChangeChatPhoto>() {

            @Override public ChangeChatPhoto createFromParcel(Parcel in) {
                ChangeChatPhoto data = new ChangeChatPhoto();

                data.chatId = in.readLong();
                data.photo = in.readParcelable(InputFile.class.getClassLoader());
                data.crop = in.readParcelable(ProfilePhotoCrop.class.getClassLoader());

                return data;
            }

            @Override public ChangeChatPhoto[] newArray(int size) {
                return new ChangeChatPhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeParcelable(this.photo, flags);
            dest.writeParcelable(this.crop, flags);
        }
	
        public ChangeChatPhoto() {
        }

        public ChangeChatPhoto(long chatId, InputFile photo, ProfilePhotoCrop crop) {
            this.chatId = chatId;
            this.photo = photo;
            this.crop = crop;
        }

        public static final int CONSTRUCTOR = 1188180943;

        @Override
        public int getConstructor() {
            return 1188180943;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ChangeChatPhoto").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photo").append(" = "); photo.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("crop").append(" = "); crop.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ChangeChatTitle extends TLFunction {
        public long chatId;
        public String title;

        public static final Creator<ChangeChatTitle> CREATOR = new Creator<ChangeChatTitle>() {

            @Override public ChangeChatTitle createFromParcel(Parcel in) {
                ChangeChatTitle data = new ChangeChatTitle();

                data.chatId = in.readLong();
                data.title = in.readString();

                return data;
            }

            @Override public ChangeChatTitle[] newArray(int size) {
                return new ChangeChatTitle[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeString(title);
        }
	
        public ChangeChatTitle() {
        }

        public ChangeChatTitle(long chatId, String title) {
            this.chatId = chatId;
            this.title = title;
        }

        public static final int CONSTRUCTOR = -503002783;

        @Override
        public int getConstructor() {
            return -503002783;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ChangeChatTitle").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ChangeName extends TLFunction {
        public String firstName;
        public String lastName;

        public static final Creator<ChangeName> CREATOR = new Creator<ChangeName>() {

            @Override public ChangeName createFromParcel(Parcel in) {
                ChangeName data = new ChangeName();

                data.firstName = in.readString();
                data.lastName = in.readString();

                return data;
            }

            @Override public ChangeName[] newArray(int size) {
                return new ChangeName[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(firstName);
            dest.writeString(lastName);
        }
	
        public ChangeName() {
        }

        public ChangeName(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public static final int CONSTRUCTOR = 1341435471;

        @Override
        public int getConstructor() {
            return 1341435471;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ChangeName").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("firstName").append(" = ").append(firstName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastName").append(" = ").append(lastName).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ChangeUsername extends TLFunction {
        public String username;

        public static final Creator<ChangeUsername> CREATOR = new Creator<ChangeUsername>() {

            @Override public ChangeUsername createFromParcel(Parcel in) {
                ChangeUsername data = new ChangeUsername();

                data.username = in.readString();

                return data;
            }

            @Override public ChangeUsername[] newArray(int size) {
                return new ChangeUsername[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(username);
        }
	
        public ChangeUsername() {
        }

        public ChangeUsername(String username) {
            this.username = username;
        }

        public static final int CONSTRUCTOR = 2015886676;

        @Override
        public int getConstructor() {
            return 2015886676;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ChangeUsername").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("username").append(" = ").append(username).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class CheckAuthPassword extends TLFunction {
        public String password;

        public static final Creator<CheckAuthPassword> CREATOR = new Creator<CheckAuthPassword>() {

            @Override public CheckAuthPassword createFromParcel(Parcel in) {
                CheckAuthPassword data = new CheckAuthPassword();

                data.password = in.readString();

                return data;
            }

            @Override public CheckAuthPassword[] newArray(int size) {
                return new CheckAuthPassword[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(password);
        }
	
        public CheckAuthPassword() {
        }

        public CheckAuthPassword(String password) {
            this.password = password;
        }

        public static final int CONSTRUCTOR = -1138590405;

        @Override
        public int getConstructor() {
            return -1138590405;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("CheckAuthPassword").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("password").append(" = ").append(password).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class CreateGroupChat extends TLFunction {
        public int[] participantIds;
        public String title;

        public static final Creator<CreateGroupChat> CREATOR = new Creator<CreateGroupChat>() {

            @Override public CreateGroupChat createFromParcel(Parcel in) {
                CreateGroupChat data = new CreateGroupChat();

                data.participantIds = in.createIntArray();
                data.title = in.readString();

                return data;
            }

            @Override public CreateGroupChat[] newArray(int size) {
                return new CreateGroupChat[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeIntArray(participantIds);
            dest.writeString(title);
        }
	
        public CreateGroupChat() {
        }

        public CreateGroupChat(int[] participantIds, String title) {
            this.participantIds = participantIds;
            this.title = title;
        }

        public static final int CONSTRUCTOR = -586035961;

        @Override
        public int getConstructor() {
            return -586035961;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("CreateGroupChat").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("participantIds").append(" = ").append("int[]").append(" {").append(Arrays.toString(participantIds)).append("}\n");
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("title").append(" = ").append(title).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class CreatePrivateChat extends TLFunction {
        public int userId;

        public static final Creator<CreatePrivateChat> CREATOR = new Creator<CreatePrivateChat>() {

            @Override public CreatePrivateChat createFromParcel(Parcel in) {
                CreatePrivateChat data = new CreatePrivateChat();

                data.userId = in.readInt();

                return data;
            }

            @Override public CreatePrivateChat[] newArray(int size) {
                return new CreatePrivateChat[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userId);
        }
	
        public CreatePrivateChat() {
        }

        public CreatePrivateChat(int userId) {
            this.userId = userId;
        }

        public static final int CONSTRUCTOR = 1204324690;

        @Override
        public int getConstructor() {
            return 1204324690;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("CreatePrivateChat").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class DeleteChatHistory extends TLFunction {
        public long chatId;

        public static final Creator<DeleteChatHistory> CREATOR = new Creator<DeleteChatHistory>() {

            @Override public DeleteChatHistory createFromParcel(Parcel in) {
                DeleteChatHistory data = new DeleteChatHistory();

                data.chatId = in.readLong();

                return data;
            }

            @Override public DeleteChatHistory[] newArray(int size) {
                return new DeleteChatHistory[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
        }
	
        public DeleteChatHistory() {
        }

        public DeleteChatHistory(long chatId) {
            this.chatId = chatId;
        }

        public static final int CONSTRUCTOR = -1065852609;

        @Override
        public int getConstructor() {
            return -1065852609;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("DeleteChatHistory").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class DeleteChatParticipant extends TLFunction {
        public long chatId;
        public int userId;

        public static final Creator<DeleteChatParticipant> CREATOR = new Creator<DeleteChatParticipant>() {

            @Override public DeleteChatParticipant createFromParcel(Parcel in) {
                DeleteChatParticipant data = new DeleteChatParticipant();

                data.chatId = in.readLong();
                data.userId = in.readInt();

                return data;
            }

            @Override public DeleteChatParticipant[] newArray(int size) {
                return new DeleteChatParticipant[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(userId);
        }
	
        public DeleteChatParticipant() {
        }

        public DeleteChatParticipant(long chatId, int userId) {
            this.chatId = chatId;
            this.userId = userId;
        }

        public static final int CONSTRUCTOR = -1090865113;

        @Override
        public int getConstructor() {
            return -1090865113;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("DeleteChatParticipant").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class DeleteChatReplyMarkup extends TLFunction {
        public long chatId;
        public int messageId;

        public static final Creator<DeleteChatReplyMarkup> CREATOR = new Creator<DeleteChatReplyMarkup>() {

            @Override public DeleteChatReplyMarkup createFromParcel(Parcel in) {
                DeleteChatReplyMarkup data = new DeleteChatReplyMarkup();

                data.chatId = in.readLong();
                data.messageId = in.readInt();

                return data;
            }

            @Override public DeleteChatReplyMarkup[] newArray(int size) {
                return new DeleteChatReplyMarkup[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(messageId);
        }
	
        public DeleteChatReplyMarkup() {
        }

        public DeleteChatReplyMarkup(long chatId, int messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        public static final int CONSTRUCTOR = 959624272;

        @Override
        public int getConstructor() {
            return 959624272;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("DeleteChatReplyMarkup").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messageId").append(" = ").append(messageId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class DeleteContacts extends TLFunction {
        public int[] userIds;

        public static final Creator<DeleteContacts> CREATOR = new Creator<DeleteContacts>() {

            @Override public DeleteContacts createFromParcel(Parcel in) {
                DeleteContacts data = new DeleteContacts();

                data.userIds = in.createIntArray();

                return data;
            }

            @Override public DeleteContacts[] newArray(int size) {
                return new DeleteContacts[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeIntArray(userIds);
        }
	
        public DeleteContacts() {
        }

        public DeleteContacts(int[] userIds) {
            this.userIds = userIds;
        }

        public static final int CONSTRUCTOR = 641913511;

        @Override
        public int getConstructor() {
            return 641913511;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("DeleteContacts").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userIds").append(" = ").append("int[]").append(" {").append(Arrays.toString(userIds)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class DeleteMessages extends TLFunction {
        public long chatId;
        public int[] messageIds;

        public static final Creator<DeleteMessages> CREATOR = new Creator<DeleteMessages>() {

            @Override public DeleteMessages createFromParcel(Parcel in) {
                DeleteMessages data = new DeleteMessages();

                data.chatId = in.readLong();
                data.messageIds = in.createIntArray();

                return data;
            }

            @Override public DeleteMessages[] newArray(int size) {
                return new DeleteMessages[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeIntArray(messageIds);
        }
	
        public DeleteMessages() {
        }

        public DeleteMessages(long chatId, int[] messageIds) {
            this.chatId = chatId;
            this.messageIds = messageIds;
        }

        public static final int CONSTRUCTOR = 1789583863;

        @Override
        public int getConstructor() {
            return 1789583863;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("DeleteMessages").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messageIds").append(" = ").append("int[]").append(" {").append(Arrays.toString(messageIds)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class DeleteProfilePhoto extends TLFunction {
        public long profilePhotoId;

        public static final Creator<DeleteProfilePhoto> CREATOR = new Creator<DeleteProfilePhoto>() {

            @Override public DeleteProfilePhoto createFromParcel(Parcel in) {
                DeleteProfilePhoto data = new DeleteProfilePhoto();

                data.profilePhotoId = in.readLong();

                return data;
            }

            @Override public DeleteProfilePhoto[] newArray(int size) {
                return new DeleteProfilePhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(profilePhotoId);
        }
	
        public DeleteProfilePhoto() {
        }

        public DeleteProfilePhoto(long profilePhotoId) {
            this.profilePhotoId = profilePhotoId;
        }

        public static final int CONSTRUCTOR = -564878026;

        @Override
        public int getConstructor() {
            return -564878026;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("DeleteProfilePhoto").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("profilePhotoId").append(" = ").append(profilePhotoId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class DownloadFile extends TLFunction {
        public int fileId;

        public static final Creator<DownloadFile> CREATOR = new Creator<DownloadFile>() {

            @Override public DownloadFile createFromParcel(Parcel in) {
                DownloadFile data = new DownloadFile();

                data.fileId = in.readInt();

                return data;
            }

            @Override public DownloadFile[] newArray(int size) {
                return new DownloadFile[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(fileId);
        }
	
        public DownloadFile() {
        }

        public DownloadFile(int fileId) {
            this.fileId = fileId;
        }

        public static final int CONSTRUCTOR = 888468545;

        @Override
        public int getConstructor() {
            return 888468545;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("DownloadFile").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fileId").append(" = ").append(fileId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ForwardMessages extends TLFunction {
        public long chatId;
        public long fromChatId;
        public int[] messageIds;

        public static final Creator<ForwardMessages> CREATOR = new Creator<ForwardMessages>() {

            @Override public ForwardMessages createFromParcel(Parcel in) {
                ForwardMessages data = new ForwardMessages();

                data.chatId = in.readLong();
                data.fromChatId = in.readLong();
                data.messageIds = in.createIntArray();

                return data;
            }

            @Override public ForwardMessages[] newArray(int size) {
                return new ForwardMessages[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeLong(fromChatId);
            dest.writeIntArray(messageIds);
        }
	
        public ForwardMessages() {
        }

        public ForwardMessages(long chatId, long fromChatId, int[] messageIds) {
            this.chatId = chatId;
            this.fromChatId = fromChatId;
            this.messageIds = messageIds;
        }

        public static final int CONSTRUCTOR = 489073125;

        @Override
        public int getConstructor() {
            return 489073125;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ForwardMessages").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fromChatId").append(" = ").append(fromChatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messageIds").append(" = ").append("int[]").append(" {").append(Arrays.toString(messageIds)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetAuthState extends TLFunction {

        public static final Creator<GetAuthState> CREATOR = new Creator<GetAuthState>() {

            @Override public GetAuthState createFromParcel(Parcel in) {
                GetAuthState data = new GetAuthState();

        
                return data;
            }

            @Override public GetAuthState[] newArray(int size) {
                return new GetAuthState[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public GetAuthState() {
        }

        public static final int CONSTRUCTOR = 1193342487;

        @Override
        public int getConstructor() {
            return 1193342487;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetAuthState").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetChat extends TLFunction {
        public long chatId;

        public static final Creator<GetChat> CREATOR = new Creator<GetChat>() {

            @Override public GetChat createFromParcel(Parcel in) {
                GetChat data = new GetChat();

                data.chatId = in.readLong();

                return data;
            }

            @Override public GetChat[] newArray(int size) {
                return new GetChat[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
        }
	
        public GetChat() {
        }

        public GetChat(long chatId) {
            this.chatId = chatId;
        }

        public static final int CONSTRUCTOR = -1645526841;

        @Override
        public int getConstructor() {
            return -1645526841;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetChat").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetChatHistory extends TLFunction {
        public long chatId;
        public int fromId;
        public int offset;
        public int limit;

        public static final Creator<GetChatHistory> CREATOR = new Creator<GetChatHistory>() {

            @Override public GetChatHistory createFromParcel(Parcel in) {
                GetChatHistory data = new GetChatHistory();

                data.chatId = in.readLong();
                data.fromId = in.readInt();
                data.offset = in.readInt();
                data.limit = in.readInt();

                return data;
            }

            @Override public GetChatHistory[] newArray(int size) {
                return new GetChatHistory[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(fromId);
            dest.writeInt(offset);
            dest.writeInt(limit);
        }
	
        public GetChatHistory() {
        }

        public GetChatHistory(long chatId, int fromId, int offset, int limit) {
            this.chatId = chatId;
            this.fromId = fromId;
            this.offset = offset;
            this.limit = limit;
        }

        public static final int CONSTRUCTOR = 1089149649;

        @Override
        public int getConstructor() {
            return 1089149649;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetChatHistory").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fromId").append(" = ").append(fromId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("offset").append(" = ").append(offset).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("limit").append(" = ").append(limit).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetChats extends TLFunction {
        public int offset;
        public int limit;

        public static final Creator<GetChats> CREATOR = new Creator<GetChats>() {

            @Override public GetChats createFromParcel(Parcel in) {
                GetChats data = new GetChats();

                data.offset = in.readInt();
                data.limit = in.readInt();

                return data;
            }

            @Override public GetChats[] newArray(int size) {
                return new GetChats[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(offset);
            dest.writeInt(limit);
        }
	
        public GetChats() {
        }

        public GetChats(int offset, int limit) {
            this.offset = offset;
            this.limit = limit;
        }

        public static final int CONSTRUCTOR = -446917367;

        @Override
        public int getConstructor() {
            return -446917367;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetChats").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("offset").append(" = ").append(offset).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("limit").append(" = ").append(limit).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetContacts extends TLFunction {

        public static final Creator<GetContacts> CREATOR = new Creator<GetContacts>() {

            @Override public GetContacts createFromParcel(Parcel in) {
                GetContacts data = new GetContacts();

        
                return data;
            }

            @Override public GetContacts[] newArray(int size) {
                return new GetContacts[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public GetContacts() {
        }

        public static final int CONSTRUCTOR = 854387241;

        @Override
        public int getConstructor() {
            return 854387241;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetContacts").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetFile extends TLFunction {
        public int fileId;

        public static final Creator<GetFile> CREATOR = new Creator<GetFile>() {

            @Override public GetFile createFromParcel(Parcel in) {
                GetFile data = new GetFile();

                data.fileId = in.readInt();

                return data;
            }

            @Override public GetFile[] newArray(int size) {
                return new GetFile[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(fileId);
        }
	
        public GetFile() {
        }

        public GetFile(int fileId) {
            this.fileId = fileId;
        }

        public static final int CONSTRUCTOR = -225569621;

        @Override
        public int getConstructor() {
            return -225569621;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetFile").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fileId").append(" = ").append(fileId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetGroupChat extends TLFunction {
        public int groupChatId;

        public static final Creator<GetGroupChat> CREATOR = new Creator<GetGroupChat>() {

            @Override public GetGroupChat createFromParcel(Parcel in) {
                GetGroupChat data = new GetGroupChat();

                data.groupChatId = in.readInt();

                return data;
            }

            @Override public GetGroupChat[] newArray(int size) {
                return new GetGroupChat[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(groupChatId);
        }
	
        public GetGroupChat() {
        }

        public GetGroupChat(int groupChatId) {
            this.groupChatId = groupChatId;
        }

        public static final int CONSTRUCTOR = 752932470;

        @Override
        public int getConstructor() {
            return 752932470;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetGroupChat").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("groupChatId").append(" = ").append(groupChatId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetGroupChatFull extends TLFunction {
        public int groupChatId;

        public static final Creator<GetGroupChatFull> CREATOR = new Creator<GetGroupChatFull>() {

            @Override public GetGroupChatFull createFromParcel(Parcel in) {
                GetGroupChatFull data = new GetGroupChatFull();

                data.groupChatId = in.readInt();

                return data;
            }

            @Override public GetGroupChatFull[] newArray(int size) {
                return new GetGroupChatFull[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(groupChatId);
        }
	
        public GetGroupChatFull() {
        }

        public GetGroupChatFull(int groupChatId) {
            this.groupChatId = groupChatId;
        }

        public static final int CONSTRUCTOR = 1598493541;

        @Override
        public int getConstructor() {
            return 1598493541;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetGroupChatFull").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("groupChatId").append(" = ").append(groupChatId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetMe extends TLFunction {

        public static final Creator<GetMe> CREATOR = new Creator<GetMe>() {

            @Override public GetMe createFromParcel(Parcel in) {
                GetMe data = new GetMe();

        
                return data;
            }

            @Override public GetMe[] newArray(int size) {
                return new GetMe[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public GetMe() {
        }

        public static final int CONSTRUCTOR = -191516033;

        @Override
        public int getConstructor() {
            return -191516033;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetMe").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetMessage extends TLFunction {
        public long chatId;
        public int messageId;

        public static final Creator<GetMessage> CREATOR = new Creator<GetMessage>() {

            @Override public GetMessage createFromParcel(Parcel in) {
                GetMessage data = new GetMessage();

                data.chatId = in.readLong();
                data.messageId = in.readInt();

                return data;
            }

            @Override public GetMessage[] newArray(int size) {
                return new GetMessage[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(messageId);
        }
	
        public GetMessage() {
        }

        public GetMessage(long chatId, int messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        public static final int CONSTRUCTOR = -1209218520;

        @Override
        public int getConstructor() {
            return -1209218520;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetMessage").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("messageId").append(" = ").append(messageId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetNotificationSettings extends TLFunction {
        public NotificationSettingsScope scope;

        public static final Creator<GetNotificationSettings> CREATOR = new Creator<GetNotificationSettings>() {

            @Override public GetNotificationSettings createFromParcel(Parcel in) {
                GetNotificationSettings data = new GetNotificationSettings();

                data.scope = in.readParcelable(NotificationSettingsScope.class.getClassLoader());

                return data;
            }

            @Override public GetNotificationSettings[] newArray(int size) {
                return new GetNotificationSettings[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.scope, flags);
        }
	
        public GetNotificationSettings() {
        }

        public GetNotificationSettings(NotificationSettingsScope scope) {
            this.scope = scope;
        }

        public static final int CONSTRUCTOR = 907144391;

        @Override
        public int getConstructor() {
            return 907144391;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetNotificationSettings").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("scope").append(" = "); scope.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetOption extends TLFunction {
        public String name;

        public static final Creator<GetOption> CREATOR = new Creator<GetOption>() {

            @Override public GetOption createFromParcel(Parcel in) {
                GetOption data = new GetOption();

                data.name = in.readString();

                return data;
            }

            @Override public GetOption[] newArray(int size) {
                return new GetOption[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
        }
	
        public GetOption() {
        }

        public GetOption(String name) {
            this.name = name;
        }

        public static final int CONSTRUCTOR = -1572495746;

        @Override
        public int getConstructor() {
            return -1572495746;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetOption").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("name").append(" = ").append(name).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetStickerSet extends TLFunction {
        public long setId;

        public static final Creator<GetStickerSet> CREATOR = new Creator<GetStickerSet>() {

            @Override public GetStickerSet createFromParcel(Parcel in) {
                GetStickerSet data = new GetStickerSet();

                data.setId = in.readLong();

                return data;
            }

            @Override public GetStickerSet[] newArray(int size) {
                return new GetStickerSet[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(setId);
        }
	
        public GetStickerSet() {
        }

        public GetStickerSet(long setId) {
            this.setId = setId;
        }

        public static final int CONSTRUCTOR = 1684803767;

        @Override
        public int getConstructor() {
            return 1684803767;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetStickerSet").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("setId").append(" = ").append(setId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetStickerSets extends TLFunction {
        public boolean onlyEnabled;

        public static final Creator<GetStickerSets> CREATOR = new Creator<GetStickerSets>() {

            @Override public GetStickerSets createFromParcel(Parcel in) {
                GetStickerSets data = new GetStickerSets();

                data.onlyEnabled = in.readInt() != 0;

                return data;
            }

            @Override public GetStickerSets[] newArray(int size) {
                return new GetStickerSets[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.onlyEnabled ? 1 : 0);
        }
	
        public GetStickerSets() {
        }

        public GetStickerSets(boolean onlyEnabled) {
            this.onlyEnabled = onlyEnabled;
        }

        public static final int CONSTRUCTOR = -700104885;

        @Override
        public int getConstructor() {
            return -700104885;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetStickerSets").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("onlyEnabled").append(" = ").append(onlyEnabled).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetStickers extends TLFunction {
        public String emoji;

        public static final Creator<GetStickers> CREATOR = new Creator<GetStickers>() {

            @Override public GetStickers createFromParcel(Parcel in) {
                GetStickers data = new GetStickers();

                data.emoji = in.readString();

                return data;
            }

            @Override public GetStickers[] newArray(int size) {
                return new GetStickers[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(emoji);
        }
	
        public GetStickers() {
        }

        public GetStickers(String emoji) {
            this.emoji = emoji;
        }

        public static final int CONSTRUCTOR = 1712531528;

        @Override
        public int getConstructor() {
            return 1712531528;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetStickers").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("emoji").append(" = ").append(emoji).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetUser extends TLFunction {
        public int userId;

        public static final Creator<GetUser> CREATOR = new Creator<GetUser>() {

            @Override public GetUser createFromParcel(Parcel in) {
                GetUser data = new GetUser();

                data.userId = in.readInt();

                return data;
            }

            @Override public GetUser[] newArray(int size) {
                return new GetUser[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userId);
        }
	
        public GetUser() {
        }

        public GetUser(int userId) {
            this.userId = userId;
        }

        public static final int CONSTRUCTOR = -501534519;

        @Override
        public int getConstructor() {
            return -501534519;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetUser").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetUserFull extends TLFunction {
        public int userId;

        public static final Creator<GetUserFull> CREATOR = new Creator<GetUserFull>() {

            @Override public GetUserFull createFromParcel(Parcel in) {
                GetUserFull data = new GetUserFull();

                data.userId = in.readInt();

                return data;
            }

            @Override public GetUserFull[] newArray(int size) {
                return new GetUserFull[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userId);
        }
	
        public GetUserFull() {
        }

        public GetUserFull(int userId) {
            this.userId = userId;
        }

        public static final int CONSTRUCTOR = -1977480168;

        @Override
        public int getConstructor() {
            return -1977480168;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetUserFull").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class GetUserProfilePhotos extends TLFunction {
        public int userId;
        public int offset;
        public int limit;

        public static final Creator<GetUserProfilePhotos> CREATOR = new Creator<GetUserProfilePhotos>() {

            @Override public GetUserProfilePhotos createFromParcel(Parcel in) {
                GetUserProfilePhotos data = new GetUserProfilePhotos();

                data.userId = in.readInt();
                data.offset = in.readInt();
                data.limit = in.readInt();

                return data;
            }

            @Override public GetUserProfilePhotos[] newArray(int size) {
                return new GetUserProfilePhotos[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userId);
            dest.writeInt(offset);
            dest.writeInt(limit);
        }
	
        public GetUserProfilePhotos() {
        }

        public GetUserProfilePhotos(int userId, int offset, int limit) {
            this.userId = userId;
            this.offset = offset;
            this.limit = limit;
        }

        public static final int CONSTRUCTOR = 1810450184;

        @Override
        public int getConstructor() {
            return 1810450184;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("GetUserProfilePhotos").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("offset").append(" = ").append(offset).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("limit").append(" = ").append(limit).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ImportContacts extends TLFunction {
        public InputContact[] inputContacts;

        public static final Creator<ImportContacts> CREATOR = new Creator<ImportContacts>() {

            @Override public ImportContacts createFromParcel(Parcel in) {
                ImportContacts data = new ImportContacts();

                data.inputContacts = in.createTypedArray(InputContact.CREATOR);

                return data;
            }

            @Override public ImportContacts[] newArray(int size) {
                return new ImportContacts[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedArray(this.inputContacts, flags);
        }
	
        public ImportContacts() {
        }

        public ImportContacts(InputContact[] inputContacts) {
            this.inputContacts = inputContacts;
        }

        public static final int CONSTRUCTOR = 577501086;

        @Override
        public int getConstructor() {
            return 577501086;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ImportContacts").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("inputContacts").append(" = ").append("InputContact[]").append(" {").append(Arrays.toString(inputContacts)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class RecoverAuthPassword extends TLFunction {
        public String recoverCode;

        public static final Creator<RecoverAuthPassword> CREATOR = new Creator<RecoverAuthPassword>() {

            @Override public RecoverAuthPassword createFromParcel(Parcel in) {
                RecoverAuthPassword data = new RecoverAuthPassword();

                data.recoverCode = in.readString();

                return data;
            }

            @Override public RecoverAuthPassword[] newArray(int size) {
                return new RecoverAuthPassword[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(recoverCode);
        }
	
        public RecoverAuthPassword() {
        }

        public RecoverAuthPassword(String recoverCode) {
            this.recoverCode = recoverCode;
        }

        public static final int CONSTRUCTOR = 130965839;

        @Override
        public int getConstructor() {
            return 130965839;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("RecoverAuthPassword").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("recoverCode").append(" = ").append(recoverCode).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class RequestAuthPasswordRecovery extends TLFunction {

        public static final Creator<RequestAuthPasswordRecovery> CREATOR = new Creator<RequestAuthPasswordRecovery>() {

            @Override public RequestAuthPasswordRecovery createFromParcel(Parcel in) {
                RequestAuthPasswordRecovery data = new RequestAuthPasswordRecovery();

        
                return data;
            }

            @Override public RequestAuthPasswordRecovery[] newArray(int size) {
                return new RequestAuthPasswordRecovery[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public RequestAuthPasswordRecovery() {
        }

        public static final int CONSTRUCTOR = -1561685090;

        @Override
        public int getConstructor() {
            return -1561685090;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("RequestAuthPasswordRecovery").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class ResetAuth extends TLFunction {
        public boolean force;

        public static final Creator<ResetAuth> CREATOR = new Creator<ResetAuth>() {

            @Override public ResetAuth createFromParcel(Parcel in) {
                ResetAuth data = new ResetAuth();

                data.force = in.readInt() != 0;

                return data;
            }

            @Override public ResetAuth[] newArray(int size) {
                return new ResetAuth[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.force ? 1 : 0);
        }
	
        public ResetAuth() {
        }

        public ResetAuth(boolean force) {
            this.force = force;
        }

        public static final int CONSTRUCTOR = -78661379;

        @Override
        public int getConstructor() {
            return -78661379;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("ResetAuth").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("force").append(" = ").append(force).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchMessages extends TLFunction {
        public long chatId;
        public String query;
        public int fromId;
        public int limit;
        public SearchMessagesFilter filter;

        public static final Creator<SearchMessages> CREATOR = new Creator<SearchMessages>() {

            @Override public SearchMessages createFromParcel(Parcel in) {
                SearchMessages data = new SearchMessages();

                data.chatId = in.readLong();
                data.query = in.readString();
                data.fromId = in.readInt();
                data.limit = in.readInt();
                data.filter = in.readParcelable(SearchMessagesFilter.class.getClassLoader());

                return data;
            }

            @Override public SearchMessages[] newArray(int size) {
                return new SearchMessages[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeString(query);
            dest.writeInt(fromId);
            dest.writeInt(limit);
            dest.writeParcelable(this.filter, flags);
        }
	
        public SearchMessages() {
        }

        public SearchMessages(long chatId, String query, int fromId, int limit, SearchMessagesFilter filter) {
            this.chatId = chatId;
            this.query = query;
            this.fromId = fromId;
            this.limit = limit;
            this.filter = filter;
        }

        public static final int CONSTRUCTOR = 65456226;

        @Override
        public int getConstructor() {
            return 65456226;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchMessages").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("query").append(" = ").append(query).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("fromId").append(" = ").append(fromId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("limit").append(" = ").append(limit).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("filter").append(" = "); filter.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchStickerSet extends TLFunction {
        public String name;

        public static final Creator<SearchStickerSet> CREATOR = new Creator<SearchStickerSet>() {

            @Override public SearchStickerSet createFromParcel(Parcel in) {
                SearchStickerSet data = new SearchStickerSet();

                data.name = in.readString();

                return data;
            }

            @Override public SearchStickerSet[] newArray(int size) {
                return new SearchStickerSet[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
        }
	
        public SearchStickerSet() {
        }

        public SearchStickerSet(String name) {
            this.name = name;
        }

        public static final int CONSTRUCTOR = 1157930222;

        @Override
        public int getConstructor() {
            return 1157930222;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchStickerSet").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("name").append(" = ").append(name).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SearchUser extends TLFunction {
        public String username;

        public static final Creator<SearchUser> CREATOR = new Creator<SearchUser>() {

            @Override public SearchUser createFromParcel(Parcel in) {
                SearchUser data = new SearchUser();

                data.username = in.readString();

                return data;
            }

            @Override public SearchUser[] newArray(int size) {
                return new SearchUser[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(username);
        }
	
        public SearchUser() {
        }

        public SearchUser(String username) {
            this.username = username;
        }

        public static final int CONSTRUCTOR = 1973882035;

        @Override
        public int getConstructor() {
            return 1973882035;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SearchUser").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("username").append(" = ").append(username).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendBotStartMessage extends TLFunction {
        public int botUserId;
        public long chatId;
        public String parameter;

        public static final Creator<SendBotStartMessage> CREATOR = new Creator<SendBotStartMessage>() {

            @Override public SendBotStartMessage createFromParcel(Parcel in) {
                SendBotStartMessage data = new SendBotStartMessage();

                data.botUserId = in.readInt();
                data.chatId = in.readLong();
                data.parameter = in.readString();

                return data;
            }

            @Override public SendBotStartMessage[] newArray(int size) {
                return new SendBotStartMessage[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(botUserId);
            dest.writeLong(chatId);
            dest.writeString(parameter);
        }
	
        public SendBotStartMessage() {
        }

        public SendBotStartMessage(int botUserId, long chatId, String parameter) {
            this.botUserId = botUserId;
            this.chatId = chatId;
            this.parameter = parameter;
        }

        public static final int CONSTRUCTOR = -952608730;

        @Override
        public int getConstructor() {
            return -952608730;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendBotStartMessage").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("botUserId").append(" = ").append(botUserId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("parameter").append(" = ").append(parameter).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendChatAction extends TLFunction {
        public long chatId;
        public SendMessageAction action;

        public static final Creator<SendChatAction> CREATOR = new Creator<SendChatAction>() {

            @Override public SendChatAction createFromParcel(Parcel in) {
                SendChatAction data = new SendChatAction();

                data.chatId = in.readLong();
                data.action = in.readParcelable(SendMessageAction.class.getClassLoader());

                return data;
            }

            @Override public SendChatAction[] newArray(int size) {
                return new SendChatAction[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeParcelable(this.action, flags);
        }
	
        public SendChatAction() {
        }

        public SendChatAction(long chatId, SendMessageAction action) {
            this.chatId = chatId;
            this.action = action;
        }

        public static final int CONSTRUCTOR = 346586363;

        @Override
        public int getConstructor() {
            return 346586363;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendChatAction").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("action").append(" = "); action.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SendMessage extends TLFunction {
        public long chatId;
        public int replyToMessageId;
        public boolean disableWebPagePreview;
        public ReplyMarkup replyMarkup;
        public InputMessageContent message;

        public static final Creator<SendMessage> CREATOR = new Creator<SendMessage>() {

            @Override public SendMessage createFromParcel(Parcel in) {
                SendMessage data = new SendMessage();

                data.chatId = in.readLong();
                data.replyToMessageId = in.readInt();
                data.disableWebPagePreview = in.readInt() != 0;
                data.replyMarkup = in.readParcelable(ReplyMarkup.class.getClassLoader());
                data.message = in.readParcelable(InputMessageContent.class.getClassLoader());

                return data;
            }

            @Override public SendMessage[] newArray(int size) {
                return new SendMessage[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(chatId);
            dest.writeInt(replyToMessageId);
            dest.writeInt(this.disableWebPagePreview ? 1 : 0);
            dest.writeParcelable(this.replyMarkup, flags);
            dest.writeParcelable(this.message, flags);
        }
	
        public SendMessage() {
        }

        public SendMessage(long chatId, int replyToMessageId, boolean disableWebPagePreview, ReplyMarkup replyMarkup, InputMessageContent message) {
            this.chatId = chatId;
            this.replyToMessageId = replyToMessageId;
            this.disableWebPagePreview = disableWebPagePreview;
            this.replyMarkup = replyMarkup;
            this.message = message;
        }

        public static final int CONSTRUCTOR = 1724012230;

        @Override
        public int getConstructor() {
            return 1724012230;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SendMessage").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("chatId").append(" = ").append(chatId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("replyToMessageId").append(" = ").append(replyToMessageId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("disableWebPagePreview").append(" = ").append(disableWebPagePreview).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("replyMarkup").append(" = "); replyMarkup.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("message").append(" = "); message.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SetAuthBotToken extends TLFunction {
        public String token;

        public static final Creator<SetAuthBotToken> CREATOR = new Creator<SetAuthBotToken>() {

            @Override public SetAuthBotToken createFromParcel(Parcel in) {
                SetAuthBotToken data = new SetAuthBotToken();

                data.token = in.readString();

                return data;
            }

            @Override public SetAuthBotToken[] newArray(int size) {
                return new SetAuthBotToken[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(token);
        }
	
        public SetAuthBotToken() {
        }

        public SetAuthBotToken(String token) {
            this.token = token;
        }

        public static final int CONSTRUCTOR = 746751619;

        @Override
        public int getConstructor() {
            return 746751619;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SetAuthBotToken").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("token").append(" = ").append(token).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SetAuthCode extends TLFunction {
        public String code;

        public static final Creator<SetAuthCode> CREATOR = new Creator<SetAuthCode>() {

            @Override public SetAuthCode createFromParcel(Parcel in) {
                SetAuthCode data = new SetAuthCode();

                data.code = in.readString();

                return data;
            }

            @Override public SetAuthCode[] newArray(int size) {
                return new SetAuthCode[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(code);
        }
	
        public SetAuthCode() {
        }

        public SetAuthCode(String code) {
            this.code = code;
        }

        public static final int CONSTRUCTOR = -1819184790;

        @Override
        public int getConstructor() {
            return -1819184790;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SetAuthCode").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("code").append(" = ").append(code).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SetAuthName extends TLFunction {
        public String firstName;
        public String lastName;

        public static final Creator<SetAuthName> CREATOR = new Creator<SetAuthName>() {

            @Override public SetAuthName createFromParcel(Parcel in) {
                SetAuthName data = new SetAuthName();

                data.firstName = in.readString();
                data.lastName = in.readString();

                return data;
            }

            @Override public SetAuthName[] newArray(int size) {
                return new SetAuthName[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(firstName);
            dest.writeString(lastName);
        }
	
        public SetAuthName() {
        }

        public SetAuthName(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public static final int CONSTRUCTOR = 2074658130;

        @Override
        public int getConstructor() {
            return 2074658130;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SetAuthName").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("firstName").append(" = ").append(firstName).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("lastName").append(" = ").append(lastName).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SetAuthPhoneNumber extends TLFunction {
        public String phoneNumber;

        public static final Creator<SetAuthPhoneNumber> CREATOR = new Creator<SetAuthPhoneNumber>() {

            @Override public SetAuthPhoneNumber createFromParcel(Parcel in) {
                SetAuthPhoneNumber data = new SetAuthPhoneNumber();

                data.phoneNumber = in.readString();

                return data;
            }

            @Override public SetAuthPhoneNumber[] newArray(int size) {
                return new SetAuthPhoneNumber[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(phoneNumber);
        }
	
        public SetAuthPhoneNumber() {
        }

        public SetAuthPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public static final int CONSTRUCTOR = 1502564090;

        @Override
        public int getConstructor() {
            return 1502564090;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SetAuthPhoneNumber").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("phoneNumber").append(" = ").append(phoneNumber).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SetNotificationSettings extends TLFunction {
        public NotificationSettingsScope scope;
        public NotificationSettings notificationSettings;

        public static final Creator<SetNotificationSettings> CREATOR = new Creator<SetNotificationSettings>() {

            @Override public SetNotificationSettings createFromParcel(Parcel in) {
                SetNotificationSettings data = new SetNotificationSettings();

                data.scope = in.readParcelable(NotificationSettingsScope.class.getClassLoader());
                data.notificationSettings = in.readParcelable(NotificationSettings.class.getClassLoader());

                return data;
            }

            @Override public SetNotificationSettings[] newArray(int size) {
                return new SetNotificationSettings[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.scope, flags);
            dest.writeParcelable(this.notificationSettings, flags);
        }
	
        public SetNotificationSettings() {
        }

        public SetNotificationSettings(NotificationSettingsScope scope, NotificationSettings notificationSettings) {
            this.scope = scope;
            this.notificationSettings = notificationSettings;
        }

        public static final int CONSTRUCTOR = -134430483;

        @Override
        public int getConstructor() {
            return -134430483;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SetNotificationSettings").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("scope").append(" = "); scope.toStringBuilder(shift, s);
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("notificationSettings").append(" = "); notificationSettings.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SetOption extends TLFunction {
        public String name;
        public OptionValue value;

        public static final Creator<SetOption> CREATOR = new Creator<SetOption>() {

            @Override public SetOption createFromParcel(Parcel in) {
                SetOption data = new SetOption();

                data.name = in.readString();
                data.value = in.readParcelable(OptionValue.class.getClassLoader());

                return data;
            }

            @Override public SetOption[] newArray(int size) {
                return new SetOption[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeParcelable(this.value, flags);
        }
	
        public SetOption() {
        }

        public SetOption(String name, OptionValue value) {
            this.name = name;
            this.value = value;
        }

        public static final int CONSTRUCTOR = 2114670322;

        @Override
        public int getConstructor() {
            return 2114670322;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SetOption").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("name").append(" = ").append(name).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("value").append(" = "); value.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class SetProfilePhoto extends TLFunction {
        public String photoPath;
        public ProfilePhotoCrop crop;

        public static final Creator<SetProfilePhoto> CREATOR = new Creator<SetProfilePhoto>() {

            @Override public SetProfilePhoto createFromParcel(Parcel in) {
                SetProfilePhoto data = new SetProfilePhoto();

                data.photoPath = in.readString();
                data.crop = in.readParcelable(ProfilePhotoCrop.class.getClassLoader());

                return data;
            }

            @Override public SetProfilePhoto[] newArray(int size) {
                return new SetProfilePhoto[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(photoPath);
            dest.writeParcelable(this.crop, flags);
        }
	
        public SetProfilePhoto() {
        }

        public SetProfilePhoto(String photoPath, ProfilePhotoCrop crop) {
            this.photoPath = photoPath;
            this.crop = crop;
        }

        public static final int CONSTRUCTOR = 1367381507;

        @Override
        public int getConstructor() {
            return 1367381507;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("SetProfilePhoto").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("photoPath").append(" = ").append(photoPath).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("crop").append(" = "); crop.toStringBuilder(shift, s);
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestCallBytes extends TLFunction {
        public byte[] x;

        public static final Creator<TestCallBytes> CREATOR = new Creator<TestCallBytes>() {

            @Override public TestCallBytes createFromParcel(Parcel in) {
                TestCallBytes data = new TestCallBytes();

                data.x = in.createByteArray();

                return data;
            }

            @Override public TestCallBytes[] newArray(int size) {
                return new TestCallBytes[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeByteArray(x);
        }
	
        public TestCallBytes() {
        }

        public TestCallBytes(byte[] x) {
            this.x = x;
        }

        public static final int CONSTRUCTOR = -372062627;

        @Override
        public int getConstructor() {
            return -372062627;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestCallBytes").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("x").append(" = ").append("bytes { "); { for (byte k : x) { int b = (int)k & 255; s.append(HEX_CHARACTERS[b >> 4]).append(HEX_CHARACTERS[b & 15]).append(' '); } } s.append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestCallEmpty extends TLFunction {

        public static final Creator<TestCallEmpty> CREATOR = new Creator<TestCallEmpty>() {

            @Override public TestCallEmpty createFromParcel(Parcel in) {
                TestCallEmpty data = new TestCallEmpty();

        
                return data;
            }

            @Override public TestCallEmpty[] newArray(int size) {
                return new TestCallEmpty[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public TestCallEmpty() {
        }

        public static final int CONSTRUCTOR = 482209969;

        @Override
        public int getConstructor() {
            return 482209969;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestCallEmpty").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestCallString extends TLFunction {
        public String x;

        public static final Creator<TestCallString> CREATOR = new Creator<TestCallString>() {

            @Override public TestCallString createFromParcel(Parcel in) {
                TestCallString data = new TestCallString();

                data.x = in.readString();

                return data;
            }

            @Override public TestCallString[] newArray(int size) {
                return new TestCallString[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(x);
        }
	
        public TestCallString() {
        }

        public TestCallString(String x) {
            this.x = x;
        }

        public static final int CONSTRUCTOR = -1706148074;

        @Override
        public int getConstructor() {
            return -1706148074;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestCallString").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("x").append(" = ").append(x).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestCallVectorInt extends TLFunction {
        public int[] x;

        public static final Creator<TestCallVectorInt> CREATOR = new Creator<TestCallVectorInt>() {

            @Override public TestCallVectorInt createFromParcel(Parcel in) {
                TestCallVectorInt data = new TestCallVectorInt();

                data.x = in.createIntArray();

                return data;
            }

            @Override public TestCallVectorInt[] newArray(int size) {
                return new TestCallVectorInt[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeIntArray(x);
        }
	
        public TestCallVectorInt() {
        }

        public TestCallVectorInt(int[] x) {
            this.x = x;
        }

        public static final int CONSTRUCTOR = 1339160226;

        @Override
        public int getConstructor() {
            return 1339160226;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestCallVectorInt").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("x").append(" = ").append("int[]").append(" {").append(Arrays.toString(x)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestCallVectorIntObject extends TLFunction {
        public TestInt[] x;

        public static final Creator<TestCallVectorIntObject> CREATOR = new Creator<TestCallVectorIntObject>() {

            @Override public TestCallVectorIntObject createFromParcel(Parcel in) {
                TestCallVectorIntObject data = new TestCallVectorIntObject();

                data.x = in.createTypedArray(TestInt.CREATOR);

                return data;
            }

            @Override public TestCallVectorIntObject[] newArray(int size) {
                return new TestCallVectorIntObject[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedArray(this.x, flags);
        }
	
        public TestCallVectorIntObject() {
        }

        public TestCallVectorIntObject(TestInt[] x) {
            this.x = x;
        }

        public static final int CONSTRUCTOR = -918272776;

        @Override
        public int getConstructor() {
            return -918272776;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestCallVectorIntObject").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("x").append(" = ").append("TestInt[]").append(" {").append(Arrays.toString(x)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestCallVectorString extends TLFunction {
        public String[] x;

        public static final Creator<TestCallVectorString> CREATOR = new Creator<TestCallVectorString>() {

            @Override public TestCallVectorString createFromParcel(Parcel in) {
                TestCallVectorString data = new TestCallVectorString();

                data.x = in.createStringArray();

                return data;
            }

            @Override public TestCallVectorString[] newArray(int size) {
                return new TestCallVectorString[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeStringArray(x);
        }
	
        public TestCallVectorString() {
        }

        public TestCallVectorString(String[] x) {
            this.x = x;
        }

        public static final int CONSTRUCTOR = 1331730807;

        @Override
        public int getConstructor() {
            return 1331730807;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestCallVectorString").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("x").append(" = ").append("String[]").append(" {").append(Arrays.toString(x)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestCallVectorStringObject extends TLFunction {
        public TestString[] x;

        public static final Creator<TestCallVectorStringObject> CREATOR = new Creator<TestCallVectorStringObject>() {

            @Override public TestCallVectorStringObject createFromParcel(Parcel in) {
                TestCallVectorStringObject data = new TestCallVectorStringObject();

                data.x = in.createTypedArray(TestString.CREATOR);

                return data;
            }

            @Override public TestCallVectorStringObject[] newArray(int size) {
                return new TestCallVectorStringObject[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedArray(this.x, flags);
        }
	
        public TestCallVectorStringObject() {
        }

        public TestCallVectorStringObject(TestString[] x) {
            this.x = x;
        }

        public static final int CONSTRUCTOR = 178414472;

        @Override
        public int getConstructor() {
            return 178414472;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestCallVectorStringObject").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("x").append(" = ").append("TestString[]").append(" {").append(Arrays.toString(x)).append("}\n");
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestSquareInt extends TLFunction {
        public int x;

        public static final Creator<TestSquareInt> CREATOR = new Creator<TestSquareInt>() {

            @Override public TestSquareInt createFromParcel(Parcel in) {
                TestSquareInt data = new TestSquareInt();

                data.x = in.readInt();

                return data;
            }

            @Override public TestSquareInt[] newArray(int size) {
                return new TestSquareInt[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(x);
        }
	
        public TestSquareInt() {
        }

        public TestSquareInt(int x) {
            this.x = x;
        }

        public static final int CONSTRUCTOR = -924631417;

        @Override
        public int getConstructor() {
            return -924631417;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestSquareInt").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("x").append(" = ").append(x).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class TestTestNet extends TLFunction {

        public static final Creator<TestTestNet> CREATOR = new Creator<TestTestNet>() {

            @Override public TestTestNet createFromParcel(Parcel in) {
                TestTestNet data = new TestTestNet();

        
                return data;
            }

            @Override public TestTestNet[] newArray(int size) {
                return new TestTestNet[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
                }
	
        public TestTestNet() {
        }

        public static final int CONSTRUCTOR = -1597860997;

        @Override
        public int getConstructor() {
            return -1597860997;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("TestTestNet").append(" {\n");
            shift += 2;
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UnblockUser extends TLFunction {
        public int userId;

        public static final Creator<UnblockUser> CREATOR = new Creator<UnblockUser>() {

            @Override public UnblockUser createFromParcel(Parcel in) {
                UnblockUser data = new UnblockUser();

                data.userId = in.readInt();

                return data;
            }

            @Override public UnblockUser[] newArray(int size) {
                return new UnblockUser[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userId);
        }
	
        public UnblockUser() {
        }

        public UnblockUser(int userId) {
            this.userId = userId;
        }

        public static final int CONSTRUCTOR = -50809642;

        @Override
        public int getConstructor() {
            return -50809642;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UnblockUser").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("userId").append(" = ").append(userId).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

    public static class UpdateStickerSet extends TLFunction {
        public long setId;
        public boolean isInstalled;
        public boolean isEnabled;

        public static final Creator<UpdateStickerSet> CREATOR = new Creator<UpdateStickerSet>() {

            @Override public UpdateStickerSet createFromParcel(Parcel in) {
                UpdateStickerSet data = new UpdateStickerSet();

                data.setId = in.readLong();
                data.isInstalled = in.readInt() != 0;
                data.isEnabled = in.readInt() != 0;

                return data;
            }

            @Override public UpdateStickerSet[] newArray(int size) {
                return new UpdateStickerSet[size];
            }
        };

        @Override public int describeContents() {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(setId);
            dest.writeInt(this.isInstalled ? 1 : 0);
            dest.writeInt(this.isEnabled ? 1 : 0);
        }
	
        public UpdateStickerSet() {
        }

        public UpdateStickerSet(long setId, boolean isInstalled, boolean isEnabled) {
            this.setId = setId;
            this.isInstalled = isInstalled;
            this.isEnabled = isEnabled;
        }

        public static final int CONSTRUCTOR = 64609373;

        @Override
        public int getConstructor() {
            return 64609373;
        }

        @Override
        protected void toStringBuilder(int shift, StringBuilder s) {
            s.append("UpdateStickerSet").append(" {\n");
            shift += 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("setId").append(" = ").append(setId).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isInstalled").append(" = ").append(isInstalled).append('\n');
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("isEnabled").append(" = ").append(isEnabled).append('\n');
            shift -= 2;
            for (int i = 0; i < shift; i++) { s.append(' '); } s.append("}\n");
        }
    }

}
