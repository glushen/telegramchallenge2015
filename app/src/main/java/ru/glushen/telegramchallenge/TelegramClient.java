package ru.glushen.telegramchallenge;

import android.os.*;
import android.support.annotation.*;
import android.util.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 23.04.2015.
 */
public class TelegramClient
{
    public static abstract class ResultObtainer<T extends TdApi.TLObject> extends CloseableObtainer<Pair<TdApi.TLFunction, TdApi.TLObject>>
    {
        protected abstract void onComplete(T data);

        protected abstract boolean onError(TdApi.Error error);

        @Override protected final void onObtain(Pair<TdApi.TLFunction, TdApi.TLObject> pair)
        {
            if (pair.second instanceof TdApi.Error)
            {
                if (!onError((TdApi.Error) pair.second))
                {
                    throwNotHandledError(pair.first, (TdApi.Error) pair.second);
                }
            }
            else
            {
                //noinspection unchecked
                onComplete((T) pair.second);
            }
        }
    }

    private static void throwNotHandledError(TdApi.TLFunction function, TdApi.Error error)
    {
        if (errorObtainer != null)
        {
            errorObtainer.obtain(Pair.create(function, error));
        }
        else
        {
            throw new AssertionError("TdApi.Error isn't handled\n\nfunction: " + function + "\n\nerror: " + error);
        }
    }

    private TelegramClient()
    {
        // don't create an instance
    }

    private static String dirPath = null;
    private static String filesDirPath = null;
    private static Handler handler = null;
    private static final SparseArray<EventDispatcher<TdApi.Update>> updateDispatchers = new SparseArray<>();
    private static Client client = null;

    static boolean isRunning()
    {
        return client != null;
    }

    static void start(@NonNull String dirPath, @NonNull String filesDirPath, @NonNull final Handler handler)
    {
        if (client == null || !dirPath.equals(TelegramClient.dirPath) || !filesDirPath.equals(TelegramClient.filesDirPath))
        {
            stop();

            TG.setDir(dirPath);
            TelegramClient.dirPath = dirPath;

            TG.setFilesDir(filesDirPath);
            TelegramClient.filesDirPath = filesDirPath;

            TelegramClient.handler = handler;

            TG.setUpdatesHandler(new Client.ResultHandler()
            {
                @Override public void onResult(final TdApi.TLObject object)
                {
                    handler.post(new Runnable()
                    {
                        @Override public void run()
                        {
                            dispatchUpdate((TdApi.Update) object);
                        }
                    });
                }
            });

            client = TG.getClientInstance();

            assert client != null;

            for (Pair<TdApi.TLFunction, Client.ResultHandler> functionAndHandler : unrequested)
            {
                client.send(functionAndHandler.first, functionAndHandler.second);
            }

            unrequested.clear();
        }
    }

    static void stop()
    {
        if (client != null)
        {
            TG.stopClient();

            dirPath = null;
            handler = null;
            client = null;
        }
    }

    @Nullable private static Obtainer<Pair<TdApi.TLFunction, TdApi.Error>> errorObtainer = null;

    public static void setErrorObtainer(@Nullable Obtainer<Pair<TdApi.TLFunction, TdApi.Error>> errorObtainer)
    {
        TelegramClient.errorObtainer = errorObtainer;
    }

    private static EventDispatcher<TdApi.Update> getDispatcher(int constructor)
    {
        EventDispatcher<TdApi.Update> dispatcher = updateDispatchers.get(constructor);

        if (dispatcher == null)
        {
            dispatcher = new EventDispatcher<>();
            updateDispatchers.put(constructor, dispatcher);
        }

        return dispatcher;
    }

    public static final int ALL_UPDATES = 0;

    public static void addUpdateObtainer(int constructor, Obtainer<TdApi.Update> obtainer, boolean strongReference)
    {
        getDispatcher(constructor).addObtainer(obtainer, strongReference);
    }

    public static void removeUpdateObtainer(int constructor, Obtainer<TdApi.Update> obtainer)
    {
        getDispatcher(constructor).removeObtainer(obtainer);
    }

    public static void dispatchUpdate(TdApi.Update update)
    {
        getDispatcher(update.getConstructor()).dispatch(update);
        getDispatcher(ALL_UPDATES).dispatch(update);
    }

    private static final ArrayList<Pair<TdApi.TLFunction, Client.ResultHandler>> unrequested = new ArrayList<>();

    public static void request(TdApi.TLFunction function, final ResultObtainer<?> obtainer)
    {
        class RequestHandler implements Client.ResultHandler, Runnable
        {
            private final TdApi.TLFunction function;

            public RequestHandler(TdApi.TLFunction function)
            {
                this.function = function;
            }

            private TdApi.TLObject data;

            @Override public void onResult(TdApi.TLObject object)
            {
                this.data = object;
                handler.post(this);
            }

            @Override public void run()
            {
                if (data instanceof TdApi.Error && obtainer == null)
                {
                    throwNotHandledError(function, (TdApi.Error) data);
                }
                else if (obtainer != null)
                {
                    obtainer.obtain(Pair.create(function, data));
                }
            }
        }

        if (client != null)
        {
            client.send(function, new RequestHandler(function));
        }
        else
        {
            unrequested.add(Pair.create(function, (Client.ResultHandler) new RequestHandler(function)));
        }
    }
}
