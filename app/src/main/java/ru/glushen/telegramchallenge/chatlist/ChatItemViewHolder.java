package ru.glushen.telegramchallenge.chatlist;

import android.content.res.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.TdApi.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.message.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.message.MessageManager.ForwardType.*;
import static ru.glushen.telegramchallenge.telegramutil.DateAndTime.*;
import static ru.glushen.telegramchallenge.telegramutil.Functions.*;

/**
 * Created by Pavel Glushen on 08.05.2015.
 */
class ChatItemViewHolder extends RecyclerViewHolder<Chat>
{
    private final SmartImageView avatarView;
    private final TextView chatNameView;
    private final TextView messageLabel;
    private final TextView timestampView;
    private final TextView inboxMessageStateView;

    private Chat chat;

    public ChatItemViewHolder(ViewGroup parent)
    {
        super(parent, R.layout.list_chat_list_item);

        avatarView = (SmartImageView) itemView.findViewById(R.id.avatar);

        chatNameView = (TextView) itemView.findViewById(R.id.chatName);
        chatNameView.setTypeface(Font.getRobotoMedium(getResources()));
        
        messageLabel = (TextView) itemView.findViewById(R.id.messageLabel);
        timestampView = (TextView) itemView.findViewById(R.id.timestamp);
        inboxMessageStateView = (TextView) itemView.findViewById(R.id.inboxMessageState);
    }

    @Override protected void onBind(Chat chat)
    {
        this.chat = chat;

        bindChatNameAndPhoto();
        bindMessageOrSystemText();
        bindTimestamp();
        bindInboxMessageState();
    }

    private void bindChatNameAndPhoto()
    {
        ChatInfo chatInfo = chat.type;

        Resources resources = itemView.getResources();

        avatarView.setBitmapDrawableCreators(AvatarBitmapDrawableCreator.newInstance(chat, resources));

        chatNameView.setText(getName(chatInfo, resources));

        int chatTypeIconRes = isGroupChat(chatInfo) ? R.drawable.ic_group_wrap : 0;
        chatNameView.setCompoundDrawablesWithIntrinsicBounds(chatTypeIconRes, 0, 0, 0);
    }

    private final CloseableObtainerManager<CharSequence> messageLabelObtainerManager = new CloseableObtainerManager<CharSequence>()
    {
        @Override protected CloseableObtainer<CharSequence> onCreate()
        {
            return new CloseableObtainer<CharSequence>()
            {
                @Override protected void onObtain(CharSequence label)
                {
                    messageLabel.setText(label);
                }
            };
        }
    };

    private void bindMessageOrSystemText()
    {
        MessageManager manager = MessageManager.getInstance(chat.topMessage, chat.topMessage.forwardFromId == 0 ? NO_FORWARD : FORWARD_FIRST);
        manager.getChatListTopMessageLabel(chat, getResources(), messageLabelObtainerManager.replace());
    }

    private void bindTimestamp()
    {
        Message message = chat.topMessage;

        String dateString = getDateOrTime(itemView.getContext(), getTimeInMillis(message.date), true);
        timestampView.setText(message.date != 0 ? dateString : "");

        int timestampIconRes = 0;

        if (!isMessageFailedToSend(message))
        {
            if (!isMessageSent(message))
            {
                timestampIconRes = R.drawable.ic_clock;
            }
            else if (isOutboxMessageUnread(chat, message))
            {
                timestampIconRes = R.drawable.ic_unread;
            }
        }

        timestampView.setCompoundDrawablesWithIntrinsicBounds(timestampIconRes, 0, 0, 0);
    }

    private void bindInboxMessageState()
    {
        int backgroundRes = 0;
        String countString = "";

        if (isMessageFailedToSend(chat.topMessage))
        {
            backgroundRes = R.drawable.ic_error;
        }
        else if (chat.unreadCount > 0)
        {
            backgroundRes = R.drawable.ic_badge;
            countString = Integer.toString(chat.unreadCount);
        }

        inboxMessageStateView.setBackgroundResource(backgroundRes);
        inboxMessageStateView.setText(countString);
    }

    @Override protected void onUnbind(Chat chat)
    {
        messageLabelObtainerManager.close();
        messageLabel.setText("");
    }
}
