package ru.glushen.telegramchallenge.chatlist;

import android.os.*;
import android.support.v7.widget.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static org.drinkless.td.libcore.telegram.TdApi.*;

/**
 * Created by Pavel Glushen on 08.05.2015.
 */
class ChatListAdapter extends RecyclerView.Adapter<RecyclerViewHolder<Chat>> implements Parcelable
{
    private static final int LOAD_ITEM_COUNT = 25;

    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_PROGRESS_BAR = 1;

    private static final int CHAT_NOT_FOUND = -1;

    public final EventDispatcher<Chat> clickDispatcher = new EventDispatcher<>();
    public final EventDispatcher<Chat> longClickDispatcher = new EventDispatcher<>();

    private final ArrayList<Chat> chatArray;

    private boolean loading = false;

    private boolean everyItemLoaded = false;

    public ChatListAdapter()
    {
        chatArray = new ArrayList<>();
    }

    private ChatListAdapter(Parcel in)
    {
        chatArray = in.createTypedArrayList(Chat.CREATOR);

        everyItemLoaded = in.readInt() != 0;

        if (in.readInt() != 0)
        {
            load();
        }
    }

    public static final Creator<ChatListAdapter> CREATOR = new Creator<ChatListAdapter>()
    {
        @Override public ChatListAdapter createFromParcel(Parcel in)
        {
            return new ChatListAdapter(in);
        }

        @Override public ChatListAdapter[] newArray(int size)
        {
            return new ChatListAdapter[size];
        }
    };

    @Override public int describeContents()
    {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeTypedList(chatArray);
        dest.writeInt(everyItemLoaded ? 1 : 0);
        dest.writeInt(loading ? 1 : 0);
    }

    private final CloseableObtainer<Info.ChatAndUpdate> updateHandler = new CloseableObtainer<Info.ChatAndUpdate>()
    {
        @Override protected void onObtain(Info.ChatAndUpdate chatAndUpdate)
        {
            int chatPosition = getChatPosition(chatAndUpdate.chat);

            if (chatPosition != CHAT_NOT_FOUND)
            {
                if (chatAndUpdate.update instanceof UpdateNewMessage)
                {
                    chatArray.remove(chatPosition);
                    chatArray.add(0, chatAndUpdate.chat);
                    notifyItemChanged(chatPosition);
                    notifyItemMoved(chatPosition, 0);
                    scrollToTop();
                }
                else if (chatAndUpdate.update instanceof UpdateDeleteMessages)
                {
                    if (chatAndUpdate.chat.topMessage.message instanceof MessageDeleted)
                    {
                        chatArray.remove(chatPosition);
                        notifyItemRemoved(chatPosition);
                    }
                    else
                    {
                        chatArray.remove(chatPosition);
                        notifyItemRemoved(chatPosition);

                        int newPosition = chatPosition;

                        while (newPosition < chatArray.size() && chatArray.get(newPosition).topMessage.date > chatAndUpdate.chat.topMessage.date)
                        {
                            newPosition++;
                        }

                        if (newPosition < chatArray.size() || everyItemLoaded)
                        {
                            chatArray.add(newPosition, chatAndUpdate.chat);
                            notifyItemInserted(newPosition);
                        }
                    }
                }
                else
                {
                    chatArray.set(chatPosition, chatAndUpdate.chat);
                    notifyItemChanged(chatPosition);
                }
            }
            else if (chatAndUpdate.update instanceof UpdateNewMessage)
            {
                chatArray.add(0, chatAndUpdate.chat);
                Info.getChatUpdateDispatcher(chatAndUpdate.chat).addObtainer(updateHandler, false);
                notifyItemInserted(0);
                scrollToTop();
            }
        }
    };
    private final TelegramClient.ResultObtainer resultHandler = new TelegramClient.ResultObtainer<Chats>()
    {
        @Override protected boolean onError(TdApi.Error error)
        {
            return false;
        }

        @Override protected void onComplete(Chats chats)
        {
            Chat[] chatArray = chats.chats;

            if (chatArray.length != 0)
            {
                Collections.addAll(ChatListAdapter.this.chatArray, chatArray);
                notifyDataSetChanged();

                for (Chat chat : chatArray)
                {
                    Info.getChatUpdateDispatcher(chat).addObtainer(updateHandler, false);
                }

                loading = false;

                if (chatArray.length < LOAD_ITEM_COUNT)
                {
                    load();
                }
            }
            else
            {
                notifyEveryItemLoaded();
            }
        }
    };

    {
        Info.NEW_MESSAGE_IN_CHAT_DISPATCHER.addObtainer(updateHandler, false);
    }

    {
        setHasStableIds(true);
    }

    private int getChatPosition(Chat chat)
    {
        for (int i = 0; i < chatArray.size(); i++)
        {
            if (chat.id == chatArray.get(i).id)
            {
                return i;
            }
        }

        return CHAT_NOT_FOUND;
    }

    public void load()
    {
        if (!everyItemLoaded && !loading)
        {
            loading = true;

            TelegramClient.request(new GetChats(chatArray.size(), LOAD_ITEM_COUNT), resultHandler);
        }
    }

    public boolean isAnyItemLoaded()
    {
        return chatArray.size() > 0;
    }

    public boolean isEveryItemLoaded()
    {
        return everyItemLoaded;
    }

    private void notifyEveryItemLoaded()
    {
        if (!everyItemLoaded)
        {
            everyItemLoaded = true;

            notifyItemRemoved(chatArray.size());
        }
    }

    @Override public RecyclerViewHolder<Chat> onCreateViewHolder(ViewGroup parent, int viewType)
    {
        if (viewType == VIEW_TYPE_ITEM)
        {
            ChatItemViewHolder holder = new ChatItemViewHolder(parent);

            holder.setOnClickListener(new RecyclerViewHolder.OnClickListener<Chat>()
            {
                @Override public void onClick(RecyclerViewHolder<Chat> holder, Chat chat)
                {
                    clickDispatcher.dispatch(chat);
                }
            });

            holder.setOnLongClickListener(new RecyclerViewHolder.OnLongClickListener<Chat>()
            {
                @Override public boolean onLongClick(RecyclerViewHolder<Chat> holder, Chat chat)
                {
                    longClickDispatcher.dispatch(chat);
                    return true;
                }
            });

            return holder;
        }
        else
        {
            return new RecyclerViewHolder<>(parent, R.layout.list_all_progress_bar);
        }
    }

    @Override public void onBindViewHolder(RecyclerViewHolder<Chat> holder, int position)
    {
        if (isItem(position))
        {
            holder.bind(chatArray.get(position));
        }
    }

    private boolean isItem(int position)
    {
        return position < chatArray.size();
    }

    @Override public int getItemViewType(int position)
    {
        return isItem(position) ? VIEW_TYPE_ITEM : VIEW_TYPE_PROGRESS_BAR;
    }

    @Override public long getItemId(int position)
    {
        return isItem(position) ? chatArray.get(position).id : Long.MAX_VALUE;
    }

    @Override public int getItemCount()
    {
        return everyItemLoaded ? chatArray.size() : chatArray.size() + 1;
    }

    private final HashSet<RecyclerView> recyclerViewSet = new HashSet<>();

    @Override public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerViewSet.add(recyclerView);
    }

    @Override public void onDetachedFromRecyclerView(RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        recyclerViewSet.remove(recyclerView);
    }

    private void scrollToTop()
    {
        for (RecyclerView recyclerView : recyclerViewSet)
        {
            recyclerView.scrollToPosition(0);
        }
    }
}
