package ru.glushen.telegramchallenge.chatlist;

import android.content.res.*;
import android.graphics.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.support.v4.view.*;
import android.support.v4.widget.*;
import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.TdApi.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.music.*;
import ru.glushen.telegramchallenge.passcode.*;
import ru.glushen.telegramchallenge.profile.*;
import ru.glushen.telegramchallenge.settings.*;
import ru.glushen.telegramchallenge.telegramui.Toolbar;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static android.support.v7.widget.RecyclerView.*;
import static ru.glushen.telegramchallenge.telegramutil.Info.*;

/**
 * Created by Pavel Glushen on 01.05.2015.
 */
public class ChatListFragment extends Fragment
{
    private static final String ADAPTER = "adapter";

    public static ChatListFragment newInstance()
    {
        return new ChatListFragment();
    }

    private ChatListAdapter adapter;

    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
        {
            adapter = new ChatListAdapter();

            getChildFragmentManager().beginTransaction().replace(R.id.miniPlayer, MiniAudioPlayerFragment.newInstance()).commit();
        }
        else
        {
            adapter = savedInstanceState.getParcelable(ADAPTER);
        }

        if (adapter != null)
        {
            adapter.load(); // todo избавиться
        }
    }

    @Override public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putParcelable(ADAPTER, adapter);
    }

    private Toolbar toolbar;

    private final Obtainer<OptionData> connectionStateHandler = new Obtainer<OptionData>()
    {
        @Override public void obtain(OptionData data)
        {
            toolbar.setTitle(Functions.getConnectionStateStringRes(data.value, R.string.chatListToolbarTitle));
        }
    };

    private LinearLayoutManager layoutManager;

    private final OnScrollListener scrollListener = new OnScrollListener()
    {
        private int lastFistVisiblePosition = 0;

        @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            super.onScrolled(recyclerView, dx, dy);

            int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();

            if (lastFistVisiblePosition != firstVisiblePosition)
            {
                lastFistVisiblePosition = firstVisiblePosition;

                int visibleCount = layoutManager.getChildCount();
                int totalCount = layoutManager.getItemCount();

                if (!adapter.isEveryItemLoaded())
                {
                    if (firstVisiblePosition + visibleCount >= totalCount - 4)
                    {
                        adapter.load();
                    }
                }
                else
                {
                    recyclerView.removeOnScrollListener(this);
                }
            }
        }
    };

    private View progressBarView;
    private View placeholderView;
    private RecyclerView chatListRecyclerView;

    private final CloseableObtainer<Chat> itemClickHandler = new CloseableObtainer<Chat>()
    {
        @Override protected void onObtain(Chat chat)
        {
            Fragment fragment = MessageListFragment.newInstance(chat.id);
            getFragmentManager().beginTransaction().replace(getId(), fragment).addToBackStack(null).commit();
        }
    };
    private final CloseableObtainer<Chat> itemLongClickHandler = new CloseableObtainer<Chat>()
    {
        @Override protected void onObtain(Chat chat)
        {
            //todo
        }
    };

    private final AdapterDataObserver dataObserver = new AdapterDataObserver()
    {
        @Override public void onChanged()
        {
            boolean anyItemLoaded = adapter.isAnyItemLoaded();
            boolean everyItemLoaded = adapter.isEveryItemLoaded();

            progressBarView.setVisibility(!anyItemLoaded && !everyItemLoaded ? VISIBLE : GONE);
            placeholderView.setVisibility(!anyItemLoaded && everyItemLoaded ? VISIBLE : GONE);
            chatListRecyclerView.setVisibility(anyItemLoaded ? VISIBLE : GONE);
        }

        @Override public void onItemRangeChanged(int positionStart, int itemCount)
        {
            onChanged();
        }

        @Override public void onItemRangeInserted(int positionStart, int itemCount)
        {
            onChanged();
        }

        @Override public void onItemRangeRemoved(int positionStart, int itemCount)
        {
            onChanged();
        }

        @Override public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount)
        {
            onChanged();
        }
    };

    private LinearLayout navigationView;
    private ImageView lockButtonView;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final DrawerLayout drawerLayout = (DrawerLayout) inflater.inflate(R.layout.fragment_chat_list, container, false);

        toolbar = (Toolbar) drawerLayout.findViewById(R.id.toolbar);

        toolbar.setNavigationButton(R.drawable.ic_menu, new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        if (Passcode.isLockEnabled(getActivity()))
        {
            lockButtonView = toolbar.addMenuButton(R.drawable.ic_lock_open, new OnClickListener()
            {
                @Override public void onClick(View view)
                {
                    boolean locked = !Passcode.isLockedByUser(view.getContext());
                    Passcode.setLockedByUser(view.getContext(), locked);
                    lockButtonView.setImageResource(locked ? R.drawable.ic_lock_close : R.drawable.ic_lock_open);
                }
            });
        }

        getOptionValue(OPTION_CONNECTION_STATE, connectionStateHandler);
        getOptionDispatcher(OPTION_CONNECTION_STATE).addObtainer(connectionStateHandler, true);

        chatListRecyclerView = (RecyclerView) drawerLayout.findViewById(R.id.chatList);

        layoutManager = new LinearLayoutManager(getActivity());
        chatListRecyclerView.setLayoutManager(layoutManager);

        placeholderView = drawerLayout.findViewById(R.id.placeholder);
        progressBarView = drawerLayout.findViewById(R.id.progressBar);

        adapter.clickDispatcher.addObtainer(itemClickHandler, true);
        adapter.longClickDispatcher.addObtainer(itemLongClickHandler, true);
        adapter.registerAdapterDataObserver(dataObserver);
        dataObserver.onChanged();

        chatListRecyclerView.setAdapter(adapter);

        chatListRecyclerView.addOnScrollListener(scrollListener);

        navigationView = (LinearLayout) drawerLayout.findViewById(R.id.navigation);

        Typeface mediumTypeface = Font.getRobotoMedium(getResources());

        final TextView settingsView = (TextView) navigationView.findViewById(R.id.settings);
        settingsView.setTypeface(mediumTypeface);

        TextView logoutView = (TextView) navigationView.findViewById(R.id.logout);
        logoutView.setTypeface(mediumTypeface);
        logoutView.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View view)
            {
                LogoutDialogFragment.newInstance().show(getFragmentManager(), null);
            }
        });

        Info.getOptionValue(OPTION_MY_ID, new Obtainer<OptionData>()
        {
            @Override public void obtain(OptionData data)
            {
                final int currentUserId = ((OptionInteger) data.value).value;
                Info.getUser(currentUserId, userAndUpdateObtainer);

                settingsView.setOnClickListener(new OnClickListener()
                {
                    @Override public void onClick(View v)
                    {
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(getId(), ProfileFragment.newInstanceForUser(currentUserId)).addToBackStack(null).commit();
                    }
                });
            }
        });

        return drawerLayout;
    }

    @Override public void onResume()
    {
        super.onResume();

        if (!Passcode.isLockEnabled(getActivity()))
        {
            lockButtonView = null;
        }

        if (lockButtonView != null)
        {
            boolean locked = Passcode.isLockedByUser(getActivity());
            lockButtonView.setImageResource(locked ? R.drawable.ic_lock_close : R.drawable.ic_lock_open);
        }
    }

    @Nullable private EventDispatcher<UserAndUpdate> userAndUpdateEventDispatcher;

    private final Obtainer<UserAndUpdate> userAndUpdateObtainer = new Obtainer<UserAndUpdate>()
    {
        @Override public void obtain(UserAndUpdate userAndUpdate)
        {
            User user = userAndUpdate.user;

            if (userAndUpdate.update == null)
            {
                userAndUpdateEventDispatcher = Info.getUserUpdateDispatcher(user);
                userAndUpdateEventDispatcher.addObtainer(userAndUpdateObtainer, false);
            }

            Resources resources = navigationView.getResources();

            SmartImageView.BitmapDrawableCreator avatarCreator = AvatarBitmapDrawableCreator.newInstance(user, resources);
            ((SmartImageView) navigationView.findViewById(R.id.avatar)).setBitmapDrawableCreators(avatarCreator);

            ((TextView) navigationView.findViewById(R.id.name)).setText(Functions.getName(user, resources));

            ((TextView) navigationView.findViewById(R.id.phoneNumber)).setText("+" + user.phoneNumber);
        }
    };

    @Override public void onDestroyView()
    {
        super.onDestroyView();

        getOptionDispatcher(OPTION_CONNECTION_STATE).removeObtainer(connectionStateHandler);

        adapter.clickDispatcher.removeObtainer(itemClickHandler);
        adapter.longClickDispatcher.removeObtainer(itemLongClickHandler);

        adapter.unregisterAdapterDataObserver(dataObserver);

        if (userAndUpdateEventDispatcher != null)
        {
            userAndUpdateEventDispatcher.removeObtainer(userAndUpdateObtainer);
        }
    }
}
