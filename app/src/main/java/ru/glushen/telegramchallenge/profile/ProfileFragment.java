package ru.glushen.telegramchallenge.profile;

import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramui.Toolbar;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 18.08.2015.
 */
public class ProfileFragment extends Fragment
{
    private static final String USER_NOT_GROUP = "userNotGroup";
    private static final String USERNAME = "username";
    private static final String ITEM_ID = "itemId";

    public static ProfileFragment newInstanceForUser(int userId)
    {
        return newInstance(true, userId, null);
    }

    public static ProfileFragment newInstanceForUser(@NonNull String username)
    {
        return newInstance(true, 0, username);
    }

    public static ProfileFragment newInstanceForGroup(int groupId)
    {
        return newInstance(false, groupId, null);
    }

    private static ProfileFragment newInstance(boolean userNotGroup, int itemId, String username)
    {
        Bundle args = new Bundle();
        args.putBoolean(USER_NOT_GROUP, userNotGroup);
        args.putString(USERNAME, username);
        args.putInt(ITEM_ID, itemId);
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable private AbstractProfile profile;
    private boolean viewDestroyed;

    @Override public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        viewDestroyed = false;

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        final Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationButton(R.drawable.ic_back, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                getFragmentManager().popBackStack();
            }
        });

        final LinearLayout contentContainer = (LinearLayout) view.findViewById(R.id.contentContainer);
        final ImageView floatingButton = (ImageView) view.findViewById(R.id.floatingButton);

        final int itemId = getArguments().getInt(ITEM_ID);
        final String username = getArguments().getString(USERNAME);

        if (username != null)
        {
            Info.searchUser(username, new Obtainer<Info.UserAndUpdate>()
            {
                @Override public void obtain(Info.UserAndUpdate userAndUpdate)
                {
                    if (userAndUpdate != null)
                    {
                        initUserProfile(userAndUpdate.user.id, toolbar, floatingButton, contentContainer, inflater);
                    }
                    else
                    {
                        getFragmentManager().popBackStack();
                        Toast.makeText(toolbar.getContext(), R.string.profileNotFound, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        else if (getArguments().getBoolean(USER_NOT_GROUP))
        {
            initUserProfile(itemId, toolbar, floatingButton, contentContainer, inflater);
        }
        else
        {
            initProfile(new GroupChatProfile(), itemId, toolbar, floatingButton, contentContainer, inflater);
        }

        return view;
    }

    private void initUserProfile(final int userId, @NonNull final Toolbar toolbar, @NonNull final ImageView floatingButton, @NonNull final LinearLayout contentContainer, @NonNull final LayoutInflater inflater)
    {
        Info.getOptionValue(Info.OPTION_MY_ID, new Obtainer<Info.OptionData>()
        {
            @Override public void obtain(Info.OptionData data)
            {
                if (userId == ((TdApi.OptionInteger) data.value).value)
                {
                    initProfile(new CurrentUserProfile(), userId, toolbar, floatingButton, contentContainer, inflater);
                }
                else
                {
                    Info.getUser(userId, new Obtainer<Info.UserAndUpdate>()
                    {
                        @Override public void obtain(Info.UserAndUpdate userAndUpdate)
                        {
                            AbstractProfile currentProfile;

                            if (userAndUpdate.user.type instanceof TdApi.UserTypeBot)
                            {
                                currentProfile = new BotProfile();
                            }
                            else
                            {
                                currentProfile = new AnotherUserProfile();
                            }

                            initProfile(currentProfile, userId, toolbar, floatingButton, contentContainer, inflater);
                        }
                    });
                }
            }
        });
    }

    private void initProfile(@NonNull AbstractProfile profile, int itemId, @NonNull Toolbar toolbar, @NonNull ImageView floatingButton, @NonNull LinearLayout contentContainer, @NonNull LayoutInflater inflater)
    {
        if (!viewDestroyed)
        {
            this.profile = profile;
            profile.handle(itemId, getFragmentManager(), getId(), toolbar, floatingButton, contentContainer, inflater);
        }
    }

    @Override public void onDestroyView()
    {
        super.onDestroyView();

        viewDestroyed = true;

        if (profile != null)
        {
            profile.close();
        }
    }
}
