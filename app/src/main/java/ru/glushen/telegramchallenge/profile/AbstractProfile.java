package ru.glushen.telegramchallenge.profile;

import android.support.annotation.*;
import android.support.v4.app.*;
import android.view.*;
import android.widget.*;

import ru.glushen.telegramchallenge.telegramui.Toolbar;

/**
 * Created by Pavel Glushen on 18.08.2015.
 */
abstract class AbstractProfile
{
    protected int itemId;
    protected FragmentManager fragmentManager;
    protected int containerId;
    protected Toolbar toolbar;
    protected ImageView floatingButton;
    protected LinearLayout contentContainer;
    protected LayoutInflater inflater;

    public void handle(int itemId, @NonNull FragmentManager fragmentManager, int containerId, @NonNull Toolbar toolbar, @NonNull ImageView floatingButton, @NonNull LinearLayout contentContainer, @NonNull LayoutInflater inflater)
    {
        this.itemId = itemId;
        this.fragmentManager = fragmentManager;
        this.containerId = containerId;
        this.toolbar = toolbar;
        this.floatingButton = floatingButton;
        this.contentContainer = contentContainer;
        this.inflater = inflater;

        onHandle();
    }

    protected abstract void onHandle();

    public final void close()
    {
        onClose();
    }

    protected abstract void onClose();

    protected void startFragment(@NonNull Fragment fragment)
    {
        fragmentManager.beginTransaction().replace(containerId, fragment).addToBackStack(null).commit();
    }
}
