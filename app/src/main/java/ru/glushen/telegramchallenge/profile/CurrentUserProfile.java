package ru.glushen.telegramchallenge.profile;

import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.passcode.*;
import ru.glushen.telegramchallenge.settings.*;
import ru.glushen.telegramchallenge.telegramui.Toolbar;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 18.08.2015.
 */
class CurrentUserProfile extends AbstractUserProfile
{
    public CurrentUserProfile()
    {
        super(false);
    }

    private TextView subtitle;
    private TextView phoneNumberView;

    @Override protected void onHandleUserProfile()
    {
        toolbar.addMenuButton(R.drawable.ic_more, R.array.settingsToolbarMenu, new Toolbar.OnSelectListener()
        {
            @Override public void onSelect(int index)
            {
                switch (index)
                {
                    case 0: // Edit Name
                        fragmentManager.beginTransaction().replace(containerId, EditNameFragment.newInstance()).addToBackStack(null).commit();
                        break;
                    case 1: // Log Out
                        LogoutDialogFragment.newInstance().show(fragmentManager, null);
                        break;
                }
            }
        });

        floatingButton.setImageResource(R.drawable.ic_attach_photo);

        phoneNumberView = (TextView) item2View.findViewById(R.id.titleItem2);
        ((TextView) item2View.findViewById(R.id.subtitleItem2)).setText(R.string.settingsPhone);
        ((ImageView) item2View.findViewById(R.id.iconItem2)).setImageResource(R.drawable.ic_phone);

        View passcodeLockView = inflater.inflate(R.layout.profile_button, contentContainer, false);
        contentContainer.addView(passcodeLockView);

        ((TextView) passcodeLockView.findViewById(R.id.title)).setText(R.string.settingsPasscodeLock);
        ((ImageView) passcodeLockView.findViewById(R.id.icon)).setImageResource(R.drawable.ic_setlock);

        subtitle = (TextView) passcodeLockView.findViewById(R.id.subtitle);
        Passcode.LOCK_DISPATCHER.addObtainer(passcodeObtainer, false);
        passcodeObtainer.obtain(null);

        passcodeLockView.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                startFragment(PasscodeLockFragment.newInstance());
            }
        });

        inflater.inflate(R.layout.shadow_down, contentContainer, true);
    }

    @Override protected void onCloseUserProfile()
    {
        Passcode.LOCK_DISPATCHER.removeObtainer(passcodeObtainer);
    }

    @Override protected void onUpdate(Info.UserAndUpdate userAndUpdate)
    {
        TdApi.User user = userAndUpdate.user;

        if (user.phoneNumber != null && !user.phoneNumber.isEmpty())
        {
            phoneNumberView.setText("+" + user.phoneNumber);
        }
        else
        {
            phoneNumberView.setText(R.string.profileUserUnknownPhone);
        }
    }

    private final Obtainer<Void> passcodeObtainer = new Obtainer<Void>()
    {
        @Override public void obtain(Void nothing)
        {
            if (Passcode.isLockEnabled(subtitle.getContext()))
            {
                Passcode.Type type = Passcode.getLockType(subtitle.getContext());
                subtitle.setText(type != null ? type.getStringRes() : 0);
            }
            else
            {
                subtitle.setText(R.string.settingsPasscodeLockStateDisabled);
            }
        }
    };
}
