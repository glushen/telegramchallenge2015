package ru.glushen.telegramchallenge.profile;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.media.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static ru.glushen.telegramchallenge.telegramutil.Functions.*;

/**
 * Created by Pavel Glushen on 18.08.2015.
 */
class GroupChatProfile extends AbstractProfile
{
    private LinearLayout participantListContainerView;
    private MiniSharedMediaHolder sharedMediaHolder;

    @Override protected void onHandle()
    {
        //todo toolbar

        floatingButton.setImageResource(R.drawable.ic_attach_photo);

        View addMemberView = inflater.inflate(R.layout.profile_button, contentContainer, false);
        contentContainer.addView(addMemberView);

        ((TextView) addMemberView.findViewById(R.id.title)).setText(R.string.profileGroupAddMember);
        ((ImageView) addMemberView.findViewById(R.id.icon)).setImageResource(R.drawable.ic_add);
        addMemberView.findViewById(R.id.subtitle).setVisibility(View.GONE);

        inflater.inflate(R.layout.shadow_down, contentContainer, true);
        inflater.inflate(R.layout.shadow_up, contentContainer, true);

        sharedMediaHolder = new MiniSharedMediaHolder(contentContainer, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                if (chatId != 0)
                {
                    startFragment(MediaFragment.newInstance(chatId, MediaFragment.TAB_PHOTO_AND_VIDEO));
                }
            }
        });
        contentContainer.addView(sharedMediaHolder.itemView);

        inflater.inflate(R.layout.shadow_down, contentContainer, true);
        inflater.inflate(R.layout.shadow_up, contentContainer, true);

        View participantListView = inflater.inflate(R.layout.profile_group_participant_list, contentContainer, false);
        contentContainer.addView(participantListView);
        participantListContainerView = (LinearLayout) participantListView.findViewById(R.id.participantListContainer);

        inflater.inflate(R.layout.shadow_down, contentContainer, true);

        Info.getGroupChatFull(itemId, obtainer);
    }

    @Override protected void onClose()
    {
        if (dispatcher != null)
        {
            dispatcher.removeObtainer(obtainer);
        }

        if (groupStatusHolder != null)
        {
            groupStatusHolder.updateStatusStringDispatcher.removeObtainer(groupStatusObtainer);
        }
    }

    private EventDispatcher<Info.GroupChatAndUpdate> dispatcher;
    @Nullable private GroupStatusHolder groupStatusHolder;

    private long chatId = 0;

    private final Obtainer<Info.GroupChatAndUpdate> obtainer = new Obtainer<Info.GroupChatAndUpdate>()
    {
        @Override public void obtain(Info.GroupChatAndUpdate groupChatAndUpdate)
        {
            Resources resources = toolbar.getResources();

            TdApi.GroupChat groupChat = groupChatAndUpdate.groupChat;
            TdApi.GroupChatFull groupChatFull = groupChatAndUpdate.groupChatFull;
            TdApi.Update update = groupChatAndUpdate.update;

            if (groupChatFull == null)
            {
                throw new AssertionError("groupChatFull must not be null");
            }

            if (update == null)
            {
                dispatcher = Info.getGroupChatUpdateDispatcher(groupChat);
                dispatcher.addObtainer(this, false);

                groupStatusHolder = new GroupStatusHolder(groupChatFull, contentContainer.getContext());
                groupStatusHolder.updateStatusStringDispatcher.addObtainer(groupStatusObtainer, false);
                groupStatusHolder.dispatchStatusString();

                Info.getChatWithGroupId(groupChat.id, new Obtainer<Info.ChatAndUpdate>()
                {
                    @Override public void obtain(Info.ChatAndUpdate chatAndUpdate)
                    {
                        chatId = chatAndUpdate.chat.id;
                        sharedMediaHolder.bind(chatId);
                    }
                });
            }

            toolbar.setTitle(getName(groupChat, resources));

            if (update == null || update instanceof TdApi.UpdateChatPhoto)
            {
                toolbar.setAvatar().setBitmapDrawableCreators(AvatarBitmapDrawableCreator.newInstance(groupChat, resources));
            }

            while (participantListContainerView.getChildCount() < groupChatFull.participants.length)
            {
                inflater.inflate(R.layout.profile_group_participant, participantListContainerView, true);
            }

            if (participantListContainerView.getChildCount() > groupChatFull.participants.length)
            {
                participantListContainerView.removeViews(0, participantListContainerView.getChildCount() - groupChatFull.participants.length);
            }

            if (update == null || update instanceof TdApi.UpdateUser)
            {
                for (int i = 0; i < participantListContainerView.getChildCount(); i++)
                {
                    View child = participantListContainerView.getChildAt(i);

                    TdApi.User user = groupChatFull.participants[i].user;

                    AvatarBitmapDrawableCreator creator = AvatarBitmapDrawableCreator.newInstance(user, resources);
                    ((SmartImageView) child.findViewById(R.id.avatar)).setBitmapDrawableCreators(creator);

                    ((TextView) child.findViewById(R.id.name)).setText(getName(user, resources));

                    if (child.getTag() instanceof UserStatusHolder)
                    {
                        ((UserStatusHolder) child.getTag()).close();
                    }

                    final TextView stateView = ((TextView) child.findViewById(R.id.state));

                    UserStatusHolder userStatusHolder = new UserStatusHolder(user, child.getContext());

                    userStatusHolder.updateStatusStringDispatcher.addObtainer(new Obtainer<String>()
                    {
                        @Override public void obtain(String data)
                        {
                            stateView.setText(data);
                        }
                    }, true);

                    userStatusHolder.dispatchStatusString();

                    child.setTag(userStatusHolder);
                }
            }
        }
    };

    private final Obtainer<String> groupStatusObtainer = new Obtainer<String>()
    {
        @Override public void obtain(String data)
        {
            toolbar.setSubtitle(data);
        }
    };
}
