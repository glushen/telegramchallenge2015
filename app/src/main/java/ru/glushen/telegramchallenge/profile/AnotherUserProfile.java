package ru.glushen.telegramchallenge.profile;

import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.media.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 18.08.2015.
 */
class AnotherUserProfile extends AbstractUserProfile
{
    public AnotherUserProfile()
    {
        super(true);
    }

    private TextView phoneNumberView;
    private MiniSharedMediaHolder sharedMediaHolder;

    @Override protected void onHandleUserProfile()
    {
        //todo toolbar

        floatingButton.setImageResource(R.drawable.ic_message);
        floatingButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                if (chatId != 0)
                {
                    startFragment(MessageListFragment.newInstance(chatId));
                }
            }
        });

        phoneNumberView = (TextView) item2View.findViewById(R.id.titleItem2);
        ((TextView) item2View.findViewById(R.id.subtitleItem2)).setText(R.string.settingsPhone);
        ((ImageView) item2View.findViewById(R.id.iconItem2)).setImageResource(R.drawable.ic_phone);

        sharedMediaHolder = new MiniSharedMediaHolder(contentContainer, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                if (chatId != 0)
                {
                    startFragment(MediaFragment.newInstance(chatId, MediaFragment.TAB_PHOTO_AND_VIDEO));
                }
            }
        });
        contentContainer.addView(sharedMediaHolder.itemView);

        inflater.inflate(R.layout.shadow_down, contentContainer, true);
    }

    @Override protected void onCloseUserProfile()
    {

    }

    private long chatId = 0;

    @Override protected void onUpdate(Info.UserAndUpdate userAndUpdate)
    {
        TdApi.User user = userAndUpdate.user;

        if (user.phoneNumber != null && !user.phoneNumber.isEmpty())
        {
            phoneNumberView.setText("+" + user.phoneNumber);
        }
        else
        {
            phoneNumberView.setText(R.string.profileUserUnknownPhone);
        }

        if (userAndUpdate.update == null)
        {
            Info.getChatWithUserId(user.id, new Obtainer<Info.ChatAndUpdate>()
            {
                @Override public void obtain(Info.ChatAndUpdate chatAndUpdate)
                {
                    chatId = chatAndUpdate.chat.id;
                    sharedMediaHolder.bind(chatId);
                }
            });
        }
    }
}
