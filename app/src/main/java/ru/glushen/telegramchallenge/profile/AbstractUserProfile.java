package ru.glushen.telegramchallenge.profile;

import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static ru.glushen.telegramchallenge.telegramutil.Functions.*;

/**
 * Created by Pavel Glushen on 18.08.2015.
 */
abstract class AbstractUserProfile extends AbstractProfile
{
    private final boolean needUserFull;

    public AbstractUserProfile(boolean needUserFull)
    {
        this.needUserFull = needUserFull;
    }

    protected abstract void onUpdate(Info.UserAndUpdate userAndUpdate);

    private TextView usernameView;

    protected View item2View;

    @Override final protected void onHandle()
    {
        View twoItemView = inflater.inflate(R.layout.profile_two_item, contentContainer, false);
        contentContainer.addView(twoItemView);

        usernameView = (TextView) twoItemView.findViewById(R.id.titleItem1);
        ((TextView) twoItemView.findViewById(R.id.subtitleItem1)).setText(R.string.settingsUsername);
        ((ImageView) twoItemView.findViewById(R.id.iconItem1)).setImageResource(R.drawable.ic_user);

        item2View = twoItemView.findViewById(R.id.item2);

        inflater.inflate(R.layout.shadow_down, contentContainer, true);
        inflater.inflate(R.layout.shadow_up, contentContainer, true);

        onHandleUserProfile();

        if (needUserFull)
        {
            Info.getUserFull(itemId, obtainer);
        }
        else
        {
            Info.getUser(itemId, obtainer);
        }
    }

    protected abstract void onHandleUserProfile();

    @Override final protected void onClose()
    {
        if (dispatcher != null)
        {
            dispatcher.removeObtainer(obtainer);
        }

        if (userStatusHolder != null)
        {
            userStatusHolder.updateStatusStringDispatcher.removeObtainer(userStatusObtainer);
        }

        onCloseUserProfile();
    }

    protected abstract void onCloseUserProfile();

    private EventDispatcher<Info.UserAndUpdate> dispatcher;
    @Nullable private UserStatusHolder userStatusHolder;

    private final Obtainer<Info.UserAndUpdate> obtainer = new Obtainer<Info.UserAndUpdate>()
    {
        @Override public void obtain(Info.UserAndUpdate userAndUpdate)
        {
            TdApi.User user = userAndUpdate.user;
            TdApi.Update update = userAndUpdate.update;

            if (update == null)
            {
                dispatcher = Info.getUserUpdateDispatcher(user);
                dispatcher.addObtainer(this, false);

                userStatusHolder = new UserStatusHolder(user, contentContainer.getContext());
                userStatusHolder.updateStatusStringDispatcher.addObtainer(userStatusObtainer, false);
                userStatusHolder.dispatchStatusString();
            }

            toolbar.setTitle(getName(user, toolbar.getResources()));

            if (update == null || update instanceof TdApi.UpdateUser)
            {
                toolbar.setAvatar().setBitmapDrawableCreators(AvatarBitmapDrawableCreator.newInstance(user, toolbar.getResources()));
            }

            if (user.username != null && !user.username.isEmpty())
            {
                usernameView.setText("@" + user.username);
            }
            else
            {
                usernameView.setText(R.string.profileUserNoUsername);
            }

            onUpdate(userAndUpdate);
        }
    };

    private final Obtainer<String> userStatusObtainer = new Obtainer<String>()
    {
        @Override public void obtain(String data)
        {
            toolbar.setSubtitle(data);
        }
    };
}
