package ru.glushen.telegramchallenge.profile;

import android.os.*;
import android.support.v4.app.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.inputmethod.*;
import ru.glushen.telegramchallenge.telegramui.Toolbar;

/**
 * Created by Pavel Glushen on 18.08.2015.
 */
public class EditNameFragment extends Fragment implements View.OnClickListener, TextView.OnEditorActionListener
{
    public static EditNameFragment newInstance()
    {
        return new EditNameFragment();
    }

    private EditText firstNameView;
    private EditText lastNameView;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_edit_name, container, false);

        firstNameView = (EditText) view.findViewById(R.id.firstName);
        InputManager.from(getActivity()).showDefaultKeyboard(firstNameView);
        lastNameView = (EditText) view.findViewById(R.id.lastName);

        lastNameView.setOnEditorActionListener(this);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.profileEditName);
        toolbar.addMenuButton(R.drawable.ic_check, this);
        toolbar.setNavigationButton(R.drawable.ic_back, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                InputManager.from(getActivity()).hideDefaultKeyboard(firstNameView);
                getFragmentManager().popBackStack();
            }
        });

        return view;
    }

    @Override public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
    {
        if (actionId == EditorInfo.IME_ACTION_DONE)
        {
            check();
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override public void onClick(View v)
    {
        check();
    }

    private void check()
    {
        String firstName = firstNameView.getText().toString();
        String lastName = lastNameView.getText().toString();

        if (!firstName.isEmpty())
        {
            TelegramClient.request(new TdApi.ChangeName(firstName, lastName), null);
            InputManager.from(getActivity()).hideDefaultKeyboard(firstNameView);
            getFragmentManager().popBackStack();
        }
    }
}
