package ru.glushen.telegramchallenge.media;

import android.support.v7.widget.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 20.08.2015.
 */
class AudioAdapter extends ScrollableRecyclerViewAdapter
{
    private final long chatId;

    public final StateDispatcher<Integer> SELECTED_COUNT_DISPATCHER = new StateDispatcher<>(0, false);
    public final Set<Integer> selectedMessageIdSet = new HashSet<>();

    private final List<TdApi.Message> messageOrNullList = new ArrayList<>();

    private boolean loading = false;
    private boolean loaded = false;

    private final CloseableObtainerManager<Info.ChatSearchedMessagesInfo> addingManager = new CloseableObtainerManager<Info.ChatSearchedMessagesInfo>()
    {
        @Override protected CloseableObtainer<Info.ChatSearchedMessagesInfo> onCreate()
        {
            return new CloseableObtainer<Info.ChatSearchedMessagesInfo>()
            {
                @Override protected void onObtain(Info.ChatSearchedMessagesInfo chatSearchedMessagesInfo)
                {
                    loading = false;
                    loaded = chatSearchedMessagesInfo.fullLoaded;
                    messageOrNullList.addAll(chatSearchedMessagesInfo.messageList);
                    invalidateMessageOrNullList();
                }
            };
        }
    };

    private final Obtainer<Info.ChatAndUpdate> updateObtainer = new Obtainer<Info.ChatAndUpdate>()
    {
        @Override public void obtain(Info.ChatAndUpdate chatAndUpdate)
        {
            TdApi.Update update = chatAndUpdate.update;

            if (update instanceof TdApi.UpdateNewMessage)
            {
                TdApi.Message message = ((TdApi.UpdateNewMessage) update).message;

                if (message.message instanceof TdApi.MessageAudio)
                {
                    messageOrNullList.add(0, message);
                    invalidateMessageOrNullList();
                }
            }
            else if (update instanceof TdApi.UpdateDeleteMessages)
            {
                for (int deletedMessageId : ((TdApi.UpdateDeleteMessages) update).messages)
                {
                    ListIterator<TdApi.Message> iterator = messageOrNullList.listIterator();

                    while (iterator.hasNext())
                    {
                        TdApi.Message messageOrNull = iterator.next();

                        if (messageOrNull != null && messageOrNull.id == deletedMessageId)
                        {
                            iterator.remove();
                            selectedMessageIdSet.remove(messageOrNull.id);
                            SELECTED_COUNT_DISPATCHER.dispatch(selectedMessageIdSet.size());
                            break;
                        }
                    }
                }

                invalidateMessageOrNullList();
            }
        }
    };

    public AudioAdapter(long chatId)
    {
        this.chatId = chatId;

        loading = true;
        loaded = false;

        Info.searchAudioInChat(chatId, 0, 20, addingManager.replace());

        Info.getChat(chatId, new Obtainer<Info.ChatAndUpdate>()
        {
            @Override public void obtain(Info.ChatAndUpdate chatAndUpdate)
            {
                Info.getChatUpdateDispatcher(chatAndUpdate.chat).addObtainer(updateObtainer, false);
            }
        });
    }

    void unselectAll()
    {
        selectedMessageIdSet.clear();
        SELECTED_COUNT_DISPATCHER.dispatch(selectedMessageIdSet.size());
        notifyDataSetChanged();
    }

    private void invalidateMessageOrNullList()
    {
        for (int i = 0; i < messageOrNullList.size(); i++)
        {
            if (messageOrNullList.get(i) == null)
            {
                messageOrNullList.remove(i);
            }
        }

        for (int i = messageOrNullList.size() - 1; i >= 0; i--)
        {
            long currentTime = Functions.getTimeInMillis(messageOrNullList.get(i).date);
            long previousTime = i > 0 ? Functions.getTimeInMillis(messageOrNullList.get(i - 1).date) : 0;

            if (!DateAndTime.equalsMonths(currentTime, previousTime))
            {
                messageOrNullList.add(i, null);
            }
        }

        notifyDataSetChanged();
    }

    @Override protected void onVisibleRangeChanged(RecyclerView recyclerView)
    {
        int lastVisiblePosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();

        if (!loading && !loaded && !messageOrNullList.isEmpty() && lastVisiblePosition > messageOrNullList.size() - 5)
        {
            loading = true;
            Info.searchAudioInChat(chatId, messageOrNullList.get(messageOrNullList.size() - 1).id, 20, addingManager.replace());
        }
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return viewType == 0 ? new AudioViewHolder(parent, this) : new MonthViewHolder(parent);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof AudioViewHolder)
        {
            ((AudioViewHolder) holder).bind(messageOrNullList.get(position));
        }
        else if (holder instanceof MonthViewHolder && position < getItemCount() - 1 && messageOrNullList.get(position + 1) != null)
        {
            ((MonthViewHolder) holder).bind(Functions.getTimeInMillis(messageOrNullList.get(position + 1).date));
        }
        else
        {
            messageOrNullList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override public int getItemViewType(int position)
    {
        return messageOrNullList.get(position) != null ? 0 : 1;
    }

    @Override public int getItemCount()
    {
        return messageOrNullList.size();
    }

    {
        setHasStableIds(true);
    }

    @Override public long getItemId(int position)
    {
        TdApi.Message messageOrNull = messageOrNullList.get(position);

        if (messageOrNull != null)
        {
            return messageOrNull.id;
        }
        else if (position < getItemCount() - 1 && messageOrNullList.get(position + 1) != null)
        {
            return (1L << 32) + messageOrNullList.get(position + 1).date;
        }
        else
        {
            return 0;
        }
    }
}
