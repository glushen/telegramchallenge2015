package ru.glushen.telegramchallenge.media;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.music.*;
import ru.glushen.telegramchallenge.telegramui.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.music.AudioPlayerService.*;
import static ru.glushen.telegramchallenge.music.AudioPlayerService.State.*;
import static ru.glushen.telegramchallenge.telegramutil.FileLoader.State.*;

/**
 * Created by Pavel Glushen on 20.08.2015.
 */
public class AudioViewHolder extends RecyclerViewHolder<TdApi.Message>
{
    private final AudioAdapter audioAdapter;

    public AudioViewHolder(@NonNull ViewGroup parent, @NonNull AudioAdapter audioAdapter)
    {
        super(parent, R.layout.list_media_audio);

        this.audioAdapter = audioAdapter;

        downloadProgressView = (DeterminateProgressBar) itemView.findViewById(R.id.downloadProgress);

        stateView = (ImageView) itemView.findViewById(R.id.state);

        titleView = (TextView) itemView.findViewById(R.id.title);
        titleView.setTypeface(Font.getRobotoMedium(getResources()));

        performerNameView = (TextView) itemView.findViewById(R.id.subtitle);
    }

    @Override protected void onClick(TdApi.Message message)
    {
        if (audioAdapter.selectedMessageIdSet.isEmpty())
        {
            switch (loader.getState())
            {
                case EMPTY:
                    loader.load();
                    break;
                case LOADING:
                    loader.cancelLoad();
                    break;
                case LOADED:
                {
                    TdApi.Audio currentAudio = AUDIO_DISPATCHER.getCurrentState();
                    AudioPlayerService.State currentState = STATE_DISPATCHER.getCurrentState();

                    if (currentState != STOPPED && currentAudio != null && currentAudio.audio.id == audio.audio.id)
                    {
                        if (currentState == PLAYING)
                        {
                            pause();
                        }
                        else if (currentState == PAUSED)
                        {
                            resume();
                        }
                    }
                    else
                    {
                        play(getContext(), message);
                    }
                }
                break;
            }
        }
        else if (audioAdapter.selectedMessageIdSet.contains(message.id))
        {
            audioAdapter.selectedMessageIdSet.remove(message.id);
            audioAdapter.SELECTED_COUNT_DISPATCHER.dispatch(audioAdapter.selectedMessageIdSet.size());

            if (audioAdapter.selectedMessageIdSet.isEmpty())
            {
                audioAdapter.notifyDataSetChanged();
            }
            else
            {
                invalidateView();
            }
        }
        else
        {
            audioAdapter.selectedMessageIdSet.add(message.id);
            audioAdapter.SELECTED_COUNT_DISPATCHER.dispatch(audioAdapter.selectedMessageIdSet.size());

            invalidateView();
        }
    }

    @Override protected boolean onLongClick(TdApi.Message message)
    {
        if (audioAdapter.selectedMessageIdSet.isEmpty())
        {
            audioAdapter.selectedMessageIdSet.add(message.id);
            audioAdapter.SELECTED_COUNT_DISPATCHER.dispatch(audioAdapter.selectedMessageIdSet.size());
            audioAdapter.notifyDataSetChanged();
            return true;
        }
        else
        {
            return false;
        }
    }

    private final DeterminateProgressBar downloadProgressView;
    private final ImageView stateView;
    private final TextView titleView;
    private final TextView performerNameView;

    private final Obtainer<FileLoader> fileLoaderStateObtainer = new Obtainer<FileLoader>()
    {
        @Override public void obtain(FileLoader loader)
        {
            invalidateView();
        }
    };

    private final Obtainer<TdApi.Audio> audioObtainer = new Obtainer<TdApi.Audio>()
    {
        @Override public void obtain(TdApi.Audio audio)
        {
            invalidateView();
        }
    };

    private final Obtainer<AudioPlayerService.State> stateObtainer = new Obtainer<AudioPlayerService.State>()
    {
        @Override public void obtain(AudioPlayerService.State state)
        {
            invalidateView();
        }
    };

    private void invalidateView()
    {
        Resources resources = itemView.getResources();

        int stateImage;
        String subtitleText = audio.performer;

        switch (loader.getState())
        {
            case LOADED:
            {
                TdApi.Audio currentAudio = AUDIO_DISPATCHER.getCurrentState();

                if (currentAudio != null && currentAudio.audio.id == audio.audio.id && STATE_DISPATCHER.getCurrentState() == PLAYING)
                {
                    stateImage = R.drawable.ic_pause;
                }
                else
                {
                    stateImage = R.drawable.ic_play_wrap;
                }
            }
            break;
            case LOADING:
                stateImage = R.drawable.ic_pause_blue;
                String ready = ByteCount.toString(resources, loader.getReady());
                String size = ByteCount.toString(resources, loader.getSize());
                subtitleText = resources.getString(R.string.messageListDocumentLoading, ready, size);
                break;
            case EMPTY:
            default:
                stateImage = R.drawable.ic_download_blue;
                break;
        }

        downloadProgressView.setVisibility(View.VISIBLE);
        downloadProgressView.setMax(loader.getSize());
        downloadProgressView.setProgress(loader.getReady());
        downloadProgressView.setBackgroundCircleColor(loader.getState() == LOADED ? 0xff68ade1 : 0xfff0f6fa);
        downloadProgressView.setEnabled(loader.getState() == LOADING);

        stateView.setImageResource(stateImage);

        performerNameView.setText(subtitleText);

        //noinspection deprecation
        itemView.setBackgroundDrawable(null);

        if (!audioAdapter.selectedMessageIdSet.isEmpty())
        {
            downloadProgressView.setVisibility(View.GONE);

            if (audioAdapter.selectedMessageIdSet.contains(message.id))
            {
                stateView.setImageResource(R.drawable.ic_audio_check);
                itemView.setBackgroundColor(0xfff5f5f5);
            }
            else
            {
                stateView.setImageResource(R.drawable.ic_audio_nocheck);
            }
        }
    }

    private TdApi.Message message;
    private TdApi.Audio audio;
    private FileLoader loader;

    @Override protected void onBind(TdApi.Message message)
    {
        this.message = message;
        audio = ((TdApi.MessageAudio) message.message).audio;

        titleView.setText(audio.title);

        loader = FileLoader.getInstance(audio.audio);
        loader.getStateDispatcher().addObtainer(fileLoaderStateObtainer, false);
        fileLoaderStateObtainer.obtain(loader);

        AUDIO_DISPATCHER.addObtainerAndObtain(audioObtainer, false);
        STATE_DISPATCHER.addObtainerAndObtain(stateObtainer, false);
    }

    @Override protected void onUnbind(TdApi.Message message)
    {
        loader.getStateDispatcher().removeObtainer(fileLoaderStateObtainer);
        AUDIO_DISPATCHER.removeObtainer(audioObtainer);
        STATE_DISPATCHER.removeObtainer(stateObtainer);
    }
}
