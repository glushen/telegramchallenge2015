package ru.glushen.telegramchallenge.media;

import android.graphics.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.image.*;
import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.photo.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 20.08.2015.
 */
public class MiniSharedMediaHolder extends RecyclerViewHolder<Long>
{
    @NonNull private final TextView countView;
    @NonNull private final RecyclerView mediaListView;
    @NonNull private final LinearLayoutManager layoutManager;
    @NonNull private final Adapter adapter = new Adapter();

    public MiniSharedMediaHolder(@NonNull ViewGroup parent, @NonNull View.OnClickListener buttonClickListener)
    {
        super(parent, R.layout.profile_shared_media);

        ((ImageView) itemView.findViewById(R.id.icon)).setImageResource(R.drawable.ic_media);
        ((TextView) itemView.findViewById(R.id.title)).setText(R.string.profileSharedMedia);
        countView = ((TextView) itemView.findViewById(R.id.subtitle));
        View button = itemView.findViewById(R.id.button);
        button.setOnClickListener(buttonClickListener);
        mediaListView = ((RecyclerView) itemView.findViewById(R.id.mediaList));
        layoutManager = new LinearLayoutManager(parent.getContext(), LinearLayoutManager.HORIZONTAL, false);
        mediaListView.setLayoutManager(layoutManager);
        mediaListView.setAdapter(adapter);
        mediaListView.addItemDecoration(new RecyclerView.ItemDecoration()
        {
            @Override public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
            {
                outRect.top = outRect.bottom = 0;
                outRect.right = Screen.dpPixelSize(8f, parent.getResources().getDisplayMetrics());
                outRect.left = parent.getChildAdapterPosition(view) != 0 ? 0 : outRect.right;
            }
        });
    }

    @Override protected void onBind(Long chatId)
    {
        adapter.setChatId(chatId);
    }

    @Override protected void onUnbind(Long chatId)
    {

    }

    private static class ViewHolder extends RecyclerViewHolder<TdApi.Message>
    {
        @NonNull private final SmartImageView thumbView;
        @NonNull private final ImageView playIconView;
        @NonNull private final TextView durationView;

        public ViewHolder(@NonNull ViewGroup parent)
        {
            super(parent, R.layout.list_profile_shared_media_mini);

            thumbView = (SmartImageView) itemView.findViewById(R.id.thumb);
            playIconView = (ImageView) itemView.findViewById(R.id.playIcon);
            durationView = (TextView) itemView.findViewById(R.id.duration);
        }

        @Override protected void onBind(TdApi.Message message)
        {
            boolean messagePhoto = message.message instanceof TdApi.MessagePhoto;
            boolean messageVideo = message.message instanceof TdApi.MessageVideo;

            playIconView.setVisibility(messageVideo ? View.VISIBLE : View.GONE);
            durationView.setVisibility(messageVideo ? View.VISIBLE : View.GONE);

            if (messagePhoto)
            {
                TdApi.PhotoSize[] photoSizeArray = ((TdApi.MessagePhoto) message.message).photo.photos;
                thumbView.setBitmapDrawableCreators(PhotoBitmapDrawableCreator.newThumbAndPhotoBitmapDrawableCreators(photoSizeArray, Image.Filter.CROP_CENTER, 0, null));
            }
            else if (messageVideo)
            {
                TdApi.Video video = ((TdApi.MessageVideo) message.message).video;
                thumbView.setBitmapDrawableCreators(new FileBitmapDrawableCreator(FileLoader.getInstance(video.thumb.photo), Image.Filter.CROP_CENTER, 0));
                durationView.setText(DateAndTime.getTimerTime(Functions.getTimeInMillis(video.duration)));
            }
        }

        @Override protected void onClick(TdApi.Message message)
        {
            if (message.message instanceof TdApi.MessagePhoto)
            {
                FragmentManager fragmentManager = SingleActivity.getActivityFragmentManager();
                PhotoDialogFragment.newInstance(message).show(fragmentManager, null);
            }
            else if (message.message instanceof TdApi.MessageVideo)
            {
                FileLoader.getInstance(((TdApi.MessageVideo) message.message).video.video).open(getContext());
            }
        }
    }

    private class Adapter extends ScrollableRecyclerViewAdapter<ViewHolder>
    {
        @NonNull private final List<TdApi.Message> messageList = new ArrayList<>();

        private boolean loading = false;
        private boolean loaded = false;
        private int approximateCount;

        private void updateApproximateCount(int approximateCount)
        {
            this.approximateCount = approximateCount;
            countView.setText(Integer.toString(approximateCount));
        }

        private final CloseableObtainerManager<Info.ChatSearchedMessagesInfo> addingManager = new CloseableObtainerManager<Info.ChatSearchedMessagesInfo>()
        {
            @Override protected CloseableObtainer<Info.ChatSearchedMessagesInfo> onCreate()
            {
                return new CloseableObtainer<Info.ChatSearchedMessagesInfo>()
                {
                    @Override protected void onObtain(Info.ChatSearchedMessagesInfo chatSearchedMessagesInfo)
                    {
                        loading = false;
                        loaded = chatSearchedMessagesInfo.fullLoaded;
                        updateApproximateCount(chatSearchedMessagesInfo.approximateCount);
                        messageList.addAll(chatSearchedMessagesInfo.messageList);
                        notifyDataSetChanged();

                        mediaListView.setVisibility(!messageList.isEmpty() ? View.VISIBLE : View.GONE);
                    }
                };
            }
        };

        private final CloseableObtainerManager<Info.ChatAndUpdate> updatingManager = new CloseableObtainerManager<Info.ChatAndUpdate>()
        {
            @Override protected CloseableObtainer<Info.ChatAndUpdate> onCreate()
            {
                return new CloseableObtainer<Info.ChatAndUpdate>()
                {
                    @Override protected void onObtain(Info.ChatAndUpdate chatAndUpdate)
                    {
                        TdApi.Update update = chatAndUpdate.update;

                        if (update instanceof TdApi.UpdateNewMessage)
                        {
                            TdApi.Message message = ((TdApi.UpdateNewMessage) update).message;

                            if (message.message instanceof TdApi.MessagePhoto || message.message instanceof TdApi.MessageVideo)
                            {
                                messageList.add(0, message);
                                updateApproximateCount(approximateCount + 1);
                                notifyDataSetChanged();
                            }
                        }
                        else if (update instanceof TdApi.UpdateDeleteMessages)
                        {
                            for (int deletedMessageId : ((TdApi.UpdateDeleteMessages) update).messages)
                            {
                                ListIterator<TdApi.Message> iterator = messageList.listIterator();

                                while (iterator.hasNext())
                                {
                                    if (iterator.next().id == deletedMessageId)
                                    {
                                        iterator.remove();
                                        updateApproximateCount(approximateCount - 1);
                                        break;
                                    }
                                }
                            }

                            notifyDataSetChanged();
                        }

                        mediaListView.setVisibility(!messageList.isEmpty() ? View.VISIBLE : View.GONE);
                    }
                };
            }
        };

        private long chatId = 0;

        public void setChatId(long chatId)
        {
            if (this.chatId != chatId)
            {
                this.chatId = chatId;
                loading = true;
                loaded = false;
                mediaListView.setVisibility(View.GONE);
                Info.searchPhotoAndVideoInChat(chatId, 0, 20, addingManager.replace());

                Info.getChat(chatId, new Obtainer<Info.ChatAndUpdate>()
                {
                    @Override public void obtain(Info.ChatAndUpdate chatAndUpdate)
                    {
                        Info.getChatUpdateDispatcher(chatAndUpdate.chat).addObtainer(updatingManager.replace(), false);
                    }
                });
            }
        }

        @Override protected void onVisibleRangeChanged(RecyclerView recyclerView)
        {
            if (!loading && !loaded && !messageList.isEmpty() && layoutManager.findLastVisibleItemPosition() > messageList.size() - 5)
            {
                loading = true;
                Info.searchPhotoAndVideoInChat(chatId, messageList.get(messageList.size() - 1).id, 20, addingManager.replace());
            }
        }

        @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            return new ViewHolder(parent);
        }

        @Override public void onBindViewHolder(ViewHolder holder, int position)
        {
            holder.bind(messageList.get(position));
        }

        @Override public int getItemCount()
        {
            return messageList.size();
        }

        {
            setHasStableIds(true);
        }

        @Override public long getItemId(int position)
        {
            return messageList.get(position).id;
        }
    }
}
