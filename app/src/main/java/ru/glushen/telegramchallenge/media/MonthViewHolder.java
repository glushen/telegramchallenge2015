package ru.glushen.telegramchallenge.media;

import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 20.08.2015.
 */
class MonthViewHolder extends RecyclerViewHolder<Long>
{
    private final TextView view;

    public MonthViewHolder(@NonNull ViewGroup parent)
    {
        super(parent, R.layout.list_media_month);

        view = (TextView) itemView;

        view.setTypeface(Font.getRobotoMedium(getResources()));
    }

    @Override protected void onBind(Long timeInMillis)
    {
        view.setText(DateAndTime.getMonthAndYear(view.getContext(), timeInMillis, false));
    }
}
