package ru.glushen.telegramchallenge.media;

import android.support.annotation.*;
import android.support.v4.app.*;
import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.image.*;
import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.photo.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 20.08.2015.
 */
class PhotoAndVideoAdapter extends ScrollableRecyclerViewAdapter<RecyclerViewHolder<?>>
{
    public final StateDispatcher<Integer> SELECTED_COUNT_DISPATCHER = new StateDispatcher<>(0, false);
    public final Set<Integer> selectedMessageIdSet = new HashSet<>();

    private final long chatId;

    int columnCount = 1;

    final GridLayoutManager.SpanSizeLookup spanSizeLookup = new GridLayoutManager.SpanSizeLookup()
    {
        @Override public int getSpanSize(int position)
        {
            return messageOrNullList.get(position) == null ? columnCount : 1;
        }
    };

    private final List<TdApi.Message> messageOrNullList = new ArrayList<>();

    private boolean loading = false;
    private boolean loaded = false;

    private final CloseableObtainerManager<Info.ChatSearchedMessagesInfo> addingManager = new CloseableObtainerManager<Info.ChatSearchedMessagesInfo>()
    {
        @Override protected CloseableObtainer<Info.ChatSearchedMessagesInfo> onCreate()
        {
            return new CloseableObtainer<Info.ChatSearchedMessagesInfo>()
            {
                @Override protected void onObtain(Info.ChatSearchedMessagesInfo chatSearchedMessagesInfo)
                {
                    loading = false;
                    loaded = chatSearchedMessagesInfo.fullLoaded;
                    messageOrNullList.addAll(chatSearchedMessagesInfo.messageList);
                    invalidateMessageOrNullList();
                }
            };
        }
    };

    private final Obtainer<Info.ChatAndUpdate> updateObtainer = new Obtainer<Info.ChatAndUpdate>()
    {
        @Override public void obtain(Info.ChatAndUpdate chatAndUpdate)
        {
            TdApi.Update update = chatAndUpdate.update;

            if (update instanceof TdApi.UpdateNewMessage)
            {
                TdApi.Message message = ((TdApi.UpdateNewMessage) update).message;

                if (message.message instanceof TdApi.MessagePhoto || message.message instanceof TdApi.MessageVideo)
                {
                    messageOrNullList.add(0, message);
                    invalidateMessageOrNullList();
                }
            }
            else if (update instanceof TdApi.UpdateDeleteMessages)
            {
                for (int deletedMessageId : ((TdApi.UpdateDeleteMessages) update).messages)
                {
                    ListIterator<TdApi.Message> iterator = messageOrNullList.listIterator();

                    while (iterator.hasNext())
                    {
                        TdApi.Message messageOrNull = iterator.next();

                        if (messageOrNull != null && messageOrNull.id == deletedMessageId)
                        {
                            iterator.remove();
                            selectedMessageIdSet.remove(messageOrNull.id);
                            SELECTED_COUNT_DISPATCHER.dispatch(selectedMessageIdSet.size());
                            break;
                        }
                    }
                }

                invalidateMessageOrNullList();
            }
        }
    };

    public PhotoAndVideoAdapter(long chatId)
    {
        this.chatId = chatId;

        loading = true;
        loaded = false;

        Info.searchPhotoAndVideoInChat(chatId, 0, 20, addingManager.replace());

        Info.getChat(chatId, new Obtainer<Info.ChatAndUpdate>()
        {
            @Override public void obtain(Info.ChatAndUpdate chatAndUpdate)
            {
                Info.getChatUpdateDispatcher(chatAndUpdate.chat).addObtainer(updateObtainer, false);
            }
        });
    }

    class ViewHolder extends RecyclerViewHolder<TdApi.Message>
    {
        @NonNull private final SmartImageView thumbView;
        @NonNull private final ImageView playIconView;
        @NonNull private final TextView durationView;
        @NonNull private final ImageView checkView;

        public ViewHolder(@NonNull ViewGroup parent)
        {
            super(parent, R.layout.list_media_photo_and_video);

            thumbView = (SmartImageView) itemView.findViewById(R.id.thumb);
            playIconView = (ImageView) itemView.findViewById(R.id.playIcon);
            durationView = (TextView) itemView.findViewById(R.id.duration);
            checkView = (ImageView) itemView.findViewById(R.id.check);
        }

        @Override protected void onBind(TdApi.Message message)
        {
            boolean messagePhoto = message.message instanceof TdApi.MessagePhoto;
            boolean messageVideo = message.message instanceof TdApi.MessageVideo;

            playIconView.setVisibility(messageVideo ? View.VISIBLE : View.GONE);
            durationView.setVisibility(messageVideo ? View.VISIBLE : View.GONE);

            if (messagePhoto)
            {
                TdApi.PhotoSize[] photoSizeArray = ((TdApi.MessagePhoto) message.message).photo.photos;
                thumbView.setBitmapDrawableCreators(PhotoBitmapDrawableCreator.newThumbAndPhotoBitmapDrawableCreators(photoSizeArray, Image.Filter.CROP_CENTER, 0, null));
            }
            else if (messageVideo)
            {
                TdApi.Video video = ((TdApi.MessageVideo) message.message).video;
                thumbView.setBitmapDrawableCreators(new FileBitmapDrawableCreator(FileLoader.getInstance(video.thumb.photo), Image.Filter.CROP_CENTER, 0));
                durationView.setText(DateAndTime.getTimerTime(Functions.getTimeInMillis(video.duration)));
            }

            updateChecked(message);
        }

        private final int checkedPadding = Screen.dpPixelSize(12, getResources().getDisplayMetrics());

        private void updateChecked(TdApi.Message message)
        {
            checkView.setVisibility(!selectedMessageIdSet.isEmpty() ? View.VISIBLE : View.GONE);

            if (selectedMessageIdSet.contains(message.id))
            {
                checkView.setImageResource(R.drawable.ic_checksm);
                thumbView.setPadding(checkedPadding, checkedPadding, checkedPadding, checkedPadding);
            }
            else
            {
                checkView.setImageResource(R.drawable.ic_photo_nocheck);
                thumbView.setPadding(0, 0, 0, 0);
            }

            thumbView.invalidate();
        }

        @Override protected void onClick(TdApi.Message message)
        {
            if (selectedMessageIdSet.isEmpty())
            {
                if (message.message instanceof TdApi.MessagePhoto)
                {
                    FragmentManager fragmentManager = SingleActivity.getActivityFragmentManager();
                    PhotoDialogFragment.newInstance(message).show(fragmentManager, null);
                }
                else if (message.message instanceof TdApi.MessageVideo)
                {
                    FileLoader.getInstance(((TdApi.MessageVideo) message.message).video.video).open(getContext());
                }
            }
            else if (selectedMessageIdSet.contains(message.id))
            {
                selectedMessageIdSet.remove(message.id);
                SELECTED_COUNT_DISPATCHER.dispatch(selectedMessageIdSet.size());

                if (selectedMessageIdSet.isEmpty())
                {
                    notifyDataSetChanged();
                }
                else
                {
                    updateChecked(message);
                }
            }
            else
            {
                selectedMessageIdSet.add(message.id);
                SELECTED_COUNT_DISPATCHER.dispatch(selectedMessageIdSet.size());

                updateChecked(message);
            }
        }

        @Override protected boolean onLongClick(TdApi.Message message)
        {
            if (selectedMessageIdSet.isEmpty())
            {
                selectedMessageIdSet.add(message.id);
                SELECTED_COUNT_DISPATCHER.dispatch(selectedMessageIdSet.size());
                notifyDataSetChanged();
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    void unselectAll()
    {
        selectedMessageIdSet.clear();
        SELECTED_COUNT_DISPATCHER.dispatch(selectedMessageIdSet.size());
        notifyDataSetChanged();
    }

    private void invalidateMessageOrNullList()
    {
        for (int i = 0; i < messageOrNullList.size(); i++)
        {
            if (messageOrNullList.get(i) == null)
            {
                messageOrNullList.remove(i);
            }
        }

        for (int i = messageOrNullList.size() - 1; i >= 0; i--)
        {
            long currentTime = Functions.getTimeInMillis(messageOrNullList.get(i).date);
            long previousTime = i > 0 ? Functions.getTimeInMillis(messageOrNullList.get(i - 1).date) : 0;

            if (!DateAndTime.equalsMonths(currentTime, previousTime))
            {
                messageOrNullList.add(i, null);
            }
        }

        notifyDataSetChanged();
    }

    @Override protected void onVisibleRangeChanged(RecyclerView recyclerView)
    {
        int lastVisiblePosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();

        if (!loading && !loaded && !messageOrNullList.isEmpty() && lastVisiblePosition > messageOrNullList.size() - 5)
        {
            loading = true;
            Info.searchPhotoAndVideoInChat(chatId, messageOrNullList.get(messageOrNullList.size() - 1).id, 20, addingManager.replace());
        }
    }

    @Override public RecyclerViewHolder<?> onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return viewType == 0 ? new ViewHolder(parent) : new MonthViewHolder(parent);
    }

    @Override public void onBindViewHolder(RecyclerViewHolder<?> holder, int position)
    {
        if (holder instanceof ViewHolder)
        {
            ((ViewHolder) holder).bind(messageOrNullList.get(position));
        }
        else if (holder instanceof MonthViewHolder && position < getItemCount() - 1 && messageOrNullList.get(position + 1) != null)
        {
            ((MonthViewHolder) holder).bind(Functions.getTimeInMillis(messageOrNullList.get(position + 1).date));
        }
        else
        {
            messageOrNullList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override public int getItemViewType(int position)
    {
        return messageOrNullList.get(position) != null ? 0 : 1;
    }

    @Override public int getItemCount()
    {
        return messageOrNullList.size();
    }

    {
        setHasStableIds(true);
    }

    @Override public long getItemId(int position)
    {
        TdApi.Message messageOrNull = messageOrNullList.get(position);

        if (messageOrNull != null)
        {
            return messageOrNull.id;
        }
        else if (position < getItemCount() - 1 && messageOrNullList.get(position + 1) != null)
        {
            return (1L << 32) + messageOrNullList.get(position + 1).date;
        }
        else
        {
            return 0;
        }
    }
}
