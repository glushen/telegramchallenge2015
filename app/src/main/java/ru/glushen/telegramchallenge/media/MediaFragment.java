package ru.glushen.telegramchallenge.media;

import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.support.v7.widget.*;
import android.util.*;
import android.view.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.music.*;
import ru.glushen.telegramchallenge.telegramui.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 20.08.2015.
 */
public class MediaFragment extends Fragment
{
    private static final String CHAT_ID = "chatId";
    private static final String TAB = "tab";

    public static final int TAB_PHOTO_AND_VIDEO = 0;
    public static final int TAB_AUDIO = 1;

    public static MediaFragment newInstance(long chatId, @IntRange(from = TAB_PHOTO_AND_VIDEO, to = TAB_AUDIO) int tab)
    {
        Bundle arguments = new Bundle();
        arguments.putLong(CHAT_ID, chatId);
        arguments.putInt(TAB, tab);
        MediaFragment fragment = new MediaFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    private PhotoAndVideoAdapter photoAndVideoAdapter;
    private AudioAdapter audioAdapter;

    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        photoAndVideoAdapter = new PhotoAndVideoAdapter(getArguments().getLong(CHAT_ID));
        photoAndVideoAdapter.SELECTED_COUNT_DISPATCHER.addObtainer(selectedCountObtainer, false);
        audioAdapter = new AudioAdapter(getArguments().getLong(CHAT_ID));
        audioAdapter.SELECTED_COUNT_DISPATCHER.addObtainer(selectedCountObtainer, false);

        if (savedInstanceState == null)
        {
            getChildFragmentManager().beginTransaction().replace(R.id.miniPlayer, MiniAudioPlayerFragment.newInstance()).commit();
        }
    }

    private final Obtainer<Integer> selectedCountObtainer = new Obtainer<Integer>()
    {
        @Override public void obtain(Integer count)
        {
            if (mainToolbar != null)
            {
                mainToolbar.setVisibility(count == 0 ? View.VISIBLE : View.INVISIBLE);
            }

            if (secondToolbar != null)
            {
                secondToolbar.setTitle(Integer.toString(count));
                secondToolbar.setVisibility(count != 0 ? View.VISIBLE : View.INVISIBLE);
            }
        }
    };

    @Nullable private Toolbar mainToolbar;
    @Nullable private Toolbar secondToolbar;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_media, container, false);

        final View miniPlayer = view.findViewById(R.id.miniPlayer);

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int columnCount = displayMetrics.widthPixels / Screen.dpPixelSize(100, displayMetrics);
        GridLayoutManager photoAndVideoLayoutManager = new GridLayoutManager(getActivity(), columnCount);
        photoAndVideoAdapter.columnCount = columnCount;
        photoAndVideoLayoutManager.setSpanSizeLookup(photoAndVideoAdapter.spanSizeLookup);
        final RecyclerView photoAndVideoListView = (RecyclerView) view.findViewById(R.id.photoAndVideoList);
        photoAndVideoListView.setLayoutManager(photoAndVideoLayoutManager);
        photoAndVideoListView.setAdapter(photoAndVideoAdapter);

        final RecyclerView audioListView = (RecyclerView) view.findViewById(R.id.audioList);
        audioListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        audioListView.setAdapter(audioAdapter);

        mainToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mainToolbar.setStyle(Toolbar.Style.WHITE);
        mainToolbar.setNavigationButton(R.drawable.ic_back_grey, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                getFragmentManager().popBackStack();
            }
        });
        mainToolbar.setSpinner(R.array.mediaTypeArray, new Toolbar.OnSelectListener()
        {
            @Override public void onSelect(int index)
            {
                photoAndVideoListView.setVisibility(index == TAB_PHOTO_AND_VIDEO ? View.VISIBLE : View.INVISIBLE);
                audioListView.setVisibility(index == TAB_AUDIO ? View.VISIBLE : View.INVISIBLE);
                miniPlayer.setVisibility(index == TAB_AUDIO ? View.VISIBLE : View.GONE);
                getArguments().putInt(TAB, index);
            }
        });
        mainToolbar.setCurrentSpinnerItem(getArguments().getInt(TAB), true);

        secondToolbar = ((Toolbar) view.findViewById(R.id.secondToolbar));
        secondToolbar.setStyle(Toolbar.Style.GREEN);
        secondToolbar.setNavigationButton(R.drawable.ic_close, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                photoAndVideoAdapter.unselectAll();
                audioAdapter.unselectAll();
            }
        });
        secondToolbar.addMenuButton(R.drawable.ic_delete, new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                Set<Integer> idSet = photoAndVideoAdapter.selectedMessageIdSet;

                if (!idSet.isEmpty())
                {
                    int[] messageIdArray = new int[idSet.size()];

                    int i = 0;

                    for (Integer id : idSet)
                    {
                        messageIdArray[i] = id;
                    }

                    Info.deleteMessages(getArguments().getLong(CHAT_ID), messageIdArray);
                }

                idSet = audioAdapter.selectedMessageIdSet;

                if (!idSet.isEmpty())
                {
                    int[] messageIdArray = new int[idSet.size()];

                    int i = 0;

                    for (Integer id : idSet)
                    {
                        messageIdArray[i] = id;
                    }

                    Info.deleteMessages(getArguments().getLong(CHAT_ID), messageIdArray);
                }
            }
        });

        selectedCountObtainer.obtain(Math.max(photoAndVideoAdapter.selectedMessageIdSet.size(), audioAdapter.selectedMessageIdSet.size()));

        return view;
    }
}
