package ru.glushen.telegramchallenge.messagelist;

import android.os.*;
import android.support.annotation.*;
import android.support.v7.widget.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.telegramchallenge.message.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static java.lang.Math.*;
import static org.drinkless.td.libcore.telegram.TdApi.*;
import static ru.glushen.telegramchallenge.TelegramClient.*;
import static ru.glushen.telegramchallenge.message.MessageManager.*;
import static ru.glushen.telegramchallenge.message.MessageManager.ForwardType.*;
import static ru.glushen.telegramchallenge.telegramutil.DateAndTime.*;
import static ru.glushen.telegramchallenge.telegramutil.Functions.*;
import static ru.glushen.telegramchallenge.telegramutil.Info.*;

/**
 * Created by Pavel Glushen on 15.06.2015.
 */
class MessageListAdapter extends RecyclerView.Adapter<RecyclerViewHolder<MessageItem>> implements Parcelable
{
    private static final int LOAD_ITEM_COUNT = 25;

    private Chat chat;

    private final ArrayList<MessageItem> items;

    public MessageListAdapter(long chatId)
    {
        items = new ArrayList<>();

        items.add(createProgressBarMessageListItem(false));
        items.add(createProgressBarMessageListItem(true));

        getChat(chatId, new CloseableObtainer<ChatAndUpdate>()
        {
            @Override protected void onObtain(Info.ChatAndUpdate chatAndUpdate)
            {
                chat = chatAndUpdate.chat;

                getChatUpdateDispatcher(chat).addObtainer(chatUpdateHandler, false);

                loadOld();
            }
        });
    }

    protected MessageListAdapter(Parcel in)
    {
        chat = in.readParcelable(Chat.class.getClassLoader());

        items = in.createTypedArrayList(MessageItem.CREATOR);

        if (in.readInt() != 0)
        {
            loadOld();
        }

        if (in.readInt() != 0)
        {
            loadNew();
        }
    }

    public static final Creator<MessageListAdapter> CREATOR = new Creator<MessageListAdapter>()
    {
        @Override public MessageListAdapter createFromParcel(Parcel in)
        {
            return new MessageListAdapter(in);
        }

        @Override public MessageListAdapter[] newArray(int size)
        {
            return new MessageListAdapter[size];
        }
    };

    @Override public int describeContents()
    {
        return 0;
    }

    @Override public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeParcelable(chat, flags);
        dest.writeTypedList(items);
        dest.writeInt(loadingOld ? 1 : 0);
        dest.writeInt(loadingNew ? 1 : 0);
    }

    private static final int NOT_FOUND = -1;

    private int getPreviousMessageIndex(int currentIndex)
    {
        for (int i = currentIndex + 1; i < items.size(); i++)
        {
            if (items.get(i).containsMessage())
            {
                return i;
            }
        }

        return NOT_FOUND;
    }

    private int getNextMessageIndex(int currentIndex)
    {
        for (int i = min(currentIndex, items.size()) - 1; i >= 0; i--)
        {
            if (items.get(i).containsMessage())
            {
                return i;
            }
        }

        return NOT_FOUND;
    }

    private CloseableObtainer<MessageAndUpdate> messageUpdateHandler = new CloseableObtainer<MessageAndUpdate>()
    {
        @Override protected void onObtain(MessageAndUpdate messageAndUpdate)
        {
            for (int i = 0; i < items.size(); i++)
            {
                if (items.get(i).message.id == messageAndUpdate.message.id)
                {
                    MessageManager oldMessageManager = items.get(i).messageManager;
                    MessageManager newMessageManager = getInstance(messageAndUpdate.message, items.get(i).messageManager.getForwardType());

                    if (oldMessageManager.instanceIndex != newMessageManager.instanceIndex)
                    {
                        items.set(i, newMessageManager.createMessageListItem(messageAndUpdate.message));
                    }
                    else
                    {
                        items.get(i).message = messageAndUpdate.message;
                    }

                    if (messageAndUpdate.update instanceof UpdateDeleteMessages)
                    {
                        items.remove(i);
                        notifyItemRemoved(i);

                        for (int j = i; j < items.size(); j++)
                        {
                            if (items.get(j).messageManager == MESSAGE_MANAGER_UNREAD_COUNT)
                            {
                                items.remove(j);
                                notifyItemRemoved(j);
                            }
                        }

                        if (items.get(i).messageManager == MESSAGE_MANAGER_DATE_DIVIDER)
                        {
                            if (i == 0 || i > 0 && !equalsDays(getTimeInMillis(items.get(i).message.date), getTimeInMillis(items.get(i - 1).message.date)))
                            {
                                items.remove(i);
                                notifyItemRemoved(i);
                            }
                        }

                        checkForwardType(getNextMessageIndex(i));
                    }
                    else
                    {
                        notifyItemChanged(i);
                    }

                    break;
                }
            }
        }
    };

    private boolean loadingOld = false;

    public void loadOld()
    {
        if (!loadingOld && !isEveryOldItemLoaded())
        {
            loadingOld = true;

            int fromId = 0;

            if (isAnyItemLoaded())
            {
                fromId = items.get(items.size() - 1 - 1 - 1).message.id;
            }
            else if (chat.topMessage.id != 0)
            {
                notifyEveryNewItemLoaded();
                addMessages(true, chat.topMessage);
                fromId = chat.topMessage.id;
            }
            else
            {
                notifyEveryOldItemLoaded();
                notifyEveryNewItemLoaded();
            }

            if (fromId != 0)
            {
                request(new GetChatHistory(chat.id, fromId, 0, LOAD_ITEM_COUNT), new ResultObtainer<Messages>()
                {
                    @Override protected boolean onError(TdApi.Error error)
                    {
                        return false;
                    }

                    @Override protected void onComplete(Messages messages)
                    {
                        TdApi.Message[] messageArray = messages.messages;

                        if (messageArray.length > 0)
                        {
                            addMessages(true, messageArray);

                            loadingOld = false;

                            if (messageArray.length < LOAD_ITEM_COUNT)
                            {
                                loadOld();
                            }
                        }
                        else
                        {
                            notifyEveryOldItemLoaded();

                            loadingOld = false;
                        }
                    }
                });
            }
        }
    }

    private boolean loadingNew = false;

    public void loadNew()
    {
        if (!loadingNew && !isEveryNewItemLoaded())
        {
            if (isAnyItemLoaded())
            {
                loadingNew = true;

                int fromId = items.get(1).message.id;
                int offset = -(LOAD_ITEM_COUNT + 1);

                request(new GetChatHistory(chat.id, fromId, offset, LOAD_ITEM_COUNT), new ResultObtainer<Messages>()
                {
                    @Override protected void onComplete(Messages messages)
                    {
                        TdApi.Message[] messageArray = messages.messages;

                        if (messageArray.length > 0)
                        {
                            addMessages(false, messageArray);

                            loadingNew = false;

                            if (messageArray.length < LOAD_ITEM_COUNT)
                            {
                                loadNew();
                            }
                        }
                        else
                        {
                            notifyEveryNewItemLoaded();

                            loadingNew = false;
                        }
                    }

                    @Override protected boolean onError(TdApi.Error error)
                    {
                        return false;
                    }
                });
            }
            else
            {
                loadOld();
            }
        }
    }

    private MessageItem createMessageItem(@NonNull TdApi.Message message, @Nullable MessageItem previousMessageItem)
    {
        ForwardType forwardType;

        if (message.forwardFromId != 0)
        {
            if (previousMessageItem != null)
            {
                boolean equalsFromId = message.fromId == previousMessageItem.message.fromId;
                boolean equalsMinutes = equalsMinutes(getTimeInMillis(message.date), getTimeInMillis(previousMessageItem.message.date));

                ForwardType previousForwardType = previousMessageItem.messageManager.getForwardType();

                if (equalsFromId && equalsMinutes && (previousForwardType == FORWARD_FIRST || previousForwardType == FORWARD_SECOND))
                {
                    forwardType = FORWARD_SECOND;
                }
                else
                {
                    forwardType = FORWARD_FIRST;
                }
            }
            else
            {
                forwardType = FORWARD_FIRST;
            }
        }
        else
        {
            forwardType = NO_FORWARD;
        }

        MessageItem messageItem = getInstance(message, forwardType).createMessageListItem(message);
        messageItem.isOutboxMessageUnread = isOutboxMessageUnread(chat, message);
        return messageItem;
    }

    private void addMessages(boolean oldMessages, @NonNull TdApi.Message... messages)
    {
        if (messages.length == 0)
        {
            return;
        }

        boolean anyItemLoaded = isAnyItemLoaded();

        int firstIndex = !isEveryNewItemLoaded() ? 1 : 0;
        int lastIndex = items.size() - (!isEveryOldItemLoaded() ? 1 : 0) - 1;

        int pickedIndex = oldMessages ? lastIndex + 1 : firstIndex;

        ArrayList<MessageItem> insertItems = new ArrayList<>();

        for (int i = messages.length - 1; i >= 0; i--)
        {
            TdApi.Message message = messages[i];

            MessageItem previousMessageItem = null;

            if (insertItems.size() > 0)
            {
                previousMessageItem = insertItems.get(insertItems.size() - 1);
            }
            else if (!oldMessages && anyItemLoaded)
            {
                previousMessageItem = items.get(pickedIndex);
            }

            if (previousMessageItem == null || !equalsDays(getTimeInMillis(message.date), getTimeInMillis(previousMessageItem.message.date)))
            {
                insertItems.add(createDateDividerMessageListItem(message.date));
            }

            insertItems.add(createMessageItem(message, previousMessageItem));

            getMessageUpdateDispatcher(message).addObtainer(messageUpdateHandler, false);
        }

        Collections.reverse(insertItems);

        if (oldMessages && anyItemLoaded && equalsDays(getTimeInMillis(items.get(lastIndex).message.date), getTimeInMillis(insertItems.get(0).message.date)))
        {
            items.remove(lastIndex);
            notifyItemRemoved(lastIndex);
            pickedIndex--;
        }

        items.addAll(pickedIndex, insertItems);
        notifyItemRangeInserted(pickedIndex, insertItems.size());

        if (oldMessages)
        {
            checkForwardType(getNextMessageIndex(lastIndex));
        }
        else
        {
            scrollToTopIfNeed();
        }
    }

    private void checkForwardType(int index)
    {
        if (index != NOT_FOUND)
        {
            int previousMessageIndex = getPreviousMessageIndex(index);
            MessageItem previousMessageItem = previousMessageIndex != NOT_FOUND ? items.get(previousMessageIndex) : null;

            MessageItem currentMessageItem = items.get(index);
            MessageItem recreateCurrentMessageItem = createMessageItem(currentMessageItem.message, previousMessageItem);

            if (currentMessageItem.messageManager.getForwardType() != recreateCurrentMessageItem.messageManager.getForwardType())
            {
                items.set(index, recreateCurrentMessageItem);
                notifyItemChanged(index);
            }
        }
    }

    public boolean isAnyItemLoaded()
    {
        return items.size() - (!isEveryOldItemLoaded() ? 1 : 0) - (!isEveryNewItemLoaded() ? 1 : 0) > 0;
    }

    public boolean isEveryOldItemLoaded()
    {
        return items.size() == 0 || !isLoadOldProgressMessageListItem(items.get(items.size() - 1));
    }

    private void notifyEveryOldItemLoaded()
    {
        if (!isEveryOldItemLoaded())
        {
            items.remove(items.size() - 1);
            notifyItemRemoved(items.size() - 1);
        }
    }

    public boolean isEveryNewItemLoaded()
    {
        return items.size() == 0 || !isLoadNewProgressMessageListItem(items.get(0));
    }

    private void notifyEveryNewItemLoaded()
    {
        if (!isEveryNewItemLoaded())
        {
            items.remove(0);
            notifyItemRemoved(0);
        }
    }

    @Override public RecyclerViewHolder<MessageItem> onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return getInstance(viewType).createMessageListViewHolder(parent);
    }

    @Override public void onBindViewHolder(RecyclerViewHolder<MessageItem> holder, int position)
    {
        holder.bind(items.get(position));
    }

    @Override public int getItemViewType(int position)
    {
        return items.get(position).messageManager.instanceIndex;
    }

    @Override public long getItemId(int position)
    {
        return getMessageListItemUniqueId(items.get(position));
    }

    @Override public int getItemCount()
    {
        return items.size();
    }

    @Override public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerViewSet.add(recyclerView);
    }

    @Override public void onDetachedFromRecyclerView(RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        recyclerViewSet.remove(recyclerView);
    }

    {
        setHasStableIds(true);
    }

    private HashSet<RecyclerView> recyclerViewSet = new HashSet<>();

    private CloseableObtainer<ChatAndUpdate> chatUpdateHandler = new CloseableObtainer<ChatAndUpdate>()
    {
        @Override protected void onObtain(ChatAndUpdate chatAndUpdate)
        {
            chat = chatAndUpdate.chat;

            if (chatAndUpdate.update instanceof UpdateNewMessage)
            {
                addMessages(false, ((UpdateNewMessage) chatAndUpdate.update).message);
            }
            else if (chatAndUpdate.update instanceof UpdateChatReadOutbox || chatAndUpdate.update instanceof UpdateChatReadInbox)
            {
                for (int i = 0; i < items.size(); i++)
                {
                    MessageItem messageItem = items.get(i);

                    boolean isOutboxMessageUnread = isOutboxMessageUnread(chat, messageItem.message);

                    if (messageItem.isOutboxMessageUnread != isOutboxMessageUnread)
                    {
                        messageItem.isOutboxMessageUnread = isOutboxMessageUnread;

                        notifyItemChanged(i);
                    }
                }
            }
        }
    };

    private void scrollToPosition(int position)
    {
        for (RecyclerView recyclerView : recyclerViewSet)
        {
            recyclerView.scrollToPosition(position);
        }
    }

    private void scrollToTopIfNeed()
    {
        for (RecyclerView recyclerView : recyclerViewSet)
        {
            if (((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition() < 2)
            {
                recyclerView.scrollToPosition(0);
            }
        }
    }
}
