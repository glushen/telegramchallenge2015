package ru.glushen.telegramchallenge.messagelist;

import android.graphics.*;
import android.support.v7.widget.*;
import android.util.*;
import android.view.*;

import ru.glushen.telegramchallenge.message.*;
import ru.glushen.util.*;

import static ru.glushen.telegramchallenge.message.MessageManager.*;
import static ru.glushen.telegramchallenge.message.MessageManager.ForwardType.*;

/**
 * Created by Pavel Glushen on 02.07.2015.
 */
class MessageItemDecoration extends RecyclerView.ItemDecoration
{
    @Override public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
    {
        DisplayMetrics displayMetrics = view.getResources().getDisplayMetrics();

        RecyclerView.Adapter<?> adapter = parent.getAdapter();

        int position = parent.getChildAdapterPosition(view);

        boolean correctPosition = 0 <= position && position < adapter.getItemCount();

        ForwardType forwardType = correctPosition ? MessageManager.getInstance(adapter.getItemViewType(position)).getForwardType() : null;

        outRect.left = outRect.right = Screen.dpPixelSize(8f, displayMetrics);
        outRect.top = forwardType != FORWARD_SECOND ? Screen.dpPixelSize(16f, displayMetrics) : 0;
        outRect.bottom = position == 0 ? Screen.dpPixelSize(16f, displayMetrics) : 0;
    }
}
