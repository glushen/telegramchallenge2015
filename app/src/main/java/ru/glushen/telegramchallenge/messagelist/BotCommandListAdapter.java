package ru.glushen.telegramchallenge.messagelist;

import android.support.annotation.*;
import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 16.08.2015.
 */
class BotCommandListAdapter extends RecyclerView.Adapter<BotCommandListAdapter.ViewHolder> implements RecyclerViewHolder.OnClickListener<BotCommandListAdapter.Item>
{
    static class Item
    {
        @NonNull public final TdApi.User bot;
        @NonNull public final TdApi.BotCommand botCommand;
        public final long stableId;

        private Item(@NonNull TdApi.User bot, @NonNull TdApi.BotCommand botCommand, long stableId)
        {
            this.bot = bot;
            this.botCommand = botCommand;
            this.stableId = stableId;
        }
    }

    static class ViewHolder extends RecyclerViewHolder<Item>
    {
        private final SmartImageView avatarView;
        private final TextView commandView;
        private final TextView descriptionView;

        public ViewHolder(@NonNull ViewGroup parent)
        {
            super(parent, R.layout.list_message_list_bot_command);

            avatarView = (SmartImageView) itemView.findViewById(R.id.avatar);
            commandView = (TextView) itemView.findViewById(R.id.command);
            descriptionView = (TextView) itemView.findViewById(R.id.description);
        }

        @Override protected void onBind(Item item)
        {
            avatarView.setBitmapDrawableCreators(AvatarBitmapDrawableCreator.newInstance(item.bot, getResources()));
            commandView.setText("/" + item.botCommand.command);
            descriptionView.setText(item.botCommand.description);
        }
    }

    private final List<Item> fullItemList = new ArrayList<>();
    private final List<Item> chosenItemList = new ArrayList<>();
    boolean isGroup;

    private void addToFullItemList(@NonNull TdApi.User user, @NonNull TdApi.BotInfoGeneral botInfo)
    {
        long i = 0;

        for (TdApi.BotCommand botCommand : botInfo.commands)
        {
            fullItemList.add(new Item(user, botCommand, (i++ << 32) + user.id));
        }
    }

    public void setData(@NonNull TdApi.UserFull userFull)
    {
        isGroup = false;

        fullItemList.clear();

        if (userFull.botInfo instanceof TdApi.BotInfoGeneral)
        {
            addToFullItemList(userFull.user, (TdApi.BotInfoGeneral) userFull.botInfo);
        }

        notifyDataSetChanged();
    }

    public void setData(@NonNull TdApi.GroupChatFull groupChatFull)
    {
        isGroup = true;

        fullItemList.clear();

        for (TdApi.ChatParticipant participant : groupChatFull.participants)
        {
            if (participant.botInfo instanceof TdApi.BotInfoGeneral)
            {
                addToFullItemList(participant.user, (TdApi.BotInfoGeneral) participant.botInfo);
            }
        }

        notifyDataSetChanged();
    }

    @Nullable private Obtainer<String> selectObtainer = null;

    public void setOnSelectObtainer(@Nullable Obtainer<String> selectObtainer)
    {
        this.selectObtainer = selectObtainer;
    }

    @Override public void onClick(RecyclerViewHolder<Item> holder, Item item)
    {
        if (selectObtainer != null)
        {
            String sendCommand = "/" + item.botCommand.command;

            if (isGroup)
            {
                sendCommand += "@" + item.bot.username;
            }
            selectObtainer.obtain(sendCommand);
        }
    }

    @Override public BotCommandListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        ViewHolder viewHolder = new ViewHolder(parent);
        viewHolder.setOnClickListener(this);
        return viewHolder;
    }

    @Override public void onBindViewHolder(BotCommandListAdapter.ViewHolder holder, int position)
    {
        holder.bind(chosenItemList.get(position));
    }

    @Override public int getItemCount()
    {
        return chosenItemList.size();
    }

    {
        setHasStableIds(true);
    }

    @Override public long getItemId(int position)
    {
        return chosenItemList.get(position).stableId;
    }

    public boolean hasItems()
    {
        return fullItemList.size() > 0;
    }

    public boolean hasChosenItems()
    {
        return chosenItemList.size() > 0;
    }

    public void setText(@NonNull String text)
    {
        chosenItemList.clear();

        if (text.length() == 0)
        {
            return;
        }

        for (Item item : fullItemList)
        {
            String command = "/" + item.botCommand.command;

            if (command.startsWith(text))
            {
                chosenItemList.add(item);
            }
        }

        notifyDataSetChanged();
    }
}