package ru.glushen.telegramchallenge.messagelist;

import android.content.*;
import android.database.*;
import android.graphics.*;
import android.net.*;
import android.os.*;
import android.provider.*;
import android.support.annotation.*;
import android.support.v7.widget.*;
import android.text.*;
import android.view.*;
import android.widget.*;

import java.io.*;
import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.*;
import ru.glushen.util.recyclerview.*;

import static android.app.Activity.*;
import static android.provider.MediaStore.*;
import static android.view.View.*;
import static ru.glushen.image.Image.Filter.*;
import static ru.glushen.image.Image.*;

/**
 * Created by Pavel Glushen on 14.06.2015.
 */
public class AttachBottomSheetFragment extends BottomSheetFragment
{
    public static final EventDispatcher<PhotoItem[]> photoChoseDispatcher = new EventDispatcher<>();

    public static class PhotoItem implements Parcelable
    {
        public final String path;
        public final int orientation;
        public final String displayName;

        private boolean checked;

        private PhotoItem(String path, int orientation, String displayName)
        {
            this.path = path;
            this.orientation = orientation;
            this.displayName = displayName;
        }

        protected PhotoItem(Parcel in)
        {
            path = in.readString();
            orientation = in.readInt();
            displayName = in.readString();

            checked = in.readInt() != 0;
        }

        public static final Creator<PhotoItem> CREATOR = new Creator<PhotoItem>()
        {
            @Override public PhotoItem createFromParcel(Parcel in)
            {
                return new PhotoItem(in);
            }

            @Override public PhotoItem[] newArray(int size)
            {
                return new PhotoItem[size];
            }
        };

        @Override public int describeContents()
        {
            return 0;
        }

        @Override public void writeToParcel(Parcel dest, int flags)
        {
            dest.writeString(path);
            dest.writeInt(orientation);
            dest.writeString(displayName);

            dest.writeInt(checked ? 1 : 0);
        }

        @Override public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || getClass() != o.getClass())
            {
                return false;
            }

            PhotoItem photoItem = (PhotoItem) o;

            return orientation == photoItem.orientation && path.equals(photoItem.path);
        }

        @Override public int hashCode()
        {
            int result = path.hashCode();
            result = 31 * result + orientation;
            return result;
        }
    }

    private static class PhotoViewHolder extends RecyclerViewHolder<PhotoItem>
    {
        private final SmartImageView photoView;
        private final View tintView;
        private final View checkView;
        
        public PhotoViewHolder(@NonNull ViewGroup parent)
        {
            super(parent, R.layout.list_chat_message_attach_photo);
            
            photoView = (SmartImageView) findViewById(R.id.photo);
            tintView = findViewById(R.id.tint);
            checkView = findViewById(R.id.check);
        }

        @Override protected void onClick(PhotoItem item)
        {
            item.checked = !item.checked;

            tintView.setVisibility(item.checked ? VISIBLE : GONE);
            checkView.setVisibility(item.checked ? VISIBLE : GONE);
        }

        @Override protected void onUnbind(PhotoItem item)
        {
            photoView.clear();
        }

        @Override protected void onBind(PhotoItem item)
        {
            photoView.setBitmapDrawableCreators(new FileBitmapDrawableCreator(item.path, CROP_CENTER, NOT_DEFINE));

            tintView.setVisibility(item.checked ? VISIBLE : GONE);
            checkView.setVisibility(item.checked ? VISIBLE : GONE);
        }
    }

    private class PhotoAdapter extends ScrollableRecyclerViewAdapter<PhotoViewHolder>
    {
        public PhotoAdapter(final ContentResolver contentResolver)
        {
            if (photoItemList == null)
            {
                photoItemList = new ArrayList<>();

                new AsyncTask<Void, PhotoItem, Void>()
                {
                    @Override protected Void doInBackground(Void... params)
                    {
                        Uri uri = Images.Media.EXTERNAL_CONTENT_URI;
                        String[] projection = {MediaColumns.DATA, Images.ImageColumns.ORIENTATION, MediaColumns.DISPLAY_NAME};
                        String sortOrder = MediaColumns.DATE_ADDED + " DESC";

                        Cursor cursor = contentResolver.query(uri, projection, null, null, sortOrder);

                        int dataColumnIndex = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
                        int orientationColumnIndex = cursor.getColumnIndexOrThrow(Images.ImageColumns.ORIENTATION);
                        int displayNameColumnIndex = cursor.getColumnIndexOrThrow(MediaColumns.DISPLAY_NAME);

                        while (cursor.moveToNext())
                        {
                            String path = cursor.getString(dataColumnIndex);
                            int orientation = cursor.getInt(orientationColumnIndex);
                            String displayName = cursor.getString(displayNameColumnIndex);

                            publishProgress(new PhotoItem(path, orientation, displayName));
                        }

                        cursor.close();

                        return null;
                    }

                    @Override protected void onProgressUpdate(PhotoItem... items)
                    {
                        Collections.addAll(photoItemList, items);
                        notifyItemRangeInserted(photoItemList.size() - items.length, items.length);
                    }
                }.execute();
            }
        }

        @Override public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            PhotoViewHolder holder = new PhotoViewHolder(parent);

            holder.setOnClickListener(new RecyclerViewHolder.OnClickListener<PhotoItem>()
            {
                @Override public void onClick(RecyclerViewHolder<PhotoItem> holder, PhotoItem item)
                {
                    addOrRemoveCheckedItem(item);
                }
            });

            return holder;
        }

        @Override public void onBindViewHolder(PhotoViewHolder holder, int position)
        {
            holder.bind(photoItemList.get(position));
        }

        @Override public int getItemCount()
        {
            return photoItemList.size();
        }
    }

    private static final String PHOTO_ITEM_LIST = "photoItemList";
    private static final String CHECKED_ITEM_LIST = "checkedItemList";
    private static final String TAKE_PHOTO_FILE_PATH = "takePhotoFilePath";

    private ArrayList<PhotoItem> photoItemList = null;

    private ArrayList<PhotoItem> checkedItemList = new ArrayList<>();

    private void addOrRemoveCheckedItem(PhotoItem item)
    {
        if (item.checked)
        {
            checkedItemList.add(item);
        }
        else
        {
            checkedItemList.remove(item);
        }

        updateChooseFromGalleryTextView();
    }

    private void updateChooseFromGalleryTextView()
    {
        if (checkedItemList.isEmpty())
        {
            chooseFromGalleryTextView.setText(R.string.messageListAttachChooseFromGallery);
        }
        else
        {
            int count = checkedItemList.size();
            String text = chooseFromGalleryTextView.getResources().getQuantityString(R.plurals.messageListAttachSendPhoto, count, count);
            chooseFromGalleryTextView.setText(Html.fromHtml(text));
        }
    }

    private String takePhotoFilePath = null;

    private TextView chooseFromGalleryTextView;

    @Override public View onCreateBottomSheetView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (savedInstanceState != null)
        {
            photoItemList = savedInstanceState.getParcelableArrayList(PHOTO_ITEM_LIST);
            checkedItemList = savedInstanceState.getParcelableArrayList(CHECKED_ITEM_LIST);
            takePhotoFilePath = savedInstanceState.getString(TAKE_PHOTO_FILE_PATH);
        }

        View view = inflater.inflate(R.layout.fragment_message_list_attach, container, false);

        RecyclerView galleryImageList = (RecyclerView) view.findViewById(R.id.galleryImageList);
        galleryImageList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        galleryImageList.setAdapter(new PhotoAdapter(getActivity().getContentResolver()));
        galleryImageList.addItemDecoration(new RecyclerView.ItemDecoration()
        {
            @Override public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
            {
                outRect.top = outRect.bottom = 0;
                outRect.right = Screen.dpPixelSize(8f, parent.getResources().getDisplayMetrics());
                outRect.left = parent.getChildAdapterPosition(view) != 0 ? 0 : outRect.right;
            }
        });

        view.findViewById(R.id.takePhotoButton).setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if (intent.resolveActivity(getActivity().getPackageManager()) != null)
                {
                    try
                    {
                        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                        File file = File.createTempFile("tempPhoto" + System.currentTimeMillis(), ".jpg", dir);
                        takePhotoFilePath = file.getAbsolutePath();
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    }
                    catch (IOException e)
                    {
                        throw new AssertionError(e);
                    }

                    startActivityForResult(intent, TAKE_PHOTO);
                }
            }
        });

        view.findViewById(R.id.chooseFromGalleryButton).setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                if (checkedItemList.isEmpty())
                {
                    Intent intent = new Intent(Intent.ACTION_PICK, Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, CHOOSE_FROM_GALLERY);
                }
                else
                {
                    notifyItemChosen(checkedItemList.toArray(new PhotoItem[checkedItemList.size()]));
                }
            }
        });

        chooseFromGalleryTextView = (TextView) view.findViewById(R.id.chooseFromGalleryText);
        updateChooseFromGalleryTextView();

        return view;
    }

    private void notifyItemChosen(PhotoItem... photoItems)
    {
        photoChoseDispatcher.dispatch(photoItems);

        dismiss();
    }

    private static final int TAKE_PHOTO = 0;
    private static final int CHOOSE_FROM_GALLERY = 1;

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PHOTO)
        {
            if (resultCode == RESULT_OK)
            {
                notifyItemChosen(new PhotoItem(takePhotoFilePath, 0, ""));
            }
        }
        else if (requestCode == CHOOSE_FROM_GALLERY)
        {
            if (resultCode == RESULT_OK && data != null)
            {
                String[] projection = new String[]{MediaColumns.DATA, Images.Media.ORIENTATION, MediaColumns.DISPLAY_NAME};

                Cursor cursor = getActivity().getContentResolver().query(data.getData(), projection, null, null, null);
                cursor.moveToFirst();

                String path = cursor.getString(cursor.getColumnIndexOrThrow(MediaColumns.DATA));
                int orientation = cursor.getInt(cursor.getColumnIndexOrThrow(Images.Media.ORIENTATION));
                String displayName = cursor.getString(cursor.getColumnIndexOrThrow(MediaColumns.DISPLAY_NAME));

                cursor.close();

                notifyItemChosen(new PhotoItem(path, orientation, displayName));
            }
        }
    }

    @Override public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(PHOTO_ITEM_LIST, photoItemList);
        outState.putParcelableArrayList(CHECKED_ITEM_LIST, checkedItemList);
        outState.putString(TAKE_PHOTO_FILE_PATH, takePhotoFilePath);
    }
}
