package ru.glushen.telegramchallenge.messagelist;

import android.content.res.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.support.v7.widget.*;
import android.text.*;
import android.text.method.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.emoji.*;
import ru.glushen.telegramchallenge.inputmethod.*;
import ru.glushen.telegramchallenge.messagelist.AttachBottomSheetFragment.*;
import ru.glushen.telegramchallenge.music.*;
import ru.glushen.telegramchallenge.profile.*;
import ru.glushen.telegramchallenge.telegramui.Toolbar;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static android.support.v7.widget.LinearLayoutManager.*;
import static android.view.View.*;
import static java.lang.Math.*;
import static org.drinkless.td.libcore.telegram.TdApi.*;
import static org.drinkless.td.libcore.telegram.TdApi.GroupChatInfo;
import static ru.glushen.telegramchallenge.inputmethod.InputManager.Type.*;
import static ru.glushen.telegramchallenge.telegramutil.Functions.*;
import static ru.glushen.telegramchallenge.telegramutil.Info.*;

/**
 * Created by Pavel Glushen on 01.05.2015.
 */
public class MessageListFragment extends Fragment
{
    private static final String CHAT_ID = "chatId";
    private static final String ADAPTER = "adapter";

    public static MessageListFragment newInstance(long chatId)
    {
        MessageListFragment fragment = new MessageListFragment();

        Bundle arguments = new Bundle();
        arguments.putLong(CHAT_ID, chatId);
        fragment.setArguments(arguments);

        return fragment;
    }

    private MessageListAdapter adapter;
    private InputManager inputManager;

    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
        {
            adapter = new MessageListAdapter(getArguments().getLong(CHAT_ID));

            getChildFragmentManager().beginTransaction().replace(R.id.miniPlayer, MiniAudioPlayerFragment.newInstance()).commit();
        }
        else
        {
            adapter = savedInstanceState.getParcelable(ADAPTER);
        }

        inputManager = InputManager.from(getActivity());
    }

    @Override public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putParcelable(ADAPTER, adapter);
    }

    private Toolbar toolbar;
    private SmartImageView toolbarAvatarView;

    @Nullable private EventDispatcher<ChatAndUpdate> chatDispatcher;

    private final CloseableObtainer<ChatAndUpdate> chatObtainer = new CloseableObtainer<ChatAndUpdate>()
    {
        @Override protected void onObtain(ChatAndUpdate chatAndUpdate)
        {
            final Chat chat = chatAndUpdate.chat;

            chatDispatcher = getChatUpdateDispatcher(chat);
            chatDispatcher.addObtainer(chatUpdateObtainer, true);

            if (chat.type instanceof PrivateChatInfo)
            {
                Info.getUserFull(((PrivateChatInfo) chat.type).user.id, new Obtainer<UserAndUpdate>()
                {
                    @Override public void obtain(UserAndUpdate userAndUpdate)
                    {
                        obtainInfo(chat, userAndUpdate.userFull, null);
                    }
                });
            }
            else if (chat.type instanceof GroupChatInfo)
            {
                Info.getGroupChatFull(((GroupChatInfo) chat.type).groupChat.id, new Obtainer<GroupChatAndUpdate>()
                {
                    @Override public void obtain(GroupChatAndUpdate groupChatAndUpdate)
                    {
                        obtainInfo(chat, null, groupChatAndUpdate.groupChatFull);
                    }
                });
            }

            chatUpdateObtainer.obtain(chatAndUpdate);
        }
    };

    @Nullable private UserStatusHolder userStatusHolder;
    @Nullable private GroupStatusHolder groupStatusHolder;

    private final CloseableObtainer<String> statusStringObtainer = new CloseableObtainer<String>()
    {
        @Override protected void onObtain(String userStatusString)
        {
            if (toolbar != null)
            {
                toolbar.setSubtitle(userStatusString);
            }
        }
    };

    @Nullable private View botCommandListBackgroundView;
    @Nullable private RecyclerView botCommandListView;
    private final BotCommandListAdapter botCommandListAdapter = new BotCommandListAdapter();

    private void updateVisibility()
    {
        if (inputMessageTextView != null && botCommandListBackgroundView != null && botCommandListView != null)
        {
            botCommandListAdapter.setText(inputMessageTextView.getText().toString());

            int visibility = inputManager.isAnyKeyboardShown() && botCommandListAdapter.hasChosenItems() ? VISIBLE : GONE;
            botCommandListBackgroundView.setVisibility(visibility);
            botCommandListView.setVisibility(visibility);

            ViewGroup.LayoutParams layoutParams = botCommandListView.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            int heightInDp = botCommandListAdapter.getItemCount() < 4 ? botCommandListAdapter.getItemCount() * 36 : 128;
            layoutParams.height = Screen.dpPixelSize(heightInDp, botCommandListView.getResources().getDisplayMetrics());
            botCommandListView.setLayoutParams(layoutParams);
        }

        if (sendButtonView != null && botKeyboardShowButtonView != null && attachButtonView != null && recordButtonView != null && recordingVoiceView != null)
        {
            boolean sendButtonShown = sendButtonView.getVisibility() == VISIBLE;
            boolean replyMarkupIsShowKeyboard = replyMarkupMessage != null && replyMarkupMessage.replyMarkup instanceof ReplyMarkupShowKeyboard;
            botKeyboardShowButtonView.setVisibility(sendButtonShown || !replyMarkupIsShowKeyboard ? GONE : VISIBLE);
            enterSlashButtonView.setVisibility(sendButtonShown || replyMarkupIsShowKeyboard || !botCommandListAdapter.hasItems() ? GONE : VISIBLE);
            attachButtonView.setVisibility(sendButtonShown ? GONE : VISIBLE);
            recordButtonView.setVisibility(sendButtonShown ? GONE : VISIBLE);
            recordingVoiceView.setVisibility(sendButtonShown ? GONE : VISIBLE);
        }
    }

    private void obtainInfo(@NonNull final Chat chat, @Nullable final UserFull userFull, @Nullable final GroupChatFull groupChatFull)
    {
        if (userFull == null && groupChatFull == null)
        {
            throw new AssertionError("userFull and groupChatFull cannot be null together");
        }

        if (userFull != null)
        {
            userStatusHolder = new UserStatusHolder(userFull.user, toolbar.getContext());
            userStatusHolder.updateStatusStringDispatcher.addObtainer(statusStringObtainer, false);
            userStatusHolder.dispatchStatusString();

            if (botCommandListAdapter != null)
            {
                botCommandListAdapter.setData(userFull);
                updateVisibility();
            }

            if (userFull.botInfo instanceof BotInfoGeneral)
            {
                Emoji.stringToSpannable(((BotInfoGeneral) userFull.botInfo).description, inputMessageTextView.getResources(), new CloseableObtainer<Spannable>()
                {
                    @Override protected void onObtain(Spannable spannable)
                    {
                        Hypertext.obtainLinks(spannable, 0);
                        botPlaceholderTextView.setText(spannable);

                        useBotPlaceholder = true;
                        adapterDataObserver.onChanged();
                    }
                });
            }

            toolbar.setHeaderOnClickListener(new OnClickListener()
            {
                @Override public void onClick(View view)
                {
                    startFragment(ProfileFragment.newInstanceForUser(userFull.user.id));
                }
            });
        }
        else
        {
            groupStatusHolder = new GroupStatusHolder(groupChatFull, toolbar.getContext());
            groupStatusHolder.updateStatusStringDispatcher.addObtainer(statusStringObtainer, false);
            groupStatusHolder.dispatchStatusString();

            if (botCommandListAdapter != null)
            {
                botCommandListAdapter.setData(groupChatFull);
                updateVisibility();
            }

            toolbar.setHeaderOnClickListener(new OnClickListener()
            {
                @Override public void onClick(View view)
                {
                    startFragment(ProfileFragment.newInstanceForGroup(groupChatFull.groupChat.id));
                }
            });
        }

        final boolean chatIsPrivate = userFull != null;

        final Resources resources = dropdownMenuButton.getResources();

        String firstString = resources.getString(R.string.messageListClearHistory);

        int secondStringRes;

        if (chatIsPrivate)
        {
            secondStringRes = !userFull.isBlocked ? R.string.messageListBlockUser : R.string.messageListUnblockUser;
        }
        else
        {
            secondStringRes = R.string.messageListLeaveGroup;
        }

        String secondString = resources.getString(secondStringRes);

        boolean muted = chat.notificationSettings.muteFor > 0;
        String thirdString = resources.getString(!muted ? R.string.messageListMute : R.string.messageListUnmute);

        final String[] stringArray = new String[]{firstString, secondString, thirdString};

        toolbar.setMenuButton(dropdownMenuButton, stringArray, new Toolbar.OnSelectListener()
        {
            @Override public void onSelect(int index)
            {
                switch (index)
                {
                    case 0: // Clear history
                    {
                        Info.clearChatHistory(chat.id);
                    }
                    break;
                    case 1:
                    {
                        if (chatIsPrivate) // Block user
                        {
                            boolean blocked = !userFull.isBlocked;
                            setUserBlocked(userFull.user.id, blocked);
                            userFull.isBlocked = blocked;

                            stringArray[1] = resources.getString(!blocked ? R.string.messageListBlockUser : R.string.messageListUnblockUser);
                            toolbar.setMenuButton(dropdownMenuButton, stringArray, this);
                        }
                        else // Leave group
                        {
                            Info.clearChatHistory(chat.id);

                            inputManager.hideAnyKeyboard(inputMessageTextView);
                            getFragmentManager().popBackStack();
                        }
                    }
                    break;
                    case 2: // Mute
                    {
                        boolean muted = chat.notificationSettings.muteFor <= 0;
                        chat.notificationSettings.muteFor = muted ? 31536000 : 0;
                        setNotificationsSettings(new NotificationSettingsForChat(chat.id), chat.notificationSettings);

                        stringArray[2] = resources.getString(!muted ? R.string.messageListMute : R.string.messageListUnmute);
                        toolbar.setMenuButton(dropdownMenuButton, stringArray, this);
                    }
                    break;
                }
            }
        });
    }

    private final CloseableObtainer<ChatAndUpdate> chatUpdateObtainer = new CloseableObtainer<ChatAndUpdate>()
    {
        @Override protected void onObtain(ChatAndUpdate chatAndUpdate)
        {
            Chat chat = chatAndUpdate.chat;
            Update update = chatAndUpdate.update;

            toolbar.setTitle(getName(chat, toolbar.getResources()));

            if (update == null || update instanceof UpdateUser || update instanceof UpdateChatPhoto)
            {
                toolbarAvatarView.setBitmapDrawableCreators(AvatarBitmapDrawableCreator.newInstance(chat, toolbarAvatarView.getResources()));
            }

            if (update == null || update instanceof UpdateChatReplyMarkup)
            {
                replyMarkupMessage = null;
                updateVisibility();
                inputManager.hideBotKeyboard();

                if (chat.replyMarkupMessageId != 0)
                {
                    Info.getMessage(chat.id, chat.replyMarkupMessageId, new Obtainer<MessageAndUpdate>()
                    {
                        @Override public void obtain(MessageAndUpdate messageAndUpdate)
                        {
                            replyMarkupMessage = messageAndUpdate.message;
                            ReplyMarkup replyMarkup = messageAndUpdate.message.replyMarkup;

                            if (replyMarkup instanceof ReplyMarkupShowKeyboard)
                            {
                                updateVisibility();

                                if (((ReplyMarkupShowKeyboard) replyMarkup).personal)
                                {
                                    botKeyboardShowButtonClickListener.onClick(botKeyboardShowButtonView);
                                }
                            }
                        }
                    });
                }
            }
        }
    };

    private boolean useBotPlaceholder = false;
    private View placeholder;
    private View botPlaceholder;
    private TextView botPlaceholderTextView;
    private TextView botStartButtonView;

    private final RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver()
    {
        @Override public void onChanged()
        {
            if (placeholder != null)
            {
                boolean visible = adapter.isEveryOldItemLoaded() && adapter.isEveryNewItemLoaded() && adapter.getItemCount() == 0;

                if (useBotPlaceholder)
                {
                    placeholder.setVisibility(GONE);
                    botStartButtonView.setVisibility(visible ? VISIBLE : GONE);
                    botPlaceholder.setVisibility(visible ? VISIBLE : GONE);
                }
                else
                {
                    botPlaceholder.setVisibility(GONE);
                    botStartButtonView.setVisibility(GONE);
                    placeholder.setVisibility(visible ? VISIBLE : GONE);
                }
            }
        }

        @Override public void onItemRangeChanged(int positionStart, int itemCount)
        {
            onChanged();
        }

        @Override public void onItemRangeChanged(int positionStart, int itemCount, Object payload)
        {
            onChanged();
        }

        @Override public void onItemRangeInserted(int positionStart, int itemCount)
        {
            onChanged();
        }

        @Override public void onItemRangeRemoved(int positionStart, int itemCount)
        {
            onChanged();
        }

        @Override public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount)
        {
            onChanged();
        }
    };

    private View enterSlashButtonView;
    private ImageView botKeyboardShowButtonView;
    private ImageView attachButtonView;
    private ImageView recordButtonView;
    private ImageView sendButtonView;
    private RecordingVoiceView recordingVoiceView;

    @Nullable private TdApi.Message replyMarkupMessage;

    private final OnClickListener botKeyboardShowButtonClickListener = new OnClickListener()
    {
        @Override public void onClick(View view)
        {
            if (inputManager.isBotKeyboardShown())
            {
                inputManager.showDefaultKeyboard(inputMessageTextView);
            }
            else if (replyMarkupMessage != null && replyMarkupMessage.replyMarkup instanceof ReplyMarkupShowKeyboard)
            {
                ReplyMarkupShowKeyboard replyMarkupShowKeyboard = (ReplyMarkupShowKeyboard) replyMarkupMessage.replyMarkup;
                String[][] rowArray = replyMarkupShowKeyboard.rows;
                boolean resize = replyMarkupShowKeyboard.resizeKeyboard;
                final boolean revertAfterUse = replyMarkupShowKeyboard.oneTime;
                inputManager.showBotKeyboard(inputMessageTextView, rowArray, resize, revertAfterUse, new InputManager.OnSelectBotKeyboardItem()
                {
                    @Override public void onSelect(String text)
                    {
                        sendMessage(new InputMessageText(text), replyMarkupMessage != null ? replyMarkupMessage.id : 0);

                        if (revertAfterUse)
                        {
                            TelegramClient.request(new DeleteChatReplyMarkup(replyMarkupMessage.chatId, replyMarkupMessage.id), null);
                        }
                    }
                });
            }
        }
    };

    private void setSendButtonShown(boolean sendButtonShown)
    {
        if (sendButtonView != null)
        {
            sendButtonView.setVisibility(sendButtonShown ? VISIBLE : GONE);
            updateVisibility();
        }
    }

    private ImageView dropdownMenuButton;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_message_list, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        toolbar.setNavigationButton(R.drawable.ic_back, new OnClickListener()
        {
            @Override public void onClick(View view)
            {
                inputManager.hideAnyKeyboard(inputMessageTextView);
                getFragmentManager().popBackStack();
            }
        });

        toolbarAvatarView = toolbar.setAvatar();

        toolbar.setSubtitle(R.string.messageListUserStatusLoading);

        dropdownMenuButton = toolbar.addMenuButton(R.drawable.ic_more, null);

        placeholder = view.findViewById(R.id.placeholder);
        botPlaceholder = view.findViewById(R.id.botPlaceholder);

        ((TextView) view.findViewById(R.id.botPlaceholderTitle)).setTypeface(Font.getRobotoMedium(getResources()));

        botPlaceholderTextView = ((TextView) view.findViewById(R.id.botPlaceholderText));
        botPlaceholderTextView.setMovementMethod(LinkMovementMethod.getInstance());

        botStartButtonView = ((TextView) view.findViewById(R.id.botStartButton));
        botStartButtonView.setTypeface(Font.getRobotoMedium(getResources()));
        botStartButtonView.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                sendMessage(new InputMessageText("/start"), 0);
            }
        });

        final RecyclerView chatListRecyclerView = (RecyclerView) view.findViewById(R.id.chatMessages);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), VERTICAL, true);
        chatListRecyclerView.setLayoutManager(layoutManager);

        adapter.registerAdapterDataObserver(adapterDataObserver);

        chatListRecyclerView.setAdapter(adapter);
        chatListRecyclerView.addItemDecoration(new MessageItemDecoration());

        chatListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            private int lastFistVisiblePosition = 0;

            @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();

                if (lastFistVisiblePosition != firstVisiblePosition)
                {
                    lastFistVisiblePosition = firstVisiblePosition;

                    int visibleCount = layoutManager.getChildCount();
                    int totalCount = layoutManager.getItemCount();

                    if (firstVisiblePosition + visibleCount >= totalCount - 10)
                    {
                        if (!adapter.isEveryOldItemLoaded())
                        {
                            adapter.loadOld();
                        }
                        else
                        {
                            chatListRecyclerView.removeOnScrollListener(this);
                        }
                    }
                }
            }
        });

        botKeyboardShowButtonView = (ImageView) view.findViewById(R.id.botKeyboardShowButton);

        botKeyboardShowButtonView.setOnClickListener(botKeyboardShowButtonClickListener);

        attachButtonView = (ImageView) view.findViewById(R.id.attachButton);

        attachButtonView.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                new AttachBottomSheetFragment().show(getFragmentManager(), null);
            }
        });

        recordButtonView = (ImageView) view.findViewById(R.id.recordButton);

        sendButtonView = (ImageView) view.findViewById(R.id.sendButton);

        sendButtonView.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                sendMessage(new InputMessageText(inputMessageTextView.getText().toString()), 0);
                inputMessageTextView.setText("");
            }
        });

        updateVisibility();

        inputMessageTextView = (EditText) view.findViewById(R.id.inputMessageText);

        updateInputMessageTextMaxLines();

        inputMessageTextView.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence string, int start, int count, int after)
            {

            }

            @Override public void onTextChanged(CharSequence string, int start, int before, int count)
            {
                setSendButtonShown(string.length() > 0);
                updateVisibility();
            }

            @Override public void afterTextChanged(Editable editable)
            {

            }
        });

        final ImageView smileKeyboardShowButtonView = (ImageView) view.findViewById(R.id.keyboardShowButton);

        inputMethodObtainer = new CloseableObtainer<InputManager.InputMethodEvent>()
        {
            @Override protected void onObtain(InputManager.InputMethodEvent event)
            {
                if (event.type == SMILE)
                {
                    smileKeyboardShowButtonView.setImageResource(event.shown ? R.drawable.ic_msg_panel_kb : R.drawable.ic_smiles);
                }
                else if (event.type == BOT)
                {
                    botKeyboardShowButtonView.setImageResource(event.shown ? R.drawable.ic_msg_panel_kb : R.drawable.ic_command);
                }

                updateInputMessageTextMaxLines();
                updateVisibility();
            }
        };

        inputManager.inputMethodDispatcher.addObtainer(inputMethodObtainer, true);

        smileKeyboardShowButtonView.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View view)
            {
                if (!inputManager.isSmileKeyboardShown())
                {
                    inputManager.showSmileKeyboard(inputMessageTextView);
                }
                else
                {
                    inputManager.showDefaultKeyboard(inputMessageTextView);
                }
            }
        });

        enterSlashButtonView = view.findViewById(R.id.enterSlashButton);

        enterSlashButtonView.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                inputMessageTextView.setText("/");
                inputMessageTextView.setSelection(1);
                inputManager.showDefaultKeyboard(inputMessageTextView);
            }
        });

        InputManager.emojiClickDispatcher.addObtainer(emojiClickObtainer, true);
        InputManager.backspaceClickDispatcher.addObtainer(backspaceClickObtainer, true);
        InputManager.stickerClickDispatcher.addObtainer(stickerClickObtainer, true);

        AttachBottomSheetFragment.photoChoseDispatcher.addObtainer(photoChoseObtainer, true);

        recordingVoiceView = (RecordingVoiceView) view.findViewById(R.id.recordingVoiceView);
        recordingVoiceView.setObtainer(new Obtainer<InputMessageVoice>()
        {
            @Override public void obtain(InputMessageVoice inputMessageVoice)
            {
                sendMessage(inputMessageVoice, 0);
            }
        });

        botCommandListBackgroundView = view.findViewById(R.id.botCommandListBackground);
        botCommandListView = (RecyclerView) view.findViewById(R.id.botCommandList);
        botCommandListView.setLayoutManager(new LinearLayoutManager(botCommandListView.getContext()));
        botCommandListView.setAdapter(botCommandListAdapter);
        updateVisibility();

        botCommandListAdapter.setOnSelectObtainer(new Obtainer<String>()
        {
            @Override public void obtain(String command)
            {
                sendMessage(new InputMessageText(command), 0);
                inputMessageTextView.setText("");
            }
        });

        Hypertext.BOT_COMMAND_CLICK_DISPATCHER.addObtainer(botCommandClickEventObtainer, false);
        Hypertext.USERNAME_CLICK_DISPATCHER.addObtainer(usernameClickObtainer, false);

        getChat(getArguments().getLong(CHAT_ID), chatObtainer);

        return view;
    }

    private void sendMessage(InputMessageContent inputMessageContent, int replyToMessageId)
    {
        long chatId = getArguments().getLong(CHAT_ID);

        TelegramClient.request(new SendMessage(chatId, replyToMessageId, false, new ReplyMarkupNone(), inputMessageContent), new TelegramClient.ResultObtainer<TdApi.Message>()
        {
            @Override protected void onComplete(TdApi.Message message)
            {
                TelegramClient.dispatchUpdate(new UpdateNewMessage(message));
            }

            @Override protected boolean onError(TdApi.Error error)
            {
                return false;
            }
        });
    }

    private void updateInputMessageTextMaxLines()
    {
        boolean shown = inputManager.isAnyKeyboardShown();

        inputMessageTextView.setMaxHeight(shown ? Integer.MAX_VALUE : round(inputMessageTextView.getLineHeight() * 3.7f));
    }

    @Override public void onDestroyView()
    {
        super.onDestroyView();

        if (userStatusHolder != null)
        {
            userStatusHolder.updateStatusStringDispatcher.removeObtainer(statusStringObtainer);
            userStatusHolder.close();
        }

        if (groupStatusHolder != null)
        {
            groupStatusHolder.updateStatusStringDispatcher.removeObtainer(statusStringObtainer);
            groupStatusHolder.close();
        }

        toolbarAvatarView.clear();

        adapter.unregisterAdapterDataObserver(adapterDataObserver);

        if (chatDispatcher != null)
        {
            chatDispatcher.removeObtainer(chatUpdateObtainer);
        }

        inputManager.inputMethodDispatcher.removeObtainer(inputMethodObtainer);

        InputManager.emojiClickDispatcher.removeObtainer(emojiClickObtainer);
        InputManager.backspaceClickDispatcher.removeObtainer(backspaceClickObtainer);
        InputManager.stickerClickDispatcher.removeObtainer(stickerClickObtainer);

        AttachBottomSheetFragment.photoChoseDispatcher.removeObtainer(photoChoseObtainer);

        Hypertext.BOT_COMMAND_CLICK_DISPATCHER.removeObtainer(botCommandClickEventObtainer);
        Hypertext.USERNAME_CLICK_DISPATCHER.removeObtainer(usernameClickObtainer);
    }

    private CloseableObtainer<InputManager.InputMethodEvent> inputMethodObtainer;

    private EditText inputMessageTextView;

    private Obtainer<InputManager.EmojiClickEvent> emojiClickObtainer = new Obtainer<InputManager.EmojiClickEvent>()
    {
        @Override public void obtain(InputManager.EmojiClickEvent event)
        {
            final int start = inputMessageTextView.getSelectionStart();
            final int end = inputMessageTextView.getSelectionEnd();

            final String emojiString = Emoji.getString(event.index);

            String string = inputMessageTextView.getText().toString();
            string = string.substring(0, start) + emojiString + string.substring(end);

            Emoji.stringToSpannable(string, inputMessageTextView.getResources(), new CloseableObtainer<Spannable>()
            {
                @Override protected void onObtain(Spannable spannable)
                {
                    inputMessageTextView.setText(spannable);
                    inputMessageTextView.setSelection(start + emojiString.length());
                }
            });
        }
    };

    private CloseableObtainer<Void> backspaceClickObtainer = new CloseableObtainer<Void>()
    {
        @Override protected void onObtain(Void nothing)
        {
            inputMessageTextView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL));
            inputMessageTextView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL));
        }
    };

    private Obtainer<InputManager.StickerClickEvent> stickerClickObtainer = new Obtainer<InputManager.StickerClickEvent>()
    {
        @Override public void obtain(InputManager.StickerClickEvent event)
        {
            sendMessage(new InputMessageSticker(new InputFileId(event.sticker.sticker.id)), 0);
        }
    };

    private CloseableObtainer<PhotoItem[]> photoChoseObtainer = new CloseableObtainer<PhotoItem[]>()
    {
        @Override protected void onObtain(PhotoItem[] photoItems)
        {
            for (PhotoItem photoItem : photoItems)
            {
                sendMessage(new InputMessagePhoto(new InputFileLocal(photoItem.path), photoItem.displayName), 0);
            }
        }
    };

    private final Obtainer<Hypertext.BotCommandClickEvent> botCommandClickEventObtainer = new Obtainer<Hypertext.BotCommandClickEvent>()
    {
        @Override public void obtain(Hypertext.BotCommandClickEvent event)
        {
            sendMessage(new InputMessageText(event.command), event.messageId);
        }
    };

    private final Obtainer<String> usernameClickObtainer = new Obtainer<String>()
    {
        @Override public void obtain(String username)
        {
            startFragment(ProfileFragment.newInstanceForUser(username));
        }
    };

    @Override public void onStart()
    {
        super.onStart();

        inputManager.startRecordDefaultKeyboardHeight();
    }

    @Override public void onStop()
    {
        super.onStop();

        inputManager.stopRecordDefaultKeyboardHeight();
    }

    public void hideAnyKeyboard()
    {
        if (inputManager != null && inputMessageTextView != null)
        {
            inputManager.hideAnyKeyboard(inputMessageTextView);
        }
    }

    private void startFragment(@NonNull Fragment fragment)
    {
        hideAnyKeyboard();
        getFragmentManager().beginTransaction().replace(getId(), fragment).addToBackStack(null).commit();
    }
}
