package ru.glushen.telegramchallenge.messagelist;

import android.content.*;
import android.graphics.*;
import android.support.annotation.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.view.animation.*;

import org.drinkless.td.libcore.telegram.*;

import java.io.*;

import ru.glushen.audio.*;
import ru.glushen.telegramchallenge.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static android.view.MotionEvent.*;
import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 30.07.2015.
 */
public class RecordingVoiceView extends View implements View.OnTouchListener
{
    private static Bitmap microphoneIcon = null;

    {
        if (microphoneIcon == null)
        {
            microphoneIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_mic_white);
        }
    }

    public RecordingVoiceView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Nullable private Obtainer<TdApi.InputMessageVoice> obtainer = null;

    public void setObtainer(@Nullable Obtainer<TdApi.InputMessageVoice> obtainer)
    {
        this.obtainer = obtainer;
    }

    {
        setOnTouchListener(this);
    }

    private final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

    private final float touchWidth = Screen.dpPixelSize(52, displayMetrics);
    private final float touchHeight = Screen.dpPixelSize(48, displayMetrics);

    private float touchX, touchY, width, height;
    private boolean recording = false;
    private long startTimeInMillis = 0, stopTimeInMillis = 0;
    private String path;

    @Override public boolean onTouch(View view, MotionEvent event)
    {
        boolean eventConsumed = false;

        if (isEnabled())
        {
            switch (event.getAction())
            {
                case ACTION_DOWN:
                case ACTION_MOVE:
                case ACTION_UP:
                case ACTION_CANCEL:
                    touchX = event.getX();
                    touchY = event.getY();
                    width = getWidth();
                    height = getHeight();

                    eventConsumed = true;

                    break;
            }

            switch (event.getAction())
            {
                case ACTION_DOWN:
                {
                    if (width - touchWidth <= touchX && touchX < width && height - touchHeight <= touchY && touchY < height)
                    {
                        recording = true;
                        startTimeInMillis = System.currentTimeMillis();
                        path = SingleActivity.generateVoiceFilePath(getContext());
                        OpusFileRecorder.startRecord(path);

                        invalidate();
                    }
                    else
                    {
                        eventConsumed = false;
                    }
                }
                break;

                case ACTION_MOVE:
                {
                    if (recording)
                    {
                        if (touchX < 2f / 3f * width)
                        {
                            OpusFileRecorder.stopRecord();
                            //noinspection ResultOfMethodCallIgnored
                            new File(path).delete();

                            recording = false;
                            stopTimeInMillis = System.currentTimeMillis();

                            eventConsumed = false;
                        }

                        invalidate();
                    }
                }
                break;

                case ACTION_UP:
                {
                    if (recording)
                    {
                        OpusFileRecorder.stopRecord();

                        int durationInMillis = (int) (System.currentTimeMillis() - startTimeInMillis);

                        if (obtainer != null && durationInMillis > 1000)
                        {
                            obtainer.obtain(new TdApi.InputMessageVoice(new TdApi.InputFileLocal(path), durationInMillis / 1000));
                        }
                        else
                        {
                            //noinspection ResultOfMethodCallIgnored
                            new File(path).delete();
                        }
                    }
                }
                // intentionally do not break
                case ACTION_CANCEL:
                {
                    if (recording)
                    {
                        recording = false;
                        stopTimeInMillis = System.currentTimeMillis();

                        invalidate();
                    }
                }
                break;
            }
        }

        return eventConsumed;
    }

    private final Paint backgroundPaint = new Paint();

    {
        backgroundPaint.setColor(0xffffffff);
    }

    private final Paint miniRedCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final float miniRedCircleRadius = Screen.dpPixelSize(5, displayMetrics);
    private final float miniRedCircleCenterX = Screen.dpPixelSize(18, displayMetrics);

    {
        miniRedCirclePaint.setColor(0xffff3b36);
    }

    private final Paint durationPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
    private final float durationStartX = Screen.dpPixelSize(32, displayMetrics);

    {
        durationPaint.setColor(0xff000000);
        durationPaint.setTextSize(Screen.spPixelSize(17, displayMetrics));
    }

    private final Paint slideToCancelPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
    private final String slideToCancelText = getResources().getString(R.string.messageListSlideToCancel);
    private final float slideToCancelArrowOffsetCenterX = Screen.dpPixelSize(14, displayMetrics);
    private final float slideToCancelArrowLengthX = Screen.dpPixelSize(5, displayMetrics);
    private final float slideToCancelArrowLengthY = Screen.dpPixelSize(5, displayMetrics);

    {
        slideToCancelPaint.setColor(0xffb3b3b3);
        slideToCancelPaint.setTextSize(Screen.spPixelSize(17, displayMetrics));
        slideToCancelPaint.setStrokeWidth(Screen.dpPixelSize(2, displayMetrics));
        slideToCancelPaint.setStrokeCap(Paint.Cap.SQUARE);
    }

    private final Paint volumeCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    {
        volumeCirclePaint.setColor(0x0D000000);
    }

    private final Paint mainRedCirclePaint = new Paint(miniRedCirclePaint);
    private final float mainRedCircleRadius = Screen.dpPixelSize(42, displayMetrics);

    private final Paint microphoneIconPaint = new Paint();

    private static final int ANIMATION_DURATION_IN_MILLIS = 200;
    private static final AccelerateInterpolator INTERPOLATOR = new AccelerateInterpolator();

    @Override protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        long currentTimeInMillis = System.currentTimeMillis();

        if (recording || ANIMATION_DURATION_IN_MILLIS > currentTimeInMillis - stopTimeInMillis)
        {
            long linearProgressInMillis;
            boolean animating = false;

            if (recording)
            {
                linearProgressInMillis = currentTimeInMillis - startTimeInMillis;
            }
            else
            {
                linearProgressInMillis = ANIMATION_DURATION_IN_MILLIS - (currentTimeInMillis - stopTimeInMillis);
                animating = true;
            }

            float linearProgress = min(linearProgressInMillis / (float) ANIMATION_DURATION_IN_MILLIS, 1);
            animating |= linearProgress < 1;
            float progress = INTERPOLATOR.getInterpolation(linearProgress);

            int alpha = (int) (progress * 255);

            float centerVertical = height - (touchHeight / 2);

            backgroundPaint.setAlpha(alpha);
            canvas.drawRect(0, height - touchHeight, width, height, backgroundPaint);

            float touchOffsetX = touchX - (width - (touchWidth / 2));

            float slideToCancelStartX = (width - slideToCancelPaint.measureText(slideToCancelText)) / 2 - max(-touchOffsetX, 0) / 3;
            float slideToCancelStartY = centerVertical - (slideToCancelPaint.descent() + slideToCancelPaint.ascent()) / 2;
            slideToCancelPaint.setAlpha((int) (alpha * (1 - max(min(-touchOffsetX / (width / 3f), 1), 0))));
            canvas.drawText(slideToCancelText, slideToCancelStartX, slideToCancelStartY, slideToCancelPaint);

            float arrowStartX = slideToCancelStartX - slideToCancelArrowOffsetCenterX;
            float arrowStopX = arrowStartX + slideToCancelArrowLengthX;
            canvas.drawLine(arrowStartX, centerVertical, arrowStopX, centerVertical - slideToCancelArrowLengthY, slideToCancelPaint);
            canvas.drawLine(arrowStartX, centerVertical, arrowStopX, centerVertical + slideToCancelArrowLengthY, slideToCancelPaint);

            miniRedCirclePaint.setAlpha(alpha);
            canvas.drawCircle(miniRedCircleCenterX, centerVertical, miniRedCircleRadius, miniRedCirclePaint);

            long durationInMillis = (recording ? currentTimeInMillis : stopTimeInMillis) - startTimeInMillis;
            long durationMinutes = durationInMillis / 1000 / 60;
            long durationSeconds = durationInMillis / 1000 % 60;
            String durationString = String.format("%d:%02d", durationMinutes, durationSeconds);
            float durationStartY = centerVertical - (durationPaint.descent() + durationPaint.ascent()) / 2;
            durationPaint.setAlpha(alpha);
            canvas.drawText(durationString, durationStartX, durationStartY, durationPaint);

            float microphoneCenterX = touchX - max(((3 - progress) / 3) * touchOffsetX, touchOffsetX);
            float progressCircleRadius = progress * mainRedCircleRadius * (1 + min(OpusFileRecorder.getVolume() * 100, 1.5f));
            canvas.drawCircle(microphoneCenterX, centerVertical, progressCircleRadius, volumeCirclePaint);
            canvas.drawCircle(microphoneCenterX, centerVertical, progress * mainRedCircleRadius, mainRedCirclePaint);

            float microphoneIconX = microphoneCenterX - (microphoneIcon.getWidth() / 2);
            float microphoneIconY = centerVertical - (microphoneIcon.getHeight() / 2);
            microphoneIconPaint.setAlpha(alpha);
            canvas.drawBitmap(microphoneIcon, microphoneIconX, microphoneIconY, microphoneIconPaint);

            if (recording || animating)
            {
                Compat.invalidateOnAnimation(this);
            }
        }
    }
}
