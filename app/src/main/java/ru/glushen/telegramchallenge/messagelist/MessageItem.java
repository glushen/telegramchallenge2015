package ru.glushen.telegramchallenge.messagelist;

import android.os.*;
import android.support.annotation.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.message.*;

import static ru.glushen.telegramchallenge.message.MessageManager.*;

/**
 * Created by Pavel Glushen on 18.05.2015.
 */
public class MessageItem implements Parcelable //todo rename to MessageItem
{
    @NonNull public TdApi.Message message;
    @NonNull public final MessageManager messageManager;
    public boolean isOutboxMessageUnread;

    public MessageItem(@NonNull TdApi.Message message, @NonNull MessageManager messageManager)
    {
        this.message = message;
        this.messageManager = messageManager;
    }

    public boolean containsMessage()
    {
        return message.id != 0;
    }

    public static final Creator<MessageItem> CREATOR = new Creator<MessageItem>()
    {
        @Override public MessageItem createFromParcel(Parcel in)
        {
            TdApi.Message message = in.readParcelable(TdApi.Message.class.getClassLoader());

            MessageItem messageItem = getInstance(in.readInt()).createMessageListItem(message);
            messageItem.isOutboxMessageUnread = in.readInt() != 0;
            messageItem.onRetainInstanceState(in);

            return messageItem;
        }

        @Override public MessageItem[] newArray(int size)
        {
            return new MessageItem[size];
        }
    };

    @Override public final int describeContents()
    {
        return 0;
    }

    @Override public final void writeToParcel(Parcel dest, int flags)
    {
        dest.writeParcelable(message, flags);
        dest.writeInt(messageManager.instanceIndex);
        dest.writeInt(isOutboxMessageUnread ? 1 : 0);
        onSaveInstanceState(dest, flags);
    }

    protected void onSaveInstanceState(Parcel parcel, int flags)
    {

    }

    protected void onRetainInstanceState(Parcel parcel)
    {

    }

    public static class ForwardItem extends MessageItem
    {
        public final MessageItem forwardedItem;

        public ForwardItem(@NonNull TdApi.Message message, @NonNull MessageManager currentMessageManager, @NonNull MessageManager forwardedManager)
        {
            super(message, currentMessageManager);

            forwardedItem = forwardedManager.createMessageListItem(message);
        }
    }
}
