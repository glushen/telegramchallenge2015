package ru.glushen.telegramchallenge.auth;

import android.os.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.inputmethod.*;

/**
 * Created by Pavel Glushen on 30.04.2015.
 */
public class PasswordChildFragment extends ChildFragment implements TextView.OnEditorActionListener, View.OnKeyListener
{
    private EditText passwordView;
    private TextView errorTextView;
    private View progressBarView;
    private boolean passwordIsWrong = false;

    public static PasswordChildFragment newInstance()
    {
        return new PasswordChildFragment();
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_auth_password, container, false);

        passwordView = (EditText) view.findViewById(R.id.password);
        passwordView.setOnEditorActionListener(this);

        errorTextView = (TextView) view.findViewById(R.id.errorText);

        progressBarView = view.findViewById(R.id.progressBar);

        return view;
    }

    @Override public void onStart()
    {
        super.onStart();

        InputManager.from(getActivity()).showDefaultKeyboard(passwordView);
    }

    @Override public void onStop()
    {
        super.onStop();

        InputManager.from(getActivity()).hideDefaultKeyboard(passwordView);
    }

    private void check()
    {
        if (!passwordIsWrong)
        {
            setLoading(true);

            TelegramClient.request(new TdApi.CheckAuthPassword(passwordView.getText().toString()), new TelegramClient.ResultObtainer<TdApi.AuthState>()
            {
                @Override public boolean onError(TdApi.Error error)
                {
                    setLoading(false);
                    setPasswordIsWrong(true);
                    return true;
                }

                @Override public void onComplete(TdApi.AuthState authState)
                {
                    setLoading(false);

                    ((SingleActivity) getActivity()).updateAuthState(authState);
                }
            });
        }
    }

    private void setLoading(boolean loading)
    {
        progressBarView.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    private void setPasswordIsWrong(boolean passwordIsWrong)
    {
        this.passwordIsWrong = passwordIsWrong;

        int passwordBackground;
        View.OnKeyListener passwordOnKeyListener;
        int errorTextVisibility;

        if (passwordIsWrong)
        {
            passwordBackground = R.drawable.edit_text_background_error;
            passwordOnKeyListener = this;
            errorTextVisibility = View.VISIBLE;
        }
        else
        {
            passwordBackground = R.drawable.edit_text_background;
            passwordOnKeyListener = null;
            errorTextVisibility = View.GONE;
        }

        passwordView.setBackgroundResource(passwordBackground);
        passwordView.setOnKeyListener(passwordOnKeyListener);

        errorTextView.setVisibility(errorTextVisibility);
    }

    @Override public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
    {
        if (actionId == EditorInfo.IME_ACTION_NEXT)
        {
            check();
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override public boolean onKey(View view, int i, KeyEvent keyEvent)
    {
        setPasswordIsWrong(false);
        return false;
    }

    @Override public int getToolbarTitleRes()
    {
        return R.string.authPasswordToolbarTitle;
    }

    @Override public void onDoneClick()
    {
        check();
    }
}
