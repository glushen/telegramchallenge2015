package ru.glushen.telegramchallenge.auth;

import android.os.*;
import android.support.v4.app.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.inputmethod.*;

/**
 * Created by Pavel Glushen on 30.04.2015.
 */
public class PhoneNumberChildFragment extends ChildFragment implements TextView.OnEditorActionListener, View.OnClickListener, View.OnKeyListener
{
    public static PhoneNumberChildFragment newInstance()
    {
        return new PhoneNumberChildFragment();
    }

    private TextView countryNameView;
    private TextView countryCodeView;
    private EditText phoneNumberView;
    private TextView errorTextView;
    private View progressBarView;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_auth_phone_number, container, false);

        countryNameView = (TextView) view.findViewById(R.id.countryName);
        countryNameView.setOnClickListener(this);

        countryCodeView = (TextView) view.findViewById(R.id.countryCode);
        countryCodeView.setOnClickListener(this);

        phoneNumberView = (EditText) view.findViewById(R.id.phoneNumber);
        phoneNumberView.setOnEditorActionListener(this);

        errorTextView = (TextView) view.findViewById(R.id.errorText);

        progressBarView = view.findViewById(R.id.progressBar);

        return view;
    }

    @Override public void onStart()
    {
        super.onStart();

        CountryInfo countryInfo = CountryInfo.getSelectedCountryInfo(getActivity());

        countryNameView.setText(countryInfo.name);
        countryCodeView.setText(countryInfo.getCallingCodeAsString());

        InputManager.from(getActivity()).showDefaultKeyboard(phoneNumberView);
    }

    @Override public void onStop()
    {
        super.onStop();

        InputManager.from(getActivity()).hideDefaultKeyboard(phoneNumberView);
    }

    @Override public void onClick(View view)
    {
        Fragment parentFragment = getParentFragment();
        FragmentTransaction transaction = parentFragment.getFragmentManager().beginTransaction();
        transaction.replace(parentFragment.getId(), CountryFragment.newInstance()).addToBackStack(null).commit();
    }

    private void check()
    {
        if (!phoneNumberIsWrong)
        {
            setLoading(true);

            TelegramClient.request(new TdApi.SetAuthPhoneNumber(getPhoneNumber()), new TelegramClient.ResultObtainer<TdApi.AuthState>()
            {
                @Override public boolean onError(TdApi.Error error)
                {
                    setLoading(false);
                    setPhoneNumberIsWrong(true);
                    return true;
                }

                @Override public void onComplete(TdApi.AuthState authState)
                {
                    setLoading(false);

                    SingleActivity.log(authState);

                    if (authState instanceof TdApi.AuthStateWaitCode)
                    {
                        ChildFragment fragment = ActivationCodeChildFragment.newInstance(getPhoneNumber());
                        getFragmentManager().beginTransaction().replace(getId(), fragment).addToBackStack(null).commit();
                    }
                    else if (authState instanceof TdApi.AuthStateWaitName)
                    {
                        ChildFragment fragment = UserNameChildFragment.newInstance(getPhoneNumber());
                        getFragmentManager().beginTransaction().replace(getId(), fragment).addToBackStack(null).commit();
                    }
                    else
                    {
                        ((SingleActivity) getActivity()).updateAuthState(authState);
                    }
                }
            });
        }
    }

    private void setLoading(boolean loading)
    {
        progressBarView.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    private String getPhoneNumber()
    {
        return countryCodeView.getText().toString() + phoneNumberView.getText().toString(); //todo format
    }

    private void setPhoneNumberIsWrong(boolean phoneNumberIsWrong)
    {
        this.phoneNumberIsWrong = phoneNumberIsWrong;

        int phoneNumberBackground;
        View.OnKeyListener phoneNumberOnKeyListener;
        int errorTextVisibility;

        if (phoneNumberIsWrong)
        {
            phoneNumberBackground = R.drawable.edit_text_background_error;
            phoneNumberOnKeyListener = this;
            errorTextVisibility = View.VISIBLE;
        }
        else
        {
            phoneNumberBackground = R.drawable.edit_text_background;
            phoneNumberOnKeyListener = null;
            errorTextVisibility = View.GONE;
        }

        phoneNumberView.setBackgroundResource(phoneNumberBackground);
        phoneNumberView.setOnKeyListener(phoneNumberOnKeyListener);

        errorTextView.setVisibility(errorTextVisibility);
    }

    @Override public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
    {
        if (actionId == EditorInfo.IME_ACTION_NEXT)
        {
            check();
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean phoneNumberIsWrong = false;

    @Override public boolean onKey(View view, int i, KeyEvent keyEvent)
    {
        setPhoneNumberIsWrong(false);
        return false;
    }

    @Override public int getToolbarTitleRes()
    {
        return R.string.authPhoneNumberToolbarTitle;
    }

    @Override public void onDoneClick()
    {
        check();
    }
}
