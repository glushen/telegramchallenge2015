package ru.glushen.telegramchallenge.auth;

import android.support.annotation.*;
import android.support.v4.app.*;

/**
 * Created by Pavel Glushen on 30.04.2015.
 */
abstract class ChildFragment extends Fragment
{
    @StringRes public abstract int getToolbarTitleRes();

    public abstract void onDoneClick();
}
