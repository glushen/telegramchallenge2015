package ru.glushen.telegramchallenge.auth;

import android.os.*;
import android.text.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.inputmethod.*;

/**
 * Created by Pavel Glushen on 30.04.2015.
 */
public class ActivationCodeChildFragment extends ChildFragment implements TextView.OnEditorActionListener, View.OnKeyListener
{
    private static final String PHONE_NUMBER = "phoneNumber";

    private EditText activationCodeView;
    private TextView errorTextView;
    private View progressBarView;
    private boolean activationCodeIsWrong = false;

    public static ActivationCodeChildFragment newInstance(String phoneNumber)
    {
        Bundle arguments = new Bundle();
        arguments.putString(PHONE_NUMBER, phoneNumber);

        ActivationCodeChildFragment fragment = new ActivationCodeChildFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_auth_activation_code, container, false);

        TextView description = (TextView) view.findViewById(R.id.description);
        String phoneNumber = getArguments().getString(PHONE_NUMBER);
        phoneNumber = phoneNumber != null ? TextUtils.htmlEncode(phoneNumber) : "";
        String descriptionString = getString(R.string.authActivationCodeDescription, phoneNumber);
        description.setText(Html.fromHtml(descriptionString));

        activationCodeView = (EditText) view.findViewById(R.id.activationCode);
        activationCodeView.setOnEditorActionListener(this);

        errorTextView = (TextView) view.findViewById(R.id.errorText);

        progressBarView = view.findViewById(R.id.progressBar);

        return view;
    }

    @Override public void onStart()
    {
        super.onStart();

        InputManager.from(getActivity()).showDefaultKeyboard(activationCodeView);
    }

    @Override public void onStop()
    {
        super.onStop();

        InputManager.from(getActivity()).hideDefaultKeyboard(activationCodeView);
    }

    private void check()
    {
        if (!activationCodeIsWrong)
        {
            setLoading(true);

            TelegramClient.request(new TdApi.SetAuthCode(activationCodeView.getText().toString()), new TelegramClient.ResultObtainer<TdApi.AuthState>()
            {
                @Override public boolean onError(TdApi.Error error)
                {
                    setLoading(false);
                    setActivationCodeIsWrong(true);
                    return true;
                }

                @Override public void onComplete(TdApi.AuthState authState)
                {
                    setLoading(false);

                    if (authState instanceof TdApi.AuthStateWaitPassword)
                    {
                        ChildFragment fragment = PasswordChildFragment.newInstance();
                        getFragmentManager().beginTransaction().replace(getId(), fragment).addToBackStack(null).commit();
                    }
                    else
                    {
                        ((SingleActivity) getActivity()).updateAuthState(authState);
                    }
                }
            });
        }
    }

    private void setLoading(boolean loading)
    {
        progressBarView.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    private void setActivationCodeIsWrong(boolean activationCodeIsWrong)
    {
        this.activationCodeIsWrong = activationCodeIsWrong;

        int activationCodeBackground;
        View.OnKeyListener activationCodeOnKeyListener;
        int errorTextVisibility;

        if (activationCodeIsWrong)
        {
            activationCodeBackground = R.drawable.edit_text_background_error;
            activationCodeOnKeyListener = this;
            errorTextVisibility = View.VISIBLE;
        }
        else
        {
            activationCodeBackground = R.drawable.edit_text_background;
            activationCodeOnKeyListener = null;
            errorTextVisibility = View.GONE;
        }

        activationCodeView.setBackgroundResource(activationCodeBackground);
        activationCodeView.setOnKeyListener(activationCodeOnKeyListener);

        errorTextView.setVisibility(errorTextVisibility);
    }

    @Override public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
    {
        if (actionId == EditorInfo.IME_ACTION_NEXT)
        {
            check();
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override public boolean onKey(View view, int i, KeyEvent keyEvent)
    {
        setActivationCodeIsWrong(false);
        return false;
    }

    @Override public int getToolbarTitleRes()
    {
        return R.string.authActivationCodeToolbarTitle;
    }

    @Override public void onDoneClick()
    {
        check();
    }
}
