package ru.glushen.telegramchallenge.auth;

import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;

import ru.glushen.telegramchallenge.*;

/**
 * Created by Pavel Glushen on 05.05.2015.
 */
abstract class CountryItemViewHolder extends RecyclerView.ViewHolder
{
    public static CountryItemViewHolder newInstance(ViewGroup parent, int viewType, View.OnClickListener clickListener)
    {
        if (viewType == CountryListAdapter.VIEW_TYPE_COUNTRY_INFO)
        {
            CountryItemViewHolder viewHolder = new CountryInfoViewHolder(parent);
            viewHolder.itemView.setOnClickListener(clickListener);
            return viewHolder;
        }
        else if (viewType == CountryListAdapter.VIEW_TYPE_HEADER)
        {
            return new HeaderViewHolder(parent);
        }
        else
        {
            return new SeparatorViewHolder(parent);
        }
    }

    public CountryItemViewHolder(ViewGroup parent, int layoutResource)
    {
        super(LayoutInflater.from(parent.getContext()).inflate(layoutResource, parent, false));
    }

    public abstract void bind(CountryItem item);

    public static class CountryInfoViewHolder extends CountryItemViewHolder
    {
        private final TextView countryName;
        private final TextView callingCode;

        public CountryInfoViewHolder(ViewGroup parent)
        {
            super(parent, R.layout.list_country_item);

            countryName = (TextView) itemView.findViewById(R.id.countryName);
            callingCode = (TextView) itemView.findViewById(R.id.countryCode);
        }

        @Override public void bind(CountryItem item)
        {
            CountryItem.CountryInfoItem countryInfoItem = (CountryItem.CountryInfoItem) item;
            countryName.setText(countryInfoItem.countryName);
            callingCode.setText(countryInfoItem.callingCode);
        }
    }
    public static class HeaderViewHolder extends CountryItemViewHolder
    {
        private final TextView header;

        public HeaderViewHolder(ViewGroup parent)
        {
            super(parent, R.layout.list_country_header);

            header = (TextView) itemView;
        }

        @Override public void bind(CountryItem item)
        {
            CountryItem.HeaderItem countryInfoItem = (CountryItem.HeaderItem) item;
            header.setText(countryInfoItem.header);
        }
    }
    public static class SeparatorViewHolder extends CountryItemViewHolder
    {
        public SeparatorViewHolder(ViewGroup parent)
        {
            super(parent, R.layout.list_country_separator);
        }

        @Override public void bind(CountryItem item)
        {

        }
    }
}
