package ru.glushen.telegramchallenge.auth;

import android.content.*;
import android.support.v7.widget.*;
import android.view.*;

import com.tonicartos.superslim.*;

import java.util.*;

/**
 * Created by Pavel Glushen on 02.05.2015.
 */
class CountryListAdapter extends RecyclerView.Adapter<CountryItemViewHolder> implements View.OnClickListener
{
    interface SelectCountryHandler
    {
        void handleSelect(CountryInfo countryInfo);
    }

    static final int VIEW_TYPE_COUNTRY_INFO = 0;
    static final int VIEW_TYPE_HEADER = 1;
    static final int VIEW_TYPE_SEPARATOR = 2;

    final int selectedCountryPosition;

    private final RecyclerView countryRecyclerView;
    private final SelectCountryHandler selectCountryHandler;
    private final ArrayList<CountryItem> items = new ArrayList<>();

    public CountryListAdapter(Context context, RecyclerView countryRecyclerView, SelectCountryHandler selectCountryHandler)
    {
        this.countryRecyclerView = countryRecyclerView;
        this.selectCountryHandler = selectCountryHandler;

        int count = CountryInfo.getCount(context);
        char headerChar = 0;
        int headerCount = 0;
        int separatorCount = 0;
        int sectionFirstPosition = 0;

        int selectedCountryIndex = CountryInfo.getSelectedCountryInfo(context).index;
        int selectedCountryPosition = 0;

        for (int i = 0; i < count; i++)
        {
            CountryInfo countryInfo = CountryInfo.getCountryByIndex(i, context);
            char newHeaderChar = countryInfo.name.charAt(0);

            if (headerChar != newHeaderChar)
            {
                if (headerCount > 0)
                {
                    items.add(new CountryItem.SeparatorItem(sectionFirstPosition));
                    separatorCount++;
                }

                sectionFirstPosition = i + headerCount + separatorCount;
                headerChar = newHeaderChar;
                items.add(new CountryItem.HeaderItem(sectionFirstPosition, newHeaderChar));
                headerCount++;
            }

            if (countryInfo.index == selectedCountryIndex)
            {
                selectedCountryPosition = items.size();
            }

            items.add(new CountryItem.CountryInfoItem(sectionFirstPosition, countryInfo));
        }

        this.selectedCountryPosition = selectedCountryPosition;
    }

    @Override public CountryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return CountryItemViewHolder.newInstance(parent, viewType, this);
    }

    @Override public void onBindViewHolder(CountryItemViewHolder holder, int position)
    {
        CountryItem item = items.get(position);
        holder.bind(item);

        GridSLM.LayoutParams params = GridSLM.LayoutParams.from(holder.itemView.getLayoutParams());
        params.setFirstPosition(item.firstPosition);
        holder.itemView.setLayoutParams(params);
    }

    @Override public int getItemViewType(int position)
    {
        return items.get(position).getType();
    }

    @Override public int getItemCount()
    {
        return items.size();
    }

    @Override public void onClick(View view)
    {
        int position = countryRecyclerView.getChildLayoutPosition(view);
        CountryInfo countryInfo = ((CountryItem.CountryInfoItem) items.get(position)).countryInfo;
        selectCountryHandler.handleSelect(countryInfo);
    }
}