package ru.glushen.telegramchallenge.auth;

/**
 * Created by Pavel Glushen on 05.05.2015.
 */
abstract class CountryItem
{
    public final int firstPosition;

    public CountryItem(int firstPosition)
    {
        this.firstPosition = firstPosition;
    }

    public abstract int getType();

    public static class CountryInfoItem extends CountryItem
    {
        public final CountryInfo countryInfo;
        public final String countryName;
        public final String callingCode;

        public CountryInfoItem(int firstPosition, CountryInfo countryInfo)
        {
            super(firstPosition);
            this.countryInfo = countryInfo;
            this.countryName = countryInfo.name;
            this.callingCode = countryInfo.getCallingCodeAsString();
        }

        @Override public int getType()
        {
            return CountryListAdapter.VIEW_TYPE_COUNTRY_INFO;
        }
    }
    public static class HeaderItem extends CountryItem
    {
        public final String header;

        public HeaderItem(int firstPosition, char headerChar)
        {
            super(firstPosition);
            this.header = String.valueOf(headerChar);
        }

        @Override public int getType()
        {
            return CountryListAdapter.VIEW_TYPE_HEADER;
        }
    }
    public static class SeparatorItem extends CountryItem
    {
        public SeparatorItem(int firstPosition)
        {
            super(firstPosition);
        }

        @Override public int getType()
        {
            return CountryListAdapter.VIEW_TYPE_SEPARATOR;
        }
    }
}
