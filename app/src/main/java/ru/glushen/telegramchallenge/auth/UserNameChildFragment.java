package ru.glushen.telegramchallenge.auth;

import android.os.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.inputmethod.*;

/**
 * Created by Pavel Glushen on 07.05.2015.
 */
public class UserNameChildFragment extends ChildFragment implements TextView.OnEditorActionListener, View.OnKeyListener
{
    private static final String PHONE_NUMBER = "phoneNumber";

    private EditText firstNameView;
    private EditText lastNameView;
    private View progressBar1View;
    private View progressBar2View;
    private TextView errorTextView;
    private boolean nameIsWrong = false;

    public static UserNameChildFragment newInstance(String phoneNumber)
    {
        Bundle arguments = new Bundle();
        arguments.putString(PHONE_NUMBER, phoneNumber);

        UserNameChildFragment fragment = new UserNameChildFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_auth_user_name, container, false);

        firstNameView = (EditText) view.findViewById(R.id.firstName);

        lastNameView = (EditText) view.findViewById(R.id.lastName);
        lastNameView.setOnEditorActionListener(this);

        progressBar1View = view.findViewById(R.id.progressBar1);

        progressBar2View = view.findViewById(R.id.progressBar2);

        errorTextView = (TextView) view.findViewById(R.id.errorText);

        return view;
    }

    @Override public void onStart()
    {
        super.onStart();

        InputManager.from(getActivity()).showDefaultKeyboard(lastNameView.hasFocus() ? lastNameView : firstNameView);
    }

    @Override public void onStop()
    {
        super.onStop();

        InputManager.from(getActivity()).hideDefaultKeyboard(lastNameView.hasFocus() ? lastNameView : firstNameView);
    }

    private void check()
    {
        if (!nameIsWrong)
        {
            setLoading(true);

            String firstName = firstNameView.getText().toString();
            String lastName = lastNameView.getText().toString();

            TelegramClient.request(new TdApi.SetAuthName(firstName, lastName), new TelegramClient.ResultObtainer<TdApi.AuthState>()
            {
                @Override public boolean onError(TdApi.Error error)
                {
                    setLoading(false);
                    setNameIsWrong(true);
                    return true;
                }

                @Override public void onComplete(TdApi.AuthState authState)
                {
                    setLoading(false);

                    if (authState instanceof TdApi.AuthStateWaitCode)
                    {
                        ChildFragment fragment = ActivationCodeChildFragment.newInstance(getArguments().getString(PHONE_NUMBER));
                        getFragmentManager().beginTransaction().replace(getId(), fragment).commit();
                    }
                    else
                    {
                        ((SingleActivity) getActivity()).updateAuthState(authState);
                    }
                }
            });
        }
    }

    private void setLoading(boolean loading)
    {
        int progressBarVisibility = loading ? View.VISIBLE : View.GONE;

        progressBar1View.setVisibility(progressBarVisibility);
        progressBar2View.setVisibility(progressBarVisibility);
    }

    private void setNameIsWrong(boolean nameIsWrong)
    {
        this.nameIsWrong = nameIsWrong;

        int inputsBackground;
        View.OnKeyListener inputsOnKeyListener;
        int errorTextVisibility;

        if (nameIsWrong)
        {
            inputsBackground = R.drawable.edit_text_background_error;
            inputsOnKeyListener = this;
            errorTextVisibility = View.VISIBLE;
        }
        else
        {
            inputsBackground = R.drawable.edit_text_background;
            inputsOnKeyListener = null;
            errorTextVisibility = View.GONE;
        }

        firstNameView.setBackgroundResource(inputsBackground);
        firstNameView.setOnKeyListener(inputsOnKeyListener);

        lastNameView.setBackgroundResource(inputsBackground);
        lastNameView.setOnKeyListener(inputsOnKeyListener);

        errorTextView.setVisibility(errorTextVisibility);
    }

    @Override public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
    {
        if (actionId == EditorInfo.IME_ACTION_NEXT)
        {
            check();
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override public boolean onKey(View view, int i, KeyEvent keyEvent)
    {
        setNameIsWrong(false);
        return false;
    }

    @Override public int getToolbarTitleRes()
    {
        return R.string.authUserNameToolbarTitle;
    }

    @Override public void onDoneClick()
    {
        check();
    }
}
