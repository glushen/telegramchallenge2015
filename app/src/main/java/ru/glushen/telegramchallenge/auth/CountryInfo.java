package ru.glushen.telegramchallenge.auth;

import android.content.*;
import android.content.res.*;
import android.support.annotation.*;
import android.telephony.*;

import org.xmlpull.v1.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;

/**
 * Created by Pavel Glushen on 26.04.2015.
 */
class CountryInfo
{
    @Nullable private static CountryInfo selectedCountryInfo = null;

    @NonNull public static CountryInfo getSelectedCountryInfo(@NonNull Context context)
    {
        return selectedCountryInfo != null ? selectedCountryInfo : getUserCountry(context);
    }

    public static void setSelectedCountryInfo(@Nullable CountryInfo countryInfo)
    {
        selectedCountryInfo = countryInfo;
    }

    public final int index;
    public final String name;
    public final String isoCode;
    public final int callingCode;

    private CountryInfo(int index, @NonNull String name, @NonNull String isoCode, int callingCode)
    {
        this.index = index;
        this.name = name;
        this.isoCode = isoCode;
        this.callingCode = callingCode;
    }

    @NonNull public String getCallingCodeAsString()
    {
        return "+" + callingCode;
    }

    @NonNull private static CountryInfo getUserCountry(@NonNull Context context)
    {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        CountryInfo countryInfo = getCountryByISOCode(tm.getSimCountryIso(), context);

        if (countryInfo != null)
        {
            return countryInfo;
        }

        countryInfo = getCountryByISOCode(tm.getNetworkCountryIso(), context);

        if (countryInfo != null)
        {
            return countryInfo;
        }

        return getCountries(context)[defaultCountryIndex];
    }

    @Nullable private static CountryInfo getCountryByISOCode(@Nullable String isoCode, @NonNull Context context)
    {
        if (isoCode != null && isoCode.length() == 2)
        {
            CountryInfo[] countries = getCountries(context);

            for (CountryInfo countryInfo : countries)
            {
                if (countryInfo.isoCode.equalsIgnoreCase(isoCode))
                {
                    return countryInfo;
                }
            }
        }

        return null;
    }

    @NonNull private static CountryInfo[] getCountries(@NonNull Context context)
    {
        if (cachedCountries == null)
        {
            ArrayList<CountryInfo> countries = new ArrayList<>();

            XmlResourceParser parser = context.getResources().getXml(R.xml.countries);

            try
            {
                while (parser.next() != XmlPullParser.END_DOCUMENT)
                {
                    if (parser.getEventType() == XmlPullParser.START_TAG)
                    {
                        String tagName = parser.getName();

                        if ("countries".equals(tagName))
                        {
                            defaultCountryIndex = Integer.parseInt(parser.getAttributeValue(null, "defaultCountryIndex"));
                        }
                        else if ("country".equals(tagName))
                        {
                            String name = parser.getAttributeValue(null, "name");
                            String isoCode = parser.getAttributeValue(null, "isoCode");
                            int callingCode = Integer.parseInt(parser.getAttributeValue(null, "callingCode"));

                            countries.add(new CountryInfo(countries.size(), name, isoCode, callingCode));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new AssertionError(e);
            }

            cachedCountries = countries.toArray(new CountryInfo[countries.size()]);
        }

        return cachedCountries;
    }

    @NonNull public static CountryInfo getCountryByIndex(int index, @NonNull Context context)
    {
        CountryInfo[] countries = getCountries(context);

        if (index < countries.length)
        {
            return countries[index];
        }
        else
        {
            throw new IndexOutOfBoundsException();
        }
    }

    public static int getCount(@NonNull Context context)
    {
        return getCountries(context).length;
    }

    private static CountryInfo[] cachedCountries = null;
    private static int defaultCountryIndex;

    @Override public String toString()
    {
        return index + ": " + name + " (" + isoCode + "): +" + callingCode;
    }
}
