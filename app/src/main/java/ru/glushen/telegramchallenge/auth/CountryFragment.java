package ru.glushen.telegramchallenge.auth;

import android.os.*;
import android.support.v4.app.*;
import android.support.v7.widget.*;
import android.view.*;

import com.tonicartos.superslim.*;

import ru.glushen.telegramchallenge.R;
import ru.glushen.telegramchallenge.telegramui.*;

/**
 * Created by Pavel Glushen on 02.05.2015.
 */
public class CountryFragment extends Fragment implements CountryListAdapter.SelectCountryHandler
{
    public static CountryFragment newInstance()
    {
        return new CountryFragment();
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_country, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationButton(R.drawable.ic_back, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                getFragmentManager().popBackStack();
            }
        });
        toolbar.setTitle(R.string.countryToolbarTitle);

        RecyclerView countryRecyclerView = (RecyclerView) view.findViewById(R.id.countryRecyclerView);
        countryRecyclerView.setLayoutManager(new LayoutManager(getActivity()));

        CountryListAdapter adapter = new CountryListAdapter(getActivity(), countryRecyclerView, this);
        countryRecyclerView.setAdapter(adapter);
        countryRecyclerView.scrollToPosition(adapter.selectedCountryPosition);

        return view;
    }

    @Override public void handleSelect(CountryInfo countryInfo)
    {
        CountryInfo.setSelectedCountryInfo(countryInfo);
        getFragmentManager().popBackStack();
    }
}
