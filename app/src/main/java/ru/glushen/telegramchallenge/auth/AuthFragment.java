package ru.glushen.telegramchallenge.auth;

import android.os.*;
import android.support.v4.app.*;
import android.view.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramui.*;

/**
 * Created by Pavel Glushen on 24.04.2015.
 */
public class AuthFragment extends Fragment implements FragmentManager.OnBackStackChangedListener
{
    private static final int CHILD_FRAGMENT_CONTAINER = R.id.fragmentAuth;

    public static AuthFragment newInstance()
    {
        return new AuthFragment();
    }

    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getChildFragmentManager().addOnBackStackChangedListener(this);

        if (savedInstanceState == null)
        {
            ChildFragment childFragment = PhoneNumberChildFragment.newInstance();
            getChildFragmentManager().beginTransaction().replace(CHILD_FRAGMENT_CONTAINER, childFragment).commit();
        }
    }

    private Toolbar toolbar;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_auth, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.addMenuButton(R.drawable.ic_check, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Fragment fragment = getChildFragmentManager().findFragmentById(CHILD_FRAGMENT_CONTAINER);

                if (fragment instanceof ChildFragment)
                {
                    ((ChildFragment) fragment).onDoneClick();
                }
            }
        });

        return view;
    }

    @Override public void onResume()
    {
        super.onResume();

        onBackStackChanged();
    }

    @Override public void onBackStackChanged()
    {
        Fragment fragment = getChildFragmentManager().findFragmentById(CHILD_FRAGMENT_CONTAINER);

        if (toolbar != null)
        {
            if (fragment instanceof ChildFragment)
            {
                toolbar.setTitle(((ChildFragment) fragment).getToolbarTitleRes());
            }

            if (getChildFragmentManager().getBackStackEntryCount() > 0)
            {
                toolbar.setNavigationButton(R.drawable.ic_back, new View.OnClickListener()
                {
                    @Override public void onClick(View v)
                    {
                        getChildFragmentManager().popBackStack();
                    }
                });
            }
            else
            {
                toolbar.removeNavigationButton();
            }
        }
    }
}
