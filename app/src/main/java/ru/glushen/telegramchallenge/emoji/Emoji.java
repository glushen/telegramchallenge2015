package ru.glushen.telegramchallenge.emoji;

import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.support.annotation.*;
import android.text.*;
import android.text.style.*;

import java.io.*;

import ru.glushen.image.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 21.06.2015.
 */
public class Emoji
{
    @NonNull public static String getString(int index)
    {
        return String.valueOf(EmojiInfo.charArrays[index]);
    }

    private static byte[][] compressedEmojiArray = null;

    public static boolean getBitmapDrawable(final int index, final int size, final Resources resources, final CloseableObtainer<BitmapDrawable> bitmapDrawableHandler)
    {
        BitmapDrawable bitmapDrawable = getBitmapDrawableFromCache(index, size);

        if (bitmapDrawable != null)
        {
            if (bitmapDrawableHandler != null)
            {
                bitmapDrawableHandler.obtain(bitmapDrawable);
            }

            return true;
        }
        else
        {
            new AsyncTask<Void, Void, BitmapDrawable>()
            {
                @Override protected BitmapDrawable doInBackground(Void... params)
                {
                    if (bitmapDrawableHandler.hasClosed())
                    {
                        cancel(false);
                        return null;
                    }

                    return getBitmapDrawableSync(index, size, resources);
                }

                @Override protected void onPostExecute(BitmapDrawable bitmapDrawable)
                {
                    if (bitmapDrawableHandler != null)
                    {
                        bitmapDrawableHandler.obtain(bitmapDrawable);
                    }
                }
            }.execute();

            return false;
        }
    }

    @Nullable private static BitmapDrawable getBitmapDrawableFromCache(int index, int size)
    {
        return Image.bitmapDrawableCache.get(generateCacheKey(index, size));
    }

    @NonNull private static BitmapDrawable getBitmapDrawableSync(int index, int size, Resources resources)
    {
        BitmapDrawable bitmapDrawable = getBitmapDrawableFromCache(index, size);

        if (bitmapDrawable == null)
        {
            bitmapDrawable = Image.createBitmapDrawable(WebP.createScaledBitmap(getCompressed(index, resources), size, size), resources);

            Image.bitmapDrawableCache.put(generateCacheKey(index, size), bitmapDrawable);
        }

        return bitmapDrawable;
    }

    @NonNull private static String generateCacheKey(int index, int size)
    {
        return Emoji.class.getName() + "::" + index + "," + size;
    }

    private static byte[] getCompressed(int index, Resources resources)
    {
        if (compressedEmojiArray == null)
        {
            try
            {
                InputStream inputStream = resources.getAssets().open("emoji", AssetManager.ACCESS_BUFFER);

                compressedEmojiArray = new byte[EmojiInfo.sizes.length][];

                for (int i = 0; i < compressedEmojiArray.length; i++)
                {
                    byte[] compressedEmoji = new byte[EmojiInfo.sizes[i]];

                    int readCount = 0;

                    while (readCount < compressedEmoji.length)
                    {
                        int justRead = inputStream.read(compressedEmoji, readCount, compressedEmoji.length - readCount);

                        if (justRead >= 0)
                        {
                            readCount += justRead;
                        }
                        else
                        {
                            throw new AssertionError();
                        }
                    }

                    compressedEmojiArray[i] = compressedEmoji;
                }

                inputStream.close();
            }
            catch (IOException e)
            {
                throw new AssertionError(e);
            }
        }

        return compressedEmojiArray[index];
    }

    /* todo remove
    public static void addImageSpan(final int index, @NonNull final Editable editable, final int start, final int end, @NonNull final Resources resources)
    {
        String emojiString = getString(index);
        editable.replace(start, end, emojiString);
        editable.setSpan(createImageSpanSync(index, resources), start, start + emojiString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }
    */
    private static Cache<String, Spannable> spannableCache = new Cache<>(new Cache.SizeGetter<String, Spannable>()
    {
        @Override public long getSize(String string, Spannable spannable)
        {
            return string.length() + 1000 * spannable.getSpans(0, spannable.length() - 1, ImageSpan.class).length;
        }
    }, 200000);

    public static void stringToSpannable(@NonNull final String string, @NonNull final Resources resources, final CloseableObtainer<Spannable> spannableHandler)
    {
        Spannable spannable = spannableCache.get(string);

        if (spannable != null)
        {
            spannableHandler.obtain(new SpannableStringBuilder(spannable));
        }
        else
        {
            new AsyncTask<Void, Void, Spannable>()
            {
                @Override protected Spannable doInBackground(Void... params)
                {
                    Spannable spannable = SpannableStringBuilder.valueOf(string);

                    char[] chars = string.toCharArray();

                    int handledPosition = Integer.MAX_VALUE;

                    for (int i = chars.length - 1; i >= 0; i--)
                    {
                        if (spannableHandler.hasClosed())
                        {
                            cancel(false);
                            return null;
                        }

                        final int NOT_FOUND = -1;

                        int firstChars = chars[i];

                        int emojiIndex = EmojiInfo.indexByFirstCharsArray.get(firstChars, NOT_FOUND);

                        if (emojiIndex != NOT_FOUND && handledPosition >= i + 1)
                        {
                            ImageSpan span = createImageSpanSync(emojiIndex, resources);
                            spannable.setSpan(span, i, i + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                            handledPosition = i;
                        }
                        else if (i > 0)
                        {
                            int prevI = i - 1;
                            firstChars |= ((int) chars[prevI]) << 16;

                            emojiIndex = EmojiInfo.indexByFirstCharsArray.get(firstChars, NOT_FOUND);

                            if (emojiIndex != NOT_FOUND)
                            {
                                char[] emojiChars = EmojiInfo.charArrays[emojiIndex];

                                if (prevI + emojiChars.length <= chars.length)
                                {
                                    boolean contains = true;

                                    for (int j = 0; j < emojiChars.length; j++)
                                    {
                                        contains &= emojiChars[j] == chars[prevI + j];
                                    }

                                    if (contains && handledPosition >= prevI + emojiChars.length)
                                    {
                                        ImageSpan span = createImageSpanSync(emojiIndex, resources);
                                        spannable.setSpan(span, prevI, prevI + emojiChars.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                                        handledPosition = prevI;
                                    }
                                }
                            }
                        }
                    }

                    return spannable;
                }

                @Override protected void onPostExecute(Spannable spannable)
                {
                    spannableCache.put(string, spannable);

                    if (spannableHandler != null)
                    {
                        spannableHandler.obtain(new SpannableStringBuilder(spannable));
                    }
                }
            }.execute();
        }
    }

    @NonNull private static ImageSpan createImageSpanSync(int index, @NonNull Resources resources)
    {
        int size = Screen.dpPixelSize(20, resources.getDisplayMetrics());

        Bitmap bitmap = WebP.createScaledBitmap(getCompressed(index, resources), size, size);
        BitmapDrawable drawable = new BitmapDrawable(null, bitmap);
        drawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());

        return new ImageSpan(drawable, DynamicDrawableSpan.ALIGN_BOTTOM);
    }

    public static class EmojiTextWatcher implements TextWatcher
    {
        @Override public void beforeTextChanged(CharSequence charSequence, int start, int count, int after)
        {

        }

        @Override public void onTextChanged(CharSequence charSequence, int start, int before, int count)
        {
            if (charSequence instanceof Spannable)
            {
                Spannable spannable = (Spannable) charSequence;

                //spannable.
            }
        }

        @Override public void afterTextChanged(Editable editable)
        {

        }
    }
}
