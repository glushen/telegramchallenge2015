package ru.glushen.telegramchallenge.inputmethod;

import android.app.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.Fragment;
import android.support.v4.view.*;
import android.support.v7.widget.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static android.support.v7.widget.RecyclerView.*;
import static android.view.ViewGroup.LayoutParams.*;
import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 21.06.2015.
 */
public class EmojiKeyboardFragment extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener
{
    private static final EmojiAdapter[] adapterArray = new EmojiAdapter[6];

    static
    {
        adapterArray[0] = new EmojiAdapter(R.drawable.ic_smiles_recent, R.drawable.ic_smiles_recent_active);
        adapterArray[1] = new EmojiAdapter(0, 189, R.drawable.ic_smiles_smile, R.drawable.ic_smiles_smile_active);
        adapterArray[2] = new EmojiAdapter(189, 305, R.drawable.ic_smiles_flower, R.drawable.ic_smiles_flower_active);
        adapterArray[3] = new EmojiAdapter(305, 535, R.drawable.ic_smiles_bell, R.drawable.ic_smiles_bell_active);
        adapterArray[4] = new EmojiAdapter(535, 636, R.drawable.ic_smiles_car, R.drawable.ic_smiles_car_active);
        adapterArray[5] = new EmojiAdapter(636, 845, R.drawable.ic_smiles_grid, R.drawable.ic_smiles_grid_active);
    }

    @Override public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        adapterArray[0].initRecently(getActivity());

        InputManager.emojiClickDispatcher.addObtainer(emojiClickObtainer, false);
    }

    @Override public void onDetach()
    {
        super.onDetach();

        InputManager.emojiClickDispatcher.removeObtainer(emojiClickObtainer);
    }

    private Obtainer<InputManager.EmojiClickEvent> emojiClickObtainer = new Obtainer<InputManager.EmojiClickEvent>()
    {
        @Override public void obtain(InputManager.EmojiClickEvent event)
        {
            if (!event.fromRecently)
            {
                adapterArray[0].pushFront(event.index, getActivity());
            }
        }
    };

    private ViewPager pager;
    private ArrayList<ImageView> tabIconViewArray = new ArrayList<>();
    private View sliderPaddingView;

    @Override public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_keyboard_emoji, container, false);

        view.findViewById(R.id.backspace).setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View view)
            {
                InputManager.backspaceClickDispatcher.dispatch(null);
            }
        });

        pager = (ViewPager) view.findViewById(R.id.keyboardPager);

        pager.setAdapter(new PagerAdapter()
        {
            @Override public int getCount()
            {
                return adapterArray.length;
            }

            private RecycledViewPool recycledViewPool = new RecycledViewPool();

            @Override public Object instantiateItem(ViewGroup container, final int position)
            {
                final RecyclerView recyclerView;

                if (destroyedRecyclerView == null)
                {
                    recyclerView = new RecyclerView(container.getContext());
                    recyclerView.setLayoutParams(new ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT));
                    recyclerView.setAdapter(adapterArray[position]);
                    recyclerView.setRecycledViewPool(recycledViewPool);

                    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                    int columnCount = displayMetrics.widthPixels / Screen.dpPixelSize(EmojiAdapter.COLUMN_WIDTH_IN_DP, displayMetrics);
                    final GridLayoutManager layoutManager = new GridLayoutManager(inflater.getContext(), columnCount);
                    recyclerView.setLayoutManager(layoutManager);
                }
                else
                {
                    recyclerView = destroyedRecyclerView;
                    destroyedRecyclerView = null;
                    recyclerView.swapAdapter(adapterArray[position], true);
                }

                container.addView(recyclerView, 0);

                return recyclerView;
            }

            @Nullable private RecyclerView destroyedRecyclerView = null;

            @Override public void destroyItem(ViewGroup container, int position, Object object)
            {
                destroyedRecyclerView = (RecyclerView) object;
                container.removeView(destroyedRecyclerView);
            }

            @Override public boolean isViewFromObject(View view, Object object)
            {
                return view.equals(object);
            }
        });

        pager.addOnPageChangeListener(this);

        LinearLayout tabContainer = (LinearLayout) view.findViewById(R.id.tabContainer);

        for (int i = 0; i < adapterArray.length + 1; i++)
        {
            View tabView = inflater.inflate(R.layout.list_keyboard_tab, tabContainer, false);
            tabView.setTag(i);
            tabView.setOnClickListener(this);

            ImageView tabIconView = (ImageView) tabView.findViewById(R.id.tabIcon);

            if (i < adapterArray.length)
            {
                tabIconView.setImageResource(adapterArray[i].defaultIconResource);
                tabIconView.setBackgroundResource(adapterArray[i].activeIconResource);
            }
            else
            {
                tabIconView.setImageResource(R.drawable.ic_smiles_sticker);
            }

            tabIconViewArray.add(tabIconView);

            tabContainer.addView(tabView);
        }

        ((LinearLayout) view.findViewById(R.id.sliderContainer)).setWeightSum(adapterArray.length + 1);

        sliderPaddingView = view.findViewById(R.id.sliderPadding);

        return view;
    }

    @Override public void onDestroyView()
    {
        super.onDestroyView();

        pager = null;
        tabIconViewArray.clear();
        sliderPaddingView = null;
    }

    @SuppressWarnings("deprecation") @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
    {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) sliderPaddingView.getLayoutParams();
        layoutParams.weight = position + positionOffset;
        sliderPaddingView.setLayoutParams(layoutParams);

        for (int i = 0; i < tabIconViewArray.size(); i++)
        {
            int foregroundAlpha;

            if (i == position)
            {
                foregroundAlpha = round(positionOffset * 255);
            }
            else if (i == position + 1)
            {
                foregroundAlpha = round((1 - positionOffset) * 255);
            }
            else
            {
                foregroundAlpha = 255;
            }

            tabIconViewArray.get(i).setAlpha(foregroundAlpha);
        }
    }

    @Override public void onPageSelected(int position)
    {

    }

    @Override public void onPageScrollStateChanged(int state)
    {

    }

    @Override public void onClick(View view)
    {
        int position = (Integer) view.getTag();

        if (position < adapterArray.length)
        {
            pager.setCurrentItem(position);
        }
        else
        {
            ((KeyboardFragment) getParentFragment()).toggleKeyboard();
        }
    }
}
