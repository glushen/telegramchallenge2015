package ru.glushen.telegramchallenge.inputmethod;

import android.app.*;
import android.content.*;
import android.support.annotation.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.*;
import android.util.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.emoji.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static android.content.Context.*;
import static android.view.ViewGroup.LayoutParams.*;
import static android.view.ViewTreeObserver.*;

/**
 * Created by Pavel Glushen on 23.06.2015.
 */
public class InputManager
{
    public enum Type
    {
        DEFAULT, SMILE, BOT
    }

    public static class InputMethodEvent
    {
        @NonNull public final Type type;
        public final boolean shown;

        public InputMethodEvent(@NonNull Type type, boolean shown)
        {
            this.type = type;
            this.shown = shown;
        }
    }

    public static InputManager from(Activity activity)
    {
        return ((SingleActivity) activity).getInputManager();
    }

    public final EventDispatcher<InputMethodEvent> inputMethodDispatcher = new EventDispatcher<>();

    public static class EmojiClickEvent
    {
        public final int index;
        public final boolean fromRecently;

        public EmojiClickEvent(int index, boolean fromRecently)
        {
            this.index = index;
            this.fromRecently = fromRecently;
        }
    }

    public static final EventDispatcher<EmojiClickEvent> emojiClickDispatcher = new EventDispatcher<>();

    public static class StickerClickEvent
    {
        public final TdApi.Sticker sticker;
        public final boolean fromRecently;

        public StickerClickEvent(TdApi.Sticker sticker, boolean fromRecently)
        {
            this.sticker = sticker;
            this.fromRecently = fromRecently;
        }
    }

    private boolean recordingDefaultKeyboardHeight = false;

    public void startRecordDefaultKeyboardHeight()
    {
        recordingDefaultKeyboardHeight = true;
    }

    public void stopRecordDefaultKeyboardHeight()
    {
        recordingDefaultKeyboardHeight = false;
    }

    public static final EventDispatcher<StickerClickEvent> stickerClickDispatcher = new EventDispatcher<>();

    public static final EventDispatcher<Void> backspaceClickDispatcher = new EventDispatcher<>();

    @NonNull private final SingleActivity activity;

    private static final String PREFERENCES_NAME = InputManager.class.getCanonicalName();
    private static final int UNKNOWN_HEIGHT = -1;

    private int savedFragmentContainerHeight = UNKNOWN_HEIGHT;

    private final View contentContainer;
    private final View fragmentContainer;
    private final FrameLayout smileKeyboardContainer;
    private final LinearLayout botKeyboardContainer;

    private boolean savedIsDefaultKeyboardShown;

    private String generateFragmentContainerHeightPreferencesKey()
    {
        DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
        return "fragmentContainerHeight for " + displayMetrics.widthPixels + "x" + displayMetrics.heightPixels;
    }

    public InputManager(@NonNull final SingleActivity activity)
    {
        this.activity = activity;

        SharedPreferences preferences = activity.getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
        savedFragmentContainerHeight = preferences.getInt(generateFragmentContainerHeightPreferencesKey(), UNKNOWN_HEIGHT);

        contentContainer = activity.findViewById(android.R.id.content);
        fragmentContainer = activity.findViewById(R.id.fragmentContainer);
        smileKeyboardContainer = (FrameLayout) activity.findViewById(R.id.smileKeyboardContainer);
        botKeyboardContainer = (LinearLayout) activity.findViewById(R.id.botKeyboardContainer);

        contentContainer.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener()
        {
            @Override public void onGlobalLayout()
            {
                if (isDefaultKeyboardShown())
                {
                    if (!savedIsDefaultKeyboardShown)
                    {
                        savedIsDefaultKeyboardShown = true;

                        inputMethodDispatcher.dispatch(new InputMethodEvent(Type.DEFAULT, true));

                        showingDefaultKeyboard = false;
                        showingDefaultKeyboardEditText = null;

                        hideCustomKeyboard();
                    }

                    int fragmentContainerHeight = fragmentContainer.getHeight();

                    if (!hidingDefaultKeyboard)
                    {
                        releaseFragmentContainerHeight();

                        if (recordingDefaultKeyboardHeight && savedFragmentContainerHeight != fragmentContainerHeight)
                        {
                            savedFragmentContainerHeight = fragmentContainerHeight;

                            SharedPreferences preferences = activity.getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
                            preferences.edit().putInt(generateFragmentContainerHeightPreferencesKey(), savedFragmentContainerHeight).apply();
                        }
                    }
                }
                else
                {
                    if (showingDefaultKeyboardEditText != null)
                    {
                        forceShowDefaultKeyboard(showingDefaultKeyboardEditText);
                        showingDefaultKeyboardEditText = null;
                    }

                    if (savedIsDefaultKeyboardShown)
                    {
                        savedIsDefaultKeyboardShown = false;

                        inputMethodDispatcher.dispatch(new InputMethodEvent(Type.DEFAULT, false));

                        hidingDefaultKeyboard = false;
                    }

                    if (isBotKeyboardResized() || !showingDefaultKeyboard && !showingSmileKeyboard && !isSmileKeyboardShown() && !showingBotKeyboard && !isBotKeyboardShown())
                    {
                        releaseFragmentContainerHeight();
                    }
                }
            }
        });
    }

    private boolean fragmentContainerHeightHeld = false;

    private void holdFragmentContainerHeight()
    {
        if (savedFragmentContainerHeight != UNKNOWN_HEIGHT && !fragmentContainerHeightHeld)
        {
            fragmentContainerHeightHeld = true;

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) fragmentContainer.getLayoutParams();
            layoutParams.weight = 0;
            layoutParams.height = savedFragmentContainerHeight;
            fragmentContainer.setLayoutParams(layoutParams);
        }
    }

    private void releaseFragmentContainerHeight()
    {
        if (fragmentContainerHeightHeld)
        {
            fragmentContainerHeightHeld = false;

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) fragmentContainer.getLayoutParams();
            layoutParams.weight = 1;
            layoutParams.height = 0;
            fragmentContainer.setLayoutParams(layoutParams);
        }
    }

    private boolean showingDefaultKeyboard = false;
    private boolean hidingDefaultKeyboard = false;
    @Nullable private EditText showingDefaultKeyboardEditText = null;

    public void showDefaultKeyboard(@NonNull EditText editText)
    {
        if (!showingDefaultKeyboard && !isDefaultKeyboardShown() || hidingDefaultKeyboard || !editText.hasFocus())
        {
            showingDefaultKeyboard = true;
            hidingDefaultKeyboard = false;

            editText.requestFocus();

            showingDefaultKeyboardEditText = editText;

            forceShowDefaultKeyboard(editText);
        }
    }

    private void forceShowDefaultKeyboard(@NonNull EditText editText)
    {
        ((InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE)).showSoftInput(editText, 0);
    }

    public void hideDefaultKeyboard(@NonNull EditText editText)
    {
        if ((showingDefaultKeyboard || isDefaultKeyboardShown()) && !hidingDefaultKeyboard)
        {
            showingDefaultKeyboard = false;
            hidingDefaultKeyboard = true;
            showingDefaultKeyboardEditText = null;

            if (!showingSmileKeyboard && !showingBotKeyboard)
            {
                releaseFragmentContainerHeight();
            }

            ((InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    public boolean isDefaultKeyboardShown()
    {
        DisplayMetrics displayMetrics = contentContainer.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels - contentContainer.getHeight() > Screen.dpPixelSize(100, displayMetrics);
    }

    private boolean showingSmileKeyboard = false;

    public void showSmileKeyboard(@NonNull EditText editText)
    {
        if (!showingSmileKeyboard && !isSmileKeyboardShown() || !editText.hasFocus())
        {
            showingSmileKeyboard = true;

            holdFragmentContainerHeight();

            hideDefaultKeyboard(editText);
            hideBotKeyboard();

            editText.requestFocus();

            FragmentManager fragmentManager = activity.getSupportFragmentManager();

            if (fragmentManager.findFragmentById(R.id.smileKeyboardContainer) == null)
            {
                fragmentManager.beginTransaction().replace(R.id.smileKeyboardContainer, new KeyboardFragment()).commit();
            }

            smileKeyboardContainer.setVisibility(View.VISIBLE);

            inputMethodDispatcher.dispatch(new InputMethodEvent(Type.SMILE, true));

            showingSmileKeyboard = false;
        }
    }

    public void hideSmileKeyboard()
    {
        if (showingSmileKeyboard || isSmileKeyboardShown())
        {
            showingSmileKeyboard = false;

            if (!showingDefaultKeyboard && !showingBotKeyboard)
            {
                releaseFragmentContainerHeight();
            }

            smileKeyboardContainer.setVisibility(View.GONE);

            inputMethodDispatcher.dispatch(new InputMethodEvent(Type.SMILE, false));
        }
    }

    public boolean isSmileKeyboardShown()
    {
        return smileKeyboardContainer.getVisibility() == View.VISIBLE;
    }

    public interface OnSelectBotKeyboardItem
    {
        void onSelect(String text);
    }

    private boolean showingBotKeyboard = false;

    public void showBotKeyboard(@NonNull final EditText editText, @NonNull String[][] rowArray, boolean resizeKeyboard, final boolean revertAfterUse, @NonNull final OnSelectBotKeyboardItem onSelectBotKeyboardItem)
    {
        if (!showingBotKeyboard && !isBotKeyboardShown() || !editText.hasFocus())
        {
            showingBotKeyboard = true;

            final boolean defaultKeyboardShown = isDefaultKeyboardShown();
            final boolean smileKeyboardShown = isSmileKeyboardShown();

            hideDefaultKeyboard(editText);
            hideSmileKeyboard();

            if (hidingDefaultKeyboard || !resizeKeyboard)
            {
                holdFragmentContainerHeight();
            }

            editText.requestFocus();

            botKeyboardContainer.removeAllViews();
            LinearLayout.LayoutParams keyboardContainerParams = new LinearLayout.LayoutParams(MATCH_PARENT, resizeKeyboard ? WRAP_CONTENT : 0, resizeKeyboard ? 0 : 1);
            botKeyboardContainer.setLayoutParams(keyboardContainerParams);
            LayoutInflater layoutInflater = LayoutInflater.from(activity);

            for (String[] row : rowArray)
            {
                LinearLayout rowLayout = new LinearLayout(activity);
                rowLayout.setOrientation(LinearLayout.HORIZONTAL);
                rowLayout.setLayoutParams(keyboardContainerParams);
                botKeyboardContainer.addView(rowLayout);

                for (final String rowItem : row)
                {
                    int layoutRes = resizeKeyboard ? R.layout.item_keyboard_bot_resize_keyboard : R.layout.item_keyboard_bot;
                    final TextView rowItemView = (TextView) layoutInflater.inflate(layoutRes, rowLayout, false);

                    Emoji.stringToSpannable(rowItem, rowItemView.getResources(), new CloseableObtainer<Spannable>()
                    {
                        @Override protected void onObtain(Spannable spannable)
                        {
                            rowItemView.setText(spannable);
                        }
                    });

                    rowItemView.setOnClickListener(new View.OnClickListener()
                    {
                        @Override public void onClick(View view)
                        {
                            onSelectBotKeyboardItem.onSelect(rowItem);

                            if (revertAfterUse)
                            {
                                if (defaultKeyboardShown)
                                {
                                    showDefaultKeyboard(editText);
                                }
                                else if (smileKeyboardShown)
                                {
                                    showSmileKeyboard(editText);
                                }
                                else
                                {
                                    hideBotKeyboard();
                                }
                            }
                        }
                    });

                    rowLayout.addView(rowItemView);
                }
            }

            botKeyboardContainer.setVisibility(View.VISIBLE);

            inputMethodDispatcher.dispatch(new InputMethodEvent(Type.BOT, true));

            showingBotKeyboard = false;
        }
    }

    public void hideBotKeyboard()
    {
        if (showingBotKeyboard || isBotKeyboardShown())
        {
            showingBotKeyboard = false;

            if (!showingDefaultKeyboard && !showingSmileKeyboard)
            {
                releaseFragmentContainerHeight();
            }
            else
            {
                holdFragmentContainerHeight();
            }

            botKeyboardContainer.setVisibility(View.GONE);

            inputMethodDispatcher.dispatch(new InputMethodEvent(Type.BOT, false));
        }
    }

    public boolean isBotKeyboardShown()
    {
        return botKeyboardContainer.getVisibility() == View.VISIBLE;
    }

    public boolean isBotKeyboardResized()
    {
        ViewGroup.LayoutParams params = botKeyboardContainer.getLayoutParams();
        return isBotKeyboardShown() && params.height == WRAP_CONTENT;
    }

    public void hideCustomKeyboard()
    {
        hideSmileKeyboard();
        hideBotKeyboard();
    }

    public boolean isCustomKeyboardShown()
    {
        return isSmileKeyboardShown() || isBotKeyboardShown();
    }

    public void hideAnyKeyboard(@NonNull EditText editText)
    {
        hideDefaultKeyboard(editText);
        hideCustomKeyboard();
    }

    public boolean isAnyKeyboardShown()
    {
        return isDefaultKeyboardShown() || isCustomKeyboardShown();
    }

    public void reset()
    {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();

        Fragment fragment = fragmentManager.findFragmentById(R.id.smileKeyboardContainer);

        if (fragment != null)
        {
            fragmentManager.beginTransaction().remove(fragment).commit();
        }
    }
}
