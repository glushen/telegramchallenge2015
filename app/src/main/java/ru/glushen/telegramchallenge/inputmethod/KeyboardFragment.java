package ru.glushen.telegramchallenge.inputmethod;

import android.os.*;
import android.support.v4.app.*;
import android.support.v4.view.*;
import android.view.*;

import ru.glushen.telegramchallenge.*;

/**
 * Created by Pavel Glushen on 26.07.2015.
 */
public class KeyboardFragment extends Fragment
{
    private ViewPager viewPager;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        viewPager = (ViewPager) inflater.inflate(R.layout.fragment_keyboard, container, false);

        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager())
        {
            @Override public Fragment getItem(int position)
            {
                return position == 0 ? new EmojiKeyboardFragment() : new StickerKeyboardFragment();
            }

            @Override public int getCount()
            {
                return 2;
            }
        });

        return viewPager;
    }

    void toggleKeyboard()
    {
        if (viewPager != null)
        {
            viewPager.setCurrentItem(viewPager.getCurrentItem() == 0 ? 1 : 0);
        }
    }
}
