package ru.glushen.telegramchallenge.inputmethod;

import android.content.*;
import android.content.res.*;
import android.graphics.drawable.*;
import android.support.annotation.*;
import android.support.v7.widget.*;
import android.text.*;
import android.util.*;
import android.view.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.emoji.*;
import ru.glushen.ui.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 21.06.2015.
 */
class EmojiAdapter extends RecyclerView.Adapter<EmojiAdapter.ViewHolder> implements RecyclerViewHolder.OnClickListener<Integer>
{
    public static final int COLUMN_WIDTH_IN_DP = 46;

    static class ViewHolder extends RecyclerViewHolder<Integer> implements SmartImageView.BitmapDrawableCreator
    {
        private static boolean dataCached = false;

        private static ViewGroup.LayoutParams layoutParams;
        private static int padding;

        private static SmartImageView createView(ViewGroup parent)
        {
            if (!dataCached)
            {
                dataCached = true;

                Resources resources = parent.getResources();
                DisplayMetrics displayMetrics = resources.getDisplayMetrics();

                int size = Screen.dpPixelSize(COLUMN_WIDTH_IN_DP, displayMetrics);
                layoutParams = new ViewGroup.LayoutParams(size, size);

                padding = Screen.dpPixelSize(8, displayMetrics);
            }

            SmartImageView view = new SmartImageView(parent.getContext());
            view.setLayoutParams(layoutParams);
            view.setPadding(padding, padding, padding, padding);
            view.setBackgroundResource(R.drawable.pressable_background_transparent);

            return view;
        }

        private SmartImageView emojiView;

        public ViewHolder(ViewGroup parent)
        {
            super(createView(parent));

            emojiView = (SmartImageView) itemView;
        }

        private int index;

        @Override protected void onBind(Integer index)
        {
            this.index = index;

            emojiView.setBitmapDrawableCreators(this);
        }

        @Override public ViewGroup.LayoutParams onCreate(int initWidth, int initHeight, CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer, Resources resources)
        {
            Emoji.getBitmapDrawable(index, min(initWidth, initHeight), resources, bitmapDrawableObtainer);

            return null;
        }
    }

    private static final String PREFERENCES_NAME = EmojiAdapter.class.getCanonicalName();
    private static final String DATA_KEY = EmojiAdapter.class.getCanonicalName();
    private static final String DATA_SEPARATOR = ";";

    private final boolean recently;

    public final int defaultIconResource;
    public final int activeIconResource;

    public EmojiAdapter(@DrawableRes int defaultIconResource, @DrawableRes int activeIconResource)
    {
        recently = true;

        this.defaultIconResource = defaultIconResource;
        this.activeIconResource = activeIconResource;
    }

    private boolean recentlyInitialized = false;
    private final List<Integer> recentlyIndexList = new ArrayList<>();

    public void initRecently(@NonNull Context context)
    {
        if (recently && !recentlyInitialized)
        {
            String joinedIndexArray = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE).getString(DATA_KEY, "");

            if (!joinedIndexArray.isEmpty())
            {
                String[] indexStringArray = joinedIndexArray.split(DATA_SEPARATOR);

                for (String indexString : indexStringArray)
                {
                    recentlyIndexList.add(Integer.parseInt(indexString));
                }
            }

            notifyDataSetChanged();

            recentlyInitialized = true;
        }
    }

    public void pushFront(int index, @NonNull Context context)
    {
        initRecently(context);

        int indexOfIndex = recentlyIndexList.indexOf(index);

        while (indexOfIndex != -1)
        {
            recentlyIndexList.remove(indexOfIndex);
            notifyItemRemoved(indexOfIndex);

            indexOfIndex = recentlyIndexList.indexOf(index);
        }

        recentlyIndexList.add(0, index);
        notifyItemInserted(0);

        String joinedIndexArray = TextUtils.join(DATA_SEPARATOR, recentlyIndexList);
        context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE).edit().putString(DATA_KEY, joinedIndexArray).apply();
    }

    private int startIndex;
    private int endIndex;

    public EmojiAdapter(int startIndex, int endIndex, @DrawableRes int defaultIconResource, @DrawableRes int activeIconResource)
    {
        recently = false;

        this.startIndex = startIndex;
        this.endIndex = endIndex;

        this.defaultIconResource = defaultIconResource;
        this.activeIconResource = activeIconResource;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new ViewHolder(parent);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.bind(recently ? recentlyIndexList.get(position) : (startIndex + position));
        holder.setOnClickListener(this);
    }

    @Override public void onClick(RecyclerViewHolder<Integer> holder, Integer index)
    {
        InputManager.emojiClickDispatcher.dispatch(new InputManager.EmojiClickEvent(index, recently));
    }

    @Override public int getItemCount()
    {
        return recently ? recentlyIndexList.size() : (endIndex - startIndex);
    }
}
