package ru.glushen.telegramchallenge.inputmethod;

import android.os.*;
import android.support.v4.app.*;
import android.support.v7.widget.*;
import android.util.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.image.Image.Filter.*;
import static ru.glushen.image.Image.*;
import static ru.glushen.ui.SmartImageView.*;
import static ru.glushen.util.Screen.*;

/**
 * Created by Pavel Glushen on 21.06.2015.
 */
public class StickerKeyboardFragment extends Fragment
{
    private final TabContainerAdapter tabContainerAdapter = new TabContainerAdapter();
    private final GridAdapter gridAdapter = new GridAdapter();

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_keyboard_sticker, container, false);

        view.findViewById(R.id.emojiKeyboardShowButton).setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                ((KeyboardFragment) getParentFragment()).toggleKeyboard();
            }
        });

        RecyclerView tabContainerView = (RecyclerView) view.findViewById(R.id.tabContainer);
        tabContainerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        tabContainerView.setAdapter(tabContainerAdapter);

        RecyclerView stickerGridView = (RecyclerView) view.findViewById(R.id.stickerGrid);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        gridColumnCount = displayMetrics.widthPixels / dpPixelSize(64, displayMetrics);
        stickerGridView.setLayoutManager(new GridLayoutManager(getActivity(), gridColumnCount));
        stickerGridView.setAdapter(gridAdapter);

        Info.getStickerSetList(stickerSetListObtainer);
        Info.STICKER_SET_LIST_DISPATCHER.addObtainer(stickerSetListObtainer, false);

        return view;
    }

    @Override public void onDestroyView()
    {
        super.onDestroyView();

        Info.STICKER_SET_LIST_DISPATCHER.removeObtainer(stickerSetListObtainer);
    }

    private int gridColumnCount = 1;

    private final Obtainer<List<TdApi.StickerSet>> stickerSetListObtainer = new Obtainer<List<TdApi.StickerSet>>()
    {
        @Override public void obtain(List<TdApi.StickerSet> stickerSetList)
        {
            tabContainerAdapter.setStickerSetList(stickerSetList);
            gridAdapter.setStickerSetList(stickerSetList, gridColumnCount);
        }
    };

    private class TabContainerAdapter extends ScrollableRecyclerViewAdapter<RecyclerViewHolder<Void>>
    {
        private final List<TdApi.StickerSet> stickerSetList = new ArrayList<>();

        public void setStickerSetList(List<TdApi.StickerSet> stickerSetList)
        {
            this.stickerSetList.clear();
            this.stickerSetList.addAll(stickerSetList);
            currentSetId = !stickerSetList.isEmpty() ? stickerSetList.get(0).id : -1;
            notifyDataSetChanged();
        }

        private long currentSetId;

        public void setCurrentSetId(long currentSetId)
        {
            this.currentSetId = currentSetId;

            notifyDataSetChanged();

            for (int i = 0; i < stickerSetList.size(); i++)
            {
                if (stickerSetList.get(i).id == currentSetId)
                {
                    scrollToPosition(i);
                    break;
                }
            }
        }

        @Override public RecyclerViewHolder<Void> onCreateViewHolder(ViewGroup parent, int position)
        {
            return new RecyclerViewHolder<>(parent, R.layout.list_sticker_tab);
        }

        @Override public void onBindViewHolder(RecyclerViewHolder<Void> viewHolder, int position)
        {
            final TdApi.StickerSet stickerSet = stickerSetList.get(position);

            if (stickerSet.id == currentSetId)
            {
                viewHolder.itemView.setBackgroundColor(0xffe2e5e7);
            }
            else
            {
                viewHolder.itemView.setBackgroundResource(R.drawable.pressable_background_transparent);
            }

            TdApi.Sticker[] stickerArray = stickerSet.stickers;

            if (stickerArray.length > 0)
            {
                TdApi.Sticker sticker = stickerArray[0];

                BitmapDrawableCreator[] creators = new BitmapDrawableCreator[2];
                creators[0] = new FileBitmapDrawableCreator(FileLoader.getInstance(sticker.thumb.photo), BOX, NOT_DEFINE);
                creators[1] = new FileBitmapDrawableCreator(FileLoader.getInstance(sticker.sticker), BOX, NOT_DEFINE);

                ((SmartImageView) viewHolder.itemView).setBitmapDrawableCreators(creators);
            }

            viewHolder.itemView.setOnClickListener(new OnClickListener()
            {
                @Override public void onClick(View v)
                {
                    currentSetId = stickerSet.id;
                    notifyDataSetChanged();
                    gridAdapter.setCurrentSetId(stickerSet.id);
                }
            });
        }

        @Override public int getItemCount()
        {
            return stickerSetList.size();
        }

        {
            setHasStableIds(true);
        }

        @Override public long getItemId(int position)
        {
            return stickerSetList.get(position).id;
        }
    }

    private class GridAdapter extends ScrollableRecyclerViewAdapter<RecyclerViewHolder<Void>>
    {
        private final List<TdApi.Sticker> stickerOrNullList = new ArrayList<>();

        public void setStickerSetList(List<TdApi.StickerSet> stickerSetList, int columnCount)
        {
            for (TdApi.StickerSet stickerSet : stickerSetList)
            {
                Collections.addAll(stickerOrNullList, stickerSet.stickers);

                while (stickerOrNullList.size() % columnCount != 0)
                {
                    stickerOrNullList.add(null);
                }
            }

            ArrayList<TdApi.Sticker> cloneOfStickerOnNullList = new ArrayList<>(stickerOrNullList);

            Collections.sort(cloneOfStickerOnNullList, new Comparator<TdApi.Sticker>()
            {
                @Override public int compare(TdApi.Sticker lhs, TdApi.Sticker rhs)
                {
                    double left = lhs != null ? lhs.rating : Double.MIN_VALUE;
                    double right = rhs != null ? rhs.rating : Double.MIN_VALUE;
                    return Double.compare(left, right);
                }
            });

            for (TdApi.Sticker stickerOrNull : cloneOfStickerOnNullList)
            {
                if (stickerOrNull != null && stickerOrNull.rating > 0)
                {
                    SingleActivity.log("Sticker: " + stickerOrNull);
                }
            }

            notifyDataSetChanged();
        }

        public void setCurrentSetId(long currentSetId)
        {
            for (int i = 0; i < stickerOrNullList.size(); i++)
            {
                TdApi.Sticker stickerOrNull = stickerOrNullList.get(i);

                if (stickerOrNull != null && stickerOrNull.setId == currentSetId)
                {
                    scrollToPositionWithOffset(i, 0);
                    break;
                }
            }
        }

        @Override public RecyclerViewHolder<Void> onCreateViewHolder(ViewGroup parent, int position)
        {
            return new RecyclerViewHolder<>(parent, R.layout.list_sticker_grid);
        }

        @Override public void onBindViewHolder(RecyclerViewHolder<Void> viewHolder, int position)
        {
            final TdApi.Sticker stickerOnNull = stickerOrNullList.get(position);

            if (stickerOnNull != null)
            {
                viewHolder.itemView.setVisibility(VISIBLE);

                BitmapDrawableCreator[] creators = new BitmapDrawableCreator[2];
                creators[0] = new FileBitmapDrawableCreator(FileLoader.getInstance(stickerOnNull.thumb.photo), BOX, NOT_DEFINE);
                creators[1] = new FileBitmapDrawableCreator(FileLoader.getInstance(stickerOnNull.sticker), BOX, NOT_DEFINE);

                ((SmartImageView) viewHolder.itemView).setBitmapDrawableCreators(creators);

                viewHolder.setOnClickListener(new RecyclerViewHolder.OnClickListener<Void>()
                {
                    @Override public void onClick(RecyclerViewHolder<Void> holder, Void item)
                    {
                        InputManager.stickerClickDispatcher.dispatch(new InputManager.StickerClickEvent(stickerOnNull, false));
                    }
                });
            }
            else
            {
                viewHolder.itemView.setVisibility(INVISIBLE);

                ((SmartImageView) viewHolder.itemView).setBitmapDrawableCreators();

                viewHolder.setOnClickListener(null);
            }
        }

        @Override public int getItemCount()
        {
            return stickerOrNullList.size();
        }

        @Override protected void onVisibleRangeChanged(RecyclerView recyclerView)
        {
            LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());

            int firstPosition = layoutManager.findFirstVisibleItemPosition();
            int lastPosition = layoutManager.findLastVisibleItemPosition();
            int position = firstPosition + (lastPosition - firstPosition) / 2;

            while (stickerOrNullList.get(position) == null)
            {
                position--;

                if (position < 0)
                {
                    return;
                }
            }

            tabContainerAdapter.setCurrentSetId(stickerOrNullList.get(position).setId);
        }
    }
}
