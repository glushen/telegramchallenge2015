package ru.glushen.telegramchallenge.telegramui;

import android.content.*;
import android.graphics.*;
import android.util.*;
import android.view.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.util.*;

import static java.lang.Math.*;
import static ru.glushen.util.Screen.*;

/**
 * Created by Pavel Glushen on 07.08.2015.
 */
public abstract class AbstractProgressBar extends View
{
    public AbstractProgressBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    private final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

    private final Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    {
        backgroundPaint.setColor(0x00000000);
    }

    private final Paint progressPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    {
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setColor(getResources().getColor(R.color.primary));
    }

    private final RectF backgroundRect = new RectF();
    private final RectF progressRect = new RectF();

    @Override protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight)
    {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        int contentWidth = width - getPaddingLeft() - getPaddingRight();
        int contentHeight = height - getPaddingTop() - getPaddingBottom();

        int strokeWidth = dpPixelSize(min(contentWidth, contentHeight) > dpPixelSize(50, displayMetrics) ? 4 : 2, displayMetrics);
        progressPaint.setStrokeWidth(strokeWidth);

        int size = min(contentWidth, contentHeight) - dpPixelSize(2, displayMetrics);

        backgroundRect.left = (width - size) / 2f;
        backgroundRect.top = (height - size) / 2f;
        backgroundRect.right = backgroundRect.left + size;
        backgroundRect.bottom = backgroundRect.top + size;

        float halfStrokeWidth = strokeWidth / 2f;

        progressRect.left = backgroundRect.left + halfStrokeWidth;
        progressRect.top = backgroundRect.top + halfStrokeWidth;
        progressRect.right = backgroundRect.right - halfStrokeWidth;
        progressRect.bottom = backgroundRect.bottom - halfStrokeWidth;
    }

    private boolean started = false;
    private long startTime;

    @Override protected final void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        canvas.drawOval(backgroundRect, backgroundPaint);

        if (isEnabled())
        {
            if (!started)
            {
                startTime = System.currentTimeMillis();
                started = true;
            }

            onDrawProgress(startTime);

            canvas.drawArc(progressRect, startAngle, sweepAngle, false, progressPaint);

            Compat.invalidateOnAnimation(this);
        }
    }

    protected abstract void onDrawProgress(long startTime);

    private float startAngle;
    private float sweepAngle;

    protected void setProgressAngles(float startAngle, float sweepAngle)
    {
        this.startAngle = startAngle;
        this.sweepAngle = sweepAngle;
    }

    public void setProgressColor(int color)
    {
        progressPaint.setColor(color);
        invalidate();
    }

    public void setBackgroundCircleColor(int color)
    {
        backgroundPaint.setColor(color);
        invalidate();
    }
}
