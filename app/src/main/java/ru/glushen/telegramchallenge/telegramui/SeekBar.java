package ru.glushen.telegramchallenge.telegramui;

import android.content.*;
import android.graphics.*;
import android.support.annotation.*;
import android.util.*;
import android.view.*;

import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 17.08.2015.
 */
public class SeekBar extends View implements View.OnTouchListener
{
    public enum Style
    {
        VOICE, MUSIC
    }

    @NonNull private Style style = Style.VOICE;

    public void setStyle(@NonNull Style style)
    {
        this.style = style;

        progressY = (getHeight() - getPaddingTop() - getPaddingBottom()) / 2f + getPaddingTop();

        if (style == Style.VOICE)
        {
            radius = 8 * dp;

            left = getPaddingLeft() + radius;
            top = progressY - dp;
            right = getWidth() - getPaddingRight() - radius;
            bottom = progressY + dp;

            backgroundPaint.setColor(0xffdcebf5);
            foregroundPaint.setColor(0xff68ade1);
        }
        else if (style == Style.MUSIC)
        {
            radius = 6 * dp;

            left = getPaddingLeft();
            top = progressY - 2 * dp;
            right = getWidth() - getPaddingRight();
            bottom = progressY + 2 * dp;

            backgroundPaint.setColor(0xffdddddd);
            foregroundPaint.setColor(0xff68ade1);
        }

        invalidateProgress();
    }

    public interface OnSeekListener
    {
        void onProgress(int progress, int max);
        void onProgressChanged(int progress, int max, boolean byUser);
    }

    @Nullable private OnSeekListener listener;

    public void setListener(@Nullable OnSeekListener listener)
    {
        this.listener = listener;
    }

    private int max = 1;

    public void setMax(int max, boolean byUser)
    {
        if (this.max != max)
        {
            this.max = max;

            if (listener != null)
            {
                listener.onProgressChanged(progress, max, byUser);
            }

            invalidateProgress();
        }
    }

    private int progress = 0;

    public void setProgress(int progress, boolean byUser)
    {
        if (this.progress != progress)
        {
            this.progress = progress;

            if (listener != null)
            {
                listener.onProgressChanged(progress, max, byUser);
            }

            invalidateProgress();
        }
    }

    private void invalidateProgress()
    {
        progressStepWidth = (right - left) / (float) (max != 0 ? max : progress);
        progressX = left + progressStepWidth * progress;

        if (!touching)
        {
            invalidate();
        }
    }

    private final float dp = getResources().getDisplayMetrics().density;

    private float radius;
    private float left;
    private float top;
    private float right;
    private float bottom;
    private float progressX;
    private float progressStepWidth;
    private float progressY;

    @Override protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight)
    {
        setStyle(style);
    }

    {
        setOnTouchListener(this);
    }

    private boolean touching = false;

    public boolean isTouching()
    {
        return touching;
    }

    @Override public boolean onTouch(View v, MotionEvent event)
    {
        if (isEnabled())
        {
            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                {
                    touching = true;
                    getParent().requestDisallowInterceptTouchEvent(true);
                    // intentionally do not return
                }
                case MotionEvent.ACTION_MOVE:
                {
                    int currentProgress = round(min(max(event.getX(), left), right) / (right - left) * max);
                    progressX = left + currentProgress * progressStepWidth;

                    if (listener != null)
                    {
                        listener.onProgress(currentProgress, max);
                    }

                    invalidate();
                }
                return true;

                case MotionEvent.ACTION_UP:
                {
                    touching = false;
                    setProgress(round(min(max(event.getX(), left), right) / (right - left) * max), true);
                    // intentionally do not return
                }
                case MotionEvent.ACTION_CANCEL:
                {
                    touching = false;
                    getParent().requestDisallowInterceptTouchEvent(false);
                    invalidateProgress();
                }
                return true;
            }
        }

        return false;
    }

    private final Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint foregroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    @Override protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        canvas.drawRect(left, top, right, bottom, backgroundPaint);
        canvas.drawRect(left, top, progressX, bottom, foregroundPaint);

        if (isEnabled())
        {
            canvas.drawCircle(progressX, progressY, radius, foregroundPaint);
        }
    }

    public SeekBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
}
