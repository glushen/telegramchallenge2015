package ru.glushen.telegramchallenge.telegramui;

import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.support.annotation.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;

import static android.view.View.MeasureSpec.*;
import static java.lang.Math.*;
import static ru.glushen.util.Screen.*;

/**
 * Created by Pavel Glushen on 03.08.2015.
 */
public class Toolbar extends ViewGroup
{
    public Toolbar(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        setWillNotDraw(false);
    }

    public enum Style
    {
        PRIMARY, WHITE, TRANSPARENT, GREEN
    }

    @NonNull private Style style = Style.PRIMARY;

    public void setStyle(@NonNull Style style)
    {
        this.style = style;

        int backgroundColor;
        int titleColor;
        int subtitleColor;
        @DrawableRes int pressableBackgroundRes;
        @DrawableRes int spinnerIconRes;

        switch (style)
        {
            case PRIMARY:
                //noinspection deprecation
                backgroundColor = getResources().getColor(R.color.primary);
                titleColor = 0xffffffff;
                subtitleColor = 0xffd2eafc;
                pressableBackgroundRes = R.drawable.pressable_background_primary;
                spinnerIconRes = R.drawable.ic_spinner_primary_theme;
                break;
            case WHITE:
                backgroundColor = 0xffffffff;
                titleColor = 0xff222222;
                subtitleColor = 0xffaaaaaa;
                pressableBackgroundRes = R.drawable.pressable_background_white;
                spinnerIconRes = R.drawable.ic_spinner_white_theme;
                break;
            case GREEN:
                backgroundColor = 0xff73c979;
                titleColor = 0xffffffff;
                subtitleColor = 0x00000000;
                pressableBackgroundRes = R.drawable.pressable_background_transparent;
                spinnerIconRes = R.drawable.ic_spinner_primary_theme;
                break;
            case TRANSPARENT:
            default:
                backgroundColor = 0x00000000;
                titleColor = 0x00000000;
                subtitleColor = 0x00000000;
                pressableBackgroundRes = R.drawable.pressable_background_transparent;
                spinnerIconRes = 0;
                break;
        }

        setBackgroundColor(backgroundColor);

        if (navigationButtonView != null)
        {
            navigationButtonView.setBackgroundResource(pressableBackgroundRes);
        }

        if (spinnerTextView != null)
        {
            spinnerTextView.setTextColor(titleColor);
            spinnerTextView.setBackgroundResource(pressableBackgroundRes);
            spinnerTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, spinnerIconRes, 0);
        }

        for (ImageView menuButton : menuButtonList)
        {
            menuButton.setBackgroundResource(pressableBackgroundRes);
        }

        titlePaint.setColor(titleColor);

        subtitlePaint.setColor(subtitleColor);

        if (headerBackground != null)
        {
            headerBackground.setBackgroundResource(pressableBackgroundRes);
        }

        requestLayout();
        invalidate();
    }

    private void dispatchUpdate()
    {
        setStyle(style);
    }

    @NonNull private final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

    private final int defaultSize = dpPixelSize(56, displayMetrics);
    private final int expandedSize = dpPixelSize(128, displayMetrics);
    private final int popupWidth = dpPixelSize(196, displayMetrics);

    @Nullable private ImageView navigationButtonView = null;

    @NonNull public ImageView setNavigationButton(@DrawableRes int iconRes, @Nullable OnClickListener onClickListener)
    {
        if (navigationButtonView == null)
        {
            navigationButtonView = new ImageView(getContext());
            navigationButtonView.setScaleType(ImageView.ScaleType.CENTER);
            addView(navigationButtonView);
            dispatchUpdate();
        }

        if (iconRes != 0)
        {
            navigationButtonView.setImageResource(iconRes);
        }
        else
        {
            navigationButtonView.setImageDrawable(null);
        }

        navigationButtonView.setOnClickListener(onClickListener);

        return navigationButtonView;
    }

    public void removeNavigationButton()
    {
        if (navigationButtonView != null)
        {
            removeView(navigationButtonView);
            navigationButtonView = null;
            dispatchUpdate();
        }
    }

    public interface OnSelectListener
    {
        void onSelect(int index);
    }

    @Nullable private TextView spinnerTextView = null;
    private String[] spinnerItemArray = null;
    @Nullable private OnSelectListener onSelectListener;

    public void setSpinner(@ArrayRes int stringArrayRes, @Nullable final OnSelectListener onSelectListener)
    {
        this.onSelectListener = onSelectListener;

        if (spinnerTextView == null)
        {
            spinnerTextView = new TextView(getContext());
            spinnerTextView.setTextSize(20);
            int paddingHorizontal = dpPixelSize(16, displayMetrics);
            int paddingVertical = dpPixelSize(14, displayMetrics);
            spinnerTextView.setPadding(paddingHorizontal, paddingVertical, paddingHorizontal, paddingVertical);
            spinnerTextView.setSingleLine(true);
            spinnerTextView.setEllipsize(TextUtils.TruncateAt.END);
            spinnerTextView.setCompoundDrawablePadding(dpPixelSize(8, displayMetrics));
            spinnerTextView.setTypeface(Font.getRobotoMedium(getResources()));
            addView(spinnerTextView);

            dispatchUpdate();
        }

        spinnerItemArray = getResources().getStringArray(stringArrayRes);

        setCurrentSpinnerItem(0, false);

        final PopupWindow popupWindow = createPopupWindow(spinnerItemArray, new OnSelectListener()
        {
            @Override public void onSelect(int index)
            {
                if (onSelectListener != null && index != currentSpinnerIndex)
                {
                    onSelectListener.onSelect(index);
                }

                setCurrentSpinnerItem(index, false);
            }
        });

        spinnerTextView.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View view)
            {
                popupWindow.showAsDropDown(view, -dpPixelSize(8, displayMetrics), -defaultSize);
            }
        });
    }

    private int currentSpinnerIndex;

    public void setCurrentSpinnerItem(int index, boolean notifyListeners)
    {
        currentSpinnerIndex = index;

        if (spinnerTextView != null && spinnerItemArray != null)
        {
            spinnerTextView.setText(spinnerItemArray[index]);

            if (notifyListeners && onSelectListener != null)
            {
                onSelectListener.onSelect(index);
            }
        }
    }

    @NonNull private final List<ImageView> menuButtonList = new ArrayList<>();

    @NonNull public ImageView addMenuButton(@DrawableRes int iconRes, @Nullable OnClickListener onClickListener)
    {
        return addMenuButton(iconRes, onClickListener, 0, null);
    }

    @NonNull public ImageView addMenuButton(@DrawableRes int iconRes, @ArrayRes int stringArrayRes, @Nullable final OnSelectListener onSelectListener)
    {
        return addMenuButton(iconRes, null, stringArrayRes, onSelectListener);
    }

    @NonNull private ImageView addMenuButton(@DrawableRes int iconRes, @Nullable OnClickListener onClickListener, @ArrayRes int stringArrayRes, @Nullable final OnSelectListener onSelectListener)
    {
        ImageView menuButton = new ImageView(getContext());
        menuButton.setScaleType(ImageView.ScaleType.CENTER);

        if (iconRes != 0)
        {
            menuButton.setImageResource(iconRes);
        }

        if (onClickListener != null)
        {
            menuButton.setOnClickListener(onClickListener);
        }
        else if (stringArrayRes != 0)
        {
            String[] itemArray = getResources().getStringArray(stringArrayRes);

            final PopupWindow popupWindow = createPopupWindow(itemArray, onSelectListener);

            menuButton.setOnClickListener(new OnClickListener()
            {
                @Override public void onClick(View view)
                {
                    popupWindow.showAsDropDown(view, -popupWidth + defaultSize, -defaultSize);
                }
            });
        }

        menuButtonList.add(menuButton);
        addView(menuButton);

        dispatchUpdate();

        return menuButton;
    }

    public void removeMenuButton(@NonNull ImageView menuButton)
    {
        menuButtonList.remove(menuButton);
        removeView(menuButton);
        dispatchUpdate();
    }

    public void setMenuButton(@NonNull ImageView menuButton, @ArrayRes int stringArrayRes, @Nullable final OnSelectListener onSelectListener)
    {
        setMenuButton(menuButton, stringArrayRes != 0 ? getResources().getStringArray(stringArrayRes) : null, onSelectListener);
    }

    public void setMenuButton(@NonNull ImageView menuButton, @Nullable String[] stringArray, @Nullable final OnSelectListener onSelectListener)
    {
        if (menuButtonList.contains(menuButton))
        {
            if (stringArray != null)
            {
                final PopupWindow popupWindow = createPopupWindow(stringArray, onSelectListener);

                menuButton.setOnClickListener(new OnClickListener()
                {
                    @Override public void onClick(View view)
                    {
                        popupWindow.showAsDropDown(view, -popupWidth + defaultSize, -defaultSize);
                    }
                });
            }
            else
            {
                menuButton.setOnClickListener(null);
            }
        }
    }

    @NonNull private PopupWindow createPopupWindow(@NonNull String[] itemArray, @Nullable final OnSelectListener onSelectListener)
    {
        LinearLayout container = new LinearLayout(getContext());
        container.setOrientation(LinearLayout.VERTICAL);
        container.setBackgroundResource(R.drawable.popup_background);

        final PopupWindow popupWindow = new PopupWindow(container, popupWidth, LayoutParams.WRAP_CONTENT, true);
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener()
        {
            @Override public void onDismiss()
            {
                // todo save instance state
            }
        });

        for (int i = 0; i < itemArray.length; i++)
        {
            TextView textView = new TextView(getContext());
            textView.setTextSize(18);
            int paddingHorizontal = dpPixelSize(16, displayMetrics);
            int paddingVertical = dpPixelSize(12, displayMetrics);
            textView.setPadding(paddingHorizontal, paddingVertical, paddingHorizontal, paddingVertical);
            textView.setTextColor(0xff222222);
            textView.setSingleLine(true);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            textView.setBackgroundResource(R.drawable.pressable_background_transparent);
            textView.setText(itemArray[i]);
            textView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

            if (onSelectListener != null)
            {
                final int finalIndex = i;

                textView.setOnClickListener(new OnClickListener()
                {
                    @Override public void onClick(View v)
                    {
                        onSelectListener.onSelect(finalIndex);
                        popupWindow.dismiss();
                    }
                });
            }

            container.addView(textView);
        }

        return popupWindow;
    }

    @Nullable private SmartImageView avatarView = null;
    private final int defaultAvatarSize = dpPixelSize(40, displayMetrics);
    private final int expandedAvatarSize = dpPixelSize(60, displayMetrics);
    private final int defaultAvatarXOffset = dpPixelSize(4, displayMetrics);
    private final int expandedAvatarX = dpPixelSize(16, displayMetrics);
    private final int defaultAvatarY = dpPixelSize(8, displayMetrics);
    private final int expandedAvatarY = dpPixelSize(64, displayMetrics);

    @NonNull public SmartImageView setAvatar()
    {
        if (avatarView == null)
        {
            avatarView = new SmartImageView(getContext());
            avatarView.setScaleType(ImageView.ScaleType.FIT_XY);
            addView(avatarView);
            dispatchUpdate();
        }

        return avatarView;
    }

    @Nullable private String title = null;
    private final int defaultTitleXOffset = dpPixelSize(16, displayMetrics);
    private final int expandedTitleXOffset = dpPixelSize(20, displayMetrics);
    private final int defaultTitleY = dpPixelSize(35, displayMetrics);
    private final int expandedTitleY = dpPixelSize(103, displayMetrics);
    private final int defaultTitleSize = spPixelSize(20, displayMetrics);
    private final int expandedTitleSize = spPixelSize(24, displayMetrics);
    private final int defaultTitleWithSubtitleY = dpPixelSize(25, displayMetrics);
    private final int expandedTitleWithSubtitleY = dpPixelSize(87, displayMetrics);
    private final int defaultTitleWithSubtitleSize = spPixelSize(18, displayMetrics);
    private final int expandedTitleWithSubtitleSize = spPixelSize(21, displayMetrics);

    public void setTitle(@StringRes int stringRes)
    {
        setTitle(stringRes != 0 ? getResources().getString(stringRes) : "");
    }

    public void setTitle(@Nullable String title)
    {
        this.title = title;

        dispatchUpdate();
    }

    @Nullable private String subtitle = null;
    private final int defaultSubtitleXOffset = dpPixelSize(8, displayMetrics);
    private final int expandedSubtitleXOffset = dpPixelSize(20, displayMetrics);
    private final int defaultSubtitleY = dpPixelSize(45, displayMetrics);
    private final int expandedSubtitleY = dpPixelSize(110, displayMetrics);
    private final int defaultSubtitleSize = spPixelSize(14, displayMetrics);
    private final int expandedSubtitleSize = spPixelSize(14, displayMetrics);

    public void setSubtitle(@StringRes int stringRes)
    {
        setSubtitle(stringRes != 0 ? getResources().getString(stringRes) : "");
    }

    public void setSubtitle(@Nullable String subtitle)
    {
        this.subtitle = subtitle;

        dispatchUpdate();
    }

    @Nullable private ImageView headerBackground;
    private final int defaultHeaderBackgroundBottom = defaultSize;
    private final int expandedHeaderBackgroundBottom = dpPixelSize(128, displayMetrics);
    private final int expandedHeaderBackgroundY = defaultSize;

    public void setHeaderOnClickListener(@Nullable OnClickListener onClickListener)
    {
        if (onClickListener != null)
        {
            if (headerBackground == null)
            {
                headerBackground = new ImageView(getContext());
                addView(headerBackground, 0);
                dispatchUpdate();
            }

            headerBackground.setOnClickListener(onClickListener);
        }
        else if (headerBackground != null)
        {
            removeView(headerBackground);
            headerBackground = null;
            dispatchUpdate();
        }
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int widthMode = getMode(widthMeasureSpec);
        int widthSize = widthMode != UNSPECIFIED ? getSize(widthMeasureSpec) : Integer.MAX_VALUE;
        widthSize = widthMode == EXACTLY ? widthSize : min(widthSize, defaultSize * 5);

        int heightMode = getMode(heightMeasureSpec);
        int heightSize = heightMode != UNSPECIFIED ? getSize(heightMeasureSpec) : Integer.MAX_VALUE;
        heightSize = heightMode == EXACTLY ? heightSize : min(heightSize, defaultSize);

        setMeasuredDimension(widthSize, heightSize);
    }

    private static int absoluteProgress(int first, int second, float progress)
    {
        return round(first + (second - first) * progress);
    }

    private int textLeft;
    private int textRight;
    private int titleTop;
    private int subtitleTop;

    @Override protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        int width = r - l;
        float progress = max(min((b - t - defaultSize) / (expandedSize - defaultSize), 1), 0);
        int defaultLeftShift = 0;
        int defaultRightShift = width;
        int expandedLeftShift = 0;

        if (navigationButtonView != null)
        {
            navigationButtonView.layout(0, 0, defaultSize, defaultSize);
            defaultLeftShift += defaultSize;
        }

        for (int i = menuButtonList.size() - 1; i >= 0; i--)
        {
            menuButtonList.get(i).layout(defaultRightShift - defaultSize, 0, defaultRightShift, defaultSize);
            defaultRightShift -= defaultSize;
        }

        if (spinnerTextView != null)
        {
            int maxWidth = max(defaultRightShift - defaultLeftShift, 0);
            spinnerTextView.measure(makeMeasureSpec(maxWidth, AT_MOST), makeMeasureSpec(defaultSize, EXACTLY));
            spinnerTextView.layout(defaultLeftShift, 0, defaultLeftShift + spinnerTextView.getMeasuredWidth(), defaultSize);
            defaultLeftShift += spinnerTextView.getMeasuredWidth();
        }

        int defaultHeaderBackgroundX = defaultLeftShift;

        if (avatarView != null)
        {
            int size = absoluteProgress(defaultAvatarSize, expandedAvatarSize, progress);
            int left = absoluteProgress(defaultLeftShift + defaultAvatarXOffset, expandedAvatarX, progress);
            int top = absoluteProgress(defaultAvatarY, expandedAvatarY, progress);
            avatarView.layout(left, top, left + size, top + size);
            defaultLeftShift += defaultAvatarXOffset + defaultAvatarSize;
            expandedLeftShift += expandedAvatarX + expandedAvatarSize;
        }

        if (subtitle == null)
        {
            textLeft = absoluteProgress(defaultLeftShift + defaultTitleXOffset, expandedLeftShift + expandedTitleXOffset, progress);
            textRight = absoluteProgress(defaultRightShift - defaultTitleXOffset, width - expandedTitleXOffset, progress);
        }
        else
        {
            textLeft = absoluteProgress(defaultLeftShift + defaultSubtitleXOffset, expandedLeftShift + expandedSubtitleXOffset, progress);
            textRight = absoluteProgress(defaultRightShift - defaultSubtitleXOffset, width - expandedSubtitleXOffset, progress);
        }

        textRight = max(textLeft, textRight);

        float textWidth = 0;

        if (title != null)
        {
            if (subtitle == null)
            {
                titlePaint.setTextSize(absoluteProgress(defaultTitleSize, expandedTitleSize, progress));
                titleTop = absoluteProgress(defaultTitleY, expandedTitleY, progress);
            }
            else
            {
                titlePaint.setTextSize(absoluteProgress(defaultTitleWithSubtitleSize, expandedTitleWithSubtitleSize, progress));
                titleTop = absoluteProgress(defaultTitleWithSubtitleY, expandedTitleWithSubtitleY, progress);
            }

            String ellipsizedTitle = TextUtils.ellipsize(title, titlePaint, textRight - textLeft, TextUtils.TruncateAt.END).toString();
            textWidth = max(titlePaint.measureText(ellipsizedTitle), textWidth);
        }

        if (subtitle != null)
        {
            subtitlePaint.setTextSize(absoluteProgress(defaultSubtitleSize, expandedSubtitleSize, progress));
            subtitleTop = absoluteProgress(defaultSubtitleY, expandedSubtitleY, progress);

            String ellipsizedSubtitle = TextUtils.ellipsize(subtitle, subtitlePaint, textRight - textLeft, TextUtils.TruncateAt.END).toString();
            textWidth = max(subtitlePaint.measureText(ellipsizedSubtitle), textWidth);
        }

        if (headerBackground != null)
        {
            int left = absoluteProgress(defaultHeaderBackgroundX, 0, progress);
            int top = absoluteProgress(0, expandedHeaderBackgroundY, progress);
            int defaultRightPadding = subtitle != null ? defaultSubtitleXOffset : defaultTitleXOffset;
            int right = absoluteProgress(min(textLeft + round(textWidth) + defaultRightPadding, defaultRightShift), width, progress);
            int bottom = absoluteProgress(defaultHeaderBackgroundBottom, expandedHeaderBackgroundBottom, progress);
            headerBackground.layout(left, top, right, bottom);
        }
    }

    private final TextPaint titlePaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);

    {
        titlePaint.setTypeface(Font.getRobotoMedium(getResources()));
    }

    private final TextPaint subtitlePaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);

    @Override protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);

        if (title != null)
        {
            canvas.drawText(TextUtils.ellipsize(title, titlePaint, textRight - textLeft, TextUtils.TruncateAt.END).toString(), textLeft, titleTop, titlePaint);
        }

        if (subtitle != null)
        {
            canvas.drawText(TextUtils.ellipsize(subtitle, subtitlePaint, textRight - textLeft, TextUtils.TruncateAt.END).toString(), textLeft, subtitleTop, subtitlePaint);
        }
    }
}