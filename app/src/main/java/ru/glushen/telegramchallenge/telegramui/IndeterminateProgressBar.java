package ru.glushen.telegramchallenge.telegramui;

import android.content.*;
import android.util.*;
import android.view.animation.*;

/**
 * Created by Pavel Glushen on 07.08.2015.
 */
public class IndeterminateProgressBar extends AbstractProgressBar
{
    public IndeterminateProgressBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    private static final AccelerateDecelerateInterpolator INTERPOLATOR = new AccelerateDecelerateInterpolator();

    @Override protected void onDrawProgress(long startTime)
    {
        float overallProgress = (System.currentTimeMillis() - startTime) % 9000 / 1.5f;
        float currentProgress = overallProgress % 1200;

        boolean increasingSweep = currentProgress < 600;

        float startAngleInterpolation = INTERPOLATOR.getInterpolation((currentProgress - 600) / 600f);
        float startAngle = increasingSweep ? 0 : (startAngleInterpolation * 270);

        float sweepAngleInterpolation = INTERPOLATOR.getInterpolation(currentProgress / 600f);
        float sweepAngle = increasingSweep ? (sweepAngleInterpolation * 270) : (270 - startAngle);

        float offsetAngleInterpolation = INTERPOLATOR.getInterpolation(currentProgress / 1200f);
        float offsetAngle = ((int) (overallProgress / 1200f)) * 144 + offsetAngleInterpolation * (144 + 90) - 90;

        setProgressAngles(startAngle + offsetAngle, sweepAngle);
    }
}
