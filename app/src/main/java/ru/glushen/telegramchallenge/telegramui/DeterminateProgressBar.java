package ru.glushen.telegramchallenge.telegramui;

import android.content.*;
import android.util.*;
import android.view.animation.*;

import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 07.08.2015.
 */
public class DeterminateProgressBar extends AbstractProgressBar
{
    public DeterminateProgressBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    private static final AccelerateDecelerateInterpolator INTERPOLATOR = new AccelerateDecelerateInterpolator();

    private float lastSweepAngle = 0;
    private long lastSweepAngleTime;
    private boolean animatingSweepAngle = false;

    @Override protected void onDrawProgress(long startTime)
    {
        long currentTimeInMillis = System.currentTimeMillis();

        float startAngle = (currentTimeInMillis - startTime) % 3000 / 3000f * 360 - 45;

        float sweepAngle = max(abs(max != 0 ? progress / (float) max : 0) * 360, 20);

        if (sweepAngle != lastSweepAngle)
        {
            if (!animatingSweepAngle)
            {
                lastSweepAngleTime = currentTimeInMillis;
                animatingSweepAngle = true;
                sweepAngle = 0;
            }
            else if (currentTimeInMillis - lastSweepAngleTime < 300)
            {
                float sweepAngleInterpolation = INTERPOLATOR.getInterpolation((currentTimeInMillis - lastSweepAngleTime) / 300f);
                sweepAngle = lastSweepAngle + sweepAngleInterpolation * (sweepAngle - lastSweepAngle);
            }
            else
            {
                animatingSweepAngle = false;
                lastSweepAngle = sweepAngle;
            }
        }

        setProgressAngles(startAngle, sweepAngle);
    }

    private int max;

    public void setMax(int max)
    {
        this.max = max;
        invalidate();
    }

    private int progress;

    public void setProgress(int progress)
    {
        this.progress = progress;
        invalidate();
    }
}
