package ru.glushen.telegramchallenge.telegramui;

import android.content.*;
import android.graphics.*;
import android.support.annotation.*;
import android.util.*;
import android.view.*;

import ru.glushen.util.*;

import static android.view.View.MeasureSpec.*;
import static java.lang.Math.*;
import static ru.glushen.util.Screen.*;

/**
 * Created by pavel on 06.08.2015.
 */
public class SwitchView extends View implements View.OnTouchListener
{
    public SwitchView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public interface OnSwitchedListener
    {
        void onSwitched(SwitchView switchView, boolean checked, boolean byUser);
    }

    @Nullable private OnSwitchedListener onSwitchedListener = null;

    public void setOnSwitchedListener(@Nullable OnSwitchedListener onSwitchedListener)
    {
        this.onSwitchedListener = onSwitchedListener;
    }

    private boolean checked = false;

    public boolean isChecked()
    {
        return checked;
    }

    public void setChecked(boolean checked, boolean byUser)
    {
        if (this.checked != checked)
        {
            this.checked = checked;

            if (onSwitchedListener != null)
            {
                onSwitchedListener.onSwitched(this, checked, byUser);
            }
        }
    }

    private final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

    private final int defaultWidth = dpPixelSize(42, displayMetrics);
    private final int defaultHeight = dpPixelSize(22, displayMetrics);

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int widthMode = getMode(widthMeasureSpec);
        int width = getSize(widthMeasureSpec);
        width = widthMode != UNSPECIFIED ? width : Integer.MAX_VALUE;
        width = widthMode == EXACTLY ? width : min(getPaddingLeft() + defaultWidth + getPaddingRight(), width);

        int heightMode = getMode(heightMeasureSpec);
        int height = getSize(heightMeasureSpec);
        height = heightMode != UNSPECIFIED ? height : Integer.MAX_VALUE;
        height = heightMode == EXACTLY ? height : min(getPaddingTop() + defaultHeight + getPaddingBottom(), height);

        setMeasuredDimension(width, height);
    }

    private final RectF backgroundRect = new RectF();

    private final float backgroundWidth = dpPixelSize(34, displayMetrics);
    private final float backgroundHeight = dpPixelSize(14, displayMetrics);
    private final float backgroundRadius = min(backgroundWidth, backgroundHeight) / 2f;

    private float minSliderX;
    private float maxSliderX;
    private float sliderY;

    private final float sliderRadius = dpPixelSize(10, displayMetrics);

    private float currentSliderX;

    @Override protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight)
    {
        backgroundRect.left = (width - backgroundWidth) / 2f;
        backgroundRect.top = (height - backgroundHeight) / 2f;
        backgroundRect.right = backgroundRect.left + backgroundWidth;
        backgroundRect.bottom = backgroundRect.top + backgroundHeight;

        minSliderX = (width - defaultWidth) / 2f + dpPixelSize(11, displayMetrics);
        maxSliderX = width - minSliderX;
        sliderY = height / 2f;
        currentSliderX = checked ? maxSliderX : minSliderX;
    }

    private final Paint backgroundDisabledPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    {
        backgroundDisabledPaint.setColor(0xffc5c5c5);
    }

    private final Paint backgroundEnabledPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    {
        backgroundEnabledPaint.setColor(0xffadcae1);
    }

    private final Paint sliderDisabledPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    {
        sliderDisabledPaint.setColor(0xfff1f1f1);

        Compat.setLayerTypeToSoftware(this, sliderDisabledPaint);

        sliderDisabledPaint.setShadowLayer(dpPixelSize(1, displayMetrics), 0, dpPixelSize(1, displayMetrics), 0x44000000);
    }

    private final Paint sliderEnabledPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    {
        sliderEnabledPaint.setColor(0xff5b95c2);
    }

    @Override protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        int alpha = (int) ((currentSliderX - minSliderX) / (maxSliderX - minSliderX) * 255);
        backgroundEnabledPaint.setAlpha(alpha);
        sliderEnabledPaint.setAlpha(alpha);

        canvas.drawRoundRect(backgroundRect, backgroundRadius, backgroundRadius, backgroundDisabledPaint);
        canvas.drawRoundRect(backgroundRect, backgroundRadius, backgroundRadius, backgroundEnabledPaint);

        canvas.drawCircle(currentSliderX, sliderY, sliderRadius, sliderDisabledPaint);
        canvas.drawCircle(currentSliderX, sliderY, sliderRadius, sliderEnabledPaint);
    }

    {
        setOnTouchListener(this);
    }

    private final float maxClickOffset = dpPixelSize(6, displayMetrics);

    private float startTouchX;
    private float startTouchY;
    private boolean clicking;

    @Override public boolean onTouch(View v, MotionEvent event)
    {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
            {
                startTouchX = event.getX();
                startTouchY = event.getY();
                clicking = true;

                currentSliderX = min(max(event.getX(), minSliderX), maxSliderX);

                getParent().requestDisallowInterceptTouchEvent(true);
            }
            break;

            case MotionEvent.ACTION_MOVE:
            {
                if (abs(event.getX() - startTouchX) > maxClickOffset || abs(event.getY() - startTouchY) > maxClickOffset)
                {
                    clicking = false;
                }

                currentSliderX = min(max(event.getX(), minSliderX), maxSliderX);
            }
            break;

            case MotionEvent.ACTION_UP:
            {
                boolean justChecked = clicking ? !checked : event.getX() > minSliderX + (maxSliderX - minSliderX) / 2f;

                currentSliderX = justChecked ? maxSliderX : minSliderX;

                setChecked(justChecked, true);

                getParent().requestDisallowInterceptTouchEvent(false);
            }
            break;

            case MotionEvent.ACTION_CANCEL:
            {
                currentSliderX = checked ? maxSliderX : minSliderX;

                getParent().requestDisallowInterceptTouchEvent(false);
            }
            break;
        }

        invalidate();

        return true;
    }
}
