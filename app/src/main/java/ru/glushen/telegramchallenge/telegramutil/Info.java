package ru.glushen.telegramchallenge.telegramutil;

import android.support.annotation.*;
import android.support.v4.util.LongSparseArray;
import android.util.*;

import org.drinkless.td.libcore.telegram.*;
import org.drinkless.td.libcore.telegram.TdApi.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.TelegramClient.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 02.06.2015.
 */
public class Info
{
    public static final String OPTION_MY_ID = "my_id";
    public static final String OPTION_CONNECTION_STATE = "connection_state";

    public static final String CONNECTION_STATE_WAITING_FOR_NETWORK = "Waiting for network";
    public static final String CONNECTION_STATE_CONNECTING = "Connecting";
    public static final String CONNECTION_STATE_UPDATING = "Updating";
    public static final String CONNECTION_STATE_READY = "Ready";

    public static final EventDispatcher<ChatAndUpdate> NEW_MESSAGE_IN_CHAT_DISPATCHER = new EventDispatcher<>();

    private static final HashMap<String, OptionInfo> optionMap = new HashMap<>();
    private static final SparseArray<UserInfo> userArray = new SparseArray<>();
    private static final LongSparseArray<ChatInfo> chatArray = new LongSparseArray<>();
    private static final SparseArray<GroupChatInfo> groupChatArray = new SparseArray<>();
    private static final HashMap<MessageId, MessageInfo> messageArray = new HashMap<>();
    private static final SparseArray<ArrayList<MessageInfo>> messagesOfUserArray = new SparseArray<>();

    private Info()
    {

    }

    public static void getOptionValue(@NonNull final String optionName, @NonNull final Obtainer<OptionData> obtainer)
    {
        OptionInfo optionInfo = optionMap.get(optionName);

        if (optionInfo != null && optionInfo.optionValue != null)
        {
            obtainer.obtain(new OptionData(optionName, optionInfo.optionValue));
        }
        else
        {
            TelegramClient.request(new GetOption(optionName), new ResultObtainer<OptionValue>()
            {
                @Override protected boolean onError(TdApi.Error error)
                {
                    return false;
                }

                @Override protected void onComplete(OptionValue optionValue)
                {
                    OptionInfo optionInfo = optionMap.get(optionName);

                    if (optionInfo != null)
                    {
                        optionInfo.optionValue = optionValue;
                    }
                    else
                    {
                        optionMap.put(optionName, new OptionInfo(optionValue));
                    }

                    obtainer.obtain(new OptionData(optionName, optionValue));
                }
            });
        }
    }

    public static EventDispatcher<OptionData> getOptionDispatcher(String optionName)
    {
        OptionInfo optionInfo = optionMap.get(optionName);

        if (optionInfo == null)
        {
            optionInfo = new OptionInfo(null);
            optionMap.put(optionName, optionInfo);
        }

        return optionInfo.dispatcher;
    }

    public static void getUser(int userId, @NonNull final Obtainer<UserAndUpdate> obtainer)
    {
        UserInfo userInfo = userArray.get(userId);

        if (userInfo != null)
        {
            obtainer.obtain(new UserAndUpdate(userInfo.user, userInfo.userFull, null));
        }
        else
        {
            TelegramClient.request(new GetUser(userId), new ResultObtainer<User>()
            {
                @Override protected void onComplete(User user)
                {
                    UserInfo userInfo = userArray.get(user.id);

                    if (userInfo != null)
                    {
                        userInfo.user = user;
                    }
                    else
                    {
                        userArray.put(user.id, new UserInfo(user, null));
                    }

                    obtainer.obtain(new UserAndUpdate(user, userInfo != null ? userInfo.userFull : null, null));
                }

                @Override protected boolean onError(TdApi.Error error)
                {
                    return false;
                }
            });
        }
    }

    public static void getUserFull(int userId, @NonNull final Obtainer<UserAndUpdate> obtainer)
    {
        UserInfo userInfo = userArray.get(userId);

        if (userInfo != null && userInfo.userFull != null)
        {
            obtainer.obtain(new UserAndUpdate(userInfo.user, userInfo.userFull, null));
        }
        else
        {
            TelegramClient.request(new GetUserFull(userId), new ResultObtainer<UserFull>()
            {
                @Override protected void onComplete(UserFull userFull)
                {
                    UserInfo userInfo = userArray.get(userFull.user.id);

                    if (userInfo != null)
                    {
                        userInfo.user = userFull.user;
                        userInfo.userFull = userFull;
                    }
                    else
                    {
                        userArray.put(userFull.user.id, new UserInfo(userFull.user, userFull));
                    }

                    obtainer.obtain(new UserAndUpdate(userFull.user, userFull, null));
                }

                @Override protected boolean onError(TdApi.Error error)
                {
                    return false;
                }
            });
        }
    }

    public static void searchUser(@NonNull String username, @NonNull final Obtainer<UserAndUpdate> obtainer)
    {
        for (int i = 0; i < userArray.size(); i++)
        {
            UserInfo userInfo = userArray.valueAt(i);

            if (username.equals(userInfo.user.username))
            {
                obtainer.obtain(new UserAndUpdate(userInfo.user, userInfo.userFull, null));

                return;
            }
        }

        TelegramClient.request(new TdApi.SearchUser(username), new TelegramClient.ResultObtainer<TdApi.User>()
        {
            @Override protected void onComplete(TdApi.User user)
            {
                UserInfo userInfo = userArray.get(user.id);

                if (userInfo != null)
                {
                    userInfo.user = user;
                }
                else
                {
                    userArray.put(user.id, new UserInfo(user, null));
                }

                obtainer.obtain(new UserAndUpdate(user, userInfo != null ? userInfo.userFull : null, null));
            }

            @Override protected boolean onError(TdApi.Error error)
            {
                obtainer.obtain(null);

                return true;
            }
        });
    }

    public static EventDispatcher<UserAndUpdate> getUserUpdateDispatcher(@NonNull User user)
    {
        UserInfo userInfo = userArray.get(user.id);

        if (userInfo == null)
        {
            userInfo = new UserInfo(user, null);
            userArray.put(user.id, userInfo);
        }

        return userInfo.dispatcher;
    }

    public static EventDispatcher<UserAndUpdate> getUserFullUpdateDispatcher(@NonNull UserFull userFull)
    {
        UserInfo userInfo = userArray.get(userFull.user.id);

        if (userInfo == null)
        {
            userInfo = new UserInfo(userFull.user, userFull);
            userArray.put(userFull.user.id, userInfo);
        }

        return userInfo.dispatcher;
    }

    public static void setUserBlocked(int userId, boolean blocked)
    {
        TelegramClient.request(blocked ? new BlockUser(userId) : new UnblockUser(userId), null);
        TelegramClient.dispatchUpdate(new UpdateUserBlocked(userId, blocked));
    }

    public static void getChat(long chatId, @NonNull final Obtainer<ChatAndUpdate> obtainer)
    {
        ChatInfo chatInfo = chatArray.get(chatId);

        if (chatInfo != null)
        {
            obtainer.obtain(new ChatAndUpdate(chatInfo.chat, null));
        }
        else
        {
            TelegramClient.request(new GetChat(chatId), new ResultObtainer<Chat>()
            {
                @Override protected void onComplete(Chat chat)
                {
                    ChatInfo chatInfo = chatArray.get(chat.id);

                    if (chatInfo != null)
                    {
                        chatInfo.chat = chat;
                    }
                    else
                    {
                        chatArray.put(chat.id, new ChatInfo(chat));
                    }

                    obtainer.obtain(new ChatAndUpdate(chat, null));
                }

                @Override protected boolean onError(TdApi.Error error)
                {
                    return false;
                }
            });
        }
    }

    public static void getChatWithUserId(int userId, @NonNull final Obtainer<ChatAndUpdate> obtainer)
    {
        for (int i = 0; i < chatArray.size(); i++)
        {
            ChatInfo chatInfo = chatArray.valueAt(i);

            if (chatInfo.chat.type instanceof PrivateChatInfo && ((PrivateChatInfo) chatInfo.chat.type).user.id == userId)
            {
                obtainer.obtain(new ChatAndUpdate(chatInfo.chat, null));
                return;
            }
        }

        TelegramClient.request(new CreatePrivateChat(userId), new ResultObtainer<Chat>()
        {
            @Override protected void onComplete(Chat chat)
            {
                ChatInfo chatInfo = chatArray.get(chat.id);

                if (chatInfo != null)
                {
                    chatInfo.chat = chat;
                }
                else
                {
                    chatArray.put(chat.id, new ChatInfo(chat));
                }

                obtainer.obtain(new ChatAndUpdate(chat, null));
            }

            @Override protected boolean onError(TdApi.Error error)
            {
                return false;
            }
        });
    }

    public static void getChatWithGroupId(int groupId, @NonNull final Obtainer<ChatAndUpdate> obtainer)
    {
        for (int i = 0; i < chatArray.size(); i++)
        {
            ChatInfo chatInfo = chatArray.valueAt(i);

            if (chatInfo.chat.type instanceof TdApi.GroupChatInfo && ((TdApi.GroupChatInfo) chatInfo.chat.type).groupChat.id == groupId)
            {
                obtainer.obtain(new ChatAndUpdate(chatInfo.chat, null));
                return;
            }
        }
    }

    public static void searchPhotoAndVideoInChat(final long chatId, final int fromId, final int limit, @NonNull final Obtainer<ChatSearchedMessagesInfo> obtainer)
    {
        if (limit <= 0)
        {
            throw new AssertionError("limit must be positive");
        }

        getChat(chatId, new Obtainer<ChatAndUpdate>()
        {
            @Override public void obtain(final ChatAndUpdate chatAndUpdate)
            {
                handle();
            }

            private void handle()
            {
                final ChatInfo chatInfo = chatArray.get(chatId);
                final List<Message> list = chatInfo.photoAndVideoMessageList;

                int approximateCount = chatInfo.photoAndVideoMessageApproximateCount;
                boolean fullLoaded = chatInfo.photoAndVideoMessageListLoaded;

                if (approximateCount >= 0)
                {
                    for (int i = 0; i < list.size() - limit || i < list.size() && fullLoaded; i++)
                    {
                        if (fromId < list.get(i).id)
                        {
                            List<Message> subList = list.subList(i, min(i + limit, list.size()));
                            obtainer.obtain(new ChatSearchedMessagesInfo(subList, approximateCount, fullLoaded));
                            return;
                        }
                    }

                    if (fullLoaded)
                    {
                        obtainer.obtain(new ChatSearchedMessagesInfo(new ArrayList<Message>(), approximateCount, true));
                        return;
                    }
                }

                int requestFromId = !list.isEmpty() ? list.get(list.size() - 1).id : 0;
                SearchMessagesFilter requestFilter = new SearchMessagesFilterPhotoAndVideo();

                final int listSizeBeforeRequest = list.size();

                TelegramClient.request(new SearchMessages(chatId, "", requestFromId, 100, requestFilter), new ResultObtainer<Messages>()
                {
                    @Override protected void onComplete(Messages messages)
                    {
                        chatInfo.photoAndVideoMessageApproximateCount = messages.totalCount;
                        chatInfo.photoAndVideoMessageListLoaded = messages.messages.length == 0;

                        if (listSizeBeforeRequest == list.size())
                        {
                            Collections.addAll(list, messages.messages);
                        }

                        handle();
                    }

                    @Override protected boolean onError(TdApi.Error error)
                    {
                        return false;
                    }
                });
            }
        });
    }

    public static void searchAudioInChat(final long chatId, final int fromId, final int limit, @Nullable final Obtainer<ChatSearchedMessagesInfo> obtainer)
    {
        if (limit <= 0)
        {
            throw new AssertionError("limit must be positive");
        }

        getChat(chatId, new Obtainer<ChatAndUpdate>()
        {
            @Override public void obtain(final ChatAndUpdate chatAndUpdate)
            {
                handle();
            }

            private void handle()
            {
                final ChatInfo chatInfo = chatArray.get(chatId);
                final List<Message> list = chatInfo.audioMessageList;

                int approximateCount = chatInfo.audioMessageApproximateCount;
                boolean fullLoaded = chatInfo.audioMessageListLoaded;

                if (approximateCount >= 0)
                {
                    for (int i = 0; i < list.size() - limit || i < list.size() && fullLoaded; i++)
                    {
                        if (fromId < list.get(i).id)
                        {
                            if (obtainer != null)
                            {
                                List<Message> subList = list.subList(i, min(i + limit, list.size()));
                                obtainer.obtain(new ChatSearchedMessagesInfo(subList, approximateCount, fullLoaded));
                            }

                            return;
                        }
                    }

                    if (fullLoaded)
                    {
                        if (obtainer != null)
                        {
                            obtainer.obtain(new ChatSearchedMessagesInfo(new ArrayList<Message>(), approximateCount, true));
                        }

                        return;
                    }
                }

                int requestFromId = !list.isEmpty() ? list.get(list.size() - 1).id : 0;
                SearchMessagesFilter requestFilter = new SearchMessagesFilterAudio();

                final int listSizeBeforeRequest = list.size();

                TelegramClient.request(new SearchMessages(chatId, "", requestFromId, 100, requestFilter), new ResultObtainer<Messages>()
                {
                    @Override protected void onComplete(Messages messages)
                    {
                        chatInfo.audioMessageApproximateCount = messages.totalCount;
                        chatInfo.audioMessageListLoaded = messages.messages.length == 0;

                        if (listSizeBeforeRequest == list.size())
                        {
                            Collections.addAll(list, messages.messages);
                        }

                        handle();
                    }

                    @Override protected boolean onError(TdApi.Error error)
                    {
                        return false;
                    }
                });
            }
        });
    }

    public static void cacheAudioInChatList(final long chatId)
    {
        searchAudioInChat(chatId, 0, Integer.MAX_VALUE, null);
    }

    public static EventDispatcher<ChatAndUpdate> getChatUpdateDispatcher(@NonNull Chat chat)
    {
        ChatInfo chatInfo = chatArray.get(chat.id);

        if (chatInfo == null)
        {
            chatInfo = new ChatInfo(chat);
            chatArray.put(chat.id, chatInfo);

            final ChatInfo finalChatInfo = chatInfo;

            if (finalChatInfo.chat.type instanceof PrivateChatInfo)
            {
                getUserUpdateDispatcher(((PrivateChatInfo) finalChatInfo.chat.type).user).addObtainer(new Obtainer<UserAndUpdate>()
                {
                    @Override public void obtain(UserAndUpdate userAndUpdate)
                    {
                        if (finalChatInfo.chat.type instanceof PrivateChatInfo)
                        {
                            ((PrivateChatInfo) finalChatInfo.chat.type).user = userAndUpdate.user;
                        }

                        finalChatInfo.dispatcher.dispatch(new ChatAndUpdate(finalChatInfo.chat, userAndUpdate.update));
                    }
                }, true);
            }
            else if (finalChatInfo.chat.type instanceof TdApi.GroupChatInfo)
            {
                getGroupChatUpdateDispatcher(((TdApi.GroupChatInfo) finalChatInfo.chat.type).groupChat).addObtainer(new Obtainer<GroupChatAndUpdate>()
                {
                    @Override public void obtain(GroupChatAndUpdate groupChatAndUpdate)
                    {
                        if (finalChatInfo.chat.type instanceof TdApi.GroupChatInfo)
                        {
                            ((TdApi.GroupChatInfo) finalChatInfo.chat.type).groupChat = groupChatAndUpdate.groupChat;
                        }

                        finalChatInfo.dispatcher.dispatch(new ChatAndUpdate(finalChatInfo.chat, groupChatAndUpdate.update));
                    }
                }, true);
            }
        }

        return chatInfo.dispatcher;
    }

    public static void setNotificationsSettings(@NonNull NotificationSettingsScope scope, @NonNull NotificationSettings settings)
    {
        TelegramClient.request(new SetNotificationSettings(scope, settings), null);
        TelegramClient.dispatchUpdate(new UpdateNotificationSettings(scope, settings));
    }

    public static void clearChatHistory(long chatId)
    {
        TelegramClient.request(new DeleteChatHistory(chatId), null);

        SparseBooleanArray messageIdSparseArray = new SparseBooleanArray();

        for (MessageInfo messageInfo : messageArray.values())
        {
            messageIdSparseArray.append(messageInfo.message.id, true);
        }

        int[] messageIdArray = new int[messageIdSparseArray.size()];

        for (int i = 0; i < messageIdArray.length; i++)
        {
            messageIdArray[i] = messageIdSparseArray.keyAt(i);
        }

        TelegramClient.dispatchUpdate(new UpdateDeleteMessages(chatId, messageIdArray));
    }

    public static void getGroupChat(int groupChatId, @NonNull final Obtainer<GroupChatAndUpdate> obtainer)
    {
        GroupChatInfo groupChatInfo = groupChatArray.get(groupChatId);

        if (groupChatInfo != null)
        {
            obtainer.obtain(new GroupChatAndUpdate(groupChatInfo.groupChat, groupChatInfo.groupChatFull, null));
        }
        else
        {
            TelegramClient.request(new GetGroupChat(groupChatId), new ResultObtainer<GroupChat>()
            {
                @Override protected void onComplete(GroupChat groupChat)
                {
                    GroupChatInfo groupChatInfo = groupChatArray.get(groupChat.id);

                    if (groupChatInfo != null)
                    {
                        groupChatInfo.groupChat = groupChat;
                    }
                    else
                    {
                        groupChatArray.put(groupChat.id, new GroupChatInfo(groupChat, null));
                    }

                    obtainer.obtain(new GroupChatAndUpdate(groupChat, groupChatInfo != null ? groupChatInfo.groupChatFull : null, null));
                }

                @Override protected boolean onError(TdApi.Error error)
                {
                    return false;
                }
            });
        }
    }

    private static void groupChatFullToCache(@NonNull GroupChatFull groupChatFull)
    {
        GroupChatInfo groupChatInfo = groupChatArray.get(groupChatFull.groupChat.id);
        GroupChatFull oldGroupChatFull;

        if (groupChatInfo != null)
        {
            groupChatInfo.groupChat = groupChatFull.groupChat;
            oldGroupChatFull = groupChatInfo.groupChatFull;
            groupChatInfo.groupChatFull = groupChatFull;
        }
        else
        {
            groupChatInfo = new GroupChatInfo(groupChatFull.groupChat, groupChatFull);
            groupChatArray.put(groupChatFull.groupChat.id, groupChatInfo);
            oldGroupChatFull = null;
        }

        for (ChatParticipant chatParticipant : groupChatFull.participants)
        {
            boolean needToAddUserDispatcher = true;

            if (oldGroupChatFull != null)
            {
                for (ChatParticipant oldChatParticipant : oldGroupChatFull.participants)
                {
                    if (chatParticipant.user.id == oldChatParticipant.user.id)
                    {
                        needToAddUserDispatcher = false;
                        break;
                    }
                }
            }

            if (needToAddUserDispatcher)
            {
                final GroupChatInfo finalGroupChatInfo = groupChatInfo;

                getUserUpdateDispatcher(chatParticipant.user).addObtainer(new Obtainer<UserAndUpdate>()
                {
                    @Override public void obtain(UserAndUpdate userAndUpdate)
                    {
                        finalGroupChatInfo.dispatcher.dispatch(new GroupChatAndUpdate(finalGroupChatInfo.groupChat, finalGroupChatInfo.groupChatFull, userAndUpdate.update));
                    }
                }, true);
            }
        }
    }

    public static void getGroupChatFull(int groupChatId, @NonNull final Obtainer<GroupChatAndUpdate> obtainer)
    {
        GroupChatInfo groupChatInfo = groupChatArray.get(groupChatId);

        if (groupChatInfo != null && groupChatInfo.groupChatFull != null)
        {
            obtainer.obtain(new GroupChatAndUpdate(groupChatInfo.groupChat, groupChatInfo.groupChatFull, null));
        }
        else
        {
            TelegramClient.request(new GetGroupChatFull(groupChatId), new ResultObtainer<GroupChatFull>()
            {
                @Override protected void onComplete(GroupChatFull groupChatFull)
                {
                    groupChatFullToCache(groupChatFull);

                    obtainer.obtain(new GroupChatAndUpdate(groupChatFull.groupChat, groupChatFull, null));
                }

                @Override protected boolean onError(TdApi.Error error)
                {
                    return false;
                }
            });
        }
    }

    public static EventDispatcher<GroupChatAndUpdate> getGroupChatUpdateDispatcher(@NonNull GroupChat groupChat)
    {
        GroupChatInfo groupChatInfo = groupChatArray.get(groupChat.id);

        if (groupChatInfo == null)
        {
            groupChatInfo = new GroupChatInfo(groupChat, null);
            groupChatArray.put(groupChat.id, groupChatInfo);
        }

        return groupChatInfo.dispatcher;
    }

    public static EventDispatcher<GroupChatAndUpdate> getGroupChatFullUpdateDispatcher(@NonNull GroupChatFull groupChatFull)
    {
        GroupChatInfo groupChatInfo = groupChatArray.get(groupChatFull.groupChat.id);

        if (groupChatInfo == null)
        {
            groupChatInfo = new GroupChatInfo(groupChatFull.groupChat, groupChatFull);
            groupChatArray.put(groupChatFull.groupChat.id, groupChatInfo);
        }

        return groupChatInfo.dispatcher;
    }

    private static void dispatchUserUpdateToMessages(int userId, Update update)
    {
        ArrayList<MessageInfo> messagesOfUser = messagesOfUserArray.get(userId);

        if (messagesOfUser != null)
        {
            for (MessageInfo messageInfo : messagesOfUser)
            {
                messageInfo.dispatcher.dispatch(new MessageAndUpdate(messageInfo.message, update));
            }
        }
    }

    public static void getMessage(long chatId, int messageId, @NonNull final Obtainer<MessageAndUpdate> obtainer)
    {
        MessageInfo messageInfo = messageArray.get(new MessageId(chatId, messageId));

        if (messageInfo != null)
        {
            obtainer.obtain(new MessageAndUpdate(messageInfo.message, null));
        }
        else
        {
            TelegramClient.request(new GetMessage(chatId, messageId), new ResultObtainer<Message>()
            {
                @Override protected void onComplete(Message message)
                {
                    MessageId messageId = new MessageId(message.chatId, message.id);
                    MessageInfo messageInfo = messageArray.get(messageId);

                    if (messageInfo != null)
                    {
                        messageInfo.message = message;
                    }
                    else
                    {
                        messageArray.put(messageId, new MessageInfo(message));
                    }

                    obtainer.obtain(new MessageAndUpdate(message, null));
                }

                @Override protected boolean onError(TdApi.Error error)
                {
                    return false;
                }
            });
        }
    }

    public static void deleteMessages(long chatId, @NonNull int... messageIds)
    {
        TelegramClient.request(new DeleteMessages(chatId, messageIds), null);

        TelegramClient.dispatchUpdate(new UpdateDeleteMessages(chatId, messageIds));
    }

    private static void addUserUpdateHandlerToMessage(int userId, @NonNull MessageInfo messageInfo)
    {
        if (userId != 0)
        {
            ArrayList<MessageInfo> messagesOfUser = messagesOfUserArray.get(userId);

            if (messagesOfUser == null)
            {
                messagesOfUser = new ArrayList<>();
                messagesOfUserArray.put(userId, messagesOfUser);
            }

            messagesOfUser.add(messageInfo);
        }
    }

    public static EventDispatcher<MessageAndUpdate> getMessageUpdateDispatcher(@NonNull Message message)
    {
        MessageId messageId = new MessageId(message);

        MessageInfo messageInfo = messageArray.get(messageId);

        if (messageInfo == null)
        {
            messageInfo = new MessageInfo(message);
            messageArray.put(messageId, messageInfo);

            addUserUpdateHandlerToMessage(message.fromId, messageInfo);

            addUserUpdateHandlerToMessage(message.forwardFromId, messageInfo);

            if (message.message instanceof MessageContact)
            {
                addUserUpdateHandlerToMessage(((MessageContact) message.message).userId, messageInfo);
            }
            else if (message.message instanceof MessageGroupChatCreate)
            {
                for (User user : ((MessageGroupChatCreate) message.message).participants)
                {
                    addUserUpdateHandlerToMessage(user.id, messageInfo);
                }
            }
            else if (message.message instanceof MessageChatAddParticipant)
            {
                addUserUpdateHandlerToMessage(((MessageChatAddParticipant) message.message).user.id, messageInfo);
            }
            else if (message.message instanceof MessageChatDeleteParticipant)
            {
                addUserUpdateHandlerToMessage(((MessageChatDeleteParticipant) message.message).user.id, messageInfo);
            }
            else if (message.message instanceof MessageChatJoinByLink)
            {
                addUserUpdateHandlerToMessage(((MessageChatJoinByLink) message.message).inviterId, messageInfo);
            }
        }

        return messageInfo.dispatcher;
    }

    public static final EventDispatcher<List<TdApi.StickerSet>> STICKER_SET_LIST_DISPATCHER = new EventDispatcher<>();

    @Nullable private static List<TdApi.StickerSet> stickerSetList = null;

    public static void getStickerSetList(@NonNull final Obtainer<List<TdApi.StickerSet>> obtainer)
    {
        if (stickerSetList != null)
        {
            obtainer.obtain(stickerSetList);
        }
        else
        {
            TelegramClient.request(new TdApi.GetStickerSets(true), new TelegramClient.ResultObtainer<TdApi.StickerSets>()
            {
                @Override protected void onComplete(TdApi.StickerSets stickerSets)
                {
                    final TdApi.StickerSetInfo[] stickerSetInfoArray = stickerSets.sets;

                    final ArrayList<TdApi.StickerSet> stickerSetList = new ArrayList<>();

                    for (TdApi.StickerSetInfo stickerSetInfo : stickerSetInfoArray)
                    {
                        TelegramClient.request(new TdApi.GetStickerSet(stickerSetInfo.id), new TelegramClient.ResultObtainer<TdApi.StickerSet>()
                        {
                            @Override protected void onComplete(TdApi.StickerSet stickerSet)
                            {
                                stickerSetList.add(stickerSet);

                                if (stickerSetList.size() == stickerSetInfoArray.length)
                                {
                                    Collections.sort(stickerSetList, new Comparator<TdApi.StickerSet>()
                                    {
                                        @Override public int compare(TdApi.StickerSet lhs, TdApi.StickerSet rhs)
                                        {
                                            return Double.compare(lhs.rating, rhs.rating);
                                        }
                                    });

                                    Info.stickerSetList = stickerSetList;
                                    obtainer.obtain(stickerSetList);
                                }
                            }

                            @Override protected boolean onError(TdApi.Error error)
                            {
                                return false;
                            }
                        });
                    }
                }

                @Override protected boolean onError(TdApi.Error error)
                {
                    return false;
                }
            });
        }
    }

    static
    {
        TelegramClient.addUpdateObtainer(TelegramClient.ALL_UPDATES, new Obtainer<Update>()
        {
            @Override public void obtain(Update data)
            {
                switch (data.getConstructor())
                {
                    case UpdateNewMessage.CONSTRUCTOR:
                    {
                        final UpdateNewMessage update = (UpdateNewMessage) data;

                        getChat(update.message.chatId, new Obtainer<ChatAndUpdate>()
                        {
                            @Override public void obtain(ChatAndUpdate chatAndUpdate)
                            {
                                getOptionValue(OPTION_MY_ID, new Obtainer<OptionData>()
                                {
                                    @Override public void obtain(final OptionData data)
                                    {
                                        ChatInfo chatInfo = chatArray.get(update.message.chatId);

                                        chatInfo.chat.topMessage = update.message;

                                        if (update.message.fromId != ((OptionInteger) data.value).value)
                                        {
                                            chatInfo.chat.unreadCount++;
                                        }

                                        if (update.message.message instanceof MessagePhoto || update.message.message instanceof MessageVideo)
                                        {
                                            chatInfo.photoAndVideoMessageList.add(0, update.message);
                                            chatInfo.photoAndVideoMessageApproximateCount++;
                                        }
                                        else if (update.message.message instanceof MessageAudio)
                                        {
                                            chatInfo.audioMessageList.add(0, update.message);
                                            chatInfo.audioMessageApproximateCount++;
                                        }

                                        ChatAndUpdate chatAndUpdate = new ChatAndUpdate(chatInfo.chat, update);
                                        chatInfo.dispatcher.dispatch(chatAndUpdate);
                                        NEW_MESSAGE_IN_CHAT_DISPATCHER.dispatch(chatAndUpdate);
                                    }
                                });
                            }
                        });
                    }
                    break;
                    case UpdateMessageId.CONSTRUCTOR:
                    {
                        final UpdateMessageId update = (UpdateMessageId) data;

                        ChatInfo chatInfo = chatArray.get(update.chatId);

                        if (chatInfo != null)
                        {
                            if (chatInfo.chat.topMessage.id == update.oldId)
                            {
                                chatInfo.chat.topMessage.id = update.newId;
                                chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                            }

                            for (Message message : chatInfo.photoAndVideoMessageList)
                            {
                                if (message.id == update.oldId)
                                {
                                    message.id = update.newId;
                                    break;
                                }
                            }

                            for (Message message : chatInfo.audioMessageList)
                            {
                                if (message.id == update.oldId)
                                {
                                    message.id = update.newId;
                                    break;
                                }
                            }
                        }

                        MessageInfo messageInfo = messageArray.remove(new MessageId(update.chatId, update.oldId));

                        if (messageInfo != null)
                        {
                            messageArray.put(new MessageId(update.chatId, update.newId), messageInfo);
                            messageInfo.message.id = update.newId;
                            messageInfo.dispatcher.dispatch(new MessageAndUpdate(messageInfo.message, update));
                        }
                    }
                    break;
                    case UpdateMessageDate.CONSTRUCTOR:
                    {
                        final UpdateMessageDate update = (UpdateMessageDate) data;

                        ChatInfo chatInfo = chatArray.get(update.chatId);

                        if (chatInfo != null)
                        {
                            if (chatInfo.chat.topMessage.id == update.messageId)
                            {
                                chatInfo.chat.topMessage.date = update.newDate;
                                chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                            }

                            for (Message message : chatInfo.photoAndVideoMessageList)
                            {
                                if (message.id == update.messageId)
                                {
                                    message.date = update.newDate;
                                    break;
                                }
                            }

                            for (Message message : chatInfo.audioMessageList)
                            {
                                if (message.id == update.messageId)
                                {
                                    message.date = update.newDate;
                                    break;
                                }
                            }
                        }

                        MessageInfo messageInfo = messageArray.get(new MessageId(update.chatId, update.messageId));

                        if (messageInfo != null && messageInfo.message.date != update.newDate)
                        {
                            messageInfo.message.date = update.newDate;
                            messageInfo.dispatcher.dispatch(new MessageAndUpdate(messageInfo.message, update));
                        }
                    }
                    break;
                    case UpdateMessageContent.CONSTRUCTOR:
                    {
                        final UpdateMessageContent update = (UpdateMessageContent) data;

                        ChatInfo chatInfo = chatArray.get(update.chatId);

                        if (chatInfo != null)
                        {
                            if (chatInfo.chat.topMessage.id == update.messageId)
                            {
                                chatInfo.chat.topMessage.message = update.newContent;
                                chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                            }

                            for (Message message : chatInfo.photoAndVideoMessageList)
                            {
                                if (message.id == update.messageId)
                                {
                                    message.message = update.newContent;
                                    break;
                                }
                            }

                            for (Message message : chatInfo.audioMessageList)
                            {
                                if (message.id == update.messageId)
                                {
                                    message.message = update.newContent;
                                    break;
                                }
                            }
                        }

                        MessageInfo messageInfo = messageArray.get(new MessageId(update.chatId, update.messageId));

                        if (messageInfo != null)
                        {
                            messageInfo.message.message = update.newContent;
                            messageInfo.dispatcher.dispatch(new MessageAndUpdate(messageInfo.message, update));
                        }
                    }
                    break;
                    case UpdateChatReadInbox.CONSTRUCTOR:
                    {
                        final UpdateChatReadInbox update = (UpdateChatReadInbox) data;

                        ChatInfo chatInfo = chatArray.get(update.chatId);

                        if (chatInfo != null)
                        {
                            chatInfo.chat.lastReadInboxMessageId = update.lastReadInboxMessageId;
                            chatInfo.chat.unreadCount = update.unreadCount;
                            chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                        }
                    }
                    break;
                    case UpdateChatReadOutbox.CONSTRUCTOR:
                    {
                        final UpdateChatReadOutbox update = (UpdateChatReadOutbox) data;

                        ChatInfo chatInfo = chatArray.get(update.chatId);

                        if (chatInfo != null)
                        {
                            chatInfo.chat.lastReadOutboxMessageId = update.lastReadOutboxMessageId;
                            chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                        }
                    }
                    break;
                    case UpdateChatReplyMarkup.CONSTRUCTOR:
                    {
                        final UpdateChatReplyMarkup update = (UpdateChatReplyMarkup) data;

                        ChatInfo chatInfo = chatArray.get(update.chatId);

                        if (chatInfo != null)
                        {
                            chatInfo.chat.replyMarkupMessageId = update.replyMarkupMessageId;
                            chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                        }
                    }
                    break;
                    case UpdateNotificationSettings.CONSTRUCTOR:
                    {
                        final UpdateNotificationSettings update = (UpdateNotificationSettings) data;

                        if (update.scope instanceof NotificationSettingsForChat)
                        {
                            final long chatId = ((NotificationSettingsForChat) update.scope).chatId;

                            ChatInfo chatInfo = chatArray.get(chatId);

                            if (chatInfo != null)
                            {
                                chatInfo.chat.notificationSettings = update.notificationSettings;
                                chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                            }
                        }
                        else
                        {
                            for (int i = 0, count = chatArray.size(); i < count; i++)
                            {
                                final ChatInfo chatInfo = chatArray.valueAt(i);

                                if (update.scope instanceof NotificationSettingsForAllChats || update.scope instanceof NotificationSettingsForPrivateChats && Functions.isPrivateChat(chatInfo.chat) || update.scope instanceof NotificationSettingsForGroupChats && Functions.isGroupChat(chatInfo.chat))
                                {
                                    chatInfo.chat.notificationSettings = update.notificationSettings;
                                    chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                                }
                            }
                        }
                    }
                    break;
                    case UpdateDeleteMessages.CONSTRUCTOR:
                    {
                        final UpdateDeleteMessages update = (UpdateDeleteMessages) data;

                        final ChatInfo chatInfo = chatArray.get(update.chatId);

                        if (chatInfo != null)
                        {
                            ListIterator<Message> iterator = chatInfo.photoAndVideoMessageList.listIterator();

                            for (int deletedMessageId : update.messages)
                            {
                                while (iterator.hasNext())
                                {
                                    if (iterator.next().id == deletedMessageId)
                                    {
                                        iterator.remove();
                                        chatInfo.photoAndVideoMessageApproximateCount--;
                                        break;
                                    }
                                }
                            }

                            iterator = chatInfo.audioMessageList.listIterator();

                            for (int deletedMessageId : update.messages)
                            {
                                while (iterator.hasNext())
                                {
                                    if (iterator.next().id == deletedMessageId)
                                    {
                                        iterator.remove();
                                        chatInfo.audioMessageApproximateCount--;
                                        break;
                                    }
                                }
                            }

                            boolean dispatched = false;

                            for (int messageId : update.messages)
                            {
                                if (chatInfo.chat.topMessage.id == messageId)
                                {
                                    TelegramClient.request(new GetChat(update.chatId), new ResultObtainer<Chat>()
                                    {
                                        @Override protected void onComplete(Chat chat)
                                        {
                                            chatInfo.chat.topMessage = chat.topMessage;
                                            chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                                        }

                                        @Override protected boolean onError(TdApi.Error error)
                                        {
                                            return false;
                                        }
                                    });

                                    dispatched = true;
                                    break;
                                }
                            }

                            if (!dispatched)
                            {
                                chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                            }
                        }

                        for (int messageId : update.messages)
                        {
                            MessageInfo messageInfo = messageArray.get(new MessageId(update.chatId, messageId));

                            if (messageInfo != null)
                            {
                                messageInfo.message.message = new MessageDeleted();
                                messageInfo.dispatcher.dispatch(new MessageAndUpdate(messageInfo.message, update));
                            }
                        }
                    }
                    break;
                    case UpdateUserAction.CONSTRUCTOR:
                    {
                        final UpdateUserAction update = (UpdateUserAction) data;

                        ChatInfo chatInfo = chatArray.get(update.chatId);

                        if (chatInfo != null)
                        {
                            chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));
                        }
                    }
                    break;
                    case UpdateUserStatus.CONSTRUCTOR:
                    {
                        final UpdateUserStatus update = (UpdateUserStatus) data;

                        UserInfo userInfo = userArray.get(update.userId);

                        if (userInfo != null)
                        {
                            userInfo.user.status = update.status;
                            userInfo.dispatcher.dispatch(new UserAndUpdate(userInfo.user, userInfo.userFull, update));
                        }

                        dispatchUserUpdateToMessages(update.userId, update);
                    }
                    break;
                    case UpdateUser.CONSTRUCTOR:
                    {
                        final UpdateUser update = (UpdateUser) data;

                        UserInfo userInfo = userArray.get(update.user.id);

                        if (userInfo != null)
                        {
                            userInfo.user = update.user;
                            userInfo.dispatcher.dispatch(new UserAndUpdate(userInfo.user, userInfo.userFull, update));
                        }

                        dispatchUserUpdateToMessages(update.user.id, update);
                    }
                    break;
                    case UpdateUserBlocked.CONSTRUCTOR:
                    {
                        final UpdateUserBlocked update = (UpdateUserBlocked) data;

                        UserInfo userInfo = userArray.get(update.userId);

                        if (userInfo != null)
                        {
                            if (userInfo.userFull != null)
                            {
                                userInfo.userFull.isBlocked = update.isBlocked;
                            }

                            userInfo.dispatcher.dispatch(new UserAndUpdate(userInfo.user, userInfo.userFull, update));
                        }

                        dispatchUserUpdateToMessages(update.userId, update);
                    }
                    break;
                    case UpdateChatTitle.CONSTRUCTOR:
                    {
                        final UpdateChatTitle update = (UpdateChatTitle) data;

                        getChat(update.chatId, new Obtainer<ChatAndUpdate>()
                        {
                            @Override public void obtain(final ChatAndUpdate chatAndUpdate)
                            {
                                ChatInfo chatInfo = chatArray.get(update.chatId);
                                chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));

                                if (chatInfo.chat.type instanceof TdApi.GroupChatInfo)
                                {
                                    final GroupChat groupChat = ((TdApi.GroupChatInfo) chatInfo.chat.type).groupChat;
                                    groupChat.title = update.title;

                                    GroupChatInfo groupChatInfo = groupChatArray.get(groupChat.id);

                                    if (groupChatInfo != null)
                                    {
                                        groupChatInfo.groupChat.title = update.title;
                                        groupChatInfo.dispatcher.dispatch(new GroupChatAndUpdate(groupChatInfo.groupChat, groupChatInfo.groupChatFull, update));
                                    }
                                }
                            }
                        });
                    }
                    break;
                    case UpdateChatParticipantsCount.CONSTRUCTOR:
                    {
                        final UpdateChatParticipantsCount update = (UpdateChatParticipantsCount) data;

                        getChat(update.chatId, new Obtainer<ChatAndUpdate>()
                        {
                            @Override public void obtain(final ChatAndUpdate chatAndUpdate)
                            {
                                ChatInfo chatInfo = chatArray.get(update.chatId);
                                chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));

                                if (chatInfo.chat.type instanceof TdApi.GroupChatInfo)
                                {
                                    final GroupChat groupChat = ((TdApi.GroupChatInfo) chatInfo.chat.type).groupChat;
                                    groupChat.participantsCount = update.participantsCount;

                                    final GroupChatInfo groupChatInfo = groupChatArray.get(groupChat.id);

                                    if (groupChatInfo != null)
                                    {
                                        groupChatInfo.groupChat.participantsCount = update.participantsCount;

                                        if (groupChatInfo.groupChatFull == null)
                                        {
                                            groupChatInfo.dispatcher.dispatch(new GroupChatAndUpdate(groupChatInfo.groupChat, null, update));
                                        }
                                        else
                                        {
                                            TelegramClient.request(new GetGroupChatFull(groupChatInfo.groupChat.id), new ResultObtainer<GroupChatFull>()
                                            {
                                                @Override protected void onComplete(GroupChatFull groupChatFull)
                                                {
                                                    groupChatFullToCache(groupChatFull);
                                                    groupChatInfo.dispatcher.dispatch(new GroupChatAndUpdate(groupChatInfo.groupChat, groupChatFull, update));
                                                }

                                                @Override protected boolean onError(TdApi.Error error)
                                                {
                                                    return false;
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        });
                    }
                    break;
                    case UpdateChatPhoto.CONSTRUCTOR:
                    {
                        final UpdateChatPhoto update = (UpdateChatPhoto) data;

                        getChat(update.chatId, new Obtainer<ChatAndUpdate>()
                        {
                            @Override public void obtain(final ChatAndUpdate chatAndUpdate)
                            {
                                ChatInfo chatInfo = chatArray.get(update.chatId);
                                chatInfo.dispatcher.dispatch(new ChatAndUpdate(chatInfo.chat, update));

                                if (chatInfo.chat.type instanceof TdApi.GroupChatInfo)
                                {
                                    final GroupChat groupChat = ((TdApi.GroupChatInfo) chatInfo.chat.type).groupChat;
                                    groupChat.photo = update.photo;

                                    GroupChatInfo groupChatInfo = groupChatArray.get(groupChat.id);

                                    if (groupChatInfo != null)
                                    {
                                        groupChatInfo.groupChat.photo = update.photo;
                                        groupChatInfo.dispatcher.dispatch(new GroupChatAndUpdate(groupChatInfo.groupChat, groupChatInfo.groupChatFull, update));
                                    }
                                }
                                else if (chatInfo.chat.type instanceof TdApi.PrivateChatInfo)
                                {
                                    final User user = ((PrivateChatInfo) chatInfo.chat.type).user;
                                    user.profilePhoto = update.photo;

                                    UserInfo userInfo = userArray.get(user.id);

                                    if (userInfo != null)
                                    {
                                        userInfo.user.profilePhoto = update.photo;
                                        userInfo.dispatcher.dispatch(new UserAndUpdate(userInfo.user, userInfo.userFull, update));
                                    }
                                }
                            }
                        });
                    }
                    break;
                    case UpdateStickers.CONSTRUCTOR:
                    {
                        stickerSetList = null;

                        getStickerSetList(new Obtainer<List<StickerSet>>()
                        {
                            @Override public void obtain(List<StickerSet> data)
                            {
                                STICKER_SET_LIST_DISPATCHER.dispatch(data);
                            }
                        });
                    }
                    break;
                    case UpdateOption.CONSTRUCTOR:
                    {
                        UpdateOption update = (UpdateOption) data;

                        OptionInfo optionInfo = optionMap.get(update.name);

                        if (optionInfo == null)
                        {
                            optionInfo = new OptionInfo(update.value);
                            optionMap.put(update.name, optionInfo);
                        }
                        else
                        {
                            optionInfo.optionValue = update.value;
                            optionInfo.dispatcher.dispatch(new OptionData(update.name, update.value));
                        }
                    }
                    break;
                }
            }
        }, true);
    }

    public static class OptionData
    {
        @NonNull public final String name;
        @NonNull public final OptionValue value;

        public OptionData(@NonNull String name, @NonNull OptionValue value)
        {
            this.name = name;
            this.value = value;
        }
    }

    private static class OptionInfo
    {
        public final EventDispatcher<OptionData> dispatcher = new EventDispatcher<>();
        @Nullable public OptionValue optionValue;

        private OptionInfo(@Nullable OptionValue optionValue)
        {
            this.optionValue = optionValue;
        }
    }

    public static class UserAndUpdate
    {
        @NonNull public final User user;
        @Nullable public final UserFull userFull;
        @Nullable public final Update update;

        public UserAndUpdate(@NonNull User user, @Nullable UserFull userFull, @Nullable Update update)
        {
            this.user = user;
            this.update = update;
            this.userFull = userFull;
        }
    }

    private static class UserInfo
    {
        @NonNull public User user;
        public UserFull userFull;
        public final EventDispatcher<UserAndUpdate> dispatcher = new EventDispatcher<>();

        private UserInfo(@NonNull User user, UserFull userFull)
        {
            this.user = user;
            this.userFull = userFull;
        }
    }

    public static class ChatAndUpdate
    {
        @NonNull public final Chat chat;
        @Nullable public final Update update;

        public ChatAndUpdate(@NonNull Chat chat, @Nullable Update update)
        {
            this.chat = chat;
            this.update = update;
        }
    }

    private static class ChatInfo
    {
        public final EventDispatcher<ChatAndUpdate> dispatcher = new EventDispatcher<>();

        @NonNull public Chat chat;

        @NonNull public final List<Message> photoAndVideoMessageList = new ArrayList<>();
        public int photoAndVideoMessageApproximateCount = Integer.MIN_VALUE / 2;
        public boolean photoAndVideoMessageListLoaded = false;

        @NonNull public final List<Message> audioMessageList = new ArrayList<>();
        public int audioMessageApproximateCount = Integer.MIN_VALUE / 2;
        public boolean audioMessageListLoaded = false;

        private ChatInfo(@NonNull Chat chat)
        {
            this.chat = chat;
        }
    }

    public static class ChatSearchedMessagesInfo
    {
        @NonNull public final List<Message> messageList;
        public final int approximateCount;
        public final boolean fullLoaded;

        public ChatSearchedMessagesInfo(@NonNull List<Message> messageList, int approximateCount, boolean fullLoaded)
        {
            this.messageList = messageList;
            this.approximateCount = approximateCount;
            this.fullLoaded = fullLoaded;
        }
    }

    public static class GroupChatAndUpdate
    {
        @NonNull public final GroupChat groupChat;
        @Nullable public final GroupChatFull groupChatFull;
        @Nullable public final Update update;

        public GroupChatAndUpdate(@NonNull GroupChat groupChat, @Nullable GroupChatFull groupChatFull, @Nullable Update update)
        {
            this.groupChat = groupChat;
            this.groupChatFull = groupChatFull;
            this.update = update;
        }
    }

    private static class GroupChatInfo
    {
        @NonNull public GroupChat groupChat;
        @Nullable public GroupChatFull groupChatFull;
        public final EventDispatcher<GroupChatAndUpdate> dispatcher = new EventDispatcher<>();

        private GroupChatInfo(@NonNull GroupChat groupChat, @Nullable GroupChatFull groupChatFull)
        {
            this.groupChat = groupChat;
            this.groupChatFull = groupChatFull;
        }
    }

    public static class MessageAndUpdate
    {
        @NonNull public final Message message;
        @Nullable public final Update update;

        public MessageAndUpdate(@NonNull Message message, @Nullable Update update)
        {
            this.message = message;
            this.update = update;
        }
    }

    private static final class MessageId
    {
        public final long chatId;
        public final int messageId;

        private MessageId(long chatId, int messageId)
        {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        private MessageId(Message message)
        {
            this.chatId = message.chatId;
            this.messageId = message.id;
        }

        @Override public boolean equals(Object another)
        {
            if (!(another instanceof MessageId))
            {
                return false;
            }

            MessageId anotherMessageId = (MessageId) another;

            return chatId == anotherMessageId.chatId && messageId == anotherMessageId.messageId;
        }

        @Override public int hashCode()
        {
            return messageId ^ (int) chatId;
        }
    }

    private static class MessageInfo
    {
        @NonNull public Message message;
        public final EventDispatcher<MessageAndUpdate> dispatcher = new EventDispatcher<>();

        private MessageInfo(@NonNull Message message)
        {
            this.message = message;
        }
    }
}
