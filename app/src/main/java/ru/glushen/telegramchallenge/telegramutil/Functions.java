package ru.glushen.telegramchallenge.telegramutil;

import android.content.res.*;
import android.support.annotation.*;

import ru.glushen.telegramchallenge.*;

import static java.lang.Math.*;
import static org.drinkless.td.libcore.telegram.TdApi.*;
import static org.drinkless.td.libcore.telegram.TdApi.ChatInfo;
import static org.drinkless.td.libcore.telegram.TdApi.GroupChatInfo;
import static ru.glushen.telegramchallenge.telegramutil.Info.*;

/**
 * Created by Pavel Glushen on 09.05.2015.
 */
public class Functions
{
    public static String getName(Chat chat, Resources resources)
    {
        return getName(chat.type, resources);
    }

    public static String getName(ChatInfo chatInfo, Resources resources)
    {
        if (chatInfo instanceof PrivateChatInfo)
        {
            return getName(((PrivateChatInfo) chatInfo).user, resources);
        }
        else if (chatInfo instanceof GroupChatInfo)
        {
            return getName(((GroupChatInfo) chatInfo).groupChat, resources);
        }
        else
        {
            throw new AssertionError("Unknown chat type: " + chatInfo);
        }
    }

    public static String getName(@NonNull User user, @NonNull Resources resources)
    {
        return getName(user.firstName, user.lastName, resources);
    }

    public static String getName(@NonNull GroupChat groupChat, @NonNull Resources resources)
    {
        return getName(groupChat.title, null, resources);
    }

    @SuppressWarnings("UnusedParameters") public static String getName(@Nullable String firstName, @Nullable String lastName, @NonNull Resources resources)
    {
        boolean firstNameIsEmpty = firstName == null || firstName.isEmpty();
        boolean lastNameIsEmpty = lastName == null || lastName.isEmpty();

        if (!firstNameIsEmpty && !lastNameIsEmpty)
        {
            return firstName + " " + lastName;
        }
        else if (!firstNameIsEmpty)
        {
            return firstName;
        }
        else if (!lastNameIsEmpty)
        {
            return lastName;
        }
        else
        {
            return resources.getString(R.string.nameUnknown);
        }
    }

    public static boolean isPrivateChat(Chat chat)
    {
        return isPrivateChat(chat.type);
    }

    public static boolean isPrivateChat(ChatInfo chatInfo)
    {
        return chatInfo instanceof PrivateChatInfo;
    }

    public static boolean isGroupChat(Chat chat)
    {
        return isGroupChat(chat.type);
    }

    public static boolean isGroupChat(ChatInfo chatInfo)
    {
        return chatInfo instanceof GroupChatInfo;
    }

    public static int getParticipantCount(Chat chat)
    {
        return chat.type instanceof GroupChatInfo ? ((GroupChatInfo) chat.type).groupChat.participantsCount : 0;
    }

    public static int getId(Chat chat)
    {
        return getId(chat.type);
    }

    public static int getId(ChatInfo chatInfo)
    {
        if (chatInfo instanceof PrivateChatInfo)
        {
            return ((PrivateChatInfo) chatInfo).user.id;
        }
        else if (chatInfo instanceof GroupChatInfo)
        {
            return ((GroupChatInfo) chatInfo).groupChat.id;
        }
        else
        {
            throw new AssertionError("Unknown chat type: " + chatInfo);
        }
    }

    public static int getId(Message message, boolean useForward)
    {
        return !useForward ? message.fromId : message.forwardFromId;
    }

    public static boolean isMessageSent(Message message)
    {
        return message.id < 1000000000;
    }

    public static boolean isMessageFailedToSend(Message message)
    {
        return message.date == 0;
    }

    public static boolean isOutboxMessageUnread(Chat chat, Message message)
    {
        return max(chat.lastReadInboxMessageId, chat.lastReadOutboxMessageId) < message.id && chat.unreadCount == 0;
    }

    public static long getTimeInMillis(int seconds)
    {
        return seconds * 1000L;
    }

    public static int getTimeInSeconds(long millis)
    {
        return (int) (millis / 1000L);
    }

    @StringRes public static int getConnectionStateStringRes(OptionValue optionValue, @StringRes int readyStringRes)
    {
        if (optionValue instanceof OptionString)
        {
            String optionString = ((OptionString) optionValue).value;

            if (CONNECTION_STATE_WAITING_FOR_NETWORK.equals(optionString))
            {
                return R.string.connectionStateWaitingForNetwork;
            }
            else if (CONNECTION_STATE_CONNECTING.equals(optionString))
            {
                return R.string.connectionStateConnecting;
            }
            else if (CONNECTION_STATE_UPDATING.equals(optionString))
            {
                return R.string.connectionStateUpdating;
            }
        }

        return readyStringRes;
    }

    public static String getConnectionStateString(OptionValue optionValue, String readyString, @NonNull Resources resources)
    {
        int connectionStateStringRes = getConnectionStateStringRes(optionValue, 0);
        return connectionStateStringRes != 0 ? resources.getString(connectionStateStringRes) : readyString;
    }
}
