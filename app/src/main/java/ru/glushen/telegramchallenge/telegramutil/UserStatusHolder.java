package ru.glushen.telegramchallenge.telegramutil;

import android.content.*;
import android.content.res.*;
import android.os.*;
import android.support.annotation.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static org.drinkless.td.libcore.telegram.TdApi.*;
import static ru.glushen.telegramchallenge.telegramutil.DateAndTime.*;
import static ru.glushen.telegramchallenge.telegramutil.Functions.*;
import static ru.glushen.telegramchallenge.telegramutil.Info.*;

/**
 * Created by Pavel Glushen on 10.06.2015.
 */
public class UserStatusHolder implements Runnable
{
    private final Handler handler = new Handler();

    @NonNull private User user;
    @NonNull private final Context context;

    public UserStatusHolder(@NonNull User user, @NonNull Context context)
    {
        this.context = context;

        this.user = user;
        Info.getUserUpdateDispatcher(user).addObtainer(userHandler, false);
        Info.getOptionDispatcher(OPTION_CONNECTION_STATE).addObtainer(connectionStateObtainer, false);
    }

    public void close()
    {
        Info.getUserUpdateDispatcher(user).removeObtainer(userHandler);
        Info.getOptionDispatcher(OPTION_CONNECTION_STATE).removeObtainer(connectionStateObtainer);
        handler.removeCallbacks(this);
    }

    public final EventDispatcher<String> updateStatusStringDispatcher = new EventDispatcher<>();

    private final Obtainer<UserAndUpdate> userHandler = new Obtainer<UserAndUpdate>()
    {
        @Override public void obtain(UserAndUpdate userAndUpdate)
        {
            if (userAndUpdate.update instanceof UpdateUserStatus)
            {
                user = userAndUpdate.user;
                dispatchStatusString();
            }
        }
    };

    private final Obtainer<OptionData> connectionStateObtainer = new Obtainer<OptionData>()
    {
        @Override public void obtain(OptionData data)
        {
            dispatchStatusString();
        }
    };

    @Override public void run()
    {
        dispatchStatusString();
    }

    public void dispatchStatusString()
    {
        handler.removeCallbacks(this);

        final Resources resources = context.getResources();
        UserStatus status = user.status;
        final String statusString;

        if (user.id == 777000)
        {
            statusString = resources.getString(R.string.messageListUserStatusServiceNotifications);
        }
        else if (user.type instanceof UserTypeBot)
        {
            statusString = resources.getString(R.string.messageListUserStatusBot);
        }
        else if (status instanceof UserStatusOffline || status instanceof UserStatusOnline && getTimeInMillis(((UserStatusOnline) status).expires) < System.currentTimeMillis())
        {
            int timeInSeconds = status instanceof UserStatusOffline ? ((UserStatusOffline) status).wasOnline : ((UserStatusOnline) status).expires;
            RelativeTime relativeTime = getRelative(context, getTimeInMillis(timeInSeconds), false, true);

            statusString = resources.getString(R.string.messageListUserStatusLastSeen, relativeTime.timeString);

            if (relativeTime.needToUpdate)
            {
                handler.postAtTime(this, relativeTime.updateTimeInMillis);
            }
        }
        else if (status instanceof UserStatusOnline)
        {
            statusString = resources.getString(R.string.messageListUserStatusOnline);
        }
        else if (status instanceof UserStatusRecently)
        {
            statusString = resources.getString(R.string.messageListUserStatusRecently);
        }
        else if (status instanceof UserStatusLastWeek)
        {
            statusString = resources.getString(R.string.messageListUserStatusLastWeek);
        }
        else if (status instanceof UserStatusLastMonth)
        {
            statusString = resources.getString(R.string.messageListUserStatusLastMonth);
        }
        else
        {
            statusString = "";
        }

        Info.getOptionValue(Info.OPTION_CONNECTION_STATE, new Obtainer<OptionData>()
        {
            @Override public void obtain(OptionData data)
            {
                updateStatusStringDispatcher.dispatch(getConnectionStateString(data.value, statusString, resources));
            }
        });
    }
}
