package ru.glushen.telegramchallenge.telegramutil;

import android.os.*;
import android.support.annotation.*;
import android.text.*;
import android.text.style.*;
import android.util.*;
import android.view.*;

import java.util.regex.*;

import ru.glushen.util.*;

/**
 * Created by Pavel Glushen on 19.08.2015.
 */
public class Hypertext
{
    public static class BotCommandClickEvent
    {
        public final int messageId;
        @NonNull public final String command;

        public BotCommandClickEvent(int messageId, @NonNull String command)
        {
            this.messageId = messageId;
            this.command = command;
        }
    }

    public static final EventDispatcher<BotCommandClickEvent> BOT_COMMAND_CLICK_DISPATCHER = new EventDispatcher<>();
    public static final EventDispatcher<String> USERNAME_CLICK_DISPATCHER = new EventDispatcher<>();

    private static class BotCommandSpan extends ClickableSpan
    {
        @NonNull private final BotCommandClickEvent event;

        public BotCommandSpan(int messageId, @NonNull String command)
        {
            event = new BotCommandClickEvent(messageId, command);
        }

        @Override public void onClick(View widget)
        {
            BOT_COMMAND_CLICK_DISPATCHER.dispatch(event);
        }

        @Override public void updateDrawState(@NonNull TextPaint ds)
        {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    private static class UsernameSpan extends ClickableSpan
    {
        @NonNull private final String username;

        public UsernameSpan(@NonNull String username)
        {
            this.username = username.length() > 0 && username.charAt(0) == '@' ? username.substring(1) : username;
        }

        @Override public void onClick(View widget)
        {
            USERNAME_CLICK_DISPATCHER.dispatch(username);
        }

        @Override public void updateDrawState(@NonNull TextPaint ds)
        {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    private static class UriSpan extends URLSpan
    {
        public UriSpan(String url)
        {
            super(url);
        }

        public UriSpan(Parcel src)
        {
            super(src);
        }

        @Override public void updateDrawState(@NonNull TextPaint ds)
        {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    private static final Pattern BOT_COMMAND_PATTERN = Pattern.compile("(?-m)(?<=^|\\s)/[a-zA-Z0-9_]+(?:@[a-zA-Z0-9_]+)?(?=\\s|$)");
    private static final Pattern USERNAME_PATTERN = Pattern.compile("(?-m)(?<=^|\\s)@[a-zA-Z0-9_]+(?=\\s|$)");
    private static final Pattern URI_HAS_SCHEME = Pattern.compile("^[a-zA-Z0-9]+://");

    public static void obtainLinks(@NonNull Spannable spannable, int messageId)
    {
        Matcher matcher = BOT_COMMAND_PATTERN.matcher(spannable);

        while (matcher.find())
        {
            spannable.setSpan(new BotCommandSpan(messageId, matcher.group()), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        matcher = USERNAME_PATTERN.matcher(spannable);

        while (matcher.find())
        {
            spannable.setSpan(new UsernameSpan(matcher.group()), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        matcher = Patterns.WEB_URL.matcher(spannable);

        while (matcher.find())
        {
            String uri = matcher.group();

            if (!URI_HAS_SCHEME.matcher(uri).find())
            {
                uri = "http://" + uri;
            }

            spannable.setSpan(new UriSpan(uri), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }
}
