package ru.glushen.telegramchallenge.telegramutil;

import android.content.*;
import android.net.*;
import android.support.annotation.*;
import android.support.v4.content.*;
import android.util.*;
import android.webkit.*;
import android.widget.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static org.drinkless.td.libcore.telegram.TdApi.*;

/**
 * Created by Pavel Glushen on 20.05.2015.
 */
public class FileLoader extends CloseableObtainer<Update>
{
    public enum State
    {
        NOT_EXIST, EMPTY, LOADING, LOADED
    }

    public static final FileLoader FILE_NOT_EXIST = new FileLoader(new File(0, "", 0, ""));

    private static final SparseArray<FileLoader> instances = new SparseArray<>();

    public static FileLoader getInstance(File file)
    {
        if (file == null)
        {
            return FILE_NOT_EXIST;
        }

        int id = getId(file);

        if (id == 0)
        {
            return FILE_NOT_EXIST;
        }

        FileLoader fileLoader = instances.get(id);

        if (fileLoader == null)
        {
            fileLoader = new FileLoader(file);
            instances.put(id, fileLoader);
        }

        return fileLoader;
    }

    @NonNull private File file;

    private FileLoader(@NonNull File file)
    {
        this.file = file;

        if (isLoaded())
        {
            ready = getSize();
        }

        TelegramClient.addUpdateObtainer(UpdateFileProgress.CONSTRUCTOR, this, false);
        TelegramClient.addUpdateObtainer(UpdateFile.CONSTRUCTOR, this, false);
    }

    public boolean isLoaded()
    {
        return file.path != null && !file.path.isEmpty();
    }

    public int getSize()
    {
        return file.size;
    }

    private final LinkedHashSet<Obtainer<String>> pathObtainers = new LinkedHashSet<>();

    public void getPath(Obtainer<String> obtainer)
    {
        if (isLoaded() || !isExist())
        {
            if (obtainer != null)
            {
                obtainer.obtain(getPath());
            }
        }
        else
        {
            pathObtainers.add(obtainer);
            load();
        }
    }

    public boolean isExist()
    {
        return getId() != 0;
    }

    @Nullable public String getPath()
    {
        return file.path != null && !file.path.isEmpty() ? file.path : null;
    }

    public void load()
    {
        if (getState() == State.EMPTY)
        {
            loading = true;

            TelegramClient.request(new DownloadFile(getId()), null);

            dispatchState();
        }
    }

    private static int getId(@NonNull File file)
    {
        return file.id;
    }

    public int getId()
    {
        return getId(file);
    }

    public State getState()
    {
        if (isExist())
        {
            if (isLoaded())
            {
                return State.LOADED;
            }
            else
            {
                return isLoading() ? State.LOADING : State.EMPTY;
            }
        }
        else
        {
            return State.NOT_EXIST;
        }
    }

    private void dispatchState()
    {
        stateDispatcher.dispatch(this);
    }

    public boolean isLoading()
    {
        return loading;
    }

    private final EventDispatcher<FileLoader> stateDispatcher = new EventDispatcher<>();

    public EventDispatcher<FileLoader> getStateDispatcher()
    {
        return stateDispatcher;
    }

    private boolean loading = false;
    private int ready = 0;

    public int getReady()
    {
        return ready;
    }

    @Override protected void onObtain(Update update)
    {
        if (update instanceof UpdateFileProgress)
        {
            UpdateFileProgress updateFileProgress = (UpdateFileProgress) update;

            if (updateFileProgress.fileId == getId())
            {
                loading = true;

                ready = updateFileProgress.ready;

                dispatchState();
            }
        }
        else
        {
            UpdateFile updateFile = (UpdateFile) update;

            if (updateFile.file.id == getId())
            {
                loading = false;
                file = updateFile.file;
                ready = file.size;

                for (Obtainer<String> obtainer : pathObtainers)
                {
                    if (obtainer != null)
                    {
                        obtainer.obtain(file.path);
                    }
                }

                pathObtainers.clear();

                dispatchState();
            }
        }
    }

    public void cancelLoad()
    {
        if (loading)
        {
            TelegramClient.request(new CancelDownloadFile(getId()), null);

            loading = false;

            dispatchState();

            pathObtainers.clear();
        }
    }

    public boolean open(Context context)
    {
        String path = getPath();

        if (path != null)
        {
            String extension = path.substring(path.lastIndexOf('.'));

            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

            if (mimeType != null)
            {
                return open(context, mimeType);
            }
        }

        return false;
    }

    public boolean open(Context context, String mimeType)
    {
        String path = getPath();

        if (path != null)
        {
            Uri uri = FileProvider.getUriForFile(context, SingleActivity.FILE_PROVIDER_AUTHORITY, new java.io.File(path));

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(uri, mimeType);

            try
            {
                context.startActivity(intent);

                return true;
            }
            catch (ActivityNotFoundException exception)
            {
                Toast.makeText(context, R.string.messageListDocumentCannotOpen, Toast.LENGTH_LONG).show();

                return false;
            }
        }
        else
        {
            return false;
        }
    }

    @Override public boolean equals(Object object)
    {
        return object instanceof FileLoader && getId() == ((FileLoader) object).getId();
    }

    @Override public int hashCode()
    {
        return getId();
    }
}
