package ru.glushen.telegramchallenge.telegramutil;

import android.content.*;
import android.content.res.*;
import android.os.*;
import android.support.annotation.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static org.drinkless.td.libcore.telegram.TdApi.*;
import static ru.glushen.telegramchallenge.telegramutil.Functions.*;
import static ru.glushen.telegramchallenge.telegramutil.Info.*;

/**
 * Created by Pavel Glushen on 10.06.2015.
 */
public class GroupStatusHolder implements Runnable
{
    private final Handler handler = new Handler();

    @NonNull private GroupChatFull groupChatFull;
    @NonNull private final Context context;

    public GroupStatusHolder(@NonNull GroupChatFull groupChatFull, @NonNull Context context)
    {
        this.context = context;

        this.groupChatFull = groupChatFull;
        Info.getGroupChatFullUpdateDispatcher(groupChatFull).addObtainer(groupChatHandler, false);
        Info.getOptionDispatcher(OPTION_CONNECTION_STATE).addObtainer(connectionStateObtainer, false);
    }

    public void close()
    {
        Info.getGroupChatFullUpdateDispatcher(groupChatFull).removeObtainer(groupChatHandler);
        Info.getOptionDispatcher(OPTION_CONNECTION_STATE).removeObtainer(connectionStateObtainer);
        handler.removeCallbacks(this);
    }

    public final EventDispatcher<String> updateStatusStringDispatcher = new EventDispatcher<>();

    private final Obtainer<GroupChatAndUpdate> groupChatHandler = new Obtainer<GroupChatAndUpdate>()
    {
        @Override public void obtain(GroupChatAndUpdate groupChatAndUpdate)
        {
            if (groupChatAndUpdate.update instanceof UpdateChatParticipantsCount || groupChatAndUpdate.update instanceof UpdateUserStatus)
            {
                if (groupChatAndUpdate.groupChatFull != null)
                {
                    groupChatFull = groupChatAndUpdate.groupChatFull;
                    dispatchStatusString();
                }
                else
                {
                    throw new AssertionError("groupChatFull must not be null");
                }
            }
        }
    };

    private final Obtainer<OptionData> connectionStateObtainer = new Obtainer<OptionData>()
    {
        @Override public void obtain(OptionData data)
        {
            dispatchStatusString();
        }
    };

    @Override public void run()
    {
        dispatchStatusString();
    }

    public void dispatchStatusString()
    {
        handler.removeCallbacks(this);

        final Resources resources = context.getResources();

        int onlineCount = 0;
        long minExpiresTimeInMillis = Long.MAX_VALUE;

        for (ChatParticipant chatParticipant : groupChatFull.participants)
        {
            UserStatus status = chatParticipant.user.status;

            if (status instanceof UserStatusOnline)
            {
                long expiresTimeInMillis = getTimeInMillis(((UserStatusOnline) status).expires);

                if (expiresTimeInMillis < System.currentTimeMillis())
                {
                    onlineCount++;
                    minExpiresTimeInMillis = Math.min(expiresTimeInMillis, minExpiresTimeInMillis);
                }
            }
        }

        if (minExpiresTimeInMillis < Long.MAX_VALUE)
        {
            handler.postAtTime(this, minExpiresTimeInMillis + 10);
        }

        int memberCount = groupChatFull.participants.length;

        int pluralsRes = onlineCount <= 1 ? R.plurals.messageListParticipantCountOffline : R.plurals.messageListParticipantCountOnline;
        final String statusString = resources.getQuantityString(pluralsRes, memberCount, memberCount, onlineCount);

        Info.getOptionValue(Info.OPTION_CONNECTION_STATE, new Obtainer<OptionData>()
        {
            @Override public void obtain(OptionData data)
            {
                updateStatusStringDispatcher.dispatch(getConnectionStateString(data.value, statusString, resources));
            }
        });
    }
}
