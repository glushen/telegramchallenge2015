package ru.glushen.telegramchallenge.telegramutil;

import android.content.res.*;
import android.graphics.*;
import android.support.annotation.*;
import android.text.*;
import android.text.style.*;

/**
 * Created by Pavel Glushen on 03.08.2015.
 */
public class Font
{
    @Nullable private static Typeface robotoMedium;

    @NonNull public static Typeface getRobotoMedium(Resources resources)
    {
        if (robotoMedium == null)
        {
            robotoMedium = Typeface.createFromAsset(resources.getAssets(), "roboto_medium.ttf");
        }

        return robotoMedium;
    }

    public static class Span extends MetricAffectingSpan
    {
        @NonNull private final Typeface typeface;

        public Span(@NonNull Typeface typeface)
        {
            this.typeface = typeface;
        }

        @Override public void updateMeasureState(TextPaint textPaint)
        {
            textPaint.setTypeface(typeface);
        }

        @Override public void updateDrawState(TextPaint textPaint)
        {
            textPaint.setTypeface(typeface);
        }
    }

    private Font()
    {

    }
}
