package ru.glushen.telegramchallenge.telegramutil;

import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.support.annotation.*;
import android.text.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;
import java.util.regex.*;

import ru.glushen.image.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;

import static android.graphics.Paint.*;
import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 30.06.2015.
 */
public class AvatarBitmapDrawableCreator implements SmartImageView.BitmapDrawableCreator
{
    private final int id;
    @NonNull private final String name;
    @NonNull private final FileLoader smallAvatarLoader;
    @NonNull private final FileLoader bigAvatarLoader;

    public static AvatarBitmapDrawableCreator newInstance(@NonNull TdApi.Chat chat, @NonNull Resources resources)
    {
        if (chat.type instanceof TdApi.PrivateChatInfo)
        {
            return newInstance(((TdApi.PrivateChatInfo) chat.type).user, resources);
        }
        else if (chat.type instanceof TdApi.GroupChatInfo)
        {
            return newInstance(((TdApi.GroupChatInfo) chat.type).groupChat, resources);
        }
        else
        {
            throw new AssertionError("Unknown chat type: " + chat.type);
        }
    }

    public static AvatarBitmapDrawableCreator newInstance(@NonNull TdApi.User user, @NonNull Resources resources)
    {
        return new AvatarBitmapDrawableCreator(user.id, Functions.getName(user, resources), user.profilePhoto);
    }

    public static AvatarBitmapDrawableCreator newInstance(@NonNull TdApi.GroupChat groupChat, @NonNull Resources resources)
    {
        return new AvatarBitmapDrawableCreator(groupChat.id, Functions.getName(groupChat, resources), groupChat.photo);
    }

    private AvatarBitmapDrawableCreator(int id, @Nullable String name, @Nullable TdApi.ProfilePhoto profilePhoto)
    {
        this.id = id;
        this.name = name != null ? name : "";

        smallAvatarLoader = profilePhoto != null ? FileLoader.getInstance(profilePhoto.small) : FileLoader.FILE_NOT_EXIST;
        bigAvatarLoader = profilePhoto != null ? FileLoader.getInstance(profilePhoto.big) : FileLoader.FILE_NOT_EXIST;
    }

    @Override public ViewGroup.LayoutParams onCreate(int initWidth, int initHeight, final CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer, final Resources resources)
    {
        final int size = min(initWidth, initHeight);

        FileLoader fileLoader = size > 180 && bigAvatarLoader.isExist() ? bigAvatarLoader : smallAvatarLoader;

        final String cacheKey = AvatarBitmapDrawableCreator.class.getName() + "::" + id + "," + name + "," + fileLoader.getId() + "," + size;

        BitmapDrawable cachedBitmapDrawable = Image.bitmapDrawableCache.get(cacheKey);

        if (cachedBitmapDrawable != null)
        {
            bitmapDrawableObtainer.obtain(cachedBitmapDrawable);
        }
        else if (fileLoader.isExist())
        {
            fileLoader.getPath(new Obtainer<String>()
            {
                @Override public void obtain(final String path)
                {
                    create(cacheKey, path, size, bitmapDrawableObtainer, resources);
                }
            });
        }
        else
        {
            create(cacheKey, null, size, bitmapDrawableObtainer, resources);
        }

        return null;
    }

    private static final int[] COLORS = {0xff7fcbd9, 0xff70b2d8, 0xff7bcb81, 0xff7bcb81, 0xffeea781, 0xffef83ac, 0xffe6c26c};
    private static final Pattern CREATE_LABEL_PATTERN = Pattern.compile("^\\s*(\\S)\\S*\\s*(?:[\\s\\S]*\\s(\\S)\\S*\\s*)?$");

    private void create(@NonNull final String cacheKey, @Nullable final String path, final int size, @NonNull final CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer, @NonNull final Resources resources)
    {
        if (bitmapDrawableObtainer.hasClosed())
        {
            return;
        }

        new AsyncTask<Void, Void, BitmapDrawable>()
        {
            @Override protected BitmapDrawable doInBackground(Void... params)
            {
                if (bitmapDrawableObtainer.hasClosed())
                {
                    cancel(false);
                    return null;
                }

                Bitmap returnableBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);

                Canvas canvas = new Canvas(returnableBitmap);

                Paint paint = new TextPaint(ANTI_ALIAS_FLAG | FILTER_BITMAP_FLAG);
                paint.setStyle(Style.FILL);
                paint.setColor(0xff000000);
                paint.setTypeface(Font.getRobotoMedium(resources));

                float radius = size / 2f;

                if (path != null)
                {
                    canvas.drawCircle(radius, radius, radius, paint);
                    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                    canvas.drawBitmap(BitmapFactory.decodeFile(path), null, new Rect(0, 0, size, size), paint);
                }
                else
                {
                    paint.setColor(COLORS[Math.abs(id % COLORS.length)]);
                    canvas.drawCircle(radius, radius, radius, paint);

                    Matcher matcher = CREATE_LABEL_PATTERN.matcher(name);

                    String text = "";

                    if (matcher.find())
                    {
                        if (matcher.group(1) != null)
                        {
                            text += matcher.group(1);
                        }

                        if (matcher.group(2) != null)
                        {
                            text += matcher.group(2);
                        }
                    }

                    text = text.toUpperCase(Locale.ENGLISH);

                    paint.setTextSize(size / 3f);
                    paint.setTextAlign(Align.CENTER);
                    paint.setColor(0xffffffff);
                    canvas.drawText(text, radius, radius - (paint.descent() + paint.ascent()) / 2, paint);
                }

                return Image.createBitmapDrawable(returnableBitmap, resources);
            }

            @Override protected void onPostExecute(BitmapDrawable bitmapDrawable)
            {
                Image.bitmapDrawableCache.put(cacheKey, bitmapDrawable);
                bitmapDrawableObtainer.obtain(bitmapDrawable);
            }
        }.execute();
    }
}
