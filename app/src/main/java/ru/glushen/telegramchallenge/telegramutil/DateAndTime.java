package ru.glushen.telegramchallenge.telegramutil;

import android.content.*;
import android.content.res.*;
import android.text.format.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;

import static java.util.Calendar.*;

/**
 * Created by Pavel Glushen on 10.05.2015.
 */
public class DateAndTime
{
    private static final int MILLIS_IN_SECOND = 1000;
    private static final int MILLIS_IN_MINUTE = 60000;
    private static final int MILLIS_IN_HOUR = 3600000;
    private static final int MILLIS_IN_DAY = 86400000;

    public static class RelativeTime
    {
        public final String timeString;
        public final long updateTimeInMillis;
        public final boolean needToUpdate;

        public RelativeTime(String timeString, long updateTimeInMillis, boolean needToUpdate)
        {
            this.timeString = timeString;
            this.updateTimeInMillis = updateTimeInMillis;
            this.needToUpdate = needToUpdate;
        }
    }

    public static RelativeTime getRelative(Context context, long timeInMillis, boolean firstChatInUpperCase, boolean dateAbbreviated)
    {
        Resources res = context.getResources();

        long currentTimeInMillis = System.currentTimeMillis();
        long timeDelta = timeInMillis - currentTimeInMillis;
        long timeDeltaAbs = Math.abs(timeDelta);

        if (timeDeltaAbs < MILLIS_IN_MINUTE)
        {
            String timeString = setFirstChar(res.getString(R.string.timeJustNow), firstChatInUpperCase);
            return new RelativeTime(timeString, timeInMillis + MILLIS_IN_MINUTE + 10, true);
        }
        else if (timeDelta < 0)
        {
            if (timeDeltaAbs < MILLIS_IN_HOUR)
            {
                int minuteCount = (int) (timeDeltaAbs / MILLIS_IN_MINUTE);
                String timeString = res.getQuantityString(R.plurals.timeMinutesAgo, minuteCount, minuteCount);
                timeString = setFirstChar(timeString, firstChatInUpperCase);
                return new RelativeTime(timeString, timeInMillis + (minuteCount + 1) * MILLIS_IN_MINUTE + 10, true);
            }
            else if (timeDeltaAbs < MILLIS_IN_DAY)
            {
                int hourCount = (int) (timeDeltaAbs / MILLIS_IN_HOUR);
                String timeString = res.getQuantityString(R.plurals.timeHoursAgo, hourCount, hourCount);
                timeString = setFirstChar(timeString, firstChatInUpperCase);
                return new RelativeTime(timeString, timeInMillis + (hourCount + 1) * MILLIS_IN_HOUR + 10, true);
            }
            else if (timeDeltaAbs < MILLIS_IN_DAY * 4)
            {
                int dayCount = (int) (timeDeltaAbs / MILLIS_IN_DAY);
                String timeString = res.getQuantityString(R.plurals.timeDaysAgo, dayCount, dayCount);
                timeString = setFirstChar(timeString, firstChatInUpperCase);
                return new RelativeTime(timeString, timeInMillis + (dayCount + 1) * MILLIS_IN_DAY + 10, true);
            }
            else
            {
                String timeString = getDateOrTime(context, timeInMillis, dateAbbreviated);
                return new RelativeTime(timeString, 0, false);
            }
        }
        else
        {
            if (timeDeltaAbs < MILLIS_IN_HOUR)
            {
                int minuteCount = (int) (timeDeltaAbs / MILLIS_IN_MINUTE);
                String timeString = res.getQuantityString(R.plurals.timeInMinutes, minuteCount, minuteCount);
                timeString = setFirstChar(timeString, firstChatInUpperCase);
                return new RelativeTime(timeString, timeInMillis - minuteCount * MILLIS_IN_MINUTE + 10, true);
            }
            else if (timeDeltaAbs < MILLIS_IN_DAY)
            {
                int hourCount = (int) (timeDeltaAbs / MILLIS_IN_HOUR);
                String timeString = res.getQuantityString(R.plurals.timeInHours, hourCount, hourCount);
                timeString = setFirstChar(timeString, firstChatInUpperCase);
                return new RelativeTime(timeString, timeInMillis - hourCount * MILLIS_IN_HOUR + 10, true);
            }
            else if (timeDeltaAbs < MILLIS_IN_DAY * 4)
            {
                int dayCount = (int) (timeDeltaAbs / MILLIS_IN_DAY);
                String timeString = res.getQuantityString(R.plurals.timeInDays, dayCount, dayCount);
                timeString = setFirstChar(timeString, firstChatInUpperCase);
                return new RelativeTime(timeString, timeInMillis - dayCount * MILLIS_IN_DAY + 10, true);
            }
            else
            {
                String timeString = getDateOrTime(context, timeInMillis, dateAbbreviated);
                return new RelativeTime(timeString, 0, false);
            }
        }
    }

    private static String setFirstChar(String string, boolean firstChatInUpperCase)
    {
        if (firstChatInUpperCase)
        {
            return string.substring(0, 1).toUpperCase(Locale.ENGLISH) + string.substring(1);
        }
        else
        {
            return string.substring(0, 1).toLowerCase(Locale.ENGLISH) + string.substring(1);
        }
    }

    public static String getDateOrTime(Context context, long timeInMillis, boolean dateAbbreviated)
    {
        Calendar calendar = getInstance();
        calendar.setTimeInMillis(timeInMillis);

        Calendar currentCalendar = getInstance();
        currentCalendar.setTimeInMillis(System.currentTimeMillis());

        boolean currentDayOfYear = (calendar.get(DAY_OF_YEAR) == currentCalendar.get(DAY_OF_YEAR));
        boolean currentYear = (calendar.get(YEAR) == currentCalendar.get(YEAR));

        if (currentDayOfYear && currentYear)
        {
            return getTime(context, timeInMillis);
        }
        else
        {
            return getDate(context, timeInMillis, dateAbbreviated);
        }
    }

    public static String getTime(Context context, long timeInMillis)
    {
        Calendar calendar = getInstance();
        calendar.setTimeInMillis(timeInMillis);

        boolean is24 = DateFormat.is24HourFormat(context);
        boolean isAM = calendar.get(AM_PM) == AM;

        int stringResource;

        if (is24)
        {
            stringResource = R.string.time24;
        }
        else if (isAM)
        {
            stringResource = R.string.time12AM;
        }
        else
        {
            stringResource = R.string.time12PM;
        }

        int hour = calendar.get(is24 ? HOUR_OF_DAY : HOUR);
        int minute = calendar.get(MINUTE);

        return context.getResources().getString(stringResource, hour, minute);
    }

    public static String getDate(Context context, long timeInMillis, boolean dateAbbreviated)
    {
        Resources resources = context.getResources();

        Calendar calendar = getInstance();
        calendar.setTimeInMillis(timeInMillis);

        int day = calendar.get(DAY_OF_MONTH);

        int month = calendar.get(MONTH);
        int monthStringArrayRes = dateAbbreviated ? R.array.monthAbbreviations : R.array.monthFullNames;
        String monthStr = resources.getStringArray(monthStringArrayRes)[month];

        int year = calendar.get(YEAR);

        if (equalsYears(timeInMillis, System.currentTimeMillis()))
        {
            return resources.getString(R.string.dateCurrentYear, day, monthStr);
        }
        else
        {
            return resources.getString(R.string.dateFull, day, monthStr, year);
        }
    }

    public static String getMonthAndYear(Context context, long timeInMillis, boolean dateAbbreviated)
    {
        Resources resources = context.getResources();

        Calendar calendar = getInstance();
        calendar.setTimeInMillis(timeInMillis);

        int month = calendar.get(MONTH);
        int monthStringArrayRes = dateAbbreviated ? R.array.monthAbbreviations : R.array.monthFullNames;
        String monthStr = resources.getStringArray(monthStringArrayRes)[month];

        int year = calendar.get(YEAR);

        return resources.getString(R.string.dateMonthAndYear, monthStr, year);
    }

    public static String getTimerTime(long timeInMillis)
    {
        int hour = (int) (timeInMillis % MILLIS_IN_DAY) / MILLIS_IN_HOUR;
        int minute = (int) (timeInMillis % MILLIS_IN_HOUR) / MILLIS_IN_MINUTE;
        int second = (int) (timeInMillis % MILLIS_IN_MINUTE) / MILLIS_IN_SECOND;

        if (hour > 0)
        {
            return String.format("%d:%02d:%02d", hour, minute, second);
        }
        else
        {
            return String.format("%d:%02d", minute, second);
        }
    }

    public static boolean equalsYears(long timeInMillis1, long timeInMillis2)
    {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(timeInMillis1);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTimeInMillis(timeInMillis2);

        return calendar1.get(YEAR) == calendar2.get(YEAR);
    }

    public static boolean equalsMonths(long timeInMillis1, long timeInMillis2)
    {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(timeInMillis1);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTimeInMillis(timeInMillis2);

        boolean yearEquals = calendar1.get(YEAR) == calendar2.get(YEAR);
        boolean monthEquals = calendar1.get(MONTH) == calendar2.get(MONTH);

        return yearEquals && monthEquals;
    }

    public static boolean equalsDays(long timeInMillis1, long timeInMillis2)
    {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(timeInMillis1);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTimeInMillis(timeInMillis2);

        boolean yearEquals = calendar1.get(YEAR) == calendar2.get(YEAR);
        boolean dayOfYearEquals = calendar1.get(DAY_OF_YEAR) == calendar2.get(DAY_OF_YEAR);

        return yearEquals && dayOfYearEquals;
    }

    public static boolean equalsMinutes(long timeInMillis1, long timeInMillis2)
    {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(timeInMillis1);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTimeInMillis(timeInMillis2);

        boolean yearEquals = calendar1.get(YEAR) == calendar2.get(YEAR);
        boolean dayOfYearEquals = calendar1.get(DAY_OF_YEAR) == calendar2.get(DAY_OF_YEAR);
        boolean hourOfDayEquals = calendar1.get(HOUR_OF_DAY) == calendar2.get(HOUR_OF_DAY);
        boolean minuteEquals = calendar1.get(MINUTE) == calendar2.get(MINUTE);

        return yearEquals && dayOfYearEquals && hourOfDayEquals && minuteEquals;
    }
}
