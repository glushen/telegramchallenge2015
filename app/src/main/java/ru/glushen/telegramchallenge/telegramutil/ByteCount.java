package ru.glushen.telegramchallenge.telegramutil;

import android.content.res.*;

import ru.glushen.telegramchallenge.*;

/**
 * Created by Pavel Glushen on 20.05.2015.
 */
public class ByteCount
{
    public static String toString(Resources resources, long byteCount)
    {
        String[] abbreviationsSmall = resources.getStringArray(R.array.byteCountAbbreviationsSmall);
        String[] abbreviationsBig = resources.getStringArray(R.array.byteCountAbbreviationsBig);

        int index = 0;
        int length = Math.min(abbreviationsSmall.length, abbreviationsBig.length);
        double value = byteCount;

        while (index < length && value > 100)
        {
            index++;
            value /= 1024;
        }

        return String.format((value < 10 ? abbreviationsSmall : abbreviationsBig)[index], value);
    }
}
