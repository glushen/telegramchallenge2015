package ru.glushen.telegramchallenge.telegramutil;

import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.view.*;

import java.net.*;
import java.util.*;

import ru.glushen.image.*;
import ru.glushen.telegramchallenge.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 30.06.2015.
 */
public class LocationBitmapDrawableCreator implements SmartImageView.BitmapDrawableCreator
{
    private final double latitude;
    private final double longitude;

    public LocationBitmapDrawableCreator(double latitude, double longitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private static final String URL_FORMAT_STRING = "https://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=17&size=%dx%d&maptype=roadmap&language=%s";

    private final Set<String> loadingURLs = new HashSet<>();

    @Override public ViewGroup.LayoutParams onCreate(int initWidth, int initHeight, final CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer, final Resources resources)
    {
        final String url = String.format(Locale.ENGLISH, URL_FORMAT_STRING, latitude, longitude, initWidth, initHeight, resources.getString(R.string.currentLanguageAbbreviation));

        final String cacheKey = LocationBitmapDrawableCreator.class.getName() + "::" + url;

        BitmapDrawable cachedBitmapDrawable = Image.bitmapDrawableCache.get(cacheKey);

        if (cachedBitmapDrawable != null)
        {
            bitmapDrawableObtainer.obtain(cachedBitmapDrawable);
        }
        else if (!loadingURLs.contains(url))
        {
            loadingURLs.add(url);

            final Handler handler = new Handler();

            new Thread(new Runnable()
            {
                private boolean loaded = false;
                private BitmapDrawable bitmapDrawable;

                @Override public void run()
                {
                    if (!loaded)
                    {
                        try
                        {
                            Bitmap bitmap = BitmapFactory.decodeStream(new URL(url).openStream());
                            bitmapDrawable = Image.createBitmapDrawable(bitmap, resources);

                            loaded = true;

                            handler.post(this);
                        }
                        catch (Exception e)
                        {
                            SingleActivity.log(e);
                        }
                    }
                    else
                    {
                        loadingURLs.remove(url);

                        Image.bitmapDrawableCache.put(cacheKey, bitmapDrawable);
                        bitmapDrawableObtainer.obtain(bitmapDrawable);
                    }
                }
            }).start();
        }

        return null;
    }
}
