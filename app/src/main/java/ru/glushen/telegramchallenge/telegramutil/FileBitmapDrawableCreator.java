package ru.glushen.telegramchallenge.telegramutil;

import android.content.res.*;
import android.graphics.drawable.*;
import android.support.annotation.*;

import ru.glushen.image.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;

import static android.view.ViewGroup.*;
import static ru.glushen.image.Image.*;

/**
 * Created by Pavel Glushen on 21.06.2015.
 */
public class FileBitmapDrawableCreator implements SmartImageView.BitmapDrawableCreator
{
    private final String filePath;
    private final FileLoader fileLoader;
    private final Filter filter;
    private final int blurRadius;
    private final int imageWidth;
    private final int imageHeight;
    private final LayoutParams layoutParams;

    public FileBitmapDrawableCreator(@NonNull String filePath, @NonNull Filter filter, int blurRadius)
    {
        this(filePath, null, filter, blurRadius, -1, -1, null);
    }

    public FileBitmapDrawableCreator(@NonNull String filePath, @NonNull Filter filter, int blurRadius, int imageWidth, int imageHeight, @Nullable LayoutParams layoutParams)
    {
        this(filePath, null, filter, blurRadius, imageWidth, imageHeight, layoutParams);
    }

    public FileBitmapDrawableCreator(@NonNull FileLoader fileLoader, @NonNull Filter filter, int blurRadius)
    {
        this(null, fileLoader, filter, blurRadius, -1, -1, null);
    }

    public FileBitmapDrawableCreator(@NonNull FileLoader fileLoader, @NonNull Filter filter, int blurRadius, int imageWidth, int imageHeight, @Nullable LayoutParams layoutParams)
    {
        this(null, fileLoader, filter, blurRadius, imageWidth, imageHeight, layoutParams);
    }

    private FileBitmapDrawableCreator(String filePath, FileLoader fileLoader, Filter filter, int blurRadius, int imageWidth, int imageHeight, LayoutParams layoutParams)
    {
        this.filePath = filePath;
        this.fileLoader = fileLoader;
        this.filter = filter;
        this.blurRadius = blurRadius;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.layoutParams = layoutParams;
    }

    @Override public LayoutParams onCreate(int initWidth, int initHeight, final CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer, final Resources resources)
    {
        final SizeParams.Creator sizeParamsCreator = new SizeParams.Creator(initWidth, initHeight, filter);

        if (filePath != null)
        {
            Image.loadAsync(filePath, sizeParamsCreator, blurRadius, bitmapDrawableObtainer, resources);
        }
        else
        {
            fileLoader.getPath(new Obtainer<String>()
            {
                @Override public void obtain(String path)
                {
                    if (path != null)
                    {
                        Image.loadAsync(path, sizeParamsCreator, blurRadius, bitmapDrawableObtainer, resources);
                    }
                }
            });
        }

        if (layoutParams != null)
        {
            SizeParams sizeParams = sizeParamsCreator.create(imageWidth, imageHeight);

            layoutParams.width = sizeParams.width;
            layoutParams.height = sizeParams.height;
        }

        return layoutParams;
    }
}
