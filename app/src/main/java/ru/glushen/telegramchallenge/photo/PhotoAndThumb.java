package ru.glushen.telegramchallenge.photo;

import android.support.annotation.*;

import java.util.*;

import ru.glushen.telegramchallenge.telegramutil.*;

import static org.drinkless.td.libcore.telegram.TdApi.*;

/**
 * Created by Pavel Glushen on 05.07.2015.
 */
class PhotoAndThumb
{
    @Nullable public final PhotoSize photo;
    @Nullable public final PhotoSize thumb;

    public PhotoAndThumb(int width, int height, PhotoSize... photoSizes)
    {
        PhotoSize photo = choosePhotoSize(width, height, photoSizes);
        PhotoSize thumb = chooseLoadedPhotoSize(width, height, photoSizes);

        if (photo != null && thumb != null && comparePhotoSizes(photo, thumb) <= 0)
        {
            photo = thumb;
            thumb = null;
        }

        this.photo = photo;
        this.thumb = thumb;
    }

    private static int comparePhotoSizes(@NonNull PhotoSize left, @NonNull PhotoSize right)
    {
        int leftFactor = left.width * left.height;
        int rightFactor = right.width * right.height;

        return leftFactor > rightFactor ? 1 : (leftFactor < rightFactor ? -1 : 0);
    }

    @NonNull private static List<PhotoSize> chooseBoxIfExistAndSort(@NonNull PhotoSize... photoSizes)
    {
        List<PhotoSize> photoSizeList = new ArrayList<>();

        Collections.addAll(photoSizeList, photoSizes);

        Iterator<PhotoSize> iterator = photoSizeList.iterator();

        while (iterator.hasNext())
        {
            String type = iterator.next().type;

            if (!("s".equals(type) || "m".equals(type) || "x".equals(type) || "y".equals(type) || "w".equals(type)))
            {
                iterator.remove();
            }
        }

        if (photoSizeList.isEmpty())
        {
            Collections.addAll(photoSizeList, photoSizes);
        }

        Collections.sort(photoSizeList, new Comparator<PhotoSize>()
        {
            @Override public int compare(PhotoSize left, PhotoSize right)
            {
                return comparePhotoSizes(left, right);
            }
        });

        return photoSizeList;
    }

    @Nullable private static PhotoSize choosePhotoSize(int width, int height, @NonNull PhotoSize... photoSizes)
    {
        List<PhotoSize> photoSizeList = chooseBoxIfExistAndSort(photoSizes);

        for (PhotoSize photoSize : photoSizeList)
        {
            if (photoSize.width >= width || photoSize.height >= height)
            {
                return photoSize;
            }
        }

        return !photoSizeList.isEmpty() ? photoSizeList.get(photoSizeList.size() - 1) : null;
    }

    @Nullable private static PhotoSize chooseLoadedPhotoSize(int width, int height, @NonNull PhotoSize... photoSizes)
    {
        List<PhotoSize> photoSizeList = chooseBoxIfExistAndSort(photoSizes);

        for (PhotoSize photoSize : photoSizeList)
        {
            if ((photoSize.width >= width || photoSize.height >= height) && FileLoader.getInstance(photoSize.photo).isLoaded())
            {
                return photoSize;
            }
        }

        Collections.reverse(photoSizeList);

        for (PhotoSize photoSize : photoSizeList)
        {
            if (FileLoader.getInstance(photoSize.photo).isLoaded())
            {
                return photoSize;
            }
        }

        return null;
    }
}