package ru.glushen.telegramchallenge.photo;

import android.app.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.provider.*;
import android.support.annotation.*;
import android.support.v4.app.DialogFragment;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.image.*;
import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramui.Toolbar;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;

import static ru.glushen.ui.SmartImageView.*;

/**
 * Created by Pavel Glushen on 03.07.2015.
 */
public class PhotoDialogFragment extends DialogFragment
{
    private static final String MESSAGE = "message";

    public static PhotoDialogFragment newInstance(@NonNull TdApi.Message message)
    {
        PhotoDialogFragment fragment = new PhotoDialogFragment();

        Bundle args = new Bundle();
        args.putParcelable(MESSAGE, message);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Dialog dialog = new Dialog(getActivity(), R.style.AppTheme);

        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        return dialog;
    }

    @Nullable private Bitmap showedBitmap;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);

        final Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        toolbar.setStyle(Toolbar.Style.TRANSPARENT);

        toolbar.setNavigationButton(R.drawable.ic_back_photo, new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                dismiss();
            }
        });

        toolbar.addMenuButton(R.drawable.ic_more_photo, R.array.photoMenu, new Toolbar.OnSelectListener()
        {
            @Override public void onSelect(int index)
            {
                switch (index)
                {
                    case 0: // Save to gallery
                    {
                        if (showedBitmap != null)
                        {
                            MediaStore.Images.Media.insertImage(toolbar.getContext().getContentResolver(), showedBitmap, "", "");
                            Toast.makeText(toolbar.getContext(), R.string.photoSavedToGallery, Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(toolbar.getContext(), R.string.photoIsNotLoaded, Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                    case 1:  // Delete
                    {
                        TdApi.Message message = getArguments().getParcelable(MESSAGE);

                        if (message == null || !(message.message instanceof TdApi.MessagePhoto))
                        {
                            throw new AssertionError("message.message must be instance of MessagePhoto: message = " + message);
                        }

                        Info.deleteMessages(message.chatId, message.id);
                        dismiss();
                    }
                    break;
                }
            }
        });

        TdApi.Message message = getArguments().getParcelable(MESSAGE);

        if (message == null || !(message.message instanceof TdApi.MessagePhoto))
        {
            throw new AssertionError("message.message must be instance of MessagePhoto: message = " + message);
        }

        TdApi.PhotoSize[] photoSizes = ((TdApi.MessagePhoto) message.message).photo.photos;
        BitmapDrawableCreator[] creators = PhotoBitmapDrawableCreator.newThumbAndPhotoBitmapDrawableCreators(photoSizes, Image.Filter.BOX, Image.NOT_DEFINE, null);

        SmartImageView photoView = (SmartImageView) view.findViewById(R.id.photo);
        photoView.setBitmapDrawableCreators(creators);
        photoView.setChangedListener(new OnBitmapDrawableChangedListener()
        {
            @Override public void onBitmapDrawableChanged(@Nullable BitmapDrawable bitmapDrawable)
            {
                showedBitmap = bitmapDrawable != null ? bitmapDrawable.getBitmap() : null;
            }
        });

        return view;
    }
}
