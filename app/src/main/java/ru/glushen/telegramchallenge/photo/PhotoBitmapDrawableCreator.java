package ru.glushen.telegramchallenge.photo;

import android.content.res.*;
import android.graphics.drawable.*;
import android.support.annotation.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.image.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 05.07.2015.
 */
public class PhotoBitmapDrawableCreator implements SmartImageView.BitmapDrawableCreator
{
    public static PhotoBitmapDrawableCreator[] newThumbAndPhotoBitmapDrawableCreators(TdApi.PhotoSize[] photoSizes, @NonNull Image.Filter filter, int blurRadius, @Nullable ViewGroup.LayoutParams layoutParams)
    {
        PhotoBitmapDrawableCreator[] creators = new PhotoBitmapDrawableCreator[2];

        creators[0] = new PhotoBitmapDrawableCreator(photoSizes, true, filter, blurRadius, layoutParams);
        creators[1] = new PhotoBitmapDrawableCreator(photoSizes, false, filter, blurRadius, layoutParams);

        return creators;
    }

    private final boolean useThumb;
    @NonNull private final Image.Filter filter;
    private final int blurRadius;
    @Nullable private final ViewGroup.LayoutParams layoutParams;
    private final TdApi.PhotoSize[] photoSizes;

    public PhotoBitmapDrawableCreator(TdApi.PhotoSize[] photoSizes, boolean useThumb, @NonNull Image.Filter filter, int blurRadius, @Nullable ViewGroup.LayoutParams layoutParams)
    {
        this.useThumb = useThumb;
        this.filter = filter;
        this.blurRadius = blurRadius;
        this.layoutParams = layoutParams;
        this.photoSizes = photoSizes;
    }

    @Override public ViewGroup.LayoutParams onCreate(int initWidth, int initHeight, CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer, Resources resources)
    {
        PhotoAndThumb photoAndThumb = new PhotoAndThumb(initWidth, initHeight, photoSizes);

        TdApi.PhotoSize photoSize = useThumb ? photoAndThumb.thumb : photoAndThumb.photo;

        if (photoSize != null)
        {
            FileLoader loader = FileLoader.getInstance(photoSize.photo);

            FileBitmapDrawableCreator creator = new FileBitmapDrawableCreator(loader, filter, blurRadius, photoSize.width, photoSize.height, layoutParams);

            return creator.onCreate(initWidth, initHeight, bitmapDrawableObtainer, resources);
        }
        else
        {
            return null;
        }
    }
}
