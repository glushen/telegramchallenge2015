package ru.glushen.telegramchallenge.passcode;

import android.os.*;
import android.support.annotation.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.inputmethod.*;

import static ru.glushen.telegramchallenge.passcode.Passcode.*;

/**
 * Created by Pavel Glushen on 22.07.2015.
 */
public class PasswordFragment extends ChildFragment
{
    @Override public int getCaption()
    {
        switch (getAction())
        {
            case CHOOSE:
                return R.string.passcodePasswordChoose;
            case CONFIRM:
                return R.string.passcodePasswordConfirm;
            case UNLOCK:
                return R.string.passcodePasswordUnlock;
            default:
                return 0;
        }
    }

    @NonNull @Override protected Type getType()
    {
        return Type.PASSWORD;
    }

    private EditText passwordView;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        passwordView = (EditText) inflater.inflate(R.layout.fragment_passcode_password, container, false);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    notifyPasscodeEntered(passwordView.getText().toString());

                    return true;
                }

                return false;
            }
        });

        return passwordView;
    }

    @Override public void onStart()
    {
        super.onResume();

        InputManager.from(getActivity()).showDefaultKeyboard(passwordView);
    }

    @Override public void onStop()
    {
        super.onStop();

        InputManager.from(getActivity()).hideDefaultKeyboard(passwordView);
    }

    @Override protected void onClearInput()
    {
        passwordView.setText("");
    }
}
