package ru.glushen.telegramchallenge.passcode;

import android.gesture.*;
import android.os.*;
import android.support.annotation.*;
import android.view.*;

import java.io.*;

import ru.glushen.telegramchallenge.*;

import static ru.glushen.telegramchallenge.passcode.Passcode.*;

/**
 * Created by Pavel Glushen on 23.07.2015.
 */
public class GestureFragment extends ChildFragment
{
    @Override public int getCaption()
    {
        switch (getAction())
        {
            case CHOOSE:
                return R.string.passcodeGestureChoose;
            case CONFIRM:
                return R.string.passcodeGestureConfirm;
            case UNLOCK:
                return R.string.passcodeGestureUnlock;
            default:
                return 0;
        }
    }

    @NonNull @Override protected Type getType()
    {
        return Type.GESTURE;
    }

    private static final String GESTURE_MAIN_ENTRY_NAME = "mainGesture";
    private static final String GESTURE_TEMP_ENTRY_NAME = "tempGesture";
    private static final String PASSCODE_TRUE = "passcodeTrue";
    private static final String PASSCODE_FALSE = "passcodeFalse";

    @NonNull private File getMainFile()
    {
        return new File(getActivity().getFilesDir(), GestureFragment.class.getCanonicalName());
    }

    private GestureLibrary gestureLibrary;

    @Override public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        File file = new File(getActivity().getFilesDir(), GestureFragment.class.getCanonicalName());
        gestureLibrary = GestureLibraries.fromFile(file);
        gestureLibrary.load();
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        GestureOverlayView view = (GestureOverlayView) inflater.inflate(R.layout.fragment_passcode_gesture, container, false);

        view.addOnGesturePerformedListener(new GestureOverlayView.OnGesturePerformedListener()
        {
            @Override public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture)
            {
                switch (getAction())
                {
                    case CHOOSE:
                    {
                        gestureLibrary.addGesture(GESTURE_TEMP_ENTRY_NAME, gesture);
                        gestureLibrary.save();

                        notifyPasscodeEntered(PASSCODE_TRUE);
                    }
                    break;
                    case CONFIRM:
                    {
                        if (check(GESTURE_TEMP_ENTRY_NAME, gesture))
                        {
                            gestureLibrary.removeEntry(GESTURE_TEMP_ENTRY_NAME);
                            gestureLibrary.removeEntry(GESTURE_MAIN_ENTRY_NAME);
                            gestureLibrary.addGesture(GESTURE_MAIN_ENTRY_NAME, gesture);
                            gestureLibrary.save();

                            notifyPasscodeEntered(PASSCODE_TRUE);
                        }
                        else
                        {
                            notifyPasscodeEntered(PASSCODE_FALSE);
                        }
                    }
                    break;
                    case UNLOCK:
                    {
                        notifyPasscodeEntered(check(GESTURE_MAIN_ENTRY_NAME, gesture) ? PASSCODE_TRUE : PASSCODE_FALSE);
                    }
                    break;
                }
            }
        });

        return view;
    }

    private boolean check(@NonNull String entryName, @NonNull Gesture gesture)
    {
        for (Prediction prediction : gestureLibrary.recognize(gesture))
        {
            if (entryName.equals(prediction.name) && prediction.score > 3)
            {
                return true;
            }
        }

        return false;
    }

    @Override protected void onClearInput()
    {

    }
}
