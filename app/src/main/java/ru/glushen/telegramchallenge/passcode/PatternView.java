package ru.glushen.telegramchallenge.passcode;

import android.annotation.*;
import android.content.*;
import android.graphics.*;
import android.support.annotation.*;
import android.util.*;
import android.view.*;

import java.util.*;

import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static android.view.MotionEvent.*;
import static android.view.View.MeasureSpec.*;
import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 23.07.2015.
 */
@SuppressLint("ViewConstructor") class PatternView extends View implements View.OnTouchListener
{
    @NonNull private final Obtainer<String> patternPerformedObtainer;

    public PatternView(Context context, @NonNull Obtainer<String> patternPerformedObtainer)
    {
        super(context);

        this.patternPerformedObtainer = patternPerformedObtainer;
    }

    private static class PointData
    {
        public final int index;
        public final PointF point = new PointF(0, 0);

        public PointData(int index)
        {
            this.index = index;
        }
    }

    private final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

    private final PointData[] pointArray = new PointData[9];

    {
        for (int i = 0; i < pointArray.length; i++)
        {
            pointArray[i] = new PointData(i);
        }
    }

    private float pointTouchRadius;

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int widthMode = getMode(widthMeasureSpec);
        int width = getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();

        int heightMode = getMode(heightMeasureSpec);
        int height = getSize(heightMeasureSpec) - getPaddingTop() - getPaddingBottom();

        int size = min(width, height);

        if (widthMode == UNSPECIFIED && heightMode == UNSPECIFIED)
        {
            width = height = Screen.dpPixelSize(250, displayMetrics);
        }
        else if (widthMode == UNSPECIFIED)
        {
            //noinspection SuspiciousNameCombination
            width = height;
        }
        else if (heightMode == UNSPECIFIED)
        {
            //noinspection SuspiciousNameCombination
            height = width;
        }
        else if (widthMode == AT_MOST && heightMode == AT_MOST)
        {
            width = height = size;
        }
        else if (widthMode == AT_MOST)
        {
            width = size;
        }
        else if (heightMode == AT_MOST)
        {
            height = size;
        }

        setMeasuredDimension(width, height);

        size = min(width, height);

        for (int i = 0; i < pointArray.length; i++)
        {
            pointArray[i].point.x = size / 3f * (i % 3 + .5f);
            pointArray[i].point.y = size / 3f * (i / 3 + .5f);
        }

        pointTouchRadius = size / 6f;
    }

    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    {
        paint.setColor(0xffffffff);
        paint.setStrokeWidth(Screen.dpPixelSize(4, displayMetrics));
    }

    private final float notElectedPointRadius = Screen.dpPixelSize(6, displayMetrics);
    private final float electedPointRadius = Screen.dpPixelSize(8, displayMetrics);

    @Override protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        for (PointData pointData : notElectedPointList)
        {
            canvas.drawCircle(pointData.point.x, pointData.point.y, notElectedPointRadius, paint);
        }

        for (int i = 0; i < electedPointList.size(); i++)
        {
            PointF currentPoint = electedPointList.get(i).point;
            PointF nextPoint = i + 1 < electedPointList.size() ? electedPointList.get(i + 1).point : currentTouchCoordinates;

            canvas.drawCircle(currentPoint.x, currentPoint.y, electedPointRadius, paint);

            canvas.drawLine(currentPoint.x, currentPoint.y, nextPoint.x, nextPoint.y, paint);
        }
    }

    {
        setOnTouchListener(this);
    }

    private final List<PointData> notElectedPointList = new ArrayList<>();
    private final List<PointData> electedPointList = new ArrayList<>();

    {
        Collections.addAll(notElectedPointList, pointArray);
    }

    private final PointF currentTouchCoordinates = new PointF(0, 0);

    @Override public boolean onTouch(View view, MotionEvent event)
    {
        switch (event.getAction())
        {
            case ACTION_MOVE:
            {
                currentTouchCoordinates.x = event.getX();
                currentTouchCoordinates.y = event.getY();

                for (int i = 0; i < notElectedPointList.size(); i++)
                {
                    float deltaX = currentTouchCoordinates.x - notElectedPointList.get(i).point.x;
                    float deltaY = currentTouchCoordinates.y - notElectedPointList.get(i).point.y;

                    if (sqrt(deltaX * deltaX + deltaY * deltaY) <= pointTouchRadius)
                    {
                        electedPointList.add(notElectedPointList.remove(i));
                        break;
                    }
                }

                invalidate();
            }
            break;

            case ACTION_UP:
            case ACTION_CANCEL:
            {
                if (electedPointList.size() >= 4)
                {
                    String pass = "";

                    for (PointData pointData : electedPointList)
                    {
                        pass += pointData.index + ";";
                    }

                    patternPerformedObtainer.obtain(pass);
                }

                Collections.addAll(notElectedPointList, pointArray);
                electedPointList.clear();

                invalidate();
            }
            break;
        }

        return true;
    }
}
