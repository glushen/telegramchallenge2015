package ru.glushen.telegramchallenge.passcode;

import android.os.*;
import android.support.annotation.*;
import android.view.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.util.obtainer.*;

import static ru.glushen.telegramchallenge.passcode.Passcode.*;

/**
 * Created by Pavel Glushen on 23.07.2015.
 */
public class PatternFragment extends ChildFragment
{
    @Override public int getCaption()
    {
        switch (getAction())
        {
            case CHOOSE:
                return R.string.passcodePatternChoose;
            case CONFIRM:
                return R.string.passcodePatternConfirm;
            case UNLOCK:
                return R.string.passcodePatternUnlock;
            default:
                return 0;
        }
    }

    @NonNull @Override protected Type getType()
    {
        return Type.PATTERN;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return new PatternView(getActivity(), new Obtainer<String>()
        {
            @Override public void obtain(String passcode)
            {
                notifyPasscodeEntered(passcode);
            }
        });
    }

    @Override protected void onClearInput()
    {

    }
}
