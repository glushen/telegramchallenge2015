package ru.glushen.telegramchallenge.passcode;

import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.view.*;
import android.widget.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.telegramui.Toolbar;

import static ru.glushen.telegramchallenge.passcode.ChildFragment.*;
import static ru.glushen.telegramchallenge.passcode.Passcode.*;

/**
 * Created by Pavel Glushen on 24.07.2015.
 */
public class ChooseFragment extends Fragment
{
    public static ChooseFragment newInstance()
    {
        return new ChooseFragment();
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
        {
            setFragment(ChildFragment.newInstance(getLastLockType(getActivity()), Action.CHOOSE));
        }
    }

    private void setFragment(ChildFragment childFragment)
    {
        setCaption(childFragment.getCaption());

        getChildFragmentManager().beginTransaction().replace(R.id.container, childFragment).commit();
    }

    @StringRes private int captionStringRes;
    @Nullable private TextView captionTextView = null;

    void setCaption(@StringRes int captionStringRes)
    {
        this.captionStringRes = captionStringRes;

        if (captionTextView != null)
        {
            if (captionStringRes != 0)
            {
                captionTextView.setText(captionStringRes);
            }
            else
            {
                captionTextView.setText("");
            }
        }
    }

    public void close()
    {
        getFragmentManager().popBackStack();
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_passcode_choose, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        toolbar.setNavigationButton(R.drawable.ic_back, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                getFragmentManager().popBackStack();
            }
        });

        toolbar.setSpinner(Type.TYPE_NAME_ARRAY_RES, new Toolbar.OnSelectListener()
        {
            @Override public void onSelect(int index)
            {
                setFragment(ChildFragment.newInstance(Type.values()[index], Action.CHOOSE));
            }
        });

        toolbar.setCurrentSpinnerItem(getLastLockType(getActivity()).ordinal(), false);

        captionTextView = ((TextView) view.findViewById(R.id.caption));
        setCaption(captionStringRes);

        return view;
    }
}
