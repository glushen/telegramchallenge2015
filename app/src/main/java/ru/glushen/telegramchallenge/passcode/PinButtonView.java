package ru.glushen.telegramchallenge.passcode;

import android.content.*;
import android.content.res.*;
import android.support.annotation.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.util.*;

/**
 * Created by Pavel Glushen on 22.07.2015.
 */
public class PinButtonView extends FrameLayout
{
    public PinButtonView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PinButtonView, 0, 0);

        digit = a.hasValue(R.styleable.PinButtonView_digit) ? a.getString(R.styleable.PinButtonView_digit) : "";
        String letters = a.hasValue(R.styleable.PinButtonView_letters) ? a.getString(R.styleable.PinButtonView_letters) : "";
        a.recycle();

        TextView digitTextView = new TextView(context);
        digitTextView.setText(digit);
        digitTextView.setTextColor(0xffffffff);
        digitTextView.setTextSize(36);
        LinearLayout.LayoutParams digitTextViewParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        digitTextViewParams.gravity = Gravity.CENTER;
        digitTextView.setLayoutParams(digitTextViewParams);

        TextView lettersTextView = new TextView(context);
        lettersTextView.setText(letters);
        lettersTextView.setTextColor(0xffd2eafc);
        lettersTextView.setTextSize(12);
        LinearLayout.LayoutParams lettersTextViewParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lettersTextViewParams.gravity = Gravity.CENTER;
        lettersTextViewParams.topMargin = Screen.dpPixelSize(-6, getResources().getDisplayMetrics());
        lettersTextView.setLayoutParams(lettersTextViewParams);

        FrameLayout.LayoutParams linearLayoutParams = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER);

        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(digitTextView);
        linearLayout.addView(lettersTextView);
        linearLayout.setLayoutParams(linearLayoutParams);

        setBackgroundResource(R.drawable.pressable_background_primary);
        addView(linearLayout);
    }

    @NonNull private final String digit;

    @NonNull public String getDigit()
    {
        return digit;
    }
}
