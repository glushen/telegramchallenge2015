package ru.glushen.telegramchallenge.passcode;

import android.os.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import ru.glushen.telegramchallenge.*;

import static ru.glushen.telegramchallenge.passcode.Passcode.*;

/**
 * Created by Pavel Glushen on 22.07.2015.
 */
public class PinFragment extends ChildFragment implements View.OnClickListener
{
    @Override public int getCaption()
    {
        switch (getAction())
        {
            case CHOOSE:
                return R.string.passcodePinChoose;
            case CONFIRM:
                return R.string.passcodePinConfirm;
            case UNLOCK:
                return R.string.passcodePinUnlock;
            default:
                return 0;
        }
    }

    @NonNull @Override protected Type getType()
    {
        return Type.PIN;
    }

    private TextView passwordView;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_passcode_pin, container, false);

        passwordView = (TextView) view.findViewById(R.id.password);

        view.findViewById(R.id.digit1).setOnClickListener(this);
        view.findViewById(R.id.digit2).setOnClickListener(this);
        view.findViewById(R.id.digit3).setOnClickListener(this);
        view.findViewById(R.id.digit4).setOnClickListener(this);
        view.findViewById(R.id.digit5).setOnClickListener(this);
        view.findViewById(R.id.digit6).setOnClickListener(this);
        view.findViewById(R.id.digit7).setOnClickListener(this);
        view.findViewById(R.id.digit8).setOnClickListener(this);
        view.findViewById(R.id.digit9).setOnClickListener(this);
        view.findViewById(R.id.digit0).setOnClickListener(this);
        view.findViewById(R.id.backspace).setOnClickListener(this);

        return view;
    }

    @Override public void onClick(View view)
    {
        if (view instanceof PinButtonView)
        {
            String passcode = passwordView.getText().toString() + ((PinButtonView) view).getDigit();

            passwordView.setText(passcode);

            if (passcode.length() == 4)
            {
                notifyPasscodeEntered(passcode);
            }
        }
        else if (view.getId() == R.id.backspace)
        {
            String password = passwordView.getText().toString();

            if (password.length() > 0)
            {
                passwordView.setText(password.substring(0, password.length() - 1));
            }
        }
    }

    @Override protected void onClearInput()
    {
        passwordView.setText("");
    }
}
