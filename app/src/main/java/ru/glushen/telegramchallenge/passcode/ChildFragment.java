package ru.glushen.telegramchallenge.passcode;

import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.*;

import static ru.glushen.telegramchallenge.passcode.Passcode.*;

/**
 * Created by Pavel Glushen on 22.07.2015.
 */
abstract class ChildFragment extends Fragment
{
    public static ChildFragment newInstance(@NonNull Type type, @NonNull Action action)
    {
        ChildFragment fragment = type.createChildFragment();
        fragment.setAction(action);
        return fragment;
    }

    public enum Action
    {
        CHOOSE, CONFIRM, UNLOCK
    }

    @Nullable private Action action;

    @NonNull public Action getAction()
    {
        if (action != null)
        {
            return action;
        }
        else
        {
            throw new AssertionError("Action is not set");
        }
    }

    public void setAction(@NonNull Action action)
    {
        this.action = action;
    }

    @StringRes public abstract int getCaption();

    @NonNull protected abstract Type getType();

    private static final String ACTION = ChildFragment.class.getCanonicalName() + "::Action";
    private static final String CHOSEN_PASSCODE = ChildFragment.class.getCanonicalName() + "::ChosenPasscode";

    @Override @CallSuper public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
        {
            if (savedInstanceState.containsKey(ACTION))
            {
                action = Action.values()[savedInstanceState.getInt(ACTION)];
            }

            chosenPasscode = savedInstanceState.getString(CHOSEN_PASSCODE);
        }
    }

    @Override @CallSuper public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        if (action != null)
        {
            outState.putInt(ACTION, action.ordinal());
        }

        outState.putString(CHOSEN_PASSCODE, chosenPasscode);
    }

    private String chosenPasscode;

    protected void notifyPasscodeEntered(@NonNull String passcode)
    {
        switch (getAction())
        {
            case CHOOSE:
            {
                chosenPasscode = passcode;
                onClearInput();

                setAction(Action.CONFIRM);
                ((ChooseFragment) getParentFragment()).setCaption(getCaption());
            }
            break;
            case CONFIRM:
            {
                if (passcode.equals(chosenPasscode))
                {
                    Passcode.lock(getActivity(), getType(), passcode);
                    ((ChooseFragment) getParentFragment()).close();
                }
                else
                {
                    onClearInput();

                    setAction(Action.CHOOSE);
                    ((ChooseFragment) getParentFragment()).setCaption(getCaption());
                }
            }
            break;
            case UNLOCK:
            {
                if (passcode.equals(Passcode.getLockPasscode(getActivity())))
                {
                    ((UnlockDialogFragment) getParentFragment()).close();
                }
                else
                {
                    onClearInput();
                }
            }
            break;
        }
    }

    protected abstract void onClearInput();
}
