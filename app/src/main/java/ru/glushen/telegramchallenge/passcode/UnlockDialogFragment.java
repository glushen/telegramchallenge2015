package ru.glushen.telegramchallenge.passcode;

import android.app.*;
import android.content.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.*;
import android.widget.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.util.*;

import static ru.glushen.telegramchallenge.passcode.ChildFragment.*;
import static ru.glushen.telegramchallenge.passcode.Passcode.*;

/**
 * Created by Pavel Glushen on 22.07.2015.
 */
public class UnlockDialogFragment extends DialogFragment
{
    public static final EventDispatcher<Void> UNLOCK_DISPATCHER = new EventDispatcher<>();

    private static final String TAG = UnlockDialogFragment.class.getCanonicalName();

    public static void showDialog(FragmentManager fragmentManager)
    {
        if (fragmentManager.findFragmentByTag(TAG) == null)
        {
            newInstance().show(fragmentManager, TAG);
        }
    }

    public static UnlockDialogFragment newInstance()
    {
        return new UnlockDialogFragment();
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Passcode.setLockedByUser(getActivity(), true);

        ChildFragment childFragment;

        if (savedInstanceState == null)
        {
            Passcode.Type type = getLockType(getActivity());

            if (type != null)
            {
                childFragment = ChildFragment.newInstance(type, Action.UNLOCK);
                getChildFragmentManager().beginTransaction().replace(R.id.container, childFragment).commit();
            }
            else
            {
                throw new AssertionError();
            }
        }
        else
        {
            childFragment = (ChildFragment) getChildFragmentManager().findFragmentById(R.id.container);
        }

        captionStringRes = childFragment.getCaption();
    }

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Dialog dialog = new Dialog(getActivity(), R.style.AppTheme);

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener()
        {
            @Override public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
            {
                if (keyCode == KeyEvent.KEYCODE_BACK)
                {
                    startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));

                    return true;
                }

                return false;
            }
        });

        Passcode.Type type = getLockType(getActivity());

        if (type != null && !type.useInputMethod())
        {
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        }

        setCancelable(false);

        return dialog;
    }

    @StringRes private int captionStringRes;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_passcode_unlock, container, false);

        ((TextView) view.findViewById(R.id.caption)).setText(captionStringRes);

        return view;
    }

    public void close()
    {
        Passcode.setLockedByUser(getActivity(), false);

        dismiss();

        UNLOCK_DISPATCHER.dispatch(null);
    }
}
