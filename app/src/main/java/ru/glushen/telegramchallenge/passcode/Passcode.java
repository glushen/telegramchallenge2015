package ru.glushen.telegramchallenge.passcode;

import android.content.*;
import android.support.annotation.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.util.*;

/**
 * Created by Pavel Glushen on 29.07.2015.
 */
public class Passcode
{
    public enum Type
    {
        PIN
            {
                @Override public int getStringRes()
                {
                    return R.string.passcodePin;
                }

                @Override ChildFragment createChildFragment()
                {
                    return new PinFragment();
                }

                @Override boolean useInputMethod()
                {
                    return false;
                }
            },
        PASSWORD
            {
                @Override public int getStringRes()
                {
                    return R.string.passcodePassword;
                }

                @Override ChildFragment createChildFragment()
                {
                    return new PasswordFragment();
                }

                @Override boolean useInputMethod()
                {
                    return true;
                }
            },
        PATTERN
            {
                @Override public int getStringRes()
                {
                    return R.string.passcodePattern;
                }

                @Override ChildFragment createChildFragment()
                {
                    return new PatternFragment();
                }

                @Override boolean useInputMethod()
                {
                    return false;
                }
            },
        GESTURE
            {
                @Override public int getStringRes()
                {
                    return R.string.passcodeGesture;
                }

                @Override ChildFragment createChildFragment()
                {
                    return new GestureFragment();
                }

                @Override boolean useInputMethod()
                {
                    return false;
                }
            };

        @StringRes public abstract int getStringRes();

        abstract ChildFragment createChildFragment();

        abstract boolean useInputMethod();

        @ArrayRes public static final int TYPE_NAME_ARRAY_RES = R.array.passcodeTypeArray;
    }

    public enum AutoLockDuration
    {
        IN_1_MINUTE
            {
                @Override public int getStringRes()
                {
                    return R.string.settingsPasscodeAutoLockIn1Minute;
                }

                @Override long getDurationInMillis()
                {
                    return 60 * 1000;
                }
            },
        IN_5_MINUTES
            {
                @Override public int getStringRes()
                {
                    return R.string.settingsPasscodeAutoLockIn5Minutes;
                }

                @Override long getDurationInMillis()
                {
                    return 5 * 60 * 1000;
                }
            },
        IN_1_HOUR
            {
                @Override public int getStringRes()
                {
                    return R.string.settingsPasscodeAutoLockIn1Hour;
                }

                @Override long getDurationInMillis()
                {
                    return 60 * 60 * 1000;
                }
            },
        IN_5_HOURS
            {
                @Override public int getStringRes()
                {
                    return R.string.settingsPasscodeAutoLockIn5Hours;
                }

                @Override long getDurationInMillis()
                {
                    return 5 * 60 * 60 * 1000;
                }
            },
        DISABLED
            {
                @Override public int getStringRes()
                {
                    return R.string.settingsPasscodeAutoLockDisabled;
                }

                @Override long getDurationInMillis()
                {
                    return Long.MAX_VALUE / 2;
                }
            };

        public @StringRes abstract int getStringRes();

        abstract long getDurationInMillis();
    }

    private static SharedPreferences getPreferences(@NonNull Context context)
    {
        return context.getSharedPreferences(Passcode.class.getCanonicalName(), Context.MODE_PRIVATE);
    }

    private static final String LOCK_ENABLED = "lockEnabled";
    private static final String LOCK_TYPE = "lockType";
    private static final String LAST_LOCK_TYPE = "lastLockType";
    private static final String PASSCODE = "passcode";
    private static final String AUTO_LOCK_DURATION = "autoLockDuration";
    private static final String APP_STOPPED_TIME = "appStoppedTime";
    private static final String LOCKED_BY_USER = "lockedByUser";

    public static boolean isLockEnabled(@NonNull Context context)
    {
        return getPreferences(context).getBoolean(LOCK_ENABLED, false);
    }

    public static boolean isLockedNow(@NonNull Context context)
    {
        SharedPreferences preferences = getPreferences(context);

        if (!preferences.getBoolean(LOCK_ENABLED, false))
        {
            return false;
        }
        else if (preferences.getBoolean(LOCKED_BY_USER, false))
        {
            return true;
        }
        else
        {
            long appStoppedTime = preferences.getLong(APP_STOPPED_TIME, 0);
            long autoLockDuration = getAutoLockDuration(context).getDurationInMillis();
            return appStoppedTime + autoLockDuration < System.currentTimeMillis();
        }
    }

    public static final EventDispatcher<Void> LOCK_DISPATCHER = new EventDispatcher<>();

    static void lock(@NonNull Context context, @NonNull Type type, @NonNull String passcode)
    {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(LOCK_ENABLED, true);
        editor.putString(LOCK_TYPE, type.name());
        editor.putString(LAST_LOCK_TYPE, type.name());
        editor.putString(PASSCODE, passcode);
        editor.apply();

        LOCK_DISPATCHER.dispatch(null);
    }

    public static void unlock(@NonNull Context context)
    {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.remove(LOCK_ENABLED);
        editor.remove(LOCK_TYPE);
        editor.remove(PASSCODE);
        editor.apply();

        LOCK_DISPATCHER.dispatch(null);
    }

    @Nullable public static Type getLockType(@NonNull Context context)
    {
        String typeName = getPreferences(context).getString(LOCK_TYPE, null);
        return typeName != null ? Type.valueOf(typeName) : null;
    }

    @NonNull public static Type getLastLockType(@NonNull Context context)
    {
        return Type.valueOf(getPreferences(context).getString(LAST_LOCK_TYPE, Type.PIN.name()));
    }

    @Nullable static String getLockPasscode(@NonNull Context context)
    {
        return getPreferences(context).getString(PASSCODE, null);
    }

    @NonNull public static AutoLockDuration getAutoLockDuration(@NonNull Context context)
    {
        return AutoLockDuration.valueOf(getPreferences(context).getString(AUTO_LOCK_DURATION, AutoLockDuration.IN_1_HOUR.name()));
    }

    public static void setAutoLockDuration(@NonNull Context context, @NonNull AutoLockDuration autoLockDuration)
    {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(AUTO_LOCK_DURATION, autoLockDuration.name());
        editor.apply();

        LOCK_DISPATCHER.dispatch(null);
    }

    public static boolean isLockedByUser(@NonNull Context context)
    {
        return getPreferences(context).getBoolean(LOCKED_BY_USER, false);
    }

    public static void setLockedByUser(@NonNull Context context, boolean lockedByUser)
    {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(LOCKED_BY_USER, lockedByUser);
        editor.apply();

        LOCK_DISPATCHER.dispatch(null);
    }

    public static void notifyAppStopped(@NonNull Context context)
    {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putLong(APP_STOPPED_TIME, System.currentTimeMillis());
        editor.apply();

        LOCK_DISPATCHER.dispatch(null);
    }

    private Passcode()
    {

    }
}
