package ru.glushen.telegramchallenge.music;

import android.graphics.drawable.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.image.*;
import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.media.*;
import ru.glushen.telegramchallenge.telegramui.*;
import ru.glushen.telegramchallenge.telegramui.SeekBar;
import ru.glushen.telegramchallenge.telegramui.Toolbar;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;

import static ru.glushen.telegramchallenge.music.AudioPlayerService.*;
import static ru.glushen.ui.SmartImageView.*;

/**
 * Created by Pavel Glushen on 17.08.2015.
 */
public class AudioPlayerFragment extends Fragment
{
    public static AudioPlayerFragment newInstance()
    {
        return new AudioPlayerFragment();
    }

    private final Obtainer<TdApi.Audio> audioObtainer = new Obtainer<TdApi.Audio>()
    {
        @Override public void obtain(@Nullable TdApi.Audio audio)
        {
            if (audio != null)
            {
                titleView.setText(audio.title);
                subtitleView.setText(audio.performer);

                BitmapDrawableCreator[] creators = new BitmapDrawableCreator[2];
                creators[0] = new FileBitmapDrawableCreator(FileLoader.getInstance(audio.albumCoverThumb.photo), Image.Filter.BOX, 0);
                creators[1] = new AlbumCoverBitmapDrawableCreator(audio);
                albumCoverView.setBitmapDrawableCreators(creators);

                FileLoader.getInstance(audio.audio).getStateDispatcher().addObtainer(fileLoaderStateObtainerManager.replace(), false);
            }
        }
    };

    private final CloseableObtainerManager<FileLoader> fileLoaderStateObtainerManager = new CloseableObtainerManager<FileLoader>()
    {
        @Override protected CloseableObtainer<FileLoader> onCreate()
        {
            return new CloseableObtainer<FileLoader>()
            {
                @Override protected void onObtain(FileLoader fileLoader)
                {
                    updateView(fileLoader, null);
                }
            };
        }
    };

    private final Obtainer<State> stateObtainer = new Obtainer<State>()
    {
        @Override public void obtain(@NonNull State state)
        {
            updateView(null, state);
        }
    };

    private void updateView(@Nullable FileLoader fileLoader, @Nullable State state)
    {
        TdApi.Audio audio = AUDIO_DISPATCHER.getCurrentState();

        if (fileLoader == null)
        {
            fileLoader = FileLoader.getInstance(audio.audio);
        }

        if (state == null)
        {
            state = AudioPlayerService.STATE_DISPATCHER.getCurrentState();
        }

        if (!fileLoader.isLoaded())
        {
            boolean loading = fileLoader.isLoading();

            stateView.setImageResource(loading ? R.drawable.ic_playerpause_blue : R.drawable.ic_player_download_blue);

            downloadProgressView.setBackgroundCircleColor(0xfff0f6fa);
            downloadProgressView.setEnabled(loading);

            offsetSeekBarView.setEnabled(false);
            offsetSeekBarView.setProgress(0, false);
        }
        else
        {
            stateView.setImageResource(state == State.PLAYING ? R.drawable.ic_playerpause : R.drawable.ic_playerplay_wrap);

            downloadProgressView.setBackgroundCircleColor(0xff68ade1);
            downloadProgressView.setEnabled(false);

            offsetSeekBarView.setEnabled(true);
        }

        progressHandlerRunnable.run();
    }

    private final Handler handler = new Handler();

    private final Runnable progressHandlerRunnable = new Runnable()
    {
        @Override public void run()
        {
            if (offsetSeekBarView != null)
            {
                FileLoader fileLoader = FileLoader.getInstance(AUDIO_DISPATCHER.getCurrentState().audio);

                downloadProgressView.setMax(fileLoader.getSize());
                downloadProgressView.setProgress(fileLoader.getReady());

                offsetSeekBarView.setMax(getDuration(), false);
                offsetSeekBarView.setProgress(getOffset(), false);

                if (!offsetSeekBarView.isTouching())
                {
                    durationView.setText(DateAndTime.getTimerTime(getDuration()));
                    offsetView.setText(DateAndTime.getTimerTime(getOffset()));
                }

                if (STATE_DISPATCHER.getCurrentState() == State.PLAYING || fileLoader.isLoading())
                {
                    handler.postDelayed(this, 15);
                }
            }
        }
    };

    private Toolbar toolbarView;
    private ImageView backButtonView;
    private ImageView playlistButtonView;
    private ImageView overflowButtonView;

    private View albumCoverBackgroundView;
    private SmartImageView albumCoverView;
    private ImageView repeatButton;
    private ImageView shuffleButton;
    private SeekBar offsetSeekBarView;
    private TextView offsetView;
    private TextView durationView;
    private TextView titleView;
    private TextView subtitleView;
    private View previousButton;
    private View nextButton;
    private DeterminateProgressBar downloadProgressView;
    private ImageView stateView;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_audio_player, container, false);

        toolbarView = (Toolbar) view.findViewById(R.id.toolbar);
        toolbarView.setStyle(Toolbar.Style.TRANSPARENT);
        backButtonView = toolbarView.setNavigationButton(R.drawable.ic_back_grey, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                getFragmentManager().popBackStack();
            }
        });
        playlistButtonView = toolbarView.addMenuButton(R.drawable.ic_playlist_grey, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                TdApi.Message message = AudioPlayerService.MESSAGE_AUDIO_DISPATCHER.getCurrentState();

                if (message != null)
                {
                    getFragmentManager().beginTransaction().replace(getId(), MediaFragment.newInstance(message.chatId, MediaFragment.TAB_AUDIO)).addToBackStack(null).commit();
                }
            }
        });
        overflowButtonView = toolbarView.addMenuButton(R.drawable.ic_more_gray, new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                // todo open menu
            }
        });

        repeatButton = (ImageView) view.findViewById(R.id.repeatButton);
        repeatButton.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View view)
            {
                PlayOrderManager.toggleRepeat(view.getContext());
                Drawable drawable = albumCoverView.getDrawable();
                BitmapDrawable bitmapDrawable = drawable instanceof BitmapDrawable ? ((BitmapDrawable) drawable) : null;
                bitmapDrawableChangedListener.onBitmapDrawableChanged(bitmapDrawable);
            }
        });
        shuffleButton = (ImageView) view.findViewById(R.id.shuffleButton);
        shuffleButton.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View view)
            {
                PlayOrderManager.toggleShuffle(view.getContext());
                Drawable drawable = albumCoverView.getDrawable();
                BitmapDrawable bitmapDrawable = drawable instanceof BitmapDrawable ? ((BitmapDrawable) drawable) : null;
                bitmapDrawableChangedListener.onBitmapDrawableChanged(bitmapDrawable);
            }
        });

        albumCoverBackgroundView = view.findViewById(R.id.albumCoverBackground);
        albumCoverView = (SmartImageView) view.findViewById(R.id.albumCover);
        albumCoverView.setChangedListener(bitmapDrawableChangedListener);
        bitmapDrawableChangedListener.onBitmapDrawableChanged(null);

        offsetSeekBarView = (SeekBar) view.findViewById(R.id.offsetSeekBar);
        offsetSeekBarView.setStyle(SeekBar.Style.MUSIC);
        offsetSeekBarView.setListener(new SeekBar.OnSeekListener()
        {
            @Override public void onProgress(int progress, int max)
            {
                durationView.setText(DateAndTime.getTimerTime(max));
                offsetView.setText(DateAndTime.getTimerTime(progress));
            }

            @Override public void onProgressChanged(int progress, int max, boolean byUser)
            {
                if (byUser)
                {
                    setOffset(progress);
                }
            }
        });

        offsetView = (TextView) view.findViewById(R.id.offset);
        durationView = (TextView) view.findViewById(R.id.duration);

        titleView = (TextView) view.findViewById(R.id.title);
        titleView.setTypeface(Font.getRobotoMedium(getResources()));

        subtitleView = (TextView) view.findViewById(R.id.subtitle);

        previousButton = view.findViewById(R.id.previousButton);
        previousButton.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                AudioPlayerService.previous(getContext());
            }
        });

        nextButton = view.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new OnClickListener()
        {
            @Override public void onClick(View v)
            {
                AudioPlayerService.next(getContext());
            }
        });

        downloadProgressView = (DeterminateProgressBar) view.findViewById(R.id.downloadProgress);

        stateView = (ImageView) view.findViewById(R.id.state);
        stateView.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                FileLoader fileLoader = FileLoader.getInstance(AUDIO_DISPATCHER.getCurrentState().audio);

                if (!fileLoader.isLoaded())
                {
                    if (!fileLoader.isLoading())
                    {
                        fileLoader.load();
                    }
                    else
                    {
                        fileLoader.cancelLoad();
                    }
                }
                else if (STATE_DISPATCHER.getCurrentState() == State.PLAYING)
                {
                    pause();
                }
                else
                {
                    resume();
                }
            }
        });

        AUDIO_DISPATCHER.addObtainerAndObtain(audioObtainer, false);
        STATE_DISPATCHER.addObtainerAndObtain(stateObtainer, false);

        return view;
    }

    private final OnBitmapDrawableChangedListener bitmapDrawableChangedListener = new OnBitmapDrawableChangedListener()
    {
        @Override public void onBitmapDrawableChanged(@Nullable BitmapDrawable bitmapDrawable)
        {
            backButtonView.setImageResource(bitmapDrawable == null ? R.drawable.ic_back_grey : R.drawable.ic_back);
            playlistButtonView.setImageResource(bitmapDrawable == null ? R.drawable.ic_playlist_grey : R.drawable.ic_playlist_white);
            overflowButtonView.setImageResource(bitmapDrawable == null ? R.drawable.ic_more_gray : R.drawable.ic_more);

            int order = PlayOrderManager.getOrder(repeatButton.getContext());

            if (order == PlayOrderManager.REPEAT)
            {
                repeatButton.setImageResource(R.drawable.ic_repeat_blue);
            }
            else
            {
                repeatButton.setImageResource(bitmapDrawable == null ? R.drawable.ic_repeat_grey : R.drawable.ic_repeat_white);
            }

            if (order == PlayOrderManager.SHUFFLE)
            {
                shuffleButton.setImageResource(R.drawable.ic_shuffle_blue);
            }
            else
            {
                shuffleButton.setImageResource(bitmapDrawable == null ? R.drawable.ic_shuffle_grey : R.drawable.ic_shuffle_white);
            }

            albumCoverBackgroundView.setBackgroundColor(bitmapDrawable == null ? 0xfff4f4f4 : 0xff000000);
        }
    };

    @Override public void onDestroyView()
    {
        super.onDestroyView();

        AUDIO_DISPATCHER.removeObtainer(audioObtainer);
        STATE_DISPATCHER.removeObtainer(stateObtainer);
        fileLoaderStateObtainerManager.close();
        handler.removeCallbacks(progressHandlerRunnable);
    }
}
