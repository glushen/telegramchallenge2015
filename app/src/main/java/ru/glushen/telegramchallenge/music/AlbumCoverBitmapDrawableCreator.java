package ru.glushen.telegramchallenge.music;

import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.media.*;
import android.os.*;
import android.support.annotation.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.image.Image;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 17.08.2015.
 */
class AlbumCoverBitmapDrawableCreator implements SmartImageView.BitmapDrawableCreator
{
    private final TdApi.Audio audio;

    public AlbumCoverBitmapDrawableCreator(@NonNull TdApi.Audio audio)
    {
        this.audio = audio;
    }

    @Override public ViewGroup.LayoutParams onCreate(final int initWidth, final int initHeight, final CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer, final Resources resources)
    {
        if (Build.VERSION.SDK_INT < 10)
        {
            return null;
        }

        final Image.SizeParams.Creator sizeParamsCreator = new Image.SizeParams.Creator(initWidth, initHeight, Image.Filter.BOX);

        FileLoader.getInstance(audio.audio).getPath(new Obtainer<String>()
        {
            @Override public void obtain(final String path)
            {
                if (bitmapDrawableObtainer.hasClosed())
                {
                    return;
                }

                final String cacheKey = AlbumCoverBitmapDrawableCreator.class.getName() + "::" + path + "," + initWidth + "," + initHeight;

                BitmapDrawable cachedBitmapDrawable = Image.bitmapDrawableCache.get(cacheKey);

                if (cachedBitmapDrawable != null)
                {
                    bitmapDrawableObtainer.obtain(cachedBitmapDrawable);
                }
                else if (path != null)
                {
                    new AsyncTask<Void, Void, BitmapDrawable>()
                    {
                        @Override protected BitmapDrawable doInBackground(Void... params)
                        {
                            if (Build.VERSION.SDK_INT >= 10)
                            {
                                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                                retriever.setDataSource(path);

                                if (bitmapDrawableObtainer.hasClosed())
                                {
                                    return null;
                                }

                                byte[] pictureData = retriever.getEmbeddedPicture();

                                if (bitmapDrawableObtainer.hasClosed())
                                {
                                    return null;
                                }

                                if (pictureData != null)
                                {
                                    Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);

                                    if (bitmapDrawableObtainer.hasClosed())
                                    {
                                        return null;
                                    }

                                    //noinspection all
                                    pictureData = null;

                                    if (bitmap != null)
                                    {
                                        Image.SizeParams sizeParams = sizeParamsCreator.create(bitmap.getWidth(), bitmap.getHeight());
                                        bitmap = Bitmap.createScaledBitmap(bitmap, sizeParams.width, sizeParams.height, true);
                                        return Image.createBitmapDrawable(bitmap, resources);
                                    }
                                }
                            }

                            return null;
                        }

                        @Override protected void onPostExecute(@Nullable BitmapDrawable bitmapDrawable)
                        {
                            if (bitmapDrawable != null)
                            {
                                Image.bitmapDrawableCache.put(cacheKey, bitmapDrawable);
                            }

                            bitmapDrawableObtainer.obtain(bitmapDrawable);
                        }
                    }.execute();
                }
            }
        });

        return null;
    }
}
