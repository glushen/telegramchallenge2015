package ru.glushen.telegramchallenge.music;

import android.app.*;
import android.content.*;
import android.media.*;
import android.os.*;
import android.support.annotation.*;

import org.drinkless.td.libcore.telegram.*;

import java.io.*;
import java.util.*;

import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static java.lang.Math.*;

public class AudioPlayerService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener
{
    public enum State
    {
        PLAYING, PAUSED, STOPPED
    }

    public static final StateDispatcher<TdApi.Message> MESSAGE_AUDIO_DISPATCHER = new StateDispatcher<>(null, false);
    public static final StateDispatcher<TdApi.Audio> AUDIO_DISPATCHER = new StateDispatcher<>(null, false);
    public static final StateDispatcher<State> STATE_DISPATCHER = new StateDispatcher<>(State.STOPPED, true);

    public static void play(@NonNull Context context, @NonNull TdApi.Message message)
    {
        if (message.message instanceof TdApi.MessageAudio)
        {
            TdApi.Message oldMessage = MESSAGE_AUDIO_DISPATCHER.getCurrentState();

            if (oldMessage == null || oldMessage.chatId != message.chatId || oldMessage.id != message.id)
            {
                Info.cacheAudioInChatList(message.chatId);
            }

            MESSAGE_AUDIO_DISPATCHER.dispatch(message);
            AUDIO_DISPATCHER.dispatch(((TdApi.MessageAudio) message.message).audio);
            STATE_DISPATCHER.dispatch(State.PAUSED);

            Intent intent = new Intent(context, AudioPlayerService.class);
            context.startService(intent);
        }
    }

    public static void pause()
    {
        if (mediaPlayer != null && prepared)
        {
            mediaPlayer.pause();

            STATE_DISPATCHER.dispatch(State.PAUSED);
        }
    }

    public static void resume()
    {
        if (mediaPlayer != null && prepared)
        {
            mediaPlayer.start();

            STATE_DISPATCHER.dispatch(State.PLAYING);
        }
    }

    private static void nextOrPrevious(@NonNull final Context context, final boolean nextNotPrevious, boolean useRepeat)
    {
        if (mediaPlayer != null && prepared)
        {
            final int order = PlayOrderManager.getOrder(context);

            if (order == PlayOrderManager.REPEAT && useRepeat)
            {
                mediaPlayer.seekTo(0);
                mediaPlayer.start();
            }
            else
            {
                release();

                final TdApi.Message message = MESSAGE_AUDIO_DISPATCHER.getCurrentState();

                if (message == null)
                {
                    stop(context);
                    return;
                }

                Info.searchAudioInChat(message.chatId, 0, Integer.MAX_VALUE, new Obtainer<Info.ChatSearchedMessagesInfo>()
                {
                    @Override public void obtain(Info.ChatSearchedMessagesInfo messagesInfo)
                    {
                        TdApi.Message currentMassage = MESSAGE_AUDIO_DISPATCHER.getCurrentState();

                        if (currentMassage == null || currentMassage.chatId != message.chatId || currentMassage.id != message.id)
                        {
                            return;
                        }

                        List<TdApi.Message> messageList = messagesInfo.messageList;

                        if (messageList.isEmpty())
                        {
                            stop(context);
                        }
                        else if (order == PlayOrderManager.IN_SUCCESSION)
                        {
                            for (TdApi.Message someMessage : messageList)
                            {
                                if (nextNotPrevious && someMessage.id < currentMassage.id)
                                {
                                    play(context, someMessage);
                                    return;
                                }
                            }

                            List<TdApi.Message> invertedMessageList = new ArrayList<>(messageList);

                            for (TdApi.Message someMessage : invertedMessageList)
                            {
                                if (!nextNotPrevious && someMessage.id > currentMassage.id)
                                {
                                    play(context, someMessage);
                                    return;
                                }
                            }

                            play(context, messageList.get(nextNotPrevious ? 0 : messageList.size() - 1));
                        }
                        else if (order == PlayOrderManager.SHUFFLE)
                        {
                            play(context, messageList.get(new Random().nextInt(messageList.size())));
                        }
                    }
                });
            }
        }
    }

    public static void previous(@NonNull final Context context)
    {
        nextOrPrevious(context, false, false);
    }

    public static void next(@NonNull final Context context)
    {
        nextOrPrevious(context, true, false);
    }

    public static void stop(@NonNull Context context)
    {
        context.stopService(new Intent(context, AudioPlayerService.class));
    }

    @Nullable private static MediaPlayer mediaPlayer = null;

    public static int getDuration()
    {
        if (mediaPlayer != null)
        {
            try
            {
                return max(mediaPlayer.getDuration(), 0);
            }
            catch (IllegalStateException ignored)
            {

            }
        }

        TdApi.Audio audio = AUDIO_DISPATCHER.getCurrentState();

        return audio != null ? audio.duration * 1000 : 0;
    }

    public static int getOffset()
    {
        try
        {
            if (mediaPlayer != null)
            {
                return max(min(mediaPlayer.getCurrentPosition(), getDuration()), 0);
            }
        }
        catch (IllegalStateException ignored)
        {

        }

        return 0;
    }

    public static void setOffset(int offsetInMillis)
    {
        if (mediaPlayer != null)
        {
            try
            {
                if (prepared)
                {
                    mediaPlayer.seekTo(max(min(offsetInMillis, getDuration()), 0));
                    resume();
                }
            }
            catch (IllegalStateException ignored)
            {

            }
        }
    }

    private static int savedOffsetInMillis = 0;
    private static boolean prepared = false;

    @Override public int onStartCommand(Intent intent, int flags, int startId)
    {
        final TdApi.Message message = MESSAGE_AUDIO_DISPATCHER.getCurrentState();
        final TdApi.Audio audio = AUDIO_DISPATCHER.getCurrentState();

        if (audio != null)
        {
            FileLoader.getInstance(audio.audio).getPath(new Obtainer<String>()
            {
                @Override public void obtain(String path)
                {
                    TdApi.Message currentMessage = MESSAGE_AUDIO_DISPATCHER.getCurrentState();

                    if (path == null || currentMessage == null || message.chatId != currentMessage.chatId || message.id != currentMessage.id)
                    {
                        stopSelf();
                        return;
                    }

                    try
                    {
                        release();

                        mediaPlayer = new MediaPlayer();
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        mediaPlayer.setLooping(false);
                        mediaPlayer.setOnPreparedListener(AudioPlayerService.this);
                        mediaPlayer.setOnCompletionListener(AudioPlayerService.this);
                        mediaPlayer.setDataSource(path);
                        mediaPlayer.prepareAsync();
                    }
                    catch (IOException exception)
                    {
                        throw new AssertionError(exception);
                    }
                }
            });
        }

        return START_NOT_STICKY;
    }

    @Override public void onPrepared(MediaPlayer mediaPlayer)
    {
        prepared = true;

        if (savedOffsetInMillis != 0)
        {
            mediaPlayer.seekTo(savedOffsetInMillis);
            savedOffsetInMillis = 0;
        }

        mediaPlayer.start();

        STATE_DISPATCHER.dispatch(State.PLAYING);
    }

    @Override public void onCompletion(MediaPlayer mediaPlayer)
    {
        nextOrPrevious(this, true, true);
    }

    @Override public void onDestroy()
    {
        super.onDestroy();

        release();

        MESSAGE_AUDIO_DISPATCHER.dispatch(null);
        AUDIO_DISPATCHER.dispatch(null);
        STATE_DISPATCHER.dispatch(State.STOPPED);
    }

    private static void release()
    {
        prepared = false;

        if (mediaPlayer != null)
        {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }

        TdApi.Audio audio = AUDIO_DISPATCHER.getCurrentState();

        if (audio != null)
        {
            FileLoader.getInstance(audio.audio).cancelLoad();
        }
    }

    @Override public IBinder onBind(Intent intent)
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
