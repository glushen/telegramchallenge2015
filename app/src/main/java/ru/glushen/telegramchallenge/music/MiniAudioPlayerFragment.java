package ru.glushen.telegramchallenge.music;

import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.text.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;

import static ru.glushen.telegramchallenge.music.AudioPlayerService.*;

/**
 * Created by Pavel Glushen on 11.08.2015.
 */
public class MiniAudioPlayerFragment extends Fragment
{
    public static MiniAudioPlayerFragment newInstance()
    {
        return new MiniAudioPlayerFragment();
    }

    private final Obtainer<TdApi.Audio> audioObtainer = new Obtainer<TdApi.Audio>()
    {
        @Override public void obtain(@Nullable TdApi.Audio audio)
        {
            if (audio != null)
            {
                Spannable spannable = SpannableStringBuilder.valueOf(audio.title + " — " + audio.performer);
                Font.Span span = new Font.Span(Font.getRobotoMedium(view.getResources()));
                spannable.setSpan(span, 0, audio.title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                titleView.setText(spannable);
            }
        }
    };

    private final Obtainer<AudioPlayerService.State> stateObtainer = new Obtainer<State>()
    {
        @Override public void obtain(@NonNull State state)
        {
            if (view != null)
            {
                if (state == State.STOPPED)
                {
                    view.setVisibility(View.GONE);
                }
                else
                {
                    view.setVisibility(View.VISIBLE);

                    boolean playing = STATE_DISPATCHER.getCurrentState() == State.PLAYING;
                    pauseButtonView.setImageResource(playing ? R.drawable.ic_pausepl : R.drawable.ic_playpl);

                    progressHandlerRunnable.run();
                }
            }
        }
    };

    private final Handler handler = new Handler();

    private final Runnable progressHandlerRunnable = new Runnable()
    {
        @Override public void run()
        {
            if (progressBar != null)
            {
                progressBar.setMax(getDuration());
                progressBar.setProgress(getOffset());

                if (STATE_DISPATCHER.getCurrentState() == State.PLAYING)
                {
                    handler.postDelayed(this, 15);
                }
            }
        }
    };

    private View view;
    private ImageView pauseButtonView;
    private TextView titleView;
    private ProgressBar progressBar;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_audio_player_mini, container, false);

        pauseButtonView = (ImageView) view.findViewById(R.id.pauseButton);

        pauseButtonView.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                if (STATE_DISPATCHER.getCurrentState() == State.PLAYING)
                {
                    pause();
                }
                else
                {
                    resume();
                }
            }
        });

        titleView = (TextView) view.findViewById(R.id.title);

        titleView.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                Fragment parent = getParentFragment();

                if (parent instanceof MessageListFragment)
                {
                    ((MessageListFragment) parent).hideAnyKeyboard();
                }

                parent.getFragmentManager().beginTransaction().replace(parent.getId(), AudioPlayerFragment.newInstance()).addToBackStack(null).commit();
            }
        });

        view.findViewById(R.id.closeButton).setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                stop(view.getContext());
            }
        });

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        AUDIO_DISPATCHER.addObtainerAndObtain(audioObtainer, false);
        STATE_DISPATCHER.addObtainerAndObtain(stateObtainer, false);

        return view;
    }

    @Override public void onDestroyView()
    {
        super.onDestroyView();

        AUDIO_DISPATCHER.removeObtainer(audioObtainer);
        STATE_DISPATCHER.removeObtainer(stateObtainer);
        handler.removeCallbacks(progressHandlerRunnable);
    }
}
