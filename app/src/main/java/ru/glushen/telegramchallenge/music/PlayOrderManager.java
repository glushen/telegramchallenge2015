package ru.glushen.telegramchallenge.music;

import android.content.*;
import android.support.annotation.*;

/**
 * Created by Pavel Glushen on 23.08.2015.
 */
class PlayOrderManager
{
    public static int IN_SUCCESSION = 0;
    public static int REPEAT = 1;
    public static int SHUFFLE = 2;

    private static final String ORDER = "order";

    public static int getOrder(@NonNull Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PlayOrderManager.class.getCanonicalName(), Context.MODE_PRIVATE);
        return sharedPreferences.getInt(ORDER, IN_SUCCESSION);
    }

    public static void setOrder(@NonNull Context context, int order)
    {
        if (order < IN_SUCCESSION || order > SHUFFLE)
        {
            throw new AssertionError("Incorrect order");
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(PlayOrderManager.class.getCanonicalName(), Context.MODE_PRIVATE);
        sharedPreferences.edit().putInt(ORDER, order).apply();
    }

    public static void toggleRepeat(@NonNull Context context)
    {
        setOrder(context, getOrder(context) != REPEAT ? REPEAT : IN_SUCCESSION);
    }

    public static void toggleShuffle(@NonNull Context context)
    {
        setOrder(context, getOrder(context) != SHUFFLE ? SHUFFLE : IN_SUCCESSION);
    }

    private PlayOrderManager()
    {

    }
}
