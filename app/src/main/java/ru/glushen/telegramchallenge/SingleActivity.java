package ru.glushen.telegramchallenge;

import android.app.*;
import android.content.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.*;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.*;

import org.drinkless.td.libcore.telegram.*;

import java.io.*;
import java.lang.ref.*;

import ru.glushen.image.*;
import ru.glushen.telegramchallenge.auth.*;
import ru.glushen.telegramchallenge.chatlist.*;
import ru.glushen.telegramchallenge.inputmethod.*;
import ru.glushen.telegramchallenge.passcode.*;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 05.05.2015.
 */
public class SingleActivity extends FragmentActivity
{
    public static final String FILE_PROVIDER_AUTHORITY = "ru.glushen.telegramchallenge.fileprovider";

    private static final String LOG_TAG = "TGC";
    private static final String TELEGRAM_CLIENT_DIR = "tgc";
    private static final String TELEGRAM_CLIENT_FILES_DIR = "tgc_files";
    private static final String TELEGRAM_CLIENT_VOICE_FILES_DIR = "voice";

    private static final int FRAGMENT_CONTAINER = R.id.fragmentContainer;

    public static void log(Object... allObjects)
    {
        for (Object object : allObjects)
        {
            Log.d(LOG_TAG, object + "");
        }
    }

    @Deprecated public static FragmentManager getActivityFragmentManager() // todo remove
    {
        SingleActivity activity = activityWeakReference.get();

        return activity != null ? activity.getSupportFragmentManager() : null;
    }

    @NonNull public static String generateVoiceFilePath(@NonNull Context context)
    {
        File filesDir = new File(context.getFilesDir(), TELEGRAM_CLIENT_FILES_DIR);
        //noinspection ResultOfMethodCallIgnored
        filesDir.mkdir();

        File voiceDir = new File(filesDir, TELEGRAM_CLIENT_VOICE_FILES_DIR);
        //noinspection ResultOfMethodCallIgnored
        voiceDir.mkdir();

        File file = new File(voiceDir, System.currentTimeMillis() + ".ogg");

        return file.getAbsolutePath();
    }

    private static WeakReference<SingleActivity> activityWeakReference = new WeakReference<>(null);

    public SingleActivity()
    {
        activityWeakReference = new WeakReference<>(this);
    }

    private boolean savedInstanceStateToNull;
    private boolean needToStartTelegramClient = false;

    @Override protected void onCreate(Bundle savedInstanceState)
    {
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

        getWindow().setBackgroundDrawable(null);

        setContentView(R.layout.activity);

        inputManager = new InputManager(this);

        savedInstanceStateToNull = !TelegramClient.isRunning();

        if (savedInstanceStateToNull)
        {
            savedInstanceState = null;
        }

        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
        {
            if (!Passcode.isLockedNow(this))
            {
                startTelegramClient();
            }
            else
            {
                needToStartTelegramClient = true;
            }
        }
    }

    @Override protected void onStart()
    {
        super.onStart();

        if (Passcode.isLockedNow(this)) // todo test
        {
            UnlockDialogFragment.showDialog(getSupportFragmentManager());
            UnlockDialogFragment.UNLOCK_DISPATCHER.addObtainer(unlockObtainer, false);
        }

        TelegramClient.setErrorObtainer(new Obtainer<Pair<TdApi.TLFunction, TdApi.Error>>()
        {
            @Override public void obtain(Pair<TdApi.TLFunction, TdApi.Error> data)
            {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SingleActivity.this);

                String text = "";

                try
                {
                    text += data.first;
                }
                catch (NullPointerException exception)
                {
                    text += "NullPointerException in TdApi.java";
                }

                text += "\n\n";

                try
                {
                    text += data.second;
                }
                catch (NullPointerException exception)
                {
                    text += "NullPointerException in TdApi.java";
                }

                dialogBuilder.setTitle(R.string.errorDialogTitle).setMessage(text).show();
            }
        });
    }

    @Override protected void onStop()
    {
        super.onStop();

        Passcode.notifyAppStopped(this);

        TelegramClient.setErrorObtainer(null);
    }

    private void startTelegramClient()
    {
        File dir = new File(getFilesDir(), TELEGRAM_CLIENT_DIR);
        //noinspection ResultOfMethodCallIgnored
        dir.mkdir();

        File filesDir = new File(getFilesDir(), TELEGRAM_CLIENT_FILES_DIR);
        //noinspection ResultOfMethodCallIgnored
        filesDir.mkdir();

        TelegramClient.start(dir.getAbsolutePath(), filesDir.getAbsolutePath(), new Handler());

        updateAuthState();
    }

    private final Obtainer<Void> unlockObtainer = new Obtainer<Void>()
    {
        @Override public void obtain(Void data)
        {
            UnlockDialogFragment.UNLOCK_DISPATCHER.removeObtainer(this);

            if (needToStartTelegramClient)
            {
                startTelegramClient();
                needToStartTelegramClient = false;
            }
        }
    };

    @Override protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState)
    {
        if (!savedInstanceStateToNull)
        {
            super.onRestoreInstanceState(savedInstanceState);
        }
    }

    @Override protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceStateToNull ? null : savedInstanceState);
    }

    private boolean instanceSaved = false;

    @Override protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        instanceSaved = true;

        UnlockDialogFragment.UNLOCK_DISPATCHER.removeObtainer(unlockObtainer);
    }

    @Override protected void onDestroy()
    {
        super.onDestroy();

        if (!instanceSaved)
        {
            UnlockDialogFragment.UNLOCK_DISPATCHER.removeObtainer(unlockObtainer);

            TelegramClient.stop();
        }
    }

    public void updateAuthState()
    {
        TelegramClient.request(new TdApi.GetAuthState(), new TelegramClient.ResultObtainer<TdApi.AuthState>()
        {
            @Override public void onComplete(TdApi.AuthState authState)
            {
                updateAuthState(authState);
            }

            @Override public boolean onError(TdApi.Error error)
            {
                return false;
            }
        });
    }

    public void updateAuthState(TdApi.AuthState authState)
    {
        boolean stateOk = authState instanceof TdApi.AuthStateOk;

        if (stateOk)
        {
            inputManager.reset();
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(FRAGMENT_CONTAINER, stateOk ? ChatListFragment.newInstance() : AuthFragment.newInstance());

        transaction.commit();
    }

    @Override public void onBackPressed()
    {
        if (inputManager.isCustomKeyboardShown())
        {
            inputManager.hideCustomKeyboard();
        }
        else
        {
            Fragment fragment = getSupportFragmentManager().findFragmentById(FRAGMENT_CONTAINER);

            FragmentManager childFragmentManager = fragment != null ? fragment.getChildFragmentManager() : null;

            if (childFragmentManager != null && childFragmentManager.getBackStackEntryCount() > 0)
            {
                childFragmentManager.popBackStack();
            }
            else
            {
                super.onBackPressed();
            }
        }
    }

    @Override public void onLowMemory()
    {
        super.onLowMemory();

        Image.bitmapDrawableCache.clear();
    }

    private InputManager inputManager;

    public InputManager getInputManager()
    {
        return inputManager;
    }
}