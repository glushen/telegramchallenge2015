package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.message.MessageManager.ForwardType.*;

/**
 * Created by Pavel Glushen on 20.07.2015.
 */
class MessageForwardSecond extends MessageManager
{
    private final MessageManager forwardedMessageManager;

    public MessageForwardSecond(int index, MessageManager forwardedMessageManager)
    {
        super(index);

        this.forwardedMessageManager = forwardedMessageManager;
    }

    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return forwardedMessageManager.isMessageSupported(message);
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new MessageItem.ForwardItem(message, this, forwardedMessageManager);
    }

    private static class ViewHolder extends RecyclerViewHolder<MessageItem>
    {
        private final RecyclerViewHolder<MessageItem> forwardedViewHolder;

        public ViewHolder(ViewGroup parent, MessageManager forwardedMessageManager)
        {
            super(parent, R.layout.list_chat_message_forward_second);

            FrameLayout containerForward = (FrameLayout) itemView.findViewById(R.id.containerForward);
            forwardedViewHolder = forwardedMessageManager.createMessageListViewHolder(containerForward, true);
            containerForward.addView(forwardedViewHolder.itemView);
        }

        @Override protected void onBind(MessageItem item)
        {
            forwardedViewHolder.bind(((MessageItem.ForwardItem) item).forwardedItem);
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwardedMessageManager);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListForwardedMessage, 0, 0);
    }

    @NonNull @Override public ForwardType getForwardType()
    {
        return FORWARD_SECOND;
    }
}
