package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.message.MessageManager.ForwardType.*;
import static ru.glushen.telegramchallenge.telegramutil.Functions.*;

/**
 * Created by Pavel Glushen on 18.07.2015.
 */
public abstract class MessageManager
{
    public enum ForwardType
    {
        NO_FORWARD, FORWARD_FIRST, FORWARD_SECOND
    }

    public static final MessageDateDivider MESSAGE_MANAGER_DATE_DIVIDER;

    @NonNull public static MessageItem createDateDividerMessageListItem(int date)
    {
        return MESSAGE_MANAGER_DATE_DIVIDER.createMessageListItem(date);
    }

    public static final MessageUnreadCount MESSAGE_MANAGER_UNREAD_COUNT;

    @NonNull public static MessageItem createUnreadCountMessageListItem(int count)
    {
        return MESSAGE_MANAGER_UNREAD_COUNT.createMessageListItem(count);
    }

    public static final MessageProgressBar MESSAGE_MANAGER_PROGRESS_BAR;

    @NonNull public static MessageItem createProgressBarMessageListItem(boolean forOldItem)
    {
        return MESSAGE_MANAGER_PROGRESS_BAR.createMessageListItem(forOldItem);
    }

    public static boolean isLoadOldProgressMessageListItem(MessageItem item)
    {
        return MESSAGE_MANAGER_PROGRESS_BAR.isLoadOldMessageListItem(item);
    }

    public static boolean isLoadNewProgressMessageListItem(MessageItem item)
    {
        return MESSAGE_MANAGER_PROGRESS_BAR.isLoadNewMessageListItem(item);
    }

    public static long getMessageListItemUniqueId(MessageItem item)
    {
        if (item.messageManager == MESSAGE_MANAGER_DATE_DIVIDER)
        {
            return (-1L << 32) + item.message.date;
        }
        else if (item.messageManager == MESSAGE_MANAGER_UNREAD_COUNT)
        {
            return (-2L << 32) + item.message.date;
        }
        else if (item.messageManager == MESSAGE_MANAGER_PROGRESS_BAR)
        {
            return (-3L << 32) + item.message.date;
        }
        else
        {
            return (((long) item.messageManager.instanceIndex) << 32) + item.message.id;
        }
    }

    private static final MessageManager[] INSTANCES;

    static
    {
        ArrayList<MessageManager> instanceList = new ArrayList<>();

        instanceList.add(new MessageText(instanceList.size()));
        instanceList.add(new MessageVoice(instanceList.size()));
        instanceList.add(new MessageAudio(instanceList.size()));
        instanceList.add(new MessageDocument(instanceList.size()));
        instanceList.add(new MessageSticker(instanceList.size()));
        instanceList.add(new MessagePhoto(instanceList.size()));
        instanceList.add(new MessageVideo(instanceList.size()));
        instanceList.add(new MessageLocation(instanceList.size()));
        instanceList.add(new MessageContact(instanceList.size()));
        instanceList.add(new MessageGroupChatCreate(instanceList.size()));
        instanceList.add(new MessageChatChangeTitle(instanceList.size()));
        instanceList.add(new MessageChatChangePhoto(instanceList.size()));
        instanceList.add(new MessageChatDeletePhoto(instanceList.size()));
        instanceList.add(new MessageChatAddParticipant(instanceList.size()));
        instanceList.add(new MessageChatDeleteParticipant(instanceList.size()));
        instanceList.add(new MessageDeleted(instanceList.size()));
        instanceList.add(new MessageUnsupported(instanceList.size()));

        for (int i = 0, initCount = instanceList.size(); i < initCount; i++)
        {
            instanceList.add(new MessageForwardFirst(instanceList.size(), instanceList.get(i)));
            instanceList.add(new MessageForwardSecond(instanceList.size(), instanceList.get(i)));
        }

        MESSAGE_MANAGER_DATE_DIVIDER = new MessageDateDivider(instanceList.size());
        instanceList.add(MESSAGE_MANAGER_DATE_DIVIDER);

        MESSAGE_MANAGER_UNREAD_COUNT = new MessageUnreadCount(instanceList.size());
        instanceList.add(MESSAGE_MANAGER_UNREAD_COUNT);

        MESSAGE_MANAGER_PROGRESS_BAR = new MessageProgressBar(instanceList.size());
        instanceList.add(MESSAGE_MANAGER_PROGRESS_BAR);

        INSTANCES = instanceList.toArray(new MessageManager[instanceList.size()]);
    }

    @NonNull public static MessageManager getInstance(@NonNull TdApi.Message message, @NonNull ForwardType forwardType)
    {
        for (MessageManager instance : INSTANCES)
        {
            if (instance.isMessageSupported(message) && instance.getForwardType() == forwardType)
            {
                return instance;
            }
        }

        throw new AssertionError("Cannot find MessageManager instance: message = " + message + ", " + forwardType);
    }

    @NonNull public static MessageManager getInstance(int index)
    {
        return INSTANCES[index];
    }

    public final int instanceIndex;

    protected MessageManager(int index)
    {
        this.instanceIndex = index;
    }

    protected abstract boolean isMessageSupported(@NonNull TdApi.Message message);

    @NonNull protected abstract MessageItem onCreateMessageListItem(@NonNull TdApi.Message message);

    @NonNull public MessageItem createMessageListItem(@NonNull TdApi.Message message)
    {
        if (isMessageSupported(message))
        {
            return onCreateMessageListItem(message);
        }
        else
        {
            throw new IllegalArgumentException("Message is not supported: " + message);
        }
    }

    @NonNull public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent)
    {
        return createMessageListViewHolder(parent, false);
    }

    @NonNull abstract RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded);

    public void getChatListTopMessageLabel(final TdApi.Chat chat, final Resources resources, final Obtainer<CharSequence> textObtainer)
    {
        if (isMessageSupported(chat.topMessage))
        {
            onGetChatListTopMessageLabel(chat, resources, textObtainer);
        }
        else
        {
            throw new IllegalArgumentException("Message is not supported: " + chat.topMessage);
        }
    }

    protected abstract void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer);

    protected void onGetChatListTopMessageLabelImpl(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer, @StringRes int defaultRes, @StringRes int anotherUserPrivateChatRes, @StringRes int objectRes)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, defaultRes, anotherUserPrivateChatRes, objectRes != 0 ? resources.getString(objectRes) : "");
    }

    protected void onGetChatListTopMessageLabelImpl(final TdApi.Chat chat, final Resources resources, final Obtainer<CharSequence> textObtainer, @StringRes final int defaultRes, @StringRes final int anotherUserPrivateChatRes, final String object)
    {
        Info.getUser(chat.topMessage.fromId, new Obtainer<Info.UserAndUpdate>()
        {
            @Override public void obtain(final Info.UserAndUpdate userAndUpdate)
            {
                Info.getOptionValue(Info.OPTION_MY_ID, new Obtainer<Info.OptionData>()
                {
                    @Override public void obtain(Info.OptionData data)
                    {
                        boolean currentUser = chat.topMessage.fromId == ((TdApi.OptionInteger) data.value).value;

                        @StringRes int resource;

                        if (!currentUser && isPrivateChat(chat) && anotherUserPrivateChatRes != 0)
                        {
                            resource = anotherUserPrivateChatRes;
                        }
                        else
                        {
                            resource = defaultRes;
                        }

                        String subject;

                        if (currentUser)
                        {
                            subject = resources.getString(R.string.chatListSubjectCurrentUser);
                        }
                        else
                        {
                            subject = getName(userAndUpdate.user, resources);
                        }

                        String text = resources.getString(resource, subject, object);

                        textObtainer.obtain(text);
                    }
                });
            }
        });
    }

    @NonNull public ForwardType getForwardType()
    {
        return NO_FORWARD;
    }
}
