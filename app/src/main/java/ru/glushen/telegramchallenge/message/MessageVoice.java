package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.os.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.audio.*;
import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramui.*;
import ru.glushen.telegramchallenge.telegramui.SeekBar;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.telegramutil.Functions.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageVoice extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageVoice;
    }

    private static class Item extends MessageItem
    {
        @NonNull public final TdApi.Voice voice;
        @NonNull public final FileLoader loader;
        @Nullable public OpusFilePlayer player = null;

        public Item(TdApi.Message message, MessageManager messageManager)
        {
            super(message, messageManager);

            voice = ((TdApi.MessageVoice) message.message).voice;
            loader = FileLoader.getInstance(voice.voice);
        }
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new Item(message, this);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_voice, useForward);

            View view = getContainedView();

            downloadProgressView = (DeterminateProgressBar) view.findViewById(R.id.downloadProgress);
            downloadProgressView.setBackgroundCircleColor(0xfff0f6fa);

            stateView = (ImageView) view.findViewById(R.id.state);

            stateView.setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick(View view)
                {
                    if (item.player != null)
                    {
                        if (item.player.stateDispatcher.getCurrentState() == OpusFilePlayer.State.PLAYING)
                        {
                            item.player.pause();
                        }
                        else
                        {
                            item.player.play();
                        }
                    }
                    else
                    {
                        if (item.loader.getState() == FileLoader.State.EMPTY)
                        {
                            item.loader.load();
                        }
                        else
                        {
                            item.loader.cancelLoad();
                        }
                    }
                }
            });

            durationSeekBarView = (SeekBar) view.findViewById(R.id.offsetSeekBar);

            durationSeekBarView.setListener(new SeekBar.OnSeekListener()
            {
                @Override public void onProgress(int progress, int max)
                {

                }

                @Override public void onProgressChanged(int progress, int max, boolean byUser)
                {
                    if (byUser && item.player != null)
                    {
                        item.player.setOffsetInMillis(getTimeInMillis(progress));
                    }
                }
            });

            durationView = (TextView) view.findViewById(R.id.duration);
        }

        private final DeterminateProgressBar downloadProgressView;
        private final ImageView stateView;
        private final SeekBar durationSeekBarView;
        private final TextView durationView;

        private Item item;

        private final Obtainer<FileLoader> loaderStateObtainer = new Obtainer<FileLoader>()
        {
            @Override public void obtain(FileLoader fileLoader)
            {
                switch (fileLoader.getState())
                {
                    case EMPTY:
                    {
                        invalidateView(R.drawable.ic_download_blue);
                    }
                    break;

                    case LOADING:
                    {
                        invalidateView(R.drawable.ic_pause_blue);
                    }
                    break;

                    case LOADED:
                    {
                        String path = fileLoader.getPath();

                        if (path != null)
                        {
                            if (item.player == null)
                            {
                                item.player = OpusFilePlayer.getInstance(path);
                            }

                            item.player.stateDispatcher.addObtainer(playerStateObtainer, false);
                            playerStateObtainer.obtain(item.player.stateDispatcher.getCurrentState());
                        }
                    }
                    break;
                }
            }
        };

        private final Obtainer<OpusFilePlayer.State> playerStateObtainer = new Obtainer<OpusFilePlayer.State>()
        {
            @Override public void obtain(OpusFilePlayer.State state)
            {
                invalidateView(state == OpusFilePlayer.State.PLAYING ? R.drawable.ic_pause : R.drawable.ic_play_wrap);
            }
        };

        private void invalidateView(@DrawableRes int stateImage)
        {
            boolean loading = item.loader.isLoading();

            downloadProgressView.setMax(item.loader.getSize());
            downloadProgressView.setProgress(loading ? item.loader.getReady() : 0);
            downloadProgressView.setEnabled(loading);
            downloadProgressView.setBackgroundCircleColor(item.player != null ? 0xff68ade1 : 0xfff0f6fa);

            stateView.setImageResource(stateImage);

            durationSeekBarView.setEnabled(item.player != null);

            durationView.setText(DateAndTime.getTimerTime(getTimeInMillis(item.voice.duration)));

            invalidateProgress();

            handler.removeCallbacks(playingHandlerRunnable);

            if (item.player != null && item.player.stateDispatcher.getCurrentState() == OpusFilePlayer.State.PLAYING)
            {
                handler.post(playingHandlerRunnable);
            }
        }

        private final Handler handler = new Handler();

        private final Runnable playingHandlerRunnable = new Runnable()
        {
            @Override public void run()
            {
                invalidateProgress();
                handler.postDelayed(this, 100);
            }
        };

        private void invalidateProgress()
        {
            if (item.player != null)
            {
                item.player.getOffsetAndDuration(new Obtainer<OpusFilePlayer.OffsetAndDuration>()
                {
                    @Override public void obtain(OpusFilePlayer.OffsetAndDuration data)
                    {
                        long durationInSeconds = data.durationInMillis / 1000;
                        long offsetInSeconds = data.offsetInMillis / 1000;

                        if (durationInSeconds <= Integer.MAX_VALUE) // if I can cast it to int
                        {
                            durationSeekBarView.setMax((int) durationInSeconds, false);
                            durationSeekBarView.setProgress((int) offsetInSeconds, false);

                            durationView.setText(DateAndTime.getTimerTime(data.durationInMillis));
                        }
                        else
                        {
                            durationSeekBarView.setEnabled(false);
                        }
                    }
                });
            }
        }

        @Override protected void onBind(MessageItem messageItem)
        {
            item = (Item) messageItem;

            item.loader.getStateDispatcher().addObtainer(loaderStateObtainer, false);
            loaderStateObtainer.obtain(item.loader);
        }

        @Override protected void onUnbind(MessageItem messageItem)
        {
            item.loader.getStateDispatcher().removeObtainer(loaderStateObtainer);

            if (item.player != null)
            {
                item.player.stateDispatcher.removeObtainer(playerStateObtainer);
            }

            handler.removeCallbacks(playingHandlerRunnable);
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(final TdApi.Chat chat, final Resources resources, final Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, R.string.chatListObjectVoice);
    }

    public MessageVoice(int index)
    {
        super(index);
    }
}
