package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.text.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.telegramutil.Info.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageChatChangeTitle extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageChatChangeTitle;
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new MessageItem(message, this);
    }

    private static class ViewHolder extends RecyclerViewHolder<MessageItem>
    {
        private final boolean useForward;

        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_system_info);

            this.useForward = useForward;
        }

        private String chatTitle;

        private final CloseableObtainer<Info.UserAndUpdate> userHandler = new CloseableObtainer<Info.UserAndUpdate>()
        {
            @Override protected void onObtain(Info.UserAndUpdate userAndUpdate)
            {
                Resources resources = itemView.getResources();
                String name = TextUtils.htmlEncode(Functions.getName(userAndUpdate.user, resources));
                String title = TextUtils.htmlEncode(chatTitle);
                String text = resources.getString(R.string.messageListChatChangeTitle, name, title);
                ((TextView) itemView).setText(Html.fromHtml(text));
            }
        };

        @Override protected void onBind(MessageItem item)
        {
            chatTitle = ((TdApi.MessageChatChangeTitle) item.message.message).title;
            getUser(Functions.getId(item.message, useForward), userHandler);
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        String title = ((TdApi.MessageChatChangeTitle) chat.topMessage.message).title;
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListGroupChangeTitle, 0, title);
    }

    public MessageChatChangeTitle(int index)
    {
        super(index);
    }
}
