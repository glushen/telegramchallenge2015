package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramui.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.telegramutil.FileLoader.State.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageDocument extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageDocument;
    }

    private static class Item extends MessageItem
    {
        public final TdApi.Document document;
        public final FileLoader loader;

        public Item(TdApi.Message message, MessageManager messageManager)
        {
            super(message, messageManager);

            document = ((TdApi.MessageDocument) message.message).document;
            loader = FileLoader.getInstance(document.document);
        }
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new Item(message, this);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_document, useForward);

            View view = getContainedView();

            view.setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick(View view11)
                {
                    switch (item.loader.getState())
                    {
                        case EMPTY:
                            item.loader.load();
                            break;
                        case LOADING:
                            item.loader.cancelLoad();
                            break;
                        case LOADED:
                            item.loader.open(view11.getContext(), item.document.mimeType);
                            break;
                    }
                }
            });

            downloadProgressView = (DeterminateProgressBar) view.findViewById(R.id.downloadProgress);
            downloadProgressView.setBackgroundCircleColor(0xfff0f6fa);

            stateView = (ImageView) view.findViewById(R.id.state);

            fileNameView = (TextView) view.findViewById(R.id.fileName);
            fileNameView.setTypeface(Font.getRobotoMedium(getResources()));

            fileSizeView = (TextView) view.findViewById(R.id.fileSize);
        }

        private final DeterminateProgressBar downloadProgressView;
        private final ImageView stateView;
        private final TextView fileNameView;
        private final TextView fileSizeView;

        private Item item;

        private final Obtainer<FileLoader> stateObtainer = new Obtainer<FileLoader>()
        {
            @Override public void obtain(FileLoader loader)
            {
                Resources resources = itemView.getResources();

                boolean loading = loader.getState() == LOADING;
                int stateImage;
                String fileSizeText;

                switch (loader.getState())
                {
                    case LOADED:
                        stateImage = R.drawable.ic_file;
                        fileSizeText = ByteCount.toString(resources, loader.getSize());

                        //todo remove logging
                        SingleActivity.log("document thumb: " + item.document.thumb);
                        break;
                    case LOADING:
                        stateImage = R.drawable.ic_pause_blue;
                        String ready = ByteCount.toString(resources, loader.getReady());
                        String size = ByteCount.toString(resources, loader.getSize());
                        fileSizeText = resources.getString(R.string.messageListDocumentLoading, ready, size);
                        break;
                    case EMPTY:
                    default:
                        stateImage = R.drawable.ic_download_blue;
                        fileSizeText = ByteCount.toString(resources, loader.getSize());
                        break;
                }

                downloadProgressView.setMax(loader.getSize());
                downloadProgressView.setProgress(loading ? loader.getReady() : 0);
                downloadProgressView.setEnabled(loading);

                stateView.setImageResource(stateImage);

                fileSizeView.setText(fileSizeText);
            }
        };

        @Override protected void onBind(MessageItem messageItem)
        {
            item = (Item) messageItem;

            fileNameView.setText(item.document.fileName);

            item.loader.getStateDispatcher().addObtainer(stateObtainer, false);
            stateObtainer.obtain(item.loader);
        }

        @Override protected void onUnbind(MessageItem messageItem)
        {
            item.loader.getStateDispatcher().removeObtainer(stateObtainer);
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, R.string.chatListObjectDocument);
    }

    public MessageDocument(int index)
    {
        super(index);
    }
}
