package ru.glushen.telegramchallenge.message;

import android.support.annotation.*;
import android.view.*;

import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 16.05.2015.
 */
abstract class RealMessageViewHolder extends RecyclerViewHolder<MessageItem>
{
    private final RealMessageView realMessageView = (RealMessageView) itemView;

    public RealMessageViewHolder(ViewGroup parent, @LayoutRes int containerLayoutResource, boolean useForward)
    {
        super(new RealMessageView(parent.getContext(), containerLayoutResource, useForward));
    }

    protected View getContainedView()
    {
        return realMessageView.getContainedView();
    }

    @Override public final void bind(MessageItem item)
    {
        super.bind(item);

        realMessageView.bindMessageItem(item);
    }
}
