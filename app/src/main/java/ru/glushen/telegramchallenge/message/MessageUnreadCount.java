package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageUnreadCount extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return true;
    }

    @NonNull public MessageItem createMessageListItem(int count)
    {
        TdApi.Message message = new TdApi.Message();
        message.date = count;

        return new MessageItem(message, this);
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new MessageItem(message, this);
    }

    private static class ViewHolder extends RecyclerViewHolder<MessageItem>
    {
        public ViewHolder(ViewGroup parent)
        {
            super(parent, R.layout.list_chat_message_unread_count);
        }

        @Override protected void onBind(MessageItem item)
        {
            int count = item.message.date;
            String text = itemView.getResources().getQuantityString(R.plurals.messageListUnreadCount, count, count);
            ((TextView) itemView).setText(text);
        }
    }

    @NonNull @Override RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {

    }

    public MessageUnreadCount(int index)
    {
        super(index);
    }
}
