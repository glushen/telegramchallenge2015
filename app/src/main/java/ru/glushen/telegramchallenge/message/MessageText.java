package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.text.*;
import android.text.method.*;
import android.text.style.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.emoji.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static android.text.Spanned.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageText extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageText || message.message instanceof TdApi.MessageWebPage;
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new MessageItem(message, this);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_text, useForward);

            textView = (TextView) getContainedView();
            textView.setMovementMethod(LinkMovementMethod.getInstance());
        }

        private final TextView textView;

        final CloseableObtainerManager<Spannable> spannableHandlerManager = new CloseableObtainerManager<Spannable>()
        {
            @Override protected CloseableObtainer<Spannable> onCreate()
            {
                return new CloseableObtainer<Spannable>()
                {
                    @Override protected void onObtain(Spannable spannable)
                    {
                        Hypertext.obtainLinks(spannable, messageId);
                        textView.setText(spannable);
                    }
                };
            }
        };

        private int messageId;

        @Override protected void onBind(MessageItem item)
        {
            TdApi.Message message = item.message;

            messageId = message.id;

            String messageText;

            if (message.message instanceof TdApi.MessageWebPage)
            {
                messageText = ((TdApi.MessageWebPage) message.message).text;
            }
            else
            {
                messageText = ((TdApi.MessageText) message.message).text;
            }

            textView.setText(messageText);

            Emoji.stringToSpannable(messageText, textView.getResources(), spannableHandlerManager.replace());
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(final TdApi.Chat chat, final Resources resources, final Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, new Obtainer<CharSequence>()
        {
            @Override public void obtain(final CharSequence data)
            {
                TdApi.Message message = chat.topMessage;

                String messageText;

                if (message.message instanceof TdApi.MessageWebPage)
                {
                    messageText = ((TdApi.MessageWebPage) message.message).text;
                }
                else
                {
                    messageText = ((TdApi.MessageText) message.message).text;
                }

                if (messageText.length() > 150)
                {
                    messageText = messageText.substring(0, 150);
                }

                Emoji.stringToSpannable(messageText, resources, new CloseableObtainer<Spannable>()
                {
                    @Override protected void onObtain(Spannable spannable)
                    {
                        spannable.setSpan(new ForegroundColorSpan(0xff8a8a8a), 0, spannable.length(), SPAN_EXCLUSIVE_EXCLUSIVE);
                        textObtainer.obtain(TextUtils.concat(data, spannable));
                    }
                });
            }
        }, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, 0);
    }

    public MessageText(int index)
    {
        super(index);
    }
}
