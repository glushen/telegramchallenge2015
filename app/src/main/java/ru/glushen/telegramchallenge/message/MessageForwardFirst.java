package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.message.MessageManager.ForwardType.*;

/**
 * Created by Pavel Glushen on 20.07.2015.
 */
class MessageForwardFirst extends MessageManager
{
    @NonNull private final MessageManager forwardedMessageManager;

    public MessageForwardFirst(int index, @NonNull MessageManager forwardedMessageManager)
    {
        super(index);

        this.forwardedMessageManager = forwardedMessageManager;
    }

    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return forwardedMessageManager.isMessageSupported(message);
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new MessageItem.ForwardItem(message, this, forwardedMessageManager);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        @NonNull private final MessageManager forwardedMessageManager;

        public ViewHolder(ViewGroup parent, @NonNull MessageManager forwardedMessageManager)
        {
            super(parent, R.layout.list_chat_message_forward_first, false);

            this.forwardedMessageManager = forwardedMessageManager;

            View view = getContainedView();

            FrameLayout containerForward = (FrameLayout) view.findViewById(R.id.containerForward);
            forwardedViewHolder = this.forwardedMessageManager.createMessageListViewHolder(containerForward, true);
            containerForward.addView(forwardedViewHolder.itemView);
        }

        private final RecyclerViewHolder<MessageItem> forwardedViewHolder;

        @Override protected void onBind(MessageItem item)
        {
            forwardedViewHolder.bind(((MessageItem.ForwardItem) item).forwardedItem);
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwardedMessageManager);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListForwardedMessage, 0, 0);
    }

    @NonNull @Override public ForwardType getForwardType()
    {
        return FORWARD_FIRST;
    }
}
