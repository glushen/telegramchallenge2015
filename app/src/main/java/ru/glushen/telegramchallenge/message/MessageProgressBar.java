package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageProgressBar extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return true;
    }

    @NonNull public MessageItem createMessageListItem(boolean forOldItem)
    {
        TdApi.Message message = new TdApi.Message();
        message.date = forOldItem ? Integer.MIN_VALUE : Integer.MAX_VALUE;

        return new MessageItem(message, this);
    }

    public boolean isLoadOldMessageListItem(MessageItem item)
    {
        return item.message.date == Integer.MIN_VALUE;
    }

    public boolean isLoadNewMessageListItem(MessageItem item)
    {
        return item.message.date == Integer.MAX_VALUE;
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new MessageItem(message, this);
    }

    @NonNull @Override RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new RecyclerViewHolder<>(parent, R.layout.list_all_progress_bar);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {

    }

    public MessageProgressBar(int index)
    {
        super(index);
    }
}
