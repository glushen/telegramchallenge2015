package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageUnsupported extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return true;
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new MessageItem(message, this);
    }

    private static class ViewHolder extends RecyclerViewHolder<MessageItem>
    {
        public ViewHolder(ViewGroup parent)
        {
            super(parent, R.layout.list_chat_message_system_info);

            ((TextView) itemView).setText(R.string.messageListUnsupported);
        }
    }

    @NonNull @Override RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, R.string.chatListObjectUnsupported);
    }

    public MessageUnsupported(int index)
    {
        super(index);
    }
}
