package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.text.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.telegramutil.Functions.*;
import static ru.glushen.telegramchallenge.telegramutil.Info.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageChatAddParticipant extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageChatAddParticipant;
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new MessageItem(message, this);
    }

    private static class ViewHolder extends RecyclerViewHolder<MessageItem>
    {
        private final boolean useForward;

        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_system_info);

            this.useForward = useForward;
        }

        private TdApi.User objectUser;

        private final CloseableObtainer<Info.UserAndUpdate> subjectUserHandler = new CloseableObtainer<Info.UserAndUpdate>()
        {
            @Override protected void onObtain(Info.UserAndUpdate userAndUpdate)
            {
                Resources resources = itemView.getResources();
                String subjectName = TextUtils.htmlEncode(getName(userAndUpdate.user, resources));
                String objectName = TextUtils.htmlEncode(getName(objectUser, resources));
                String text = resources.getString(R.string.messageListChatAddParticipant, subjectName, objectName);
                ((TextView) itemView).setText(Html.fromHtml(text));
            }
        };

        @Override protected void onBind(MessageItem item)
        {
            objectUser = ((TdApi.MessageChatAddParticipant) item.message.message).user;
            getUser(getId(item.message, useForward), subjectUserHandler);
        }
    }

    @NonNull @Override RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(final TdApi.Chat chat, final Resources resources, final Obtainer<CharSequence> textObtainer)
    {
        final TdApi.User objectUser = ((TdApi.MessageChatAddParticipant) chat.topMessage.message).user;

        if (chat.topMessage.fromId == objectUser.id)
        {
            onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListGroupAddParticipantSameUser, 0, 0);
        }
        else
        {
            Info.getOptionValue(Info.OPTION_MY_ID, new Obtainer<Info.OptionData>()
            {
                @Override public void obtain(OptionData data)
                {
                    boolean currentUser = objectUser.id == ((TdApi.OptionInteger) data.value).value;

                    String object;

                    if (currentUser)
                    {
                        object = resources.getString(R.string.chatListObjectCurrentUser);
                    }
                    else
                    {
                        object = getName(objectUser, resources);
                    }

                    onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListGroupAddParticipantDifferentUser, 0, object);
                }
            });
        }
    }

    public MessageChatAddParticipant(int index)
    {
        super(index);
    }
}
