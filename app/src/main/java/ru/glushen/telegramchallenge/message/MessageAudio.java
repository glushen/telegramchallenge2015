package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramui.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.music.AudioPlayerService.*;
import static ru.glushen.telegramchallenge.music.AudioPlayerService.State.*;
import static ru.glushen.telegramchallenge.telegramutil.FileLoader.State.*;

/**
 * Created by Pavel Glushen on 10.08.2015.
 */
class MessageAudio extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageAudio;
    }

    private static class Item extends MessageItem
    {
        public final TdApi.Audio audio;
        public final FileLoader loader;

        public Item(TdApi.Message message, MessageManager messageManager)
        {
            super(message, messageManager);

            audio = ((TdApi.MessageAudio) message.message).audio;
            loader = FileLoader.getInstance(audio.audio);
        }
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new Item(message, this);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_audio, useForward);

            View view = getContainedView();

            view.setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick(View view)
                {
                    switch (item.loader.getState())
                    {
                        case EMPTY:
                            item.loader.load();
                            break;
                        case LOADING:
                            item.loader.cancelLoad();
                            break;
                        case LOADED:
                        {
                            TdApi.Audio currentAudio = AUDIO_DISPATCHER.getCurrentState();
                            State currentState = STATE_DISPATCHER.getCurrentState();

                            if (currentState != STOPPED && currentAudio != null && currentAudio.audio.id == item.audio.audio.id)
                            {
                                if (currentState == PLAYING)
                                {
                                    pause();
                                }
                                else if (currentState == PAUSED)
                                {
                                    resume();
                                }
                            }
                            else
                            {
                                play(view.getContext(), item.message);
                            }
                        }
                        break;
                    }
                }
            });

            downloadProgressView = (DeterminateProgressBar) view.findViewById(R.id.downloadProgress);

            stateView = (ImageView) view.findViewById(R.id.state);

            titleView = (TextView) view.findViewById(R.id.title);
            titleView.setTypeface(Font.getRobotoMedium(getResources()));

            performerNameView = (TextView) view.findViewById(R.id.subtitle);
        }

        private final DeterminateProgressBar downloadProgressView;
        private final ImageView stateView;
        private final TextView titleView;
        private final TextView performerNameView;

        private Item item;

        private final Obtainer<FileLoader> fileLoaderStateObtainer = new Obtainer<FileLoader>()
        {
            @Override public void obtain(FileLoader loader)
            {
                invalidateView();
            }
        };

        private final Obtainer<TdApi.Audio> audioObtainer = new Obtainer<TdApi.Audio>()
        {
            @Override public void obtain(TdApi.Audio audio)
            {
                invalidateView();
            }
        };

        private final Obtainer<State> stateObtainer = new Obtainer<State>()
        {
            @Override public void obtain(State state)
            {
                invalidateView();
            }
        };

        private void invalidateView()
        {
            Resources resources = itemView.getResources();
            FileLoader loader = item.loader;

            int stateImage;
            String subtitleText = item.audio.performer;

            switch (loader.getState())
            {
                case LOADED:
                {
                    TdApi.Audio currentAudio = AUDIO_DISPATCHER.getCurrentState();

                    if (currentAudio != null && currentAudio.audio.id == item.audio.audio.id && STATE_DISPATCHER.getCurrentState() == PLAYING)
                    {
                        stateImage = R.drawable.ic_pause;
                    }
                    else
                    {
                        stateImage = R.drawable.ic_play_wrap;
                    }
                }
                break;
                case LOADING:
                    stateImage = R.drawable.ic_pause_blue;
                    String ready = ByteCount.toString(resources, loader.getReady());
                    String size = ByteCount.toString(resources, loader.getSize());
                    subtitleText = resources.getString(R.string.messageListDocumentLoading, ready, size);
                    break;
                case EMPTY:
                default:
                    stateImage = R.drawable.ic_download_blue;
                    break;
            }

            downloadProgressView.setMax(loader.getSize());
            downloadProgressView.setProgress(loader.getReady());
            downloadProgressView.setBackgroundCircleColor(loader.getState() == LOADED ? 0xff68ade1 : 0xfff0f6fa);
            downloadProgressView.setEnabled(loader.getState() == LOADING);

            stateView.setImageResource(stateImage);

            performerNameView.setText(subtitleText);
        }

        @Override protected void onBind(MessageItem messageItem)
        {
            item = (Item) messageItem;

            titleView.setText(item.audio.title);

            item.loader.getStateDispatcher().addObtainer(fileLoaderStateObtainer, false);
            fileLoaderStateObtainer.obtain(item.loader);

            AUDIO_DISPATCHER.addObtainerAndObtain(audioObtainer, false);
            STATE_DISPATCHER.addObtainerAndObtain(stateObtainer, false);
        }

        @Override protected void onUnbind(MessageItem messageItem)
        {
            item.loader.getStateDispatcher().removeObtainer(fileLoaderStateObtainer);
            AUDIO_DISPATCHER.removeObtainer(audioObtainer);
            STATE_DISPATCHER.removeObtainer(stateObtainer);
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, R.string.chatListObjectAudio);
    }

    public MessageAudio(int index)
    {
        super(index);
    }
}
