package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.image.Image.Filter.*;
import static ru.glushen.image.Image.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageSticker extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageSticker;
    }

    private static class Item extends MessageItem
    {
        public final TdApi.Sticker sticker;
        public final FileLoader thumbLoader;
        public final FileLoader stickerLoader;

        public Item(TdApi.Message message, MessageManager messageManager)
        {
            super(message, messageManager);

            sticker = ((TdApi.MessageSticker) message.message).sticker;

            thumbLoader = FileLoader.getInstance(sticker.thumb.photo);
            thumbLoader.load();

            stickerLoader = FileLoader.getInstance(sticker.sticker);
            stickerLoader.load();
        }
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new Item(message, this);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_sticker, useForward);

            stickerView = (SmartImageView) getContainedView();
        }

        final SmartImageView stickerView;

        @Override protected void onBind(MessageItem messageItem)
        {
            TdApi.Sticker sticker = ((Item) messageItem).sticker;

            SmartImageView.BitmapDrawableCreator[] creators = new SmartImageView.BitmapDrawableCreator[2];
            creators[0] = new FileBitmapDrawableCreator(FileLoader.getInstance(sticker.thumb.photo), BOX, NOT_DEFINE);
            creators[1] = new FileBitmapDrawableCreator(FileLoader.getInstance(sticker.sticker), BOX, NOT_DEFINE);

            stickerView.setBitmapDrawableCreators(creators);
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, R.string.chatListObjectSticker);
    }

    public MessageSticker(int index)
    {
        super(index);
    }
}
