package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.support.v4.app.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.photo.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.image.Image.Filter.*;
import static ru.glushen.image.Image.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessagePhoto extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessagePhoto;
    }

    private static class Item extends MessageItem
    {
        public final TdApi.Photo photo;

        public Item(TdApi.Message message, MessageManager messageManager)
        {
            super(message, messageManager);

            photo = ((TdApi.MessagePhoto) message.message).photo;
        }
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new Item(message, this);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_photo, useForward);

            photoView = (SmartImageView) getContainedView();

            photoView.setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick(View view11)
                {
                    FragmentManager fragmentManager = SingleActivity.getActivityFragmentManager();
                    PhotoDialogFragment.newInstance(item.message).show(fragmentManager, null);
                }
            });
        }

        private final SmartImageView photoView;

        private Item item;

        @Override protected void onBind(MessageItem messageItem)
        {
            item = (Item) messageItem;
            TdApi.PhotoSize[] photoSizes = item.photo.photos;
            ViewGroup.LayoutParams layoutParams = photoView.getLayoutParams();
            photoView.setBitmapDrawableCreators(PhotoBitmapDrawableCreator.newThumbAndPhotoBitmapDrawableCreators(photoSizes, BOX, NOT_DEFINE, layoutParams));
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, R.string.chatListObjectPhoto);
    }

    public MessagePhoto(int index)
    {
        super(index);
    }
}
