package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.telegramutil.Info.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageContact extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageContact;
    }

    private static class Item extends MessageItem
    {
        public final TdApi.MessageContact contact;

        public Item(TdApi.Message message, MessageManager messageManager)
        {
            super(message, messageManager);

            contact = (TdApi.MessageContact) message.message;
        }
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new Item(message, this);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_contact, useForward);

            View view = getContainedView();

            avatarView = (SmartImageView) view.findViewById(R.id.avatar);
            userNameView = (TextView) view.findViewById(R.id.name);
            phoneNumberView = (TextView) view.findViewById(R.id.phoneNumber);
        }

        final SmartImageView avatarView;
        final TextView userNameView;
        final TextView phoneNumberView;

        private Item item;

        private int userId;
        private TdApi.User user;

        private final CloseableObtainerManager<Info.UserAndUpdate> userHandlerManager = new CloseableObtainerManager<Info.UserAndUpdate>()
        {
            @Override protected CloseableObtainer<Info.UserAndUpdate> onCreate()
            {
                return new CloseableObtainer<Info.UserAndUpdate>()
                {
                    @Override protected void onObtain(Info.UserAndUpdate userAndUpdate)
                    {
                        user = userAndUpdate.user;

                        avatarView.setBitmapDrawableCreators(AvatarBitmapDrawableCreator.newInstance(user, getResources()));
                    }
                };
            }
        };

        @Override protected void onBind(MessageItem messageItem)
        {
            item = (Item) messageItem;

            userId = item.contact.userId;

            if (userId != 0)
            {
                getUser(userId, userHandlerManager.replace());
            }

            userNameView.setText(Functions.getName(item.contact.firstName, item.contact.lastName, userNameView.getResources()));
            phoneNumberView.setText(item.contact.lastName);
        }

        @Override protected void onUnbind(MessageItem item)
        {
            avatarView.clear();
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, R.string.chatListObjectContact);
    }

    public MessageContact(int index)
    {
        super(index);
    }
}
