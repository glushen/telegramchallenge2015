package ru.glushen.telegramchallenge.message;

import android.content.*;
import android.content.res.*;
import android.net.*;
import android.support.annotation.*;
import android.view.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageLocation extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageLocation;
    }

    private static class Item extends MessageItem
    {
        public final TdApi.Location location;
        public final LocationBitmapDrawableCreator bitmapDrawableCreator;

        public Item(TdApi.Message message, MessageManager messageManager)
        {
            super(message, messageManager);

            location = ((TdApi.MessageLocation) message.message).location;
            bitmapDrawableCreator = new LocationBitmapDrawableCreator(location.latitude, location.longitude);
        }
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new Item(message, this);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_geo_point, useForward);

            View view = getContainedView();

            mapView = (SmartImageView) view.findViewById(R.id.map);
            view.setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick(View view11)
                {
                    String url = String.format(Locale.ENGLISH, "https://maps.google.com/?q=%f,%f", item.location.latitude, item.location.longitude);
                    view11.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                }
            });
        }

        private final SmartImageView mapView;

        private Item item;

        @Override protected void onBind(MessageItem messageItem)
        {
            item = (Item) messageItem;

            mapView.setBitmapDrawableCreators(item.bitmapDrawableCreator);
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, R.string.chatListObjectLocation);
    }

    public MessageLocation(int index)
    {
        super(index);
    }
}
