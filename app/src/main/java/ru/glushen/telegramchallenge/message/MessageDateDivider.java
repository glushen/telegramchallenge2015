package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import java.util.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.telegramchallenge.telegramutil.Functions.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageDateDivider extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return true;
    }

    @NonNull public MessageItem createMessageListItem(int date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getTimeInMillis(date));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        TdApi.Message message = new TdApi.Message();
        message.date = getTimeInSeconds(calendar.getTimeInMillis());

        return new MessageItem(message, this);
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return createMessageListItem(message.date);
    }

    private static class ViewHolder extends RecyclerViewHolder<MessageItem>
    {
        public ViewHolder(ViewGroup parent)
        {
            super(parent, R.layout.list_chat_message_date_divider);
        }

        @Override protected void onBind(MessageItem item)
        {
            long timeInMillis = getTimeInMillis(item.message.date);
            String date = DateAndTime.getDate(itemView.getContext(), timeInMillis, false);
            ((TextView) itemView).setText(date);
        }
    }

    @NonNull @Override RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {

    }

    public MessageDateDivider(int index)
    {
        super(index);
    }
}
