package ru.glushen.telegramchallenge.message;

import android.content.res.*;
import android.support.annotation.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramui.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;
import ru.glushen.util.recyclerview.*;

import static ru.glushen.image.Image.Filter.*;
import static ru.glushen.image.Image.*;
import static ru.glushen.telegramchallenge.telegramutil.FileLoader.State.*;

/**
 * Created by Pavel Glushen on 19.07.2015.
 */
class MessageVideo extends MessageManager
{
    @Override public boolean isMessageSupported(@NonNull TdApi.Message message)
    {
        return message.message instanceof TdApi.MessageVideo;
    }

    private static class Item extends MessageItem
    {
        public final TdApi.Video video;
        public final FileLoader thumbLoader;
        public final FileLoader videoLoader;

        public Item(TdApi.Message message, MessageManager messageManager)
        {
            super(message, messageManager);

            video = ((TdApi.MessageVideo) message.message).video;

            thumbLoader = FileLoader.getInstance(video.thumb.photo);
            thumbLoader.load();

            videoLoader = FileLoader.getInstance(video.video);
        }
    }

    @NonNull @Override protected MessageItem onCreateMessageListItem(@NonNull TdApi.Message message)
    {
        return new Item(message, this);
    }

    private static class ViewHolder extends RealMessageViewHolder
    {
        public ViewHolder(ViewGroup parent, boolean useForward)
        {
            super(parent, R.layout.list_chat_message_video, useForward);

            containerView = (FrameLayout) getContainedView();

            containerView.setOnClickListener(new View.OnClickListener()
            {
                @Override public void onClick(View view)
                {
                    switch (item.videoLoader.getState())
                    {
                        case EMPTY:
                            item.videoLoader.load();
                            break;
                        case LOADING:
                            item.videoLoader.cancelLoad();
                            break;
                        case LOADED:
                            item.videoLoader.open(view.getContext());
                            break;
                    }
                }
            });

            thumbView = (SmartImageView) containerView.findViewById(R.id.thumb);

            downloadProgress = (DeterminateProgressBar) containerView.findViewById(R.id.downloadProgress);
            downloadProgress.setProgressColor(0xffffffff);
            downloadProgress.setBackgroundCircleColor(0x72000000);

            stateView = (ImageView) containerView.findViewById(R.id.state);
        }

        private final FrameLayout containerView;
        private final SmartImageView thumbView;
        private final DeterminateProgressBar downloadProgress;
        private final ImageView stateView;

        private Item item;

        private final Obtainer<FileLoader> stateObtainer = new Obtainer<FileLoader>()
        {
            @Override public void obtain(FileLoader loader)
            {
                boolean loading = loader.getState() == LOADING;
                int stateImage;

                switch (loader.getState())
                {
                    case LOADED:
                        stateImage = R.drawable.ic_play;
                        break;
                    case LOADING:
                        stateImage = R.drawable.ic_pause;
                        break;
                    case EMPTY:
                    default:
                        stateImage = R.drawable.ic_download;
                        break;
                }

                downloadProgress.setMax(loader.getSize());
                downloadProgress.setProgress(loading ? loader.getReady() : 0);

                stateView.setImageResource(stateImage);
            }
        };

        @Override protected void onBind(MessageItem messageItem)
        {
            item = (Item) messageItem;

            item.videoLoader.getStateDispatcher().addObtainer(stateObtainer, false);
            stateObtainer.obtain(item.videoLoader);

            TdApi.PhotoSize photoSize = item.video.thumb;
            ViewGroup.LayoutParams layoutParams = thumbView.getLayoutParams();
            FileLoader loader = FileLoader.getInstance(photoSize.photo);

            thumbView.setBitmapDrawableCreators(new FileBitmapDrawableCreator(loader, BOX, NOT_DEFINE, photoSize.width, photoSize.height, layoutParams));
        }

        @Override protected void onUnbind(MessageItem messageItem)
        {
            item.videoLoader.getStateDispatcher().removeObtainer(stateObtainer);
        }
    }

    @NonNull @Override public RecyclerViewHolder<MessageItem> createMessageListViewHolder(@NonNull ViewGroup parent, boolean forwarded)
    {
        return new ViewHolder(parent, forwarded);
    }

    @Override protected void onGetChatListTopMessageLabel(TdApi.Chat chat, Resources resources, Obtainer<CharSequence> textObtainer)
    {
        onGetChatListTopMessageLabelImpl(chat, resources, textObtainer, R.string.chatListPopular, R.string.chatListPopularAnotherUserPrivateChat, R.string.chatListObjectVideo);
    }

    public MessageVideo(int index)
    {
        super(index);
    }
}
