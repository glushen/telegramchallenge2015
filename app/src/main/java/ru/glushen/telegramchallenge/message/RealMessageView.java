package ru.glushen.telegramchallenge.message;

import android.annotation.*;
import android.content.*;
import android.graphics.*;
import android.support.annotation.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.messagelist.*;
import ru.glushen.telegramchallenge.telegramutil.*;
import ru.glushen.ui.*;
import ru.glushen.util.obtainer.*;

import static android.view.View.MeasureSpec.*;
import static android.view.ViewGroup.LayoutParams.*;
import static java.lang.Math.*;
import static ru.glushen.telegramchallenge.telegramutil.DateAndTime.*;
import static ru.glushen.telegramchallenge.telegramutil.Functions.*;
import static ru.glushen.telegramchallenge.telegramutil.Info.*;
import static ru.glushen.util.Screen.*;

/**
 * Created by Pavel Glushen on 02.07.2015.
 */
@SuppressLint("ViewConstructor") class RealMessageView extends ViewGroup
{
    private final SmartImageView avatarView;
    private final TextView userNameView;
    private final TextView timestampView;
    private final ImageView statusIconView;

    private View containedView;

    public View getContainedView()
    {
        return containedView;
    }

    private final boolean useForward;

    private final int avatarMarginRight;
    private final int userNameMarginRight;
    private final int timeMarginRight;
    private final int statusIconMarginRight;
    private final int containedMarginTop;

    public RealMessageView(@NonNull Context context, @LayoutRes int containerLayoutResource, boolean useForward)
    {
        this(context, useForward);

        containedView = LayoutInflater.from(context).inflate(containerLayoutResource, this, false);
        addView(containedView);
    }

    private RealMessageView(@NonNull Context context, boolean useForward)
    {
        super(context);

        avatarView = new SmartImageView(context);
        avatarView.setScaleType(ImageView.ScaleType.MATRIX);
        addView(avatarView);

        userNameView = new TextView(context);
        userNameView.setTextSize(15);
        userNameView.setTypeface(Typeface.DEFAULT_BOLD);
        userNameView.setTextColor(0xff569ace);
        userNameView.setSingleLine(true);
        userNameView.setEllipsize(TextUtils.TruncateAt.END);
        addView(userNameView);

        timestampView = new TextView(context);
        timestampView.setTextSize(13);
        timestampView.setTextColor(0xffb2b2b2);
        userNameView.setSingleLine(true);
        userNameView.setEllipsize(TextUtils.TruncateAt.END);
        addView(timestampView);

        statusIconView = new ImageView(context);
        statusIconView.setScaleType(ImageView.ScaleType.CENTER);
        addView(statusIconView);

        this.useForward = useForward;

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

        avatarMarginRight = dpPixelSize(10, displayMetrics);
        userNameMarginRight = dpPixelSize(6, displayMetrics);
        timeMarginRight = dpPixelSize(6, displayMetrics);
        statusIconMarginRight = dpPixelSize(3, displayMetrics);
        containedMarginTop = dpPixelSize(2, displayMetrics);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

        int widthMode = getMode(widthMeasureSpec);

        int width;

        if (widthMode == EXACTLY || widthMode == AT_MOST)
        {
            width = getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        }
        else
        {
            width = dpPixelSize(250, displayMetrics);
        }

        int avatarViewMeasureSpec = makeMeasureSpec(dpPixelSize(41, displayMetrics), EXACTLY);
        avatarView.measure(avatarViewMeasureSpec, avatarViewMeasureSpec);

        int statusIconViewMeasureSpec = makeMeasureSpec(dpPixelSize(12, displayMetrics), EXACTLY);
        statusIconView.measure(statusIconViewMeasureSpec, statusIconViewMeasureSpec);

        int timestampViewMaxWidth = max(width - avatarView.getMeasuredWidth() - avatarMarginRight - statusIconView.getMeasuredWidth() - statusIconMarginRight - timeMarginRight, 0);
        timestampView.measure(makeMeasureSpec(timestampViewMaxWidth, AT_MOST), makeMeasureSpec(0, UNSPECIFIED));

        int userNameViewMaxWidth = max(timestampViewMaxWidth - timestampView.getMeasuredWidth() - userNameMarginRight, 0);
        userNameView.measure(makeMeasureSpec(userNameViewMaxWidth, AT_MOST), makeMeasureSpec(0, UNSPECIFIED));

        int containedViewMaxWidth = max(width - avatarView.getMeasuredWidth() - avatarMarginRight, 0);

        LayoutParams containedViewParams = containedView.getLayoutParams();

        int containedViewWidthMeasureSpec;

        if (containedViewParams == null || containedViewParams.width == MATCH_PARENT)
        {
            containedViewWidthMeasureSpec = makeMeasureSpec(containedViewMaxWidth, EXACTLY);
        }
        else if (containedViewParams.width == WRAP_CONTENT)
        {
            containedViewWidthMeasureSpec = makeMeasureSpec(containedViewMaxWidth, AT_MOST);
        }
        else
        {
            containedViewWidthMeasureSpec = makeMeasureSpec(containedViewParams.width, EXACTLY);
        }

        int containedViewHeightMeasureSpec;

        if (containedViewParams == null || containedViewParams.height == MATCH_PARENT || containedViewParams.height == WRAP_CONTENT)
        {
            containedViewHeightMeasureSpec = makeMeasureSpec(0, UNSPECIFIED);
        }
        else
        {
            containedViewHeightMeasureSpec = makeMeasureSpec(containedViewParams.height, EXACTLY);
        }

        containedView.measure(containedViewWidthMeasureSpec, containedViewHeightMeasureSpec);

        int heightMode = getMode(heightMeasureSpec);

        int height;

        if (heightMode == EXACTLY)
        {
            height = getSize(heightMeasureSpec);
        }
        else
        {
            int maxHeight = heightMode == AT_MOST ? getSize(heightMeasureSpec) : Integer.MAX_VALUE;

            height = min(max(avatarView.getMeasuredHeight(), userNameView.getMeasuredHeight() + containedMarginTop + containedView.getMeasuredHeight()), maxHeight);
        }

        setMeasuredDimension(width + getPaddingLeft() + getPaddingRight(), height + getPaddingTop() + getPaddingBottom());
    }

    @Override protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();

        int left = paddingLeft;
        int top = paddingTop;
        int right = left + avatarView.getMeasuredWidth();
        int bottom = top + avatarView.getMeasuredHeight();

        avatarView.layout(left, top, right, bottom);

        left = right + avatarMarginRight;
        top = paddingTop;
        right = left + userNameView.getMeasuredWidth();
        bottom = top + userNameView.getMeasuredHeight();

        userNameView.layout(left, top, right, bottom);

        left = right + userNameMarginRight;
        top = paddingTop + userNameView.getBaseline() - timestampView.getBaseline();
        right = left + timestampView.getMeasuredWidth();
        bottom = top + timestampView.getMeasuredHeight();

        timestampView.layout(left, top, right, bottom);

        left = getMeasuredWidth() - paddingRight - statusIconView.getMeasuredWidth() - statusIconMarginRight;
        top = paddingTop + userNameView.getBaseline() - statusIconView.getMeasuredHeight();
        right = left + statusIconView.getMeasuredWidth();
        bottom = top + statusIconView.getMeasuredHeight();

        statusIconView.layout(left, top, right, bottom);

        left = paddingLeft + avatarView.getMeasuredWidth() + avatarMarginRight;
        top = paddingTop + userNameView.getMeasuredHeight() + containedMarginTop;
        right = left + containedView.getMeasuredWidth();
        bottom = top + containedView.getMeasuredHeight();

        containedView.layout(left, top, right, bottom);
    }

    private MessageItem messageItem;

    public void bindMessageItem(MessageItem messageItem)
    {
        this.messageItem = messageItem;

        getUser(Functions.getId(messageItem.message, useForward), userHandlerManager.replace());
    }

    private final CloseableObtainerManager<UserAndUpdate> userHandlerManager = new CloseableObtainerManager<UserAndUpdate>()
    {
        @Override protected CloseableObtainer<UserAndUpdate> onCreate()
        {
            return new CloseableObtainer<UserAndUpdate>()
            {
                @Override protected void onObtain(UserAndUpdate userAndUpdate)
                {
                    TdApi.User user = userAndUpdate.user;

                    avatarView.setBitmapDrawableCreators(AvatarBitmapDrawableCreator.newInstance(user, getResources()));

                    userNameView.setText(getName(user, getResources()));

                    String timestamp;
                    int timestampIconRes = 0;

                    if (!useForward)
                    {
                        timestamp = messageItem.message.date != 0 ? getTime(getContext(), getTimeInMillis(messageItem.message.date)) : "";

                        if (isMessageFailedToSend(messageItem.message))
                        {
                            timestampIconRes = R.drawable.ic_error_min;
                        }
                        else if (!isMessageSent(messageItem.message))
                        {
                            timestampIconRes = R.drawable.ic_clock;
                        }
                        else if (messageItem.isOutboxMessageUnread)
                        {
                            timestampIconRes = R.drawable.ic_unread;
                        }
                    }
                    else
                    {
                        timestamp = getDateOrTime(getContext(), getTimeInMillis(messageItem.message.forwardDate), true);
                    }

                    timestampView.setText(timestamp);

                    statusIconView.setImageResource(timestampIconRes);

                    invalidate();
                    requestLayout();
                }
            };
        }
    };
}
