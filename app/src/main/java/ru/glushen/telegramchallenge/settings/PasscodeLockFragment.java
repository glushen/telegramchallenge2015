package ru.glushen.telegramchallenge.settings;

import android.os.*;
import android.support.v4.app.*;
import android.view.*;
import android.widget.*;

import ru.glushen.telegramchallenge.*;
import ru.glushen.telegramchallenge.passcode.*;
import ru.glushen.telegramchallenge.telegramui.*;
import ru.glushen.telegramchallenge.telegramui.Toolbar;
import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 29.07.2015.
 */
public class PasscodeLockFragment extends Fragment
{
    public static PasscodeLockFragment newInstance()
    {
        return new PasscodeLockFragment();
    }

    private final Obtainer<Passcode.AutoLockDuration> autoLockDurationObtainer = new Obtainer<Passcode.AutoLockDuration>()
    {
        @Override public void obtain(Passcode.AutoLockDuration data)
        {
            if (autoLockTimeView != null)
            {
                autoLockTimeView.setText(Passcode.getAutoLockDuration(getActivity()).getStringRes());
            }
        }
    };

    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        AutoLockDialogFragment.AUTO_LOCK_DURATION_CHOSEN_DISPATCHER.addObtainer(autoLockDurationObtainer, false);
    }

    @Override public void onDestroy()
    {
        super.onDestroy();

        AutoLockDialogFragment.AUTO_LOCK_DURATION_CHOSEN_DISPATCHER.removeObtainer(autoLockDurationObtainer);
    }

    private SwitchView passcodeLockSwitchView;

    private View passcodeChangeButtonView;
    private TextView passcodeChangeTextView;
    private int passcodeChangeTextDefaultColor;

    private View autoLockButtonView;
    private TextView autoLockTextView;
    private int autoLockTextDefaultColor;
    private TextView autoLockTimeView;
    private int autoLockTimeDefaultColor;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_settings_passcode_lock, container, false);

        Toolbar toolbar = ((Toolbar) view.findViewById(R.id.toolbar));
        toolbar.setTitle(R.string.settingsPasscodeLock);
        toolbar.setNavigationButton(R.drawable.ic_back, new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                getFragmentManager().popBackStack();
            }
        });

        passcodeLockSwitchView = (SwitchView) view.findViewById(R.id.passcodeLockSwitch);

        passcodeLockSwitchView.setOnSwitchedListener(new SwitchView.OnSwitchedListener()
        {
            @Override public void onSwitched(SwitchView switchView, boolean checked, boolean byUser)
            {
                if (byUser)
                {
                    if (checked)
                    {
                        getFragmentManager().beginTransaction().replace(getId(), ChooseFragment.newInstance()).addToBackStack(null).commit();
                    }
                    else
                    {
                        Passcode.unlock(getActivity());
                        invalidateLockState(false);
                    }
                }
            }
        });

        view.findViewById(R.id.passcodeLockButton).setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                passcodeLockSwitchView.setChecked(!passcodeLockSwitchView.isChecked(), true);
            }
        });

        passcodeChangeButtonView = view.findViewById(R.id.passcodeChangeButton);
        passcodeChangeButtonView.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                getFragmentManager().beginTransaction().replace(getId(), ChooseFragment.newInstance()).addToBackStack(null).commit();
            }
        });

        passcodeChangeTextView = (TextView) passcodeChangeButtonView;
        passcodeChangeTextDefaultColor = passcodeChangeTextView.getTextColors().getDefaultColor();

        autoLockButtonView = view.findViewById(R.id.autoLockButton);
        autoLockButtonView.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                new AutoLockDialogFragment().show(getFragmentManager(), null);
            }
        });

        autoLockTextView = (TextView) autoLockButtonView.findViewById(R.id.autoLockText);
        autoLockTextDefaultColor = autoLockTextView.getTextColors().getDefaultColor();

        autoLockTimeView = (TextView) view.findViewById(R.id.autoLockTime);
        autoLockTimeView.setText(Passcode.getAutoLockDuration(getActivity()).getStringRes());
        autoLockTimeDefaultColor = autoLockTimeView.getTextColors().getDefaultColor();

        return view;
    }

    @Override public void onStart()
    {
        super.onStart();

        invalidateLockState(Passcode.isLockEnabled(getActivity()));
    }

    private void invalidateLockState(boolean locked)
    {
        passcodeLockSwitchView.setChecked(locked, false);

        passcodeChangeButtonView.setClickable(locked);
        passcodeChangeTextView.setTextColor(locked ? passcodeChangeTextDefaultColor : 0xffa6a6a6);

        autoLockButtonView.setClickable(locked);
        autoLockTextView.setTextColor(locked ? autoLockTextDefaultColor : 0xffa6a6a6);
        autoLockTimeView.setTextColor(locked ? autoLockTimeDefaultColor : 0xffd0d0d0);
    }
}
