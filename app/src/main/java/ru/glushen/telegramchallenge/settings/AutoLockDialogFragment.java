package ru.glushen.telegramchallenge.settings;

import android.app.*;
import android.content.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.DialogFragment;

import ru.glushen.telegramchallenge.passcode.*;
import ru.glushen.util.*;

/**
 * Created by Pavel Glushen on 30.07.2015.
 */
public class AutoLockDialogFragment extends DialogFragment
{
    static final EventDispatcher<Passcode.AutoLockDuration> AUTO_LOCK_DURATION_CHOSEN_DISPATCHER = new EventDispatcher<>();

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        final Passcode.AutoLockDuration[] values = Passcode.AutoLockDuration.values();

        String[] items = new String[values.length];

        for (int i = 0; i < values.length; i++)
        {
            items[i] = getString(values[i].getStringRes());
        }

        return new AlertDialog.Builder(getActivity()).setItems(items, new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog, int which)
            {
                Passcode.setAutoLockDuration(getActivity(), values[which]);
                AUTO_LOCK_DURATION_CHOSEN_DISPATCHER.dispatch(values[which]);
            }
        }).create();
    }
}
