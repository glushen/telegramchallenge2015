package ru.glushen.telegramchallenge.settings;

import android.app.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.DialogFragment;

import org.drinkless.td.libcore.telegram.*;

import ru.glushen.telegramchallenge.*;

/**
 * Created by Pavel Glushen on 08.08.2015.
 */
public class LogoutDialogFragment extends DialogFragment
{
    public static LogoutDialogFragment newInstance()
    {
        return new LogoutDialogFragment();
    }

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        TelegramClient.request(new TdApi.ResetAuth(false), new TelegramClient.ResultObtainer<TdApi.AuthState>()
        {
            @Override protected void onComplete(TdApi.AuthState authState)
            {
                if (!isDetached())
                {
                    dismiss();
                }

                ((SingleActivity) getActivity()).updateAuthState(authState);
            }

            @Override protected boolean onError(TdApi.Error error)
            {
                if (!isDetached())
                {
                    dismiss();
                }

                ((SingleActivity) getActivity()).updateAuthState();

                return true;
            }
        });

        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.chatListNavigationLogoutProgressMessage));
        dialog.setCancelable(false);

        return dialog;
    }
}
