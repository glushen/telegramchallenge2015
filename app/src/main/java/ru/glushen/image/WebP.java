package ru.glushen.image;

import android.graphics.*;
import android.support.annotation.*;

public final class WebP
{
    public static Bitmap createBitmap(@NonNull byte[] data)
    {
        return createBitmap(data, false, 0, 0, 0, 0, false, 0, 0);
    }

    public static Bitmap createCroppedBitmap(@NonNull byte[] data, int cropLeft, int cropTop, int cropWidth, int cropHeight)
    {
        return createBitmap(data, true, cropLeft, cropTop, cropWidth, cropHeight, false, 0, 0);
    }

    public static Bitmap createScaledBitmap(@NonNull byte[] data, int scaleWidth, int scaleHeight)
    {
        return createBitmap(data, false, 0, 0, 0, 0, true, scaleWidth, scaleHeight);
    }

    public static Bitmap createCroppedAndScaledBitmap(@NonNull byte[] data, int cropLeft, int cropTop, int cropWidth, int cropHeight, int scaleWidth, int scaleHeight)
    {
        return createBitmap(data, true, cropLeft, cropTop, cropWidth, cropHeight, true, scaleWidth, scaleHeight);
    }

    private static native Bitmap createBitmap(@NonNull byte[] data, boolean useCropping, int cropLeft, int cropTop, int cropWidth, int cropHeight, boolean useScaling, int scaleWidth, int scaleHeight);

    static
    {
        System.loadLibrary("telegramchallengeimage");
    }

    private WebP()
    {

    }
}
