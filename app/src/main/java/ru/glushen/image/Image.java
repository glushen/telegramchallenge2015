package ru.glushen.image;

import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.support.annotation.*;

import java.io.*;

import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static android.graphics.Bitmap.Config.*;
import static android.graphics.Paint.*;
import static java.lang.Math.*;
import static ru.glushen.image.Image.Filter.*;

/**
 * Created by Pavel Glushen on 29.05.2015.
 */
public class Image
{
    public static final Cache<String, BitmapDrawable> bitmapDrawableCache = new Cache<>(new Cache.SizeGetter<String, BitmapDrawable>()
    {
        @Override public long getSize(String key, BitmapDrawable bitmapDrawable)
        {
            return Compat.getBitmapSize(bitmapDrawable.getBitmap());
        }
    }, Runtime.getRuntime().maxMemory() / 4);
    public static final int NOT_DEFINE = -1;
    public static final int USUAL_BLUR_RADIUS = 20;
    public enum Filter
    {
        BOX, CROP_START, CROP_CENTER, CROP_END
    }
    public static class SizeParams
    {
        public static class Creator
        {
            private final int containerWidth;
            private final int containerHeight;
            private final Filter filter;

            public Creator(int containerWidth, int containerHeight, Filter filter)
            {
                this.containerWidth = containerWidth;
                this.containerHeight = containerHeight;
                this.filter = filter;
            }

            public SizeParams create(int imageWidth, int imageHeight)
            {
                return new SizeParams(containerWidth, containerHeight, filter, imageWidth, imageHeight);
            }

            @Override public String toString()
            {
                return containerWidth + "x" + containerHeight + ", " + filter;
            }
        }

        public final int width, height, paddingLeft, paddingTop, paddingRight, paddingBottom;

        public SizeParams(int containerWidth, int containerHeight, Filter filter, int imageWidth, int imageHeight)
        {
            int pickedWidth = round(containerHeight * imageWidth / (float) imageHeight);
            int pickedHeight = round(containerWidth * imageHeight / (float) imageWidth);

            int paddingLeft = 0, paddingTop = 0, paddingRight = 0, paddingBottom = 0;

            if (containerWidth == NOT_DEFINE && containerHeight == NOT_DEFINE)
            {
                width = imageWidth;
                height = imageHeight;
            }
            else if (containerWidth == NOT_DEFINE)
            {
                width = pickedWidth;
                height = containerHeight;
            }
            else if (containerHeight == NOT_DEFINE)
            {
                width = containerWidth;
                height = pickedHeight;
            }
            else
            {
                if (filter == Filter.BOX)
                {
                    width = min(containerWidth, pickedWidth);
                    height = min(containerHeight, pickedHeight);
                }
                else
                {
                    width = containerWidth;
                    height = containerHeight;
                }

                if (filter == CROP_START || filter == CROP_CENTER || filter == CROP_END)
                {
                    int fullWidth = max(containerWidth, pickedWidth);
                    int fullHeight = max(containerHeight, pickedHeight);

                    switch (filter)
                    {
                        case CROP_START:
                            paddingRight = fullWidth - width;
                            paddingBottom = fullHeight - height;
                            break;
                        case CROP_CENTER:
                            paddingLeft = (fullWidth - width) / 2;
                            paddingTop = (fullHeight - height) / 2;
                            paddingRight = fullWidth - width - paddingLeft;
                            paddingBottom = fullHeight - height - paddingTop;
                            break;
                        case CROP_END:
                            paddingLeft = fullWidth - width;
                            paddingTop = fullHeight - height;
                            break;
                    }
                }
            }

            this.paddingLeft = paddingLeft;
            this.paddingTop = paddingTop;
            this.paddingRight = paddingRight;
            this.paddingBottom = paddingBottom;
        }

        public SizeParams(SizeParams sp, int imageWidth, int imageHeight)
        {
            float widthRatio = imageWidth / (float) (sp.paddingLeft + sp.width + sp.paddingRight);
            float heightRatio = imageHeight / (float) (sp.paddingTop + sp.height + sp.paddingBottom);

            width = round(sp.width * widthRatio);
            height = round(sp.height * heightRatio);
            paddingLeft = round(sp.paddingLeft * widthRatio);
            paddingTop = round(sp.paddingTop * heightRatio);
            paddingRight = imageWidth - width - paddingLeft;
            paddingBottom = imageHeight - height - paddingTop;
        }

        @Override public String toString()
        {
            return width + "x" + height + "," + paddingLeft + "," + paddingTop + "," + paddingRight + "," + paddingBottom;
        }
    }

    public static void loadAsync(final String path, final SizeParams.Creator sizeParamsCreator, final int blurRadius, final CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer, final Resources resources)
    {
        final String cacheKey = Image.class.getName() + "::" + path + "," + sizeParamsCreator + "," + blurRadius;

        BitmapDrawable cachedBitmapDrawable = bitmapDrawableCache.get(cacheKey);

        if (cachedBitmapDrawable != null)
        {
            if (bitmapDrawableObtainer != null)
            {
                bitmapDrawableObtainer.obtain(cachedBitmapDrawable);
            }
        }
        else
        {
            new AsyncTask<Void, Void, BitmapDrawable>()
            {
                @Override protected BitmapDrawable doInBackground(Void... voids)
                {
                    if (bitmapDrawableObtainer.hasClosed())
                    {
                        cancel(false);
                        return null;
                    }

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(path, options);
                    options.inJustDecodeBounds = false;

                    SizeParams sizeParams;
                    Bitmap fileBitmap;

                    if (bitmapDrawableObtainer.hasClosed())
                    {
                        cancel(false);
                        return null;
                    }

                    if (options.outWidth > 0 && options.outHeight > 0)
                    {
                        sizeParams = sizeParamsCreator.create(options.outWidth, options.outHeight);

                        options.inSampleSize = getSampleSize(sizeParams, options);

                        fileBitmap = BitmapFactory.decodeFile(path, options);
                    }
                    else
                    {
                        try
                        {
                            fileBitmap = WebP.createBitmap(Utils.toByteArray(path));

                            if (fileBitmap == null)
                            {
                                return null;
                            }

                            sizeParams = sizeParamsCreator.create(fileBitmap.getWidth(), fileBitmap.getHeight());
                        }
                        catch (IOException exception)
                        {
                            throw new AssertionError("Cannot open image: " + path + "\n" + exception);
                        }
                    }

                    if (bitmapDrawableObtainer.hasClosed())
                    {
                        cancel(false);
                        return null;
                    }

                    SizeParams scaledSizeParams = new SizeParams(sizeParams, fileBitmap.getWidth(), fileBitmap.getHeight());
                    int left = scaledSizeParams.paddingLeft;
                    int top = scaledSizeParams.paddingTop;
                    int right = scaledSizeParams.width + scaledSizeParams.paddingLeft;
                    int bottom = scaledSizeParams.height + scaledSizeParams.paddingTop;
                    Rect inRect = new Rect(left, top, right, bottom);
                    Rect outRect = new Rect(0, 0, sizeParams.width, sizeParams.height);

                    Bitmap bitmap = Bitmap.createBitmap(sizeParams.width, sizeParams.height, ARGB_8888);
                    new Canvas(bitmap).drawBitmap(fileBitmap, inRect, outRect, new Paint(FILTER_BITMAP_FLAG));

                    if (bitmapDrawableObtainer.hasClosed())
                    {
                        cancel(false);
                        return null;
                    }

                    return createBitmapDrawable(bitmap, resources);
                }

                @Override protected void onPostExecute(BitmapDrawable bitmapDrawable)
                {
                    if (bitmapDrawable != null)
                    {
                        bitmapDrawableCache.put(cacheKey, bitmapDrawable);
                    }

                    if (bitmapDrawableObtainer != null)
                    {
                        bitmapDrawableObtainer.obtain(bitmapDrawable);
                    }
                }
            }.execute();
        }
    }

    private static int getSampleSize(SizeParams sizeParams, BitmapFactory.Options options)
    {
        int sampleWidth = 1;

        while (options.outWidth / sampleWidth > sizeParams.width)
        {
            sampleWidth *= 2;
        }

        int sampleHeight = 1;

        while (options.outHeight / sampleHeight > sizeParams.height)
        {
            sampleHeight *= 2;
        }

        return min(sampleWidth, sampleHeight);
    }

    @NonNull public static BitmapDrawable createBitmapDrawable(@NonNull Bitmap bitmap, @NonNull Resources resources)
    {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, bitmap);

        bitmapDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());

        return bitmapDrawable;
    }
}