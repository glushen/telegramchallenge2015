package ru.glushen.util.recyclerview;

import android.support.annotation.*;
import android.support.v7.widget.*;

import java.util.*;

/**
 * Created by Pavel Glushen on 28.06.2015.
 */
public abstract class ScrollableRecyclerViewAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH>
{
    private final List<RecyclerView> attachedRecyclerViewList = new ArrayList<>();

    private final List<RecyclerView> unmodifiableAttachedRecyclerViewList = Collections.unmodifiableList(attachedRecyclerViewList);

    protected List<RecyclerView> getAttachedRecyclerViewList()
    {
        return unmodifiableAttachedRecyclerViewList;
    }

    protected void scrollToPosition(int position)
    {
        for (RecyclerView recyclerView : unmodifiableAttachedRecyclerViewList)
        {
            recyclerView.scrollToPosition(position);
        }
    }

    protected void scrollToPositionWithOffset(int position, int offset)
    {
        for (RecyclerView recyclerView : unmodifiableAttachedRecyclerViewList)
        {
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();

            if (layoutManager instanceof LinearLayoutManager)
            {
                ((LinearLayoutManager) layoutManager).scrollToPositionWithOffset(position, offset);
            }
        }
    }

    private boolean useInitialScroll = false;
    private int initialScrollPosition;

    protected void addInitialScrollPosition(int position)
    {
        useInitialScroll = true;
        initialScrollPosition = position;
    }

    protected void removeInitialScrollPosition()
    {
        useInitialScroll = false;
    }

    protected void onScrolled(RecyclerView recyclerView, int dx, int dy)
    {

    }

    protected void onScrollStateChanged(RecyclerView recyclerView, int newState)
    {

    }

    protected void onVisibleRangeChanged(RecyclerView recyclerView)
    {

    }

    @CallSuper @Override public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);

        attachedRecyclerViewList.add(recyclerView);

        if (useInitialScroll)
        {
            recyclerView.scrollToPosition(initialScrollPosition);
        }

        recyclerView.addOnScrollListener(scrollListener);
    }

    @CallSuper @Override public void onDetachedFromRecyclerView(RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);

        attachedRecyclerViewList.remove(recyclerView);

        recyclerView.removeOnScrollListener(scrollListener);
    }

    private final RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener()
    {
        @Override public void onScrollStateChanged(RecyclerView recyclerView, int newState)
        {
            ScrollableRecyclerViewAdapter.this.onScrollStateChanged(recyclerView, newState);
        }

        private int firstVisiblePosition = 0;
        private int firstCompletelyVisiblePosition = 0;
        private int lastVisiblePosition = 0;
        private int lastCompletelyVisiblePosition = 0;

        @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();

            if (layoutManager instanceof LinearLayoutManager)
            {
                LinearLayoutManager linearLayoutManager = ((LinearLayoutManager) layoutManager);

                int firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                boolean visibleRangeChanged = this.firstVisiblePosition != firstVisiblePosition;
                this.firstVisiblePosition = firstVisiblePosition;

                int firstCompletelyVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                visibleRangeChanged |= this.firstCompletelyVisiblePosition != firstCompletelyVisiblePosition;
                this.firstCompletelyVisiblePosition = firstCompletelyVisiblePosition;

                int lastVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                visibleRangeChanged |= this.lastVisiblePosition != lastVisiblePosition;
                this.lastVisiblePosition = lastVisiblePosition;

                int lastCompletelyVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition();
                visibleRangeChanged |= this.lastCompletelyVisiblePosition != lastCompletelyVisiblePosition;
                this.lastCompletelyVisiblePosition = lastCompletelyVisiblePosition;

                if (visibleRangeChanged)
                {
                    ScrollableRecyclerViewAdapter.this.onVisibleRangeChanged(recyclerView);
                }
            }

            ScrollableRecyclerViewAdapter.this.onScrolled(recyclerView, dx, dy);
        }
    };
}
