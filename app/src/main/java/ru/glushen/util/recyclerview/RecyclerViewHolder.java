package ru.glushen.util.recyclerview;

import android.content.*;
import android.content.res.*;
import android.support.annotation.*;
import android.support.v7.widget.*;
import android.view.*;

/**
 * Created by Pavel Glushen on 15.05.2015.
 */
public class RecyclerViewHolder<I> extends RecyclerView.ViewHolder
{
    public interface OnClickListener<I>
    {
        void onClick(RecyclerViewHolder<I> holder, I item);
    }

    public interface OnLongClickListener<I>
    {
        boolean onLongClick(RecyclerViewHolder<I> holder, I item);
    }

    public RecyclerViewHolder(@NonNull ViewGroup parent, @LayoutRes int layoutResource)
    {
        this(LayoutInflater.from(parent.getContext()).inflate(layoutResource, parent, false));
    }

    public RecyclerViewHolder(View view)
    {
        super(view);

        class Listener implements View.OnClickListener, View.OnLongClickListener
        {
            @Override public void onClick(View v)
            {
                RecyclerViewHolder.this.onClick(savedItem);

                if (onClickListener != null)
                {
                    onClickListener.onClick(RecyclerViewHolder.this, savedItem);
                }
            }

            @Override public boolean onLongClick(View v)
            {
                boolean consumed = RecyclerViewHolder.this.onLongClick(savedItem);

                if (!consumed && onLongClickListener != null)
                {
                    return onLongClickListener.onLongClick(RecyclerViewHolder.this, savedItem);
                }
                else
                {
                    return consumed;
                }
            }
        }

        Listener listener = new Listener();

        itemView.setOnClickListener(listener);
        itemView.setOnLongClickListener(listener);
    }

    protected void onClick(I item)
    {

    }

    protected boolean onLongClick(I item)
    {
        return false;
    }

    protected Context getContext()
    {
        return itemView.getContext();
    }

    protected Resources getResources()
    {
        return itemView.getResources();
    }

    protected View findViewById(@IdRes int id)
    {
        return itemView.findViewById(id);
    }

    private I savedItem;

    public I getItem()
    {
        return savedItem;
    }

    @Nullable private OnClickListener<I> onClickListener = null;

    public void setOnClickListener(@Nullable OnClickListener<I> onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    @Nullable private OnLongClickListener<I> onLongClickListener = null;

    public void setOnLongClickListener(@Nullable OnLongClickListener<I> onLongClickListener)
    {
        this.onLongClickListener = onLongClickListener;
    }

    private boolean bound = false;

    @CallSuper public void bind(I item)
    {
        if (bound)
        {
            onUnbind(savedItem);
        }
        else
        {
            bound = true;
        }

        savedItem = item;

        onBind(item);
    }

    @Override protected void finalize() throws Throwable
    {
        super.finalize();

        if (bound)
        {
            onUnbind(savedItem);
        }
    }

    protected void onBind(I item)
    {

    }

    protected void onUnbind(I item)
    {

    }
}
