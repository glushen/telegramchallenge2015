package ru.glushen.util;

import android.os.*;

import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 10.08.2015.
 */
public class StateDispatcher<T> extends EventDispatcher<T>
{
    private T currentState;
    private final boolean dispatchSameState;

    public StateDispatcher(T initialState, boolean dispatchSameState)
    {
        this(initialState, dispatchSameState, null);
    }

    public StateDispatcher(T initialState, boolean dispatchSameState, Handler handler)
    {
        super(handler);

        currentState = initialState;
        this.dispatchSameState = dispatchSameState;
    }

    public final T getCurrentState()
    {
        return currentState;
    }

    @Override public void dispatch(T state)
    {
        if (dispatchSameState || currentState != null && !currentState.equals(state) || currentState == null && state != null)
        {
            currentState = state;

            super.dispatch(state);
        }
    }

    public void addObtainerAndObtain(Obtainer<T> obtainer, boolean strongReference)
    {
        addObtainer(obtainer, strongReference);

        if (obtainer != null)
        {
            obtainer.obtain(currentState);
        }
    }
}
