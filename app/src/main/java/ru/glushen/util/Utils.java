package ru.glushen.util;

import java.io.*;

/**
 * Created by Pavel Glushen on 25.06.2015.
 */
@SuppressWarnings("TryFinallyCanBeTryWithResources") public class Utils
{
    public static byte[] toByteArray(String filePath) throws IOException
    {
        InputStream inputStream = new FileInputStream(filePath);

        try
        {
            return toByteArray(inputStream);
        }
        finally
        {
            inputStream.close();
        }
    }

    public static byte[] toByteArray(InputStream inputStream) throws IOException
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try
        {
            byte[] buffer = new byte[Math.max(inputStream.available(), 4096)];

            int readCount;

            while ((readCount = inputStream.read(buffer)) != -1)
            {
                outputStream.write(buffer, 0, readCount);
            }

            return outputStream.toByteArray();
        }
        finally
        {
            outputStream.close();
        }
    }
}
