package ru.glushen.util.obtainer;

import android.support.annotation.*;

/**
 * Created by Pavel Glushen on 30.06.2015.
 */
public abstract class CloseableObtainerManager<T>
{
    @Nullable private CloseableObtainer<T> savedObtainer = null;

    public CloseableObtainer<T> replace()
    {
        if (savedObtainer != null)
        {
            savedObtainer.close();
        }

        savedObtainer = onCreate();

        return savedObtainer;
    }

    public void close()
    {
        if (savedObtainer != null)
        {
            savedObtainer.close();

            savedObtainer = null;
        }
    }

    protected abstract CloseableObtainer<T> onCreate();
}
