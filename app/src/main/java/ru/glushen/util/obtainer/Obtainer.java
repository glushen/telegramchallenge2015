package ru.glushen.util.obtainer;

/**
 * Created by Pavel Glushen on 28.06.2015.
 */
public interface Obtainer<T>
{
    void obtain(T data);
}
