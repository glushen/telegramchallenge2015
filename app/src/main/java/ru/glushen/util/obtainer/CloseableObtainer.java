package ru.glushen.util.obtainer;

import android.support.annotation.*;

/**
 * Created by Pavel Glushen on 04.05.2015.
 */
public abstract class CloseableObtainer<T> implements Obtainer<T>
{
    private boolean closed = false;

    public boolean hasClosed()
    {
        return closed;
    }

    public void close()
    {
        closed = true;
    }

    @CallSuper public void obtain(T data)
    {
        if (!closed)
        {
            onObtain(data);
        }
    }

    protected abstract void onObtain(T data);
}
