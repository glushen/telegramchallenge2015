package ru.glushen.util;

import android.support.annotation.*;

import java.lang.ref.*;

/**
 * Created by Pavel Glushen on 04.05.2015.
 */
public class Reference<T>
{
    private boolean strongReference = true;
    private T strongReferenceObject;
    private WeakReference<T> weakReferenceObject = null;

    public Reference(@Nullable T object, boolean strongReference)
    {
        strongReferenceObject = object;
        setStrongReference(this, strongReference);
    }

    public static <T> void setStrongReference(@Nullable Reference<T> reference, boolean strongReference)
    {
        if (reference != null)
        {
            if (reference.strongReference != strongReference)
            {
                reference.strongReference = strongReference;

                if (strongReference)
                {
                    reference.strongReferenceObject = reference.weakReferenceObject.get();
                    reference.weakReferenceObject = null;
                }
                else
                {
                    reference.weakReferenceObject = new WeakReference<>(reference.strongReferenceObject);
                    reference.strongReferenceObject = null;
                }
            }
        }
    }

    public static <T> boolean hasStrongReference(@Nullable Reference<T> reference)
    {
        return contains(reference) && reference.strongReference;
    }

    public static <T> boolean contains(@Nullable Reference<T> reference)
    {
        return reference != null && get(reference) != null;
    }

    @Nullable public static <T> T get(@Nullable Reference<T> reference)
    {
        if (reference != null)
        {
            if (reference.strongReference)
            {
                return reference.strongReferenceObject;
            }
            else
            {
                return reference.weakReferenceObject.get();
            }
        }
        else
        {
            return null;
        }
    }

    @Override public boolean equals(@Nullable Object another)
    {
        if (!(another instanceof Reference))
        {
            return false;
        }

        Object object1 = get(this);
        Object object2 = get((Reference) another);

        return object1 != null && object2 != null && object1.equals(object2) && object2.equals(object1);
    }

    @Override public int hashCode()
    {
        Object object = get(this);
        return object == null ? 0 : object.hashCode();
    }
}
