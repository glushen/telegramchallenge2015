package ru.glushen.util;

import android.os.*;
import android.support.annotation.*;

import java.util.*;

import ru.glushen.util.obtainer.*;

/**
 * Created by Pavel Glushen on 04.05.2015.
 */
public class EventDispatcher<T>
{
    @Nullable private final Handler handler;

    public EventDispatcher()
    {
        this(null);
    }

    public EventDispatcher(Handler handler)
    {
        this.handler = handler;
    }

    private final LinkedHashSet<Reference<Obtainer<T>>> allReferences = new LinkedHashSet<>();

    public void addObtainer(Obtainer<T> obtainer, boolean strongReference)
    {
        Reference<Obtainer<T>> reference = new Reference<>(obtainer, strongReference);

        allReferences.remove(reference);
        allReferences.add(reference);
    }

    public void removeObtainer(Obtainer<T> obtainer)
    {
        allReferences.remove(new Reference<>(obtainer, true));
    }

    public void dispatch(final T event)
    {
        if (handler != null)
        {
            handler.post(new Runnable()
            {
                @Override public void run()
                {
                    dispatchImpl(event);
                }
            });
        }
        else
        {
            dispatchImpl(event);
        }
    }

    private void dispatchImpl(T event)
    {
        @SuppressWarnings("unchecked") LinkedHashSet<Reference<Obtainer<T>>> cloneOfAllReferences = (LinkedHashSet<Reference<Obtainer<T>>>) allReferences.clone();

        for (Reference<Obtainer<T>> reference : cloneOfAllReferences)
        {
            Obtainer<T> obtainer = Reference.get(reference);

            if (obtainer != null)
            {
                obtainer.obtain(event);
            }
        }
    }
}
