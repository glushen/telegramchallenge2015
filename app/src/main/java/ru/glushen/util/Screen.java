package ru.glushen.util;

import android.support.annotation.*;
import android.util.*;

/**
 * Created by Pavel Glushen on 22.06.2015.
 */
public class Screen
{
    public static int dpPixelSize(float dp, @NonNull DisplayMetrics displayMetrics)
    {
        return (int) (dp * displayMetrics.density + .5f);
    }

    public static int spPixelSize(float sp, @NonNull DisplayMetrics displayMetrics)
    {
        return (int) (sp * displayMetrics.scaledDensity + .5f);
    }
}
