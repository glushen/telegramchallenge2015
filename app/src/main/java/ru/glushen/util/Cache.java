package ru.glushen.util;

import android.support.annotation.*;

import java.util.*;

/**
 * Created by Pavel Glushen on 27.06.2015.
 */
public class Cache<K, V>
{
    public interface SizeGetter<K, V>
    {
        @IntRange(from = 0) long getSize(K key, V value);
    }

    @Nullable private final SizeGetter<K, V> sizeGetter;

    private final long maxSize;

    public Cache(@Nullable SizeGetter<K, V> sizeGetter, long maxSize)
    {
        this.sizeGetter = sizeGetter;
        this.maxSize = maxSize;
    }

    private final LinkedHashMap<K, Reference<V>> referenceMap = new LinkedHashMap<>();

    @Nullable public synchronized V get(@NonNull K key)
    {
        V value = Reference.get(referenceMap.get(key));

        return value != null ? put(key, value) : null;
    }

    @Nullable public synchronized V put(@NonNull K key, @NonNull V value)
    {
        V previousValue = remove(key);

        Reference<V> reference = new Reference<>(value, true);

        referenceMap.put(key, reference);

        holdMemory(key, reference);

        return previousValue;
    }

    @Nullable public synchronized V remove(@NonNull K key)
    {
        Reference<V> reference = referenceMap.remove(key);

        releaseMemory(key, reference);

        return Reference.get(reference);
    }

    public synchronized void clear()
    {
        for (K key : referenceMap.keySet())
        {
            Reference<V> reference = referenceMap.get(key);

            releaseMemory(key, reference);

            Reference.setStrongReference(reference, false);
        }
    }

    private void releaseMemory(@NonNull K key, @Nullable Reference<V> reference)
    {
        usedSize -= getSize(key, reference);
    }

    private long getSize(@NonNull K key, @Nullable Reference<V> reference)
    {
        if (Reference.hasStrongReference(reference))
        {
            return sizeGetter != null ? sizeGetter.getSize(key, Reference.get(reference)) : 1L;
        }
        else
        {
            return 0L;
        }
    }

    private long usedSize = 0;

    private void holdMemory(@NonNull K key, @Nullable Reference<V> reference)
    {
        usedSize += getSize(key, reference);

        if (usedSize > maxSize || usedSize < 0)
        {
            for (Reference<V> anotherReference : referenceMap.values())
            {
                releaseMemory(key, anotherReference);

                Reference.setStrongReference(anotherReference, false);

                if (usedSize <= maxSize && usedSize >= 0)
                {
                    break;
                }
            }
        }
    }
}
