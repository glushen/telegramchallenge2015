package ru.glushen.util;

import android.graphics.*;
import android.os.*;
import android.support.annotation.*;
import android.view.*;

/**
 * Created by Pavel Glushen on 08.08.2015.
 */
public class Compat
{
    public static int getBitmapSize(@Nullable Bitmap bitmap)
    {
        if (bitmap == null)
        {
            return 0;
        }
        else if (Build.VERSION.SDK_INT >= 19)
        {
            return bitmap.getAllocationByteCount();
        }
        else if (Build.VERSION.SDK_INT >= 12)
        {
            return bitmap.getByteCount();
        }
        else
        {
            return bitmap.getWidth() * bitmap.getHeight() * 4;
        }
    }

    public static void invalidateOnAnimation(@NonNull View view)
    {
        if (Build.VERSION.SDK_INT >= 16)
        {
            view.postInvalidateOnAnimation();
        }
        else
        {
            view.postInvalidateDelayed(15);
        }
    }

    public static void setLayerTypeToSoftware(@NonNull View view, @Nullable Paint paint)
    {
        if (Build.VERSION.SDK_INT >= 11)
        {
            view.setLayerType(View.LAYER_TYPE_SOFTWARE, paint);
        }
    }

    private Compat()
    {

    }
}
