package ru.glushen.audio;

/**
 * Created by Pavel Glushen on 15.08.2015.
 */
class SystemLibraryLoader
{
    private static boolean loaded = false;

    public static synchronized void load()
    {
        if (!loaded)
        {
            loaded = true;
            System.loadLibrary("telegramchallengeaudio");
        }
    }
}
