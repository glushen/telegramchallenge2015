package ru.glushen.audio;

import android.media.*;
import android.support.annotation.*;

import java.nio.*;
import java.util.concurrent.*;

import static android.media.AudioFormat.*;
import static android.media.AudioRecord.*;

/**
 * Created by Pavel Glushen on 15.07.2015.
 */
public class OpusFileRecorder
{
    private static volatile float volume = 0;

    public static float getVolume()
    {
        return volume;
    }

    private static final int AUDIO_SOURCE = MediaRecorder.AudioSource.MIC;
    private static final int FRAME_RATE_IN_HZ = 16000;
    private static final int CHANNEL_CONFIG = CHANNEL_IN_MONO;
    private static final int AUDIO_FORMAT = ENCODING_PCM_16BIT;
    private static final int BUFFER_SIZE_IN_BYTES = Math.max(getMinBufferSize(FRAME_RATE_IN_HZ, CHANNEL_CONFIG, AUDIO_FORMAT) * 4, 2560);

    private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

    @Nullable private static ByteBuffer BUFFER = null;

    private static volatile boolean recording = false;
    @Nullable private static Future<?> executorFuture = null;

    public static synchronized void startRecord(@NonNull final String path)
    {
        if (recording)
        {
            return;
        }

        recording = true;

        executorFuture = EXECUTOR.submit(new Runnable()
        {
            @Override public void run()
            {
                nativeInit(path);

                if (BUFFER == null)
                {
                    BUFFER = ByteBuffer.allocateDirect(BUFFER_SIZE_IN_BYTES);
                }

                AudioRecord audioRecord = new AudioRecord(AUDIO_SOURCE, FRAME_RATE_IN_HZ, CHANNEL_CONFIG, AUDIO_FORMAT, BUFFER_SIZE_IN_BYTES);
                audioRecord.startRecording();

                while (recording)
                {
                    int length = audioRecord.read(BUFFER, BUFFER.capacity());
                    nativeWrite(BUFFER, length);
                    volume = nativeComputeAverageVolume(BUFFER, length);
                }

                audioRecord.stop();
                audioRecord.release();

                nativeRelease();
            }
        });
    }

    public static synchronized void stopRecord()
    {
        recording = false;

        if (executorFuture != null)
        {
            try
            {
                executorFuture.get();
            }
            catch (InterruptedException | ExecutionException exception)
            {
                throw new AssertionError(exception);
            }
        }
    }

    private static native float nativeComputeAverageVolume(@NonNull ByteBuffer buffer, int length);

    private static native void nativeInit(@NonNull String path);

    private static native void nativeWrite(@NonNull ByteBuffer buffer, int length);

    private static native void nativeRelease();

    static
    {
        SystemLibraryLoader.load();
    }
}
