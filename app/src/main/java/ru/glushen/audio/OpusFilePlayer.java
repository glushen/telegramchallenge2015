package ru.glushen.audio;

import android.media.*;
import android.os.*;
import android.support.annotation.*;

import java.util.*;
import java.util.concurrent.*;

import ru.glushen.util.*;
import ru.glushen.util.obtainer.*;

import static android.media.AudioFormat.*;
import static android.media.AudioManager.*;
import static android.media.AudioTrack.*;
import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 15.07.2015.
 */
public class OpusFilePlayer
{
    private static final Map<String, OpusFilePlayer> INSTANCE_MAP = new HashMap<>();

    @NonNull public static OpusFilePlayer getInstance(@NonNull String path)
    {
        OpusFilePlayer instance = INSTANCE_MAP.get(path);

        if (instance == null)
        {
            instance = new OpusFilePlayer(path);
            INSTANCE_MAP.put(path, instance);
        }

        return instance;
    }

    @NonNull private final String path;

    private OpusFilePlayer(@NonNull String path)
    {
        this.path = path;
    }

    public enum State
    {
        PLAYING, PAUSED
    }

    @NonNull private final Handler handler = new Handler();

    public final StateDispatcher<State> stateDispatcher = new StateDispatcher<>(State.PAUSED, false, handler);

    private static final int STREAM_TYPE = STREAM_MUSIC;
    private static final int FRAME_RATE_IN_HZ = 48000;
    private static final int FRAME_RATE_IN_KHZ = FRAME_RATE_IN_HZ / 1000;
    private static final int CHANNEL_CONFIG = CHANNEL_OUT_STEREO;
    private static final int AUDIO_FORMAT = ENCODING_PCM_16BIT;
    private static final int BUFFER_SIZE_IN_BYTES = max(getMinBufferSize(FRAME_RATE_IN_HZ, CHANNEL_CONFIG, AUDIO_FORMAT) * 2, 65536);
    private static final int MODE = MODE_STREAM;

    private static final ScheduledExecutorService EXECUTOR = Executors.newSingleThreadScheduledExecutor();

    @Nullable private static AudioTrack audioTrack = null;

    @Nullable private static OpusFilePlayer currentInstance;

    public void play()
    {
        EXECUTOR.submit(new Runnable()
        {
            @Override public void run()
            {
                syncPlay(OpusFilePlayer.this, true);
            }
        });
    }

    private static void syncPlay(@NonNull OpusFilePlayer opusFilePlayer, boolean dispatchState)
    {
        if (currentInstance == opusFilePlayer)
        {
            return;
        }

        if (currentInstance != null)
        {
            syncPause(currentInstance, true);
        }

        currentInstance = opusFilePlayer;

        opusFilePlayer.durationInFrames = nativeInit(opusFilePlayer.path);

        if (opusFilePlayer.offsetInFrames != 0)
        {
            nativeSeek(opusFilePlayer.offsetInFrames);
        }

        if (audioTrack == null)
        {
            audioTrack = new AudioTrack(STREAM_TYPE, FRAME_RATE_IN_HZ, CHANNEL_CONFIG, AUDIO_FORMAT, BUFFER_SIZE_IN_BYTES, MODE);
        }

        markerAtEnd = false;
        syncBuffer();
        audioTrack.play();

        playStartTimestamp = System.currentTimeMillis();

        if (dispatchState)
        {
            opusFilePlayer.stateDispatcher.dispatch(State.PLAYING);
        }
    }

    private static long playStartTimestamp;

    private static boolean markerAtEnd = true;
    @Nullable private static short[] BUFFER = null;
    private static int readOffsetInShorts = 0;
    private static int readSizeInShorts = 0;
    private static int writtenFrameCount = 0;
    @Nullable private static ScheduledFuture<?> scheduledFuture;

    private static void syncBuffer()
    {
        if (currentInstance == null || audioTrack == null)
        {
            return;
        }

        if (!markerAtEnd)
        {
            if (BUFFER == null)
            {
                BUFFER = new short[BUFFER_SIZE_IN_BYTES / 2];
            }

            if (readSizeInShorts <= 0)
            {
                readOffsetInShorts = 0;
                readSizeInShorts = nativeReadStereo(BUFFER) * 2;
            }

            int delayInMillis;

            if (readSizeInShorts > 0)
            {
                int writeSizeInShorts = audioTrack.write(BUFFER, readOffsetInShorts, readSizeInShorts);

                if (writeSizeInShorts < 0)
                {
                    throw new RuntimeException("Cannot write data to audioTrack");
                }

                readOffsetInShorts += writeSizeInShorts;
                readSizeInShorts -= writeSizeInShorts;

                writtenFrameCount += writeSizeInShorts / 2;

                delayInMillis = writtenFrameCount / FRAME_RATE_IN_KHZ - (int) (System.currentTimeMillis() - playStartTimestamp) - 300;
            }
            else
            {
                markerAtEnd = true;

                delayInMillis = writtenFrameCount / FRAME_RATE_IN_KHZ - (int) (System.currentTimeMillis() - playStartTimestamp);
            }

            scheduledFuture = EXECUTOR.schedule(new Runnable()
            {
                @Override public void run()
                {
                    syncBuffer();
                }
            }, max(delayInMillis, 0), TimeUnit.MILLISECONDS);
        }
        else
        {
            OpusFilePlayer instance = currentInstance;
            syncPause(currentInstance, true);
            instance.offsetInFrames = 0;
        }
    }

    public void pause()
    {
        EXECUTOR.submit(new Runnable()
        {
            @Override public void run()
            {
                syncPause(OpusFilePlayer.this, true);
            }
        });
    }

    private static void syncPause(@NonNull OpusFilePlayer opusFilePlayer, boolean dispatchState)
    {
        if (currentInstance == opusFilePlayer && audioTrack != null)
        {
            audioTrack.pause();
            audioTrack.flush();

            opusFilePlayer.offsetInFrames += (System.currentTimeMillis() - playStartTimestamp) * FRAME_RATE_IN_KHZ;

            currentInstance = null;

            markerAtEnd = true;
            readOffsetInShorts = 0;
            readSizeInShorts = 0;
            writtenFrameCount = 0;

            if (scheduledFuture != null)
            {
                scheduledFuture.cancel(false);
                scheduledFuture = null;
            }

            if (dispatchState)
            {
                opusFilePlayer.stateDispatcher.dispatch(State.PAUSED);
            }
        }
    }

    private long durationInFrames = -1;

    private int offsetInFrames = 0;

    public void setOffsetInMillis(final long offsetInMillis)
    {
        EXECUTOR.submit(new Runnable()
        {
            @Override public void run()
            {
                boolean playing = currentInstance == OpusFilePlayer.this && audioTrack != null && audioTrack.getPlayState() == PLAYSTATE_PLAYING;

                if (playing)
                {
                    syncPause(OpusFilePlayer.this, false);
                }

                offsetInFrames = (int) (offsetInMillis * FRAME_RATE_IN_KHZ);

                syncPlay(OpusFilePlayer.this, !playing);
            }
        });
    }

    public static class OffsetAndDuration
    {
        public final long durationInMillis;
        public final long offsetInMillis;

        public OffsetAndDuration(long durationInMillis, long offsetInMillis)
        {
            this.durationInMillis = durationInMillis;
            this.offsetInMillis = Math.min(offsetInMillis, durationInMillis);
        }
    }

    public void getOffsetAndDuration(final Obtainer<OffsetAndDuration> obtainer)
    {
        EXECUTOR.submit(new Runnable()
        {
            @Override public void run()
            {
                if (durationInFrames < 0)
                {
                    durationInFrames = nativeGetDurationInFrames(path);
                }

                long currentOffsetInFrames = offsetInFrames;

                if (currentInstance == OpusFilePlayer.this && audioTrack != null && audioTrack.getPlayState() == PLAYSTATE_PLAYING)
                {
                    currentOffsetInFrames += (int) (System.currentTimeMillis() - playStartTimestamp) * FRAME_RATE_IN_KHZ;
                }

                final long durationInMillis = durationInFrames / FRAME_RATE_IN_KHZ;
                final long offsetInMillis = currentOffsetInFrames / FRAME_RATE_IN_KHZ;

                handler.post(new Runnable()
                {
                    @Override public void run()
                    {
                        obtainer.obtain(new OffsetAndDuration(durationInMillis, offsetInMillis));
                    }
                });
            }
        });
    }

    public static void release()
    {
        EXECUTOR.submit(new Runnable()
        {
            @Override public void run()
            {
                syncRelease();
            }
        });
    }

    private static void syncRelease()
    {
        if (currentInstance != null)
        {
            syncPause(currentInstance, true);
        }

        if (audioTrack != null)
        {
            audioTrack.release();
            audioTrack = null;
        }

        BUFFER = null;

        nativeRelease();
    }

    private static native long nativeGetDurationInFrames(@NonNull String path);

    private static native long nativeInit(@NonNull String path);

    private static native int nativeReadStereo(@NonNull short[] buffer);

    private static native void nativeSeek(long offsetInFrames);

    private static native void nativeRelease();

    static
    {
        SystemLibraryLoader.load();
    }
}
