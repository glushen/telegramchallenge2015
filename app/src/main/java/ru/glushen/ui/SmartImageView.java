package ru.glushen.ui;

import android.annotation.*;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.net.*;
import android.os.*;
import android.support.annotation.*;
import android.util.*;
import android.widget.*;

import java.util.*;

import ru.glushen.util.obtainer.*;

import static android.view.ViewGroup.*;

/**
 * Created by Pavel Glushen on 21.06.2015.
 */
public class SmartImageView extends ImageView
{
    public interface BitmapDrawableCreator
    {// todo проверить необходимость передавать LayoutParams
        LayoutParams onCreate(int initWidth, int initHeight, CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer, Resources resources);
    }

    public interface OnBitmapDrawableChangedListener
    {
        void onBitmapDrawableChanged(@Nullable BitmapDrawable bitmapDrawable);
    }

    public SmartImageView(Context context)
    {
        super(context);
    }

    public SmartImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SmartImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP) public SmartImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Deprecated @Override public void setImageResource(int resId)
    {
        super.setImageResource(resId);
    }

    @Deprecated @Override public void setImageURI(Uri uri)
    {
        super.setImageURI(uri);
    }

    @Deprecated @Override public void setImageDrawable(Drawable drawable)
    {
        super.setImageDrawable(drawable);
    }

    @Deprecated @Override public void setImageBitmap(Bitmap bm)
    {
        super.setImageBitmap(bm);
    }

    @Nullable private OnBitmapDrawableChangedListener changedListener = null;

    public void setChangedListener(@Nullable OnBitmapDrawableChangedListener changedListener)
    {
        this.changedListener = changedListener;
    }

    private BitmapDrawableCreator[] bitmapCreators = null;

    public void setBitmapDrawableCreators(@NonNull BitmapDrawableCreator... bitmapDrawableCreators)
    {
        clear();

        this.bitmapCreators = bitmapDrawableCreators;

        createAndChangeBitmap();
    }

    public void clear()
    {
        super.setImageDrawable(null);

        if (changedListener != null)
        {
            changedListener.onBitmapDrawableChanged(null);
        }

        for (CloseableObtainer<BitmapDrawable> obtainer : bitmapDrawableObtainers)
        {
            obtainer.close();
        }

        bitmapDrawableObtainers.clear();

        bitmapCreators = null;

        lastIndex = -1;
    }

    private void createAndChangeBitmap()
    {
        if (sizeInit && bitmapCreators != null)
        {
            useAnimation = false;

            LayoutParams newLayoutParams = null;

            for (int i = bitmapCreators.length - 1; i >= 0; i--)
            {
                final int currentIndex = i;

                CloseableObtainer<BitmapDrawable> bitmapDrawableObtainer = new CloseableObtainer<BitmapDrawable>()
                {
                    @Override protected void onObtain(@Nullable BitmapDrawable bitmapDrawable)
                    {
                        if (lastIndex <= currentIndex)
                        {
                            lastIndex = currentIndex;

                            SmartImageView.super.setImageDrawable(bitmapDrawable);

                            if (changedListener != null)
                            {
                                changedListener.onBitmapDrawableChanged(bitmapDrawable);
                            }
                        }
                    }
                };

                bitmapDrawableObtainers.add(bitmapDrawableObtainer);

                newLayoutParams = bitmapCreators[i].onCreate(initWidth, initHeight, bitmapDrawableObtainer, getResources());
            }

            if (newLayoutParams != null)
            {
                setLayoutParams(newLayoutParams);
            }
            else
            {
                LayoutParams layoutParams = getLayoutParams();

                if (layoutParams.width != initWidth || layoutParams.height != initHeight)
                {
                    layoutParams.width = initWidth;
                    layoutParams.height = initHeight;
                    setLayoutParams(layoutParams);
                }
            }

            useAnimation = true;
        }
    }

    private boolean sizeInit = false;
    private int initWidth;
    private int initHeight;

    @Override protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight)
    {
        super.onSizeChanged(width, height, oldWidth, oldHeight);

        if (!sizeInit)
        {
            sizeInit = true;

            initWidth = width;
            initHeight = height;

            createAndChangeBitmap();
        }
    }

    private List<CloseableObtainer<BitmapDrawable>> bitmapDrawableObtainers = new ArrayList<>();
    private int lastIndex;
    private boolean useAnimation;
}
