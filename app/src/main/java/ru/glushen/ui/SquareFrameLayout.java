package ru.glushen.ui;

import android.content.*;
import android.util.*;
import android.widget.*;

import ru.glushen.util.*;

import static android.view.View.MeasureSpec.*;
import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 11.08.2015.
 */
public class SquareFrameLayout extends FrameLayout
{
    public SquareFrameLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int widthMode = getMode(widthMeasureSpec);
        int width = getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();

        int heightMode = getMode(heightMeasureSpec);
        int height = getSize(heightMeasureSpec) - getPaddingTop() - getPaddingBottom();

        int size = min(width, height);

        if (widthMode == UNSPECIFIED && heightMode == UNSPECIFIED)
        {
            width = height = Screen.dpPixelSize(250, getResources().getDisplayMetrics());
        }
        else if (widthMode == UNSPECIFIED)
        {
            //noinspection SuspiciousNameCombination
            width = height;
        }
        else if (heightMode == UNSPECIFIED)
        {
            //noinspection SuspiciousNameCombination
            height = width;
        }
        else if (widthMode == AT_MOST && heightMode == AT_MOST)
        {
            width = height = size;
        }
        else if (widthMode == AT_MOST)
        {
            width = size;
        }
        else if (heightMode == AT_MOST)
        {
            height = size;
        }

        widthMeasureSpec = makeMeasureSpec(width, EXACTLY);
        heightMeasureSpec = makeMeasureSpec(height, EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
