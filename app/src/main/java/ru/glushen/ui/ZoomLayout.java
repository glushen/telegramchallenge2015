package ru.glushen.ui;

import android.content.*;
import android.graphics.*;
import android.support.annotation.*;
import android.util.*;
import android.view.*;
import android.view.animation.*;
import android.widget.*;

import ru.glushen.util.*;

import static android.view.GestureDetector.*;
import static android.view.MotionEvent.*;
import static java.lang.Math.*;

/**
 * Created by Pavel Glushen on 09.07.2015.
 */
public class ZoomLayout extends FrameLayout implements View.OnTouchListener
{
    public ZoomLayout(Context context)
    {
        super(context);
    }

    public ZoomLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public ZoomLayout(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    {
        super.setOnTouchListener(this);
    }

    @Nullable private OnTouchListener customOnTouchListener = null;

    @Override public void setOnTouchListener(@Nullable OnTouchListener onTouchListener)
    {
        customOnTouchListener = onTouchListener;
    }

    private float scaleFactor = 1;
    private float translateX = 0, translateY = 0;

    private float touchX, touchY;
    private float focusSpan, focusX, focusY;

    private final GestureDetector gestureDetector = new GestureDetector(getContext(), new SimpleOnGestureListener()
    {
        @Override public boolean onDoubleTap(MotionEvent event)
        {
            focusX = event.getX();
            focusY = event.getY();

            startAnimation(scaleFactor - MIN_SCALE_FACTOR < MAX_SCALE_FACTOR - scaleFactor ? MAX_SCALE_FACTOR : MIN_SCALE_FACTOR, true);

            return true;
        }
    });

    @Override public boolean onTouch(View view, MotionEvent event)
    {
        if (customOnTouchListener != null && customOnTouchListener.onTouch(view, event) || gestureDetector.onTouchEvent(event))
        {
            return true;
        }

        switch (event.getActionMasked())
        {
            case ACTION_DOWN:
            {
                touchX = event.getX(0);
                touchY = event.getY(0);
            }
            break;
            case ACTION_POINTER_DOWN:
            {
                float x0 = event.getX(0), x1 = event.getX(1), deltaX = x0 - x1;
                float y0 = event.getY(0), y1 = event.getY(1), deltaY = y0 - y1;

                focusSpan = (float) sqrt(deltaX * deltaX + deltaY * deltaY);
                focusX = (x0 + x1) / 2;
                focusY = (y0 + y1) / 2;
            }
            break;
            case ACTION_POINTER_UP:
            {
                int actionIndex = event.getActionIndex();

                if (event.getPointerCount() > 2)
                {
                    int index0 = actionIndex != 0 ? 0 : 2, index1 = actionIndex != 1 ? 1 : 2;

                    float x0 = event.getX(index0), x1 = event.getX(index1), deltaX = x0 - x1;
                    float y0 = event.getY(index0), y1 = event.getY(index1), deltaY = y0 - y1;

                    focusSpan = (float) sqrt(deltaX * deltaX + deltaY * deltaY);
                    focusX = (x0 + x1) / 2;
                    focusY = (y0 + y1) / 2;
                }
                else
                {
                    int index = actionIndex != 0 ? 0 : 1;

                    touchX = event.getX(index);
                    touchY = event.getY(index);
                }
            }
            break;
            case ACTION_MOVE:
            {
                if (event.getPointerCount() > 1)
                {
                    float x0 = event.getX(0), x1 = event.getX(1), deltaX = x0 - x1;
                    float y0 = event.getY(0), y1 = event.getY(1), deltaY = y0 - y1;

                    float newFocusSpan = (float) sqrt(deltaX * deltaX + deltaY * deltaY);

                    float newScaleFactor = scaleFactor * newFocusSpan / focusSpan;
                    focusSpan = newFocusSpan;
                    focusX = (x0 + x1) / 2;
                    focusY = (y0 + y1) / 2;

                    float deltaScaleFactor = newScaleFactor - scaleFactor;
                    scaleFactor = newScaleFactor;

                    translateX -= deltaScaleFactor * focusX;
                    translateY -= deltaScaleFactor * focusY;
                }
                else
                {
                    float newTouchX = event.getX();
                    float newTouchY = event.getY();

                    translateX += newTouchX - touchX;
                    translateY += newTouchY - touchY;

                    touchX = newTouchX;
                    touchY = newTouchY;
                }

                invalidate();
            }
            break;
            case ACTION_UP:
            case ACTION_CANCEL:
            {
                startAnimation(false);
            }
            break;
        }

        return true;
    }

    private static final float MIN_SCALE_FACTOR = 1;
    private static final float MAX_SCALE_FACTOR = 3;

    private boolean animating = false;
    private long animationStartTimeInMillis;

    private float animationStartScaleFactor, animationStopScaleFactor;
    private float animationStartTranslateX, animationStopTranslateX;
    private float animationStartTranslateY, animationStopTranslateY;

    private void startAnimation(boolean force)
    {
        startAnimation(scaleFactor, force);
    }

    private void startAnimation(float targetScaleFactor, boolean force)
    {
        if (!force && animating)
        {
            return;
        }

        animationStartScaleFactor = scaleFactor;
        animationStopScaleFactor = max(min(targetScaleFactor, MAX_SCALE_FACTOR), MIN_SCALE_FACTOR);

        float deltaScaleFactor = animationStopScaleFactor - animationStartScaleFactor;

        animationStartTranslateX = translateX;
        animationStopTranslateX = min(max(translateX - deltaScaleFactor * focusX, -getWidth() * (animationStopScaleFactor - 1)), 0);

        animationStartTranslateY = translateY;
        animationStopTranslateY = min(max(translateY - deltaScaleFactor * focusY, -getHeight() * (animationStopScaleFactor - 1)), 0);

        boolean incorrectScaleFactor = animationStartScaleFactor != animationStopScaleFactor;
        boolean incorrectTranslateX = animationStartTranslateX != animationStopTranslateX;
        boolean incorrectTranslateY = animationStartTranslateY != animationStopTranslateY;

        if (incorrectScaleFactor || incorrectTranslateX || incorrectTranslateY)
        {
            animationStartTimeInMillis = System.currentTimeMillis();

            animating = true;

            invalidate();
        }
    }

    private static final int ANIMATION_DURATION_IN_MILLIS = 200;
    private static final DecelerateInterpolator INTERPOLATOR = new DecelerateInterpolator();

    @Override protected void dispatchDraw(@NonNull Canvas canvas)
    {
        if (animating)
        {
            long deltaTime = System.currentTimeMillis() - animationStartTimeInMillis;

            if (deltaTime < ANIMATION_DURATION_IN_MILLIS)
            {
                float interpolation = INTERPOLATOR.getInterpolation(deltaTime / (float) ANIMATION_DURATION_IN_MILLIS);

                scaleFactor = animationStartScaleFactor + (animationStopScaleFactor - animationStartScaleFactor) * interpolation;
                translateX = animationStartTranslateX + (animationStopTranslateX - animationStartTranslateX) * interpolation;
                translateY = animationStartTranslateY + (animationStopTranslateY - animationStartTranslateY) * interpolation;

                Compat.invalidateOnAnimation(this);
            }
            else
            {
                scaleFactor = animationStopScaleFactor;
                translateX = animationStopTranslateX;
                translateY = animationStopTranslateY;

                animating = false;
            }
        }

        canvas.save();

        canvas.translate(translateX, translateY);
        canvas.scale(scaleFactor, scaleFactor);

        super.dispatchDraw(canvas);

        canvas.restore();
    }
}
