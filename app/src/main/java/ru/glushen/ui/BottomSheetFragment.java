package ru.glushen.ui;

import android.app.*;
import android.os.*;
import android.support.annotation.*;
import android.support.v4.app.DialogFragment;
import android.view.*;
import android.widget.*;

import java.lang.reflect.*;

import ru.glushen.telegramchallenge.*;

public abstract class BottomSheetFragment extends DialogFragment
{
    public BottomSheetFragment()
    {
        int classModifiers = getClass().getModifiers();

        if (!Modifier.isPublic(classModifiers))
        {
            throw new AssertionError(getClass().getCanonicalName() + " must be public.");
        }
        else if (getClass().isMemberClass() && !Modifier.isStatic(classModifiers))
        {
            throw new AssertionError(getClass().getCanonicalName() + " must be static.");
        }
        else
        {
            boolean hasPublicDefaultConstructor = false;

            try
            {
                int constructorModifiers = getClass().getConstructor().getModifiers();
                hasPublicDefaultConstructor = Modifier.isPublic(constructorModifiers);
            }
            catch (NoSuchMethodException ignored)
            {

            }

            if (!hasPublicDefaultConstructor)
            {
                throw new AssertionError(getClass().getCanonicalName() + " must have public default constructor.");
            }
        }
    }

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Dialog dialog = new Dialog(getActivity(), R.style.BottomSheetFragment);

        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        return dialog;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.bottom_sheet_container, container, false);

        view.findViewById(R.id.bottom_sheet_background).setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                dismiss();
            }
        });

        FrameLayout bottomSheetContainer = (FrameLayout) view.findViewById(R.id.bottom_sheet_container);

        View bottomSheetView = onCreateBottomSheetView(inflater, bottomSheetContainer, savedInstanceState);

        if (bottomSheetView == null)
        {
            throw new AssertionError(getClass().getCanonicalName() + "#onCreateBottomSheetView must not return null.");
        }

        bottomSheetContainer.addView(bottomSheetView);

        return view;
    }

    public abstract View onCreateBottomSheetView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);
}