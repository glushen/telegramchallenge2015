#pragma once

#include <jni.h>
#include <android/bitmap.h>

class BitmapWrapper
{
private:
    JNIEnv* env;
    bool wrapped;
    jobject bitmap;

public:
    AndroidBitmapInfo bitmapInfo;
    uint8_t* bitmapData;

    BitmapWrapper(JNIEnv* env)
    {
        this->env = env;
        wrapped = false;
    }

    bool wrap(jobject& bitmap)
    {
        if (!release())
        {
            return false;
        }

        this->bitmap = bitmap;

        if (AndroidBitmap_getInfo(env, bitmap, &bitmapInfo) != ANDROID_BITMAP_RESULT_SUCCESS)
        {
            return false;
        }

        void* targetDataElements;

        if (AndroidBitmap_lockPixels(env, bitmap, &targetDataElements) != ANDROID_BITMAP_RESULT_SUCCESS)
        {
            return false;
        }

        bitmapData = reinterpret_cast<uint8_t*>(targetDataElements);

        wrapped = true;

        return true;
    }

    bool release()
    {
        if (wrapped)
        {
            wrapped = false;

            return AndroidBitmap_unlockPixels(env, bitmap) == ANDROID_BITMAP_RESULT_SUCCESS;
        }
        else
        {
            return true;
        }
    }

    ~BitmapWrapper()
    {
        release();
    }
};

jobject createBitmap(JNIEnv* env, jint width, jint height)
{
    jclass bitmapClass = env->FindClass("android/graphics/Bitmap");
    jmethodID createBitmapMethodId = env->GetStaticMethodID(bitmapClass, "createBitmap", "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");

    jclass bitmapConfigClass = env->FindClass("android/graphics/Bitmap$Config");
    jstring bitmapConfigName = env->NewStringUTF("ARGB_8888");
    jmethodID valueOfMethodId = env->GetStaticMethodID(bitmapConfigClass, "valueOf", "(Ljava/lang/String;)Landroid/graphics/Bitmap$Config;");
    jobject bitmapConfig = env->CallStaticObjectMethod(bitmapConfigClass, valueOfMethodId, bitmapConfigName);

    return env->CallStaticObjectMethod(bitmapClass, createBitmapMethodId, width, height, bitmapConfig);
}