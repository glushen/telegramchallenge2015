#include <jni.h>
#include <android/bitmap.h>
#include "bitmap.cpp"
#include "webp/webp/decode.h"
#include "../raii.cpp"

extern "C" jobject Java_ru_glushen_image_WebP_createBitmap(JNIEnv* env, jclass clazz, jbyteArray data, jboolean useCropping, jint cropLeft, jint cropTop, jint cropWidth, jint cropHeight, jboolean useScaling, jint scaleWidth, jint scaleHeight)
{
    JniByteArrayWrapper dataWrapper(env, data, false);

    if (dataWrapper.data == NULL)
    {
        env->ThrowNew(env->FindClass("java/lang/NullPointerException"), "data must not be null");
        return NULL;
    }

    const uint8_t* dataBytes = reinterpret_cast<const uint8_t*>(dataWrapper.data);
    size_t dataSize = dataWrapper.length;

    int32_t bitmapWidth = 0, bitmapHeight = 0;

    if (useScaling)
    {
        bitmapWidth = scaleWidth;
        bitmapHeight = scaleHeight;
    }
    else if (useCropping)
    {
        bitmapWidth = cropWidth;
        bitmapHeight = cropHeight;
    }
    else
    {
        WebPBitstreamFeatures features;

        if (WebPGetFeatures(dataBytes, dataSize, &features) != VP8_STATUS_OK)
        {
            return NULL;
        }

        bitmapWidth = features.width;
        bitmapHeight = features.height;
    }

    jobject bitmap = createBitmap(env, bitmapWidth, bitmapHeight);

    BitmapWrapper bitmapWrapper(env);

    if (!bitmapWrapper.wrap(bitmap))
    {
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"), "Cannot get bitmap info or lock bitmap pixels");
        return NULL;
    }

    WebPDecoderConfig config;
    WebPInitDecoderConfig(&config);

    config.output.colorspace = MODE_rgbA;
    config.output.is_external_memory = true;
    config.output.u.RGBA.rgba = bitmapWrapper.bitmapData;
    config.output.u.RGBA.stride = bitmapWrapper.bitmapInfo.stride;
    config.output.u.RGBA.size = bitmapWrapper.bitmapInfo.stride * bitmapWrapper.bitmapInfo.height;

    config.options.use_cropping = useCropping;
    config.options.crop_left = cropLeft;
    config.options.crop_top = cropTop;
    config.options.crop_width = cropWidth;
    config.options.crop_height = cropHeight;

    config.options.use_scaling = useScaling;
    config.options.scaled_width = scaleWidth;
    config.options.scaled_height = scaleHeight;

    if (WebPDecode(dataBytes, dataSize, &config) != VP8_STATUS_OK)
    {
        return NULL;
    }

    if (!bitmapWrapper.release())
    {
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"), "Cannot unlock bitmap pixels");
        return NULL;
    }

    return bitmap;
}