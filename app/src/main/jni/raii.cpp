#pragma once

#include <jni.h>

class JniStringWrapper
{
private:
    JNIEnv* env;
    jstring string;

public:
    const char* data;

    JniStringWrapper(JNIEnv* env, jstring& string)
    {
        this->env = env;
        this->string = string;

        data = env->GetStringUTFChars(string, NULL);
    }
    ~JniStringWrapper()
    {
        env->ReleaseStringUTFChars(string, data);
    }
};

class JniByteArrayWrapper
{
private:
    JNIEnv* env;
    jbyteArray array;

public:
    jbyte* data;
    size_t length;
    bool copyBack;

    JniByteArrayWrapper(JNIEnv* env, jbyteArray& array, bool copyBack)
    {
        this->env = env;
        this->array = array;

        data = env->GetByteArrayElements(array, NULL);
        length = env->GetArrayLength(array);
        this->copyBack = copyBack;
    }
    ~JniByteArrayWrapper()
    {
        env->ReleaseByteArrayElements(array, data, copyBack ? 0 : JNI_ABORT);
    }
};

class JniShortArrayWrapper
{
private:
    JNIEnv* env;
    jshortArray array;

public:
    jshort* data;
    size_t length;
    bool copyBack;

    JniShortArrayWrapper(JNIEnv* env, jshortArray& array, bool copyBack)
    {
        this->env = env;
        this->array = array;

        data = env->GetShortArrayElements(array, NULL);
        length = env->GetArrayLength(array);
        this->copyBack = copyBack;
    }
    ~JniShortArrayWrapper()
    {
        env->ReleaseShortArrayElements(array, data, copyBack ? 0 : JNI_ABORT);
    }
};