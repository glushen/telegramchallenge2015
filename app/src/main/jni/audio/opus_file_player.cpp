#pragma once

#include <jni.h>
#include "opusfile/include/opusfile.h"
#include "../raii.cpp"

extern "C" jlong Java_ru_glushen_audio_OpusFilePlayer_nativeGetDurationInFrames(JNIEnv* env, jclass clazz, jstring path)
{
    JniStringWrapper pathWrapper(env, path);

    OggOpusFile* file = op_open_file(pathWrapper.data, NULL);

    jlong durationInFrames = 0;

    if (file == NULL)
    {
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"), "Cannot open file");
        return 0;
    }

    durationInFrames = op_pcm_total(file, -1);

    if (durationInFrames < 0)
    {
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"), "Cannot get duration");
        // intentionally do not return
    }

    op_free(file);

    return durationInFrames;
}

static OggOpusFile* opusFile = NULL;

extern "C" void Java_ru_glushen_audio_OpusFilePlayer_nativeRelease(JNIEnv* env, jclass clazz)
{
    if (opusFile != NULL)
    {
        op_free(opusFile);
        opusFile = NULL;
    }
}

extern "C" jlong Java_ru_glushen_audio_OpusFilePlayer_nativeInit(JNIEnv* env, jclass clazz, jstring path)
{
    Java_ru_glushen_audio_OpusFilePlayer_nativeRelease(env, clazz);

    JniStringWrapper pathWrapper(env, path);

    opusFile = op_open_file(pathWrapper.data, NULL);

    if (opusFile == NULL)
    {
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"), "Cannot open file");
        return 0;
    }

    jlong durationInFrames = op_pcm_total(opusFile, -1);

    if (durationInFrames < 0)
    {
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"), "Cannot get duration");
        return 0;
    }

    return durationInFrames;
}

extern "C" jint Java_ru_glushen_audio_OpusFilePlayer_nativeReadStereo(JNIEnv* env, jclass clazz, jshortArray buffer)
{
    if (opusFile == NULL)
    {
        env->ThrowNew(env->FindClass("java/lang/IllegalStateException"), "opusFile is not initialized");
        return 0;
    }

    JniShortArrayWrapper bufferWrapper(env, buffer, true);

    jint readFramesCount = op_read_stereo(opusFile, bufferWrapper.data, bufferWrapper.length);

    if (readFramesCount < 0)
    {
        env->ThrowNew(env->FindClass("java/lang/RuntimeException"), "Cannot read frames");
        return 0;
    }

    return readFramesCount;
}

extern "C" void Java_ru_glushen_audio_OpusFilePlayer_nativeSeek(JNIEnv* env, jclass clazz, jlong offsetInFrames)
{
    if (opusFile != NULL)
    {
        op_pcm_seek(opusFile, offsetInFrames);
    }
}