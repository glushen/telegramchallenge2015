LOCAL_PATH := $(call my-dir)

### tdjni ################################################################################

include $(CLEAR_VARS)

LOCAL_MODULE := tdjni

LOCAL_SRC_FILES := tdjni/$(TARGET_ARCH_ABI)/libtdjni.so

include $(PREBUILT_SHARED_LIBRARY)

### image ################################################################################

include $(CLEAR_VARS)

LOCAL_MODULE := telegramchallengeimage

ifneq ($(findstring armeabi-v7a, $(TARGET_ARCH_ABI)),)
    IMAGE_WEBP_NEON := c.neon
else
    IMAGE_WEBP_NEON := c
endif

LOCAL_SRC_FILES := \
    image/webp.cpp \
    image/webp/dec/alpha.c \
    image/webp/dec/buffer.c \
    image/webp/dec/frame.c \
    image/webp/dec/idec.c \
    image/webp/dec/io.c \
    image/webp/dec/quant.c \
    image/webp/dec/tree.c \
    image/webp/dec/vp8.c \
    image/webp/dec/vp8l.c \
    image/webp/dec/webp.c \
    image/webp/dsp/alpha_processing.c \
    image/webp/dsp/alpha_processing_mips_dsp_r2.c \
    image/webp/dsp/alpha_processing_sse2.c \
    image/webp/dsp/alpha_processing_sse41.c \
    image/webp/dsp/argb.c \
    image/webp/dsp/argb_mips_dsp_r2.c \
    image/webp/dsp/argb_sse2.c \
    image/webp/dsp/cpu.c \
    image/webp/dsp/dec.c \
    image/webp/dsp/dec_clip_tables.c \
    image/webp/dsp/dec_mips32.c \
    image/webp/dsp/dec_mips_dsp_r2.c \
    image/webp/dsp/dec_neon.$(IMAGE_WEBP_NEON) \
    image/webp/dsp/dec_sse2.c \
    image/webp/dsp/dec_sse41.c \
    image/webp/dsp/filters.c \
    image/webp/dsp/filters_mips_dsp_r2.c \
    image/webp/dsp/filters_sse2.c \
    image/webp/dsp/lossless.c \
    image/webp/dsp/lossless_mips_dsp_r2.c \
    image/webp/dsp/lossless_neon.$(IMAGE_WEBP_NEON) \
    image/webp/dsp/lossless_sse2.c \
    image/webp/dsp/rescaler.c \
    image/webp/dsp/rescaler_mips32.c \
    image/webp/dsp/rescaler_mips_dsp_r2.c \
    image/webp/dsp/upsampling.c \
    image/webp/dsp/upsampling_mips_dsp_r2.c \
    image/webp/dsp/upsampling_neon.$(IMAGE_WEBP_NEON) \
    image/webp/dsp/upsampling_sse2.c \
    image/webp/dsp/yuv.c \
    image/webp/dsp/yuv_mips32.c \
    image/webp/dsp/yuv_mips_dsp_r2.c \
    image/webp/dsp/yuv_sse2.c \
    image/webp/utils/bit_reader.c \
    image/webp/utils/color_cache.c \
    image/webp/utils/filters.c \
    image/webp/utils/huffman.c \
    image/webp/utils/quant_levels_dec.c \
    image/webp/utils/random.c \
    image/webp/utils/rescaler.c \
    image/webp/utils/thread.c \
    image/webp/utils/utils.c \

LOCAL_CFLAGS := -Wall -DANDROID -DHAVE_MALLOC_H -DHAVE_PTHREAD -DWEBP_USE_THREAD
LOCAL_CFLAGS += -ffast-math -Os

LOCAL_STATIC_LIBRARIES := cpufeatures

LOCAL_LDLIBS := -ljnigraphics
#LOCAL_LDLIBS += -llog

LOCAL_ARM_MODE := thumb

include $(BUILD_SHARED_LIBRARY)

### audio ################################################################################

include $(CLEAR_VARS)

LOCAL_MODULE := telegramchallengeaudio

LOCAL_SRC_FILES := \
    audio/opus_file_player.cpp \
    audio/opus_file_recorder.cpp \
    audio/opus/src/opus.c \
    audio/opus/src/opus_decoder.c \
    audio/opus/src/opus_encoder.c \
    audio/opus/src/opus_multistream.c \
    audio/opus/src/opus_multistream_encoder.c \
    audio/opus/src/opus_multistream_decoder.c \
    audio/opus/src/repacketizer.c \
    audio/opus/celt/bands.c \
    audio/opus/celt/celt.c \
    audio/opus/celt/celt_encoder.c \
    audio/opus/celt/celt_decoder.c \
    audio/opus/celt/cwrs.c \
    audio/opus/celt/entcode.c \
    audio/opus/celt/entdec.c \
    audio/opus/celt/entenc.c \
    audio/opus/celt/kiss_fft.c \
    audio/opus/celt/laplace.c \
    audio/opus/celt/mathops.c \
    audio/opus/celt/mdct.c \
    audio/opus/celt/modes.c \
    audio/opus/celt/pitch.c \
    audio/opus/celt/celt_lpc.c \
    audio/opus/celt/quant_bands.c \
    audio/opus/celt/rate.c \
    audio/opus/celt/vq.c \
    audio/opus/celt/arm/armcpu.c \
    audio/opus/celt/arm/arm_celt_map.c \
    audio/opus/silk/CNG.c \
    audio/opus/silk/code_signs.c \
    audio/opus/silk/init_decoder.c \
    audio/opus/silk/decode_core.c \
    audio/opus/silk/decode_frame.c \
    audio/opus/silk/decode_parameters.c \
    audio/opus/silk/decode_indices.c \
    audio/opus/silk/decode_pulses.c \
    audio/opus/silk/decoder_set_fs.c \
    audio/opus/silk/dec_API.c \
    audio/opus/silk/enc_API.c \
    audio/opus/silk/encode_indices.c \
    audio/opus/silk/encode_pulses.c \
    audio/opus/silk/gain_quant.c \
    audio/opus/silk/interpolate.c \
    audio/opus/silk/LP_variable_cutoff.c \
    audio/opus/silk/NLSF_decode.c \
    audio/opus/silk/NSQ.c \
    audio/opus/silk/NSQ_del_dec.c \
    audio/opus/silk/PLC.c \
    audio/opus/silk/shell_coder.c \
    audio/opus/silk/tables_gain.c \
    audio/opus/silk/tables_LTP.c \
    audio/opus/silk/tables_NLSF_CB_NB_MB.c \
    audio/opus/silk/tables_NLSF_CB_WB.c \
    audio/opus/silk/tables_other.c \
    audio/opus/silk/tables_pitch_lag.c \
    audio/opus/silk/tables_pulses_per_block.c \
    audio/opus/silk/VAD.c \
    audio/opus/silk/control_audio_bandwidth.c \
    audio/opus/silk/quant_LTP_gains.c \
    audio/opus/silk/VQ_WMat_EC.c \
    audio/opus/silk/HP_variable_cutoff.c \
    audio/opus/silk/NLSF_encode.c \
    audio/opus/silk/NLSF_VQ.c \
    audio/opus/silk/NLSF_unpack.c \
    audio/opus/silk/NLSF_del_dec_quant.c \
    audio/opus/silk/process_NLSFs.c \
    audio/opus/silk/stereo_LR_to_MS.c \
    audio/opus/silk/stereo_MS_to_LR.c \
    audio/opus/silk/check_control_input.c \
    audio/opus/silk/control_SNR.c \
    audio/opus/silk/init_encoder.c \
    audio/opus/silk/control_codec.c \
    audio/opus/silk/A2NLSF.c \
    audio/opus/silk/ana_filt_bank_1.c \
    audio/opus/silk/biquad_alt.c \
    audio/opus/silk/bwexpander_32.c \
    audio/opus/silk/bwexpander.c \
    audio/opus/silk/debug.c \
    audio/opus/silk/decode_pitch.c \
    audio/opus/silk/inner_prod_aligned.c \
    audio/opus/silk/lin2log.c \
    audio/opus/silk/log2lin.c \
    audio/opus/silk/LPC_analysis_filter.c \
    audio/opus/silk/LPC_inv_pred_gain.c \
    audio/opus/silk/table_LSF_cos.c \
    audio/opus/silk/NLSF2A.c \
    audio/opus/silk/NLSF_stabilize.c \
    audio/opus/silk/NLSF_VQ_weights_laroia.c \
    audio/opus/silk/pitch_est_tables.c \
    audio/opus/silk/resampler.c \
    audio/opus/silk/resampler_down2_3.c \
    audio/opus/silk/resampler_down2.c \
    audio/opus/silk/resampler_private_AR2.c \
    audio/opus/silk/resampler_private_down_FIR.c \
    audio/opus/silk/resampler_private_IIR_FIR.c \
    audio/opus/silk/resampler_private_up2_HQ.c \
    audio/opus/silk/resampler_rom.c \
    audio/opus/silk/sigm_Q15.c \
    audio/opus/silk/sort.c \
    audio/opus/silk/sum_sqr_shift.c \
    audio/opus/silk/stereo_decode_pred.c \
    audio/opus/silk/stereo_encode_pred.c \
    audio/opus/silk/stereo_find_predictor.c \
    audio/opus/silk/stereo_quant_pred.c \
    audio/opus/silk/fixed/LTP_analysis_filter_FIX.c \
    audio/opus/silk/fixed/LTP_scale_ctrl_FIX.c \
    audio/opus/silk/fixed/corrMatrix_FIX.c \
    audio/opus/silk/fixed/encode_frame_FIX.c \
    audio/opus/silk/fixed/find_LPC_FIX.c \
    audio/opus/silk/fixed/find_LTP_FIX.c \
    audio/opus/silk/fixed/find_pitch_lags_FIX.c \
    audio/opus/silk/fixed/find_pred_coefs_FIX.c \
    audio/opus/silk/fixed/noise_shape_analysis_FIX.c \
    audio/opus/silk/fixed/prefilter_FIX.c \
    audio/opus/silk/fixed/process_gains_FIX.c \
    audio/opus/silk/fixed/regularize_correlations_FIX.c \
    audio/opus/silk/fixed/residual_energy16_FIX.c \
    audio/opus/silk/fixed/residual_energy_FIX.c \
    audio/opus/silk/fixed/solve_LS_FIX.c \
    audio/opus/silk/fixed/warped_autocorrelation_FIX.c \
    audio/opus/silk/fixed/apply_sine_window_FIX.c \
    audio/opus/silk/fixed/autocorr_FIX.c \
    audio/opus/silk/fixed/burg_modified_FIX.c \
    audio/opus/silk/fixed/k2a_FIX.c \
    audio/opus/silk/fixed/k2a_Q16_FIX.c \
    audio/opus/silk/fixed/pitch_analysis_core_FIX.c \
    audio/opus/silk/fixed/vector_ops_FIX.c \
    audio/opus/silk/fixed/schur64_FIX.c \
    audio/opus/silk/fixed/schur_FIX.c \
    audio/ogg/src/bitwise.c \
    audio/ogg/src/framing.c \
    audio/opusfile/src/http.c \
    audio/opusfile/src/info.c \
    audio/opusfile/src/internal.c \
    audio/opusfile/src/opusfile.c \
    audio/opusfile/src/stream.c \
    audio/opusfile/src/wincerts.c \

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/audio/opus/include \
    $(LOCAL_PATH)/audio/opus/celt \
    $(LOCAL_PATH)/audio/opus/silk \
    $(LOCAL_PATH)/audio/opus/silk/fixed \
    $(LOCAL_PATH)/audio/ogg/include/ \
    $(LOCAL_PATH)/audio/opusfile/include/ \

LOCAL_CFLAGS := -DNULL=0 -DSOCKLEN_T=socklen_t -DLOCALE_NOT_USED -D_LARGEFILE_SOURCE=1
LOCAL_CFLAGS += -D_FILE_OFFSET_BITS=64 -Drestrict='' -D__EMX__ -DOPUS_BUILD -DFIXED_POINT=1
LOCAL_CFLAGS += -DUSE_ALLOCA -DHAVE_LRINT -DHAVE_LRINTF -DAVOID_TABLES -DDISABLE_FLOAT_API
LOCAL_CFLAGS += -DOP_FIXED_POINT -DOP_DISABLE_FLOAT_API -w -std=gnu99 -ffast-math -Os

LOCAL_CPPFLAGS := -DBSD=1

LOCAL_LDLIBS += -llog

LOCAL_ARM_MODE := thumb

include $(BUILD_SHARED_LIBRARY)

### import modules #######################################################################

$(call import-module, android/cpufeatures)